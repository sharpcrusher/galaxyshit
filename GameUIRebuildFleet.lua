local GameUIRebuildFleet = LuaObjectManager:GetLuaObject("GameUIRebuildFleet")
local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
GameUIRebuildFleet.InfobarState = {UPGRADING = 1, WAITING = 0}
GameUIRebuildFleet.UnlockLevel = 30
GameUIRebuildFleet.UnlockAccelerateAll = 5
GameUIRebuildFleet.helpData = {}
GameUIRebuildFleet.Entrance = {
  FactoryInner = "FactoryInner",
  MainPlanet = "MainPlanet",
  Alliance = "Alliance"
}
GameUIRebuildFleet.mShareStoryChecked = {}
GameUIRebuildFleet.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP] = true
function GameUIRebuildFleet:OnAddToGameState()
  DebugOut("GameUIRebuildFleet OnAddToGameState")
  DebugOut(self.OnModelInfoChange)
  GameGlobalData:RegisterDataChangeCallback("remodel_info", self.OnModelInfoChange)
  GameGlobalData:RegisterDataChangeCallback("remodel_help", self.OnModelHelpInfoChange)
  GameGlobalData:RegisterDataChangeCallback("alliance", self.OnAllianceResourceChange)
  if GameUIRebuildFleet.myAccelerateBarVisible == nil then
    GameUIRebuildFleet.myAccelerateBarVisible = true
  end
  if GameUIRebuildFleet.myAccelerateBarTime == nil then
    GameUIRebuildFleet.myAccelerateBarTime = os.time()
  end
end
function GameUIRebuildFleet:TryAccelerateAll()
  if self:GetIsCanAccelerateAll() then
    local content = {}
    local helpData = self.helpData
    if helpData then
      content.help_list = {}
      for k, v in pairs(helpData) do
        local helpItem = {}
        helpItem.user_id = v.user_id
        helpItem.type = v.type
        table.insert(content.help_list, helpItem)
      end
      DebugOut("TryAccelerateAll:")
      DebugTable(content)
      NetMessageMgr:SendMsg(NetAPIList.remodel_help_others_req.Code, content, nil, false, nil)
    end
  else
    local desc = GameLoader:GetGameText("LC_MENU_UNLOCK_LEVEL_OR_VIP_INFO")
    desc = string.gsub(desc, "<playerLevel_num>", GameDataAccessHelper:GetLevelLimit("remodel_speed_up"))
    desc = string.gsub(desc, "<vipLevel_num>", GameDataAccessHelper:GetVIPLimit("remodel_speed_up"))
    GameTip:Show(desc)
  end
end
function GameUIRebuildFleet:GetIsCanAccelerateAll(...)
  local level_info = GameGlobalData:GetData("levelinfo")
  local curVipLevel = GameVipDetailInfoPanel:GetVipLevel()
  local limitVip = GameDataAccessHelper:GetVIPLimit("remodel_speed_up")
  local limitLevel = GameDataAccessHelper:GetLevelLimit("remodel_speed_up")
  if curVipLevel >= limitVip or limitLevel <= level_info.level then
    return true
  else
    return false
  end
end
function GameUIRebuildFleet:AccelerateOther(itemIndex)
  local content = {}
  self.curItemIndex = itemIndex
  local helpData = self.helpData
  if helpData and helpData[itemIndex] then
    content.help_list = {}
    local helpItem = {}
    helpItem.user_id = helpData[itemIndex].user_id
    helpItem.type = helpData[itemIndex].type
    table.insert(content.help_list, helpItem)
    DebugOut("AccelerateOther:")
    DebugTable(content)
    NetMessageMgr:SendMsg(NetAPIList.remodel_help_others_req.Code, content, nil, false, nil)
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAccelerateItemBtn", itemIndex, false)
  end
end
function GameUIRebuildFleet.HelpOthersAck(msgType, content)
  if msgType == NetAPIList.remodel_help_others_ack.Code then
    if content.code ~= 0 then
    end
    DebugOut("GameUIRebuildFleet.HelpOthersAck = ")
    DebugTable(content)
    local otherInfo = content.other_info
    local helpData = GameGlobalData:GetData("remodel_help")
    local flag = false
    for index, data in ipairs(helpData) do
      for _, v in ipairs(otherInfo) do
        if helpItem.user_id == v.user_id then
          helpItem.type = v.type
          helpItem.cur_num = v.cur_num
          helpItem.max_num = v.max_num
          helpItem.is_help = v.is_help
          GameUIRebuildFleet:SetAccelerateItemData(index)
          flag = true
        end
      end
    end
    if #otherInfo == 0 then
    end
    if not flag then
      table.remove(helpData, GameUIRebuildFleet.curItemIndex)
      GameUIRebuildFleet.UpdateAccelerateList()
    end
    return true
  end
  return false
end
function GameUIRebuildFleet:CheckVisible(entrance)
  if self:IsRebuildVisible(true) then
    DebugOut("request rebuildData")
    DebugOut(self.RequestModelInfoCallback)
    NetMessageMgr:SendMsg(NetAPIList.remodel_info_req.Code, nil, self.RequestModelInfoCallback, true, nil)
    self:PlayEnterAnimation(entrance)
    if GameUIFactory:GetFlashObject() then
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetRebuildBtnState", true)
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetRepairBtnState", false)
      local alliance = GameGlobalData:GetData("alliance")
      if alliance.id == "" or alliance.id == 0 then
        GameUIFactory:GetFlashObject():InvokeASCallback("_root", "setUnAllianceTip", false)
      else
        GameUIFactory:GetFlashObject():InvokeASCallback("_root", "setUnAllianceTip", true)
      end
    end
  end
end
function GameUIRebuildFleet:IsRebuildVisible(isShowErrorMessage)
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local buildingInfo = GameGlobalData:GetBuildingInfo("factory")
  GameUIRebuildFleet.UnlockLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("remodel")
  if levelInfo and tonumber(levelInfo.level) >= GameUIRebuildFleet.UnlockLevel then
    if buildingInfo and buildingInfo.level > 0 then
      DebugOut("IsRebuildVisible:" .. tostring(buildingInfo.level))
      return true
    end
    if isShowErrorMessage then
      local notification = GameLoader:GetGameText("LC_MENU_REBUILD_NO_FACTORY")
      GameTip:Show(notification)
    end
    return false
  end
  if isShowErrorMessage then
    local notification = GameLoader:GetGameText("LC_MENU_REBUILD_LEVEL_ALERT")
    notification = string.format(notification, tostring(GameUIRebuildFleet.UnlockLevel))
    GameTip:Show(notification)
  end
  return false
end
function GameUIRebuildFleet:IsAskForHelpFuncVisible()
  local data_alliance = GameGlobalData:GetData("alliance")
  DebugOut("IsAskForHelpFuncVisible")
  DebugTable(data_alliance)
  DebugOut(data_alliance.id)
  if nil ~= data_alliance and data_alliance.id ~= nil and data_alliance.id ~= "" then
    DebugOut("true")
    local partData = self:GetUpgradingRebuildPartData()
    if partData and self.myAccelerateBarVisible then
      return true
    end
  end
  return false
end
function GameUIRebuildFleet:IsHelpOthersFuncVisible()
  local data_alliance = GameGlobalData:GetData("alliance")
  DebugOut("IsHelpOthersFuncVisible")
  DebugTable(data_alliance)
  DebugOut(data_alliance.id)
  if nil ~= data_alliance and data_alliance.id ~= nil and data_alliance.id ~= "" then
    DebugOut("true")
    return true
  end
  return false
end
function GameUIRebuildFleet.SetHelpOthersBtn()
  if GameUIRebuildFleet:IsHelpOthersFuncVisible() then
    local helpData = GameGlobalData:GetData("remodel_help")
    local helpNum = 0
    if helpData ~= nil then
      for k, v in pairs(helpData) do
        if v.is_help == 0 then
          helpNum = helpNum + 1
        end
      end
    end
    if GameUIFactory:GetFlashObject() then
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetHelpOthersBtn", true, helpNum)
    end
  elseif GameUIFactory:GetFlashObject() then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetHelpOthersBtn", false, 0)
  end
end
function GameUIRebuildFleet.OnAllianceResourceChange()
  GameUIRebuildFleet.SetHelpOthersBtn()
  if GameUIFactory:GetFlashObject() then
    if GameUIRebuildFleet:IsAskForHelpFuncVisible() then
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAskForHelpBtn", true)
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetMyAccelerateBarVisible", true)
    else
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAskForHelpBtn", false)
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetMyAccelerateBarVisible", false)
    end
  end
end
function GameUIRebuildFleet.AddUpdateUpgradingTimeData(infobarIndex, left_time, all_time)
  local upgradingData = {}
  upgradingData.infobarIndex = infobarIndex
  upgradingData.finishTime = os.time() + left_time
  upgradingData.allTime = all_time
  table.insert(GameUIRebuildFleet.UpgradingData, upgradingData)
end
function GameUIRebuildFleet:PlayEnterAnimation(entrance)
  local shareStoryEnabled = false
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP) then
    shareStoryEnabled = true
  end
  local isOver = self:IsFirstRebuildPageOver()
  local isOver2 = self:IsSecondRebuildPageOver()
  local curPage = 1
  if isOver and not isOver2 then
    curPage = 2
  elseif isOver2 then
    curPage = 3
  end
  if entrance == self.Entrance.FactoryInner then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "MoveFromRepairToRebuild", shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP], curPage)
  elseif entrance == self.Entrance.MainPlanet then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "ShowRebuildWindowDirectly", shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP], curPage)
  elseif entrance == self.Entrance.Alliance then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "ShowRebuildWindowDirectly", shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP], curPage)
  end
end
function GameUIRebuildFleet.RequestModelInfoCallback(msgtype, content)
  DebugOut("RequestModelInfoCallback:")
  DebugOut(msgtype)
  DebugOut(content.remodel_info_ack)
  if msgtype and msgtype == NetAPIList.remodel_info_ack.Code then
    DebugOut("RequestModelInfoCallback/true")
    GameGlobalData:UpdateGameData("remodel_info", content.remodel_info_ack)
    GameUIRebuildFleet.UpdateRebuildData()
    return true
  end
  return false
end
function GameUIRebuildFleet.OnModelInfoChange()
  GameUIRebuildFleet.UpdateRebuildData()
end
function GameUIRebuildFleet.OnModelHelpInfoChange()
  local helpData = GameGlobalData:GetData("remodel_help")
  if helpData and GameUIRebuildFleet.helpData and #GameUIRebuildFleet.helpData ~= #helpData then
    GameUIRebuildFleet._UpdateAccelerateList()
  else
    GameUIRebuildFleet.ResetHelpData()
  end
end
function GameUIRebuildFleet.ResetHelpData()
  local helpData = GameGlobalData:GetData("remodel_help")
  if not helpData then
    return
  end
  if not GameUIRebuildFleet.helpData then
    return
  end
  for index, value in ipairs(GameUIRebuildFleet.helpData) do
    for _, m in ipairs(helpData) do
      if value.user_id == m.user_id then
        value.type = m.type
        value.cur_num = m.cur_num
        value.max_num = m.max_num
        value.is_help = m.is_help
        GameUIRebuildFleet:SetAccelerateItemData(index)
      end
    end
  end
  GameUIRebuildFleet.SetHelpOthersBtn()
  if GameUIFactory:GetFlashObject() then
    if helpData and GameUIRebuildFleet.CanHelpOther(helpData) then
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAccelerateAllBtn", true)
    else
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAccelerateAllBtn", false)
    end
  end
end
function GameUIRebuildFleet:IsFirstRebuildPageOver()
  local rebuildData = GameGlobalData:GetData("remodel_info")
  DebugOut(" IsFirstRebuildPageOver ")
  DebugTable(rebuildData)
  if rebuildData ~= nil then
    table.sort(rebuildData, function(v1, v2)
      return v1.type < v2.type
    end)
    DebugOut("IsFirstRebuildPageOver :")
    DebugTable(rebuildData)
    for i = 1, 5 do
      if rebuildData[i].cur_level ~= rebuildData[i].max_level then
        return false
      end
    end
    return true
  end
  return false
end
function GameUIRebuildFleet:IsSecondRebuildPageOver()
  local rebuildData = GameGlobalData:GetData("remodel_info")
  DebugOut(" IsSecondRebuildPageOver ")
  DebugTable(rebuildData)
  if rebuildData ~= nil then
    table.sort(rebuildData, function(v1, v2)
      return v1.type < v2.type
    end)
    DebugOut("IsSecondRebuildPageOver :")
    DebugTable(rebuildData)
    for i = 6, 10 do
      if rebuildData[i] and rebuildData[i].cur_level ~= rebuildData[i].max_level then
        return false
      end
    end
    return true
  end
  return false
end
function GameUIRebuildFleet.UpdateRebuildData()
  GameUIRebuildFleet.UpgradingData = {}
  local rebuildData = GameGlobalData:GetData("remodel_info")
  DebugOut("UpdateRebuildData/rebuildData:")
  DebugTable(rebuildData)
  local infobarName
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if rebuildData == nil then
    DebugOut("rebuildData is nil")
    return
  end
  table.sort(rebuildData, function(v1, v2)
    return v1.type < v2.type
  end)
  for k, v in pairs(rebuildData) do
    DebugOut("rebuildData:index")
    DebugTable(v)
    DebugOut(type(v.state))
    local curPage = 1
    local curItemIndex = v.type
    if v.type > 5 and v.type < 11 then
      curPage = 2
      curItemIndex = v.type - 5
    elseif v.type > 10 then
      curPage = 3
      curItemIndex = v.type - 10
    end
    infobarName = GameLoader:GetGameText("LC_MENU_REBUILD_MODULE_NAME_" .. tostring(v.type))
    if v.state == GameUIRebuildFleet.InfobarState.UPGRADING then
      if GameUIFactory:GetFlashObject() then
        GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetInfobarData", curItemIndex, infobarName, v.cur_level, v.addhp, v.left_time, v.nextlevel_time, "upgrading", curPage, GameLoader:GetGameText("LC_MENU_Level"))
      end
      GameUIRebuildFleet.AddUpdateUpgradingTimeData(v.type, v.left_time, v.nextlevel_time)
    elseif v.state == GameUIRebuildFleet.InfobarState.WAITING then
      if levelInfo.level == v.cur_level or v.cur_level == v.max_level then
        if GameUIFactory:GetFlashObject() then
          GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetInfobarData", curItemIndex, infobarName, v.cur_level, v.addhp, v.left_time, v.nextlevel_time, "max", curPage, GameLoader:GetGameText("LC_MENU_Level"))
        end
      elseif GameUIFactory:GetFlashObject() then
        GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetInfobarData", curItemIndex, infobarName, v.cur_level, v.addhp, v.left_time, v.nextlevel_time, "waiting", curPage, GameLoader:GetGameText("LC_MENU_Level"))
      end
    end
  end
  if GameUIFactory:GetFlashObject() then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "switchRebuildPage")
  end
  GameUIRebuildFleet:SetMyAccelerateItemData()
  GameUIRebuildFleet.InitHelpMessageList()
  GameUIRebuildFleet.OnAllianceResourceChange()
end
function GameUIRebuildFleet:Update()
  if GameUIRebuildFleet.UpgradingData then
    for k, v in pairs(GameUIRebuildFleet.UpgradingData) do
      if v.finishTime <= os.time() then
        NetMessageMgr:SendMsg(NetAPIList.remodel_info_req.Code, nil, self.RequestModelInfoCallback, true, nil)
        GameUIRebuildFleet.UpgradingData = {}
        break
      end
      local curPage = 1
      local curItemIndex = v.infobarIndex
      if v.infobarIndex > 5 and v.infobarIndex < 11 then
        curPage = 2
        curItemIndex = v.infobarIndex - 5
      elseif v.infobarIndex > 10 then
        curPage = 3
        curItemIndex = v.infobarIndex - 10
      end
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetUpgradingInfobarData", curItemIndex, v.finishTime - os.time(), v.allTime, curPage)
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "UpdatePopinfoTime", v.finishTime - os.time(), v.allTime)
    end
  end
  if not GameUIRebuildFleet.myAccelerateBarVisible and GameUIRebuildFleet.myAccelerateBarTime <= os.time() then
    DebugOut("GameUIRebuildFleet/Update")
    GameUIRebuildFleet.myAccelerateBarVisible = true
    GameUIRebuildFleet:UpdateAccelerateBtn()
  end
end
function GameUIRebuildFleet:UpdateAccelerateBtn()
  DebugOut("UpdateAccelerateBtn")
  DebugOut(self.myAccelerateBarVisible)
  if GameUIRebuildFleet.myAccelerateBarVisible then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAskForHelpBtn", true)
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetMyAccelerateBarVisible", true)
  else
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAskForHelpBtn", false)
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetMyAccelerateBarVisible", false)
  end
end
function GameUIRebuildFleet.RemodelPushInfoNtf(content)
  DebugOut("RemodelPushInfoNtf")
  if content == nil then
    DebugOut("RemodelPushInfoNtf:nil")
    return
  end
  DebugTable(content)
  GameUIRebuildFleet.remodel_push_info = content.ntf
  GameUIRebuildFleet.InitHelpMessageList()
end
function GameUIRebuildFleet.InitHelpMessageList()
  if GameUIFactory:GetFlashObject() == nil then
    DebugOut("InitHelpMessageList nil")
    return
  end
  local alliance = GameGlobalData:GetData("alliance")
  if GameUIRebuildFleet.remodel_push_info == nil or alliance.id == "" or alliance.id == 0 then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "InitHelpMessageList", 0)
    return
  end
  GameUIFactory:GetFlashObject():InvokeASCallback("_root", "InitHelpMessageList", #GameUIRebuildFleet.remodel_push_info)
end
function GameUIRebuildFleet.UpdateAccelerateList()
  if not GameUIRebuildFleet.IsShowHelpOthersWindow then
    DebugOut("IsShowHelpOthersWindow:", GameUIRebuildFleet.IsShowHelpOthersWindow)
    return
  end
  local vipData = GameGlobalData:GetData("vipinfo")
  local helpData = GameGlobalData:GetData("remodel_help")
  DebugOut("UpdateAccelerateList:")
  DebugTable(helpData)
  if helpData == nil then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "InitAccelerateList", 0)
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "ShowHelpOthersWindow")
    return
  end
  GameUIFactory:GetFlashObject():InvokeASCallback("_root", "InitAccelerateList", #helpData)
  GameUIRebuildFleet.SetHelpOthersBtn()
  if helpData and GameUIRebuildFleet.CanHelpOther(helpData) then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAccelerateAllBtn", true)
  else
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAccelerateAllBtn", false)
  end
end
function GameUIRebuildFleet._UpdateAccelerateList()
  if not GameUIRebuildFleet.IsShowHelpOthersWindow then
    DebugOut("IsShowHelpOthersWindow:", GameUIRebuildFleet.IsShowHelpOthersWindow)
    return
  end
  local vipData = GameGlobalData:GetData("vipinfo")
  GameUIRebuildFleet.helpData = GameGlobalData:GetData("remodel_help")
  DebugOut("UpdateAccelerateList:")
  DebugTable(GameUIRebuildFleet.helpData)
  GameUIFactory:GetFlashObject():InvokeASCallback("_root", "InitAccelerateList", #GameUIRebuildFleet.helpData)
  GameUIRebuildFleet.SetHelpOthersBtn()
  if GameUIRebuildFleet.helpData and GameUIRebuildFleet.CanHelpOther(GameUIRebuildFleet.helpData) then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAccelerateAllBtn", true)
  else
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAccelerateAllBtn", false)
  end
end
function GameUIRebuildFleet.CanHelpOther(helpData)
  for k, v in pairs(helpData) do
    if v.is_help == 0 then
      return true
    end
  end
  return false
end
function GameUIRebuildFleet:GetAccelerateItemData(itemIndex)
  if nil == itemIndex then
    return nil
  end
  return GameUIRebuildFleet.helpData[itemIndex]
end
function GameUIRebuildFleet:SetAccelerateItemData(itemIndex)
  local itemData = self:GetAccelerateItemData(itemIndex)
  if itemData == nil then
    return
  end
  local info = GameLoader:GetGameText("LC_MENU_REBUILD_MODULE_NAME_" .. tostring(itemData.type))
  info = string.format(GameLoader:GetGameText("LC_MENU_REBUILD_BEING_MODIFIED"), info)
  local infoNum = tostring(itemData.cur_num) .. "/" .. tostring(itemData.max_num)
  local progress = math.max(1, math.floor(100 * itemData.cur_num / itemData.max_num))
  local progressDesc = GameLoader:GetGameText("LC_MENU_REBUILD_CAN_HELP_NUMBER")
  local btnDesc = GameLoader:GetGameText("LC_MENU_REBUILD_SPEED_UP_BUTTON")
  if GameUIFactory:GetFlashObject() then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAccelerateItemData", itemIndex, GameUtils:GetUserDisplayName(itemData.user_name), info, infoNum, progress, progressDesc, btnDesc)
    local isVisible = itemData.is_help == 0
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetAccelerateItemBtn", itemIndex, isVisible)
  end
end
function GameUIRebuildFleet:SetMyAccelerateItemData()
  DebugOut("SetMyAccelerateItemData")
  local upgradingPartData = self:GetUpgradingRebuildPartData()
  local nameStr = GameLoader:GetGameText("LC_MENU_REBUILD_MYSELF")
  if not GameUIFactory:GetFlashObject() then
    return
  end
  if nil == upgradingPartData then
    local info = GameLoader:GetGameText("LC_MENU_REBUILD_WAIT_UPGRADE")
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetMyAccelerateBarData", nameStr, info, "0/0", 1, false)
  else
    local info = GameLoader:GetGameText("LC_MENU_REBUILD_BEING_MODIFIED")
    info = string.format(info, GameLoader:GetGameText("LC_MENU_REBUILD_MODULE_NAME_" .. tostring(upgradingPartData.type)))
    local infoNum = tostring(upgradingPartData.cur_help) .. "/" .. tostring(upgradingPartData.max_help)
    local progress = math.max(1, math.floor(100 * upgradingPartData.cur_help / upgradingPartData.max_help))
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetMyAccelerateBarData", nameStr, info, infoNum, progress, true)
  end
end
function GameUIRebuildFleet:SetHelpMessageItem(itemIndex)
  if self.remodel_push_info == nil then
    DebugOut("SetHelpMessageItem nil")
    return
  end
  local itemData = self.remodel_push_info[#self.remodel_push_info - itemIndex + 1]
  if itemData == nil then
    DebugOut("SetHelpMessageItem nil 2")
    return
  end
  local partName = GameLoader:GetGameText("LC_MENU_REBUILD_MODULE_NAME_" .. itemData.type)
  local desc
  if itemData.action == 0 then
    desc = GameLoader:GetGameText("LC_MENU_REBUILD_HELP_INFO_2")
  elseif itemData.action == 1 then
    desc = GameLoader:GetGameText("LC_MENU_REBUILD_HELP_INFO_1")
  end
  local nameTable = LuaUtils:string_split(itemData.name, ".")
  local userName = string.sub(itemData.name, 0, string.len(itemData.name) - string.len(nameTable[#nameTable]) - 1)
  desc = string.gsub(desc, "<playerName_char>", userName)
  desc = string.gsub(desc, "<reshape_char>", partName)
  local timePast = GameUIRebuildFleet.FormatDate(itemData.time)
  DebugOut("SetHelpMessageItem:", itemIndex)
  DebugOut(desc, timePast)
  GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetHelpMessageItem", itemIndex, true, desc, timePast)
end
function GameUIRebuildFleet.FormatDate(time)
  if time >= os.time() then
    return "0S"
  end
  local past = os.date("*t", os.time() - time)
  DebugOut("past")
  DebugTable(past)
  local start = os.date("*t", 0)
  past.year = past.year - start.year
  past.month = past.month - start.month
  past.day = past.day - start.day
  past.hour = past.hour - start.hour
  past.min = past.min - start.min
  past.sec = past.sec - start.sec
  if past.year ~= 0 then
    return tostring(past.year) .. "y" .. tostring(past.month) .. "m"
  elseif past.month ~= 0 then
    return tostring(past.month) .. "m" .. tostring(past.day) .. "d"
  elseif past.day ~= 0 then
    return tostring(past.day) .. "d" .. tostring(past.hour) .. "H"
  elseif past.hour ~= 0 then
    return tostring(past.hour) .. "H" .. tostring(past.min) .. "M"
  elseif past.min ~= 0 then
    return tostring(past.min) .. "M" .. tostring(past.sec) .. "S"
  else
    return tostring(past.sec) .. "S"
  end
end
function GameUIRebuildFleet:ShowHelpWindow()
  GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
  GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_REBUILD_HELP"))
end
function GameUIRebuildFleet:GetRebuildPartData(partType)
  local remodelData = GameGlobalData:GetData("remodel_info")
  if remodelData == nil then
    DebugOut("GameUIRebuildFleet/GetRebuildPartData nil")
    return nil
  end
  for k, v in pairs(remodelData) do
    if v.type == partType then
      return v
    end
  end
  return nil
end
function GameUIRebuildFleet:GetUpgradingRebuildPartData()
  local remodelData = GameGlobalData:GetData("remodel_info")
  if remodelData == nil then
    DebugOut("GameUIRebuildFleet/GetUpgradingRebuildPartData nil")
    return nil
  end
  for k, v in pairs(remodelData) do
    if v.state == self.InfobarState.UPGRADING then
      return v
    end
  end
  return nil
end
function GameUIRebuildFleet:ShowInfobar(infobarType)
  DebugOut("GameUIRebuildFleet/ShowInfobar")
  DebugOut(infobarType)
  self.curSelectInfobar = infobarType
  local infobarData = self:GetRebuildPartData(infobarType)
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local partName = GameLoader:GetGameText("LC_MENU_REBUILD_MODULE_NAME_" .. tostring(infobarType))
  local finishItemCnt = GameGlobalData:GetItemCount(2519)
  local hasItemAcc = false
  DebugOut("finishItemCnt:", finishItemCnt)
  if finishItemCnt > 0 then
    hasItemAcc = true
  end
  if infobarData.state == self.InfobarState.UPGRADING then
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetUpgradingFleetPartData", partName, infobarData.left_time, infobarData.nextlevel_time, infobarData.credit, hasItemAcc)
  elseif infobarData.state == self.InfobarState.WAITING then
    if levelInfo.level == infobarData.cur_level or infobarData.cur_level == infobarData.max_level then
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetMaxFleetPartData", partName)
    else
      GameUIFactory:GetFlashObject():InvokeASCallback("_root", "SetWaitFleetPartData", partName, infobarData.nextlevel_time, infobarData.nextlevel_money, infobarData.nextlevel_addhp)
    end
  end
end
function GameUIRebuildFleet.IsAnyPartUpgradingNow()
  local partData = GameGlobalData:GetData("remodel_info")
  for k, v in pairs(partData) do
    if v.state == GameUIRebuildFleet.InfobarState.UPGRADING then
      return true
    end
  end
  return false
end
function GameUIRebuildFleet:AskForUpgrade()
  if self.curSelectInfobar == nil then
    DebugOut("GameUIRebuildFleet/AskForUpgrade:curSelectInfobar is nil")
    return
  end
  if GameUIRebuildFleet.IsAnyPartUpgradingNow() then
    GameTip:Show(GameLoader:GetGameText("LC_ALERT_mech_is_levelup"))
    return
  end
  local userResource = GameGlobalData:GetData("resource")
  local partData = self:GetRebuildPartData(self.curSelectInfobar)
  local userInfo = GameGlobalData:GetData("userinfo")
  local content = {}
  content.user_id = userInfo.player_id
  content.type = partData.type
  NetMessageMgr:SendMsg(NetAPIList.remodel_levelup_req.Code, content, nil, false, nil)
  GameUtils:RecordForTongdui(NetAPIList.remodel_levelup_req.Code, content, nil, false, nil)
  self.myAccelerateBarVisible = true
end
function GameUIRebuildFleet:AskForFinishUpgrade(finishType)
  if self.curSelectInfobar == nil then
    DebugOut("GameUIRebuildFleet/AskForFinishUpgrade:curSelectInfobar is nil")
    return
  end
  local userResource = GameGlobalData:GetData("resource")
  local partData = self:GetRebuildPartData(self.curSelectInfobar)
  if finishType == 1 then
    DebugOut("AskForFinishUpgrade")
    DebugTable(partData)
    DebugTable(userResource)
    if userResource and userResource.credit >= partData.credit then
      do
        local content = {}
        content.type = partData.type
        content.item_id = 0
        DebugOut("GameUIRebuildFleet/AskForFinishUpgrade:")
        DebugOut(GameSettingData.EnablePayRemind)
        if not GameSettingData.EnablePayRemind then
          DebugOut("send directly")
          NetMessageMgr:SendMsg(NetAPIList.remodel_speed_req.Code, content, nil, false, nil)
        else
          GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
          GameUIMessageDialog:SetYesButton(function()
            NetMessageMgr:SendMsg(NetAPIList.remodel_speed_req.Code, content, nil, false, nil)
          end)
          local notification = GameLoader:GetGameText("LC_MENU_REBUILD_CREDIT_UPGRADE")
          notification = string.format(notification, partData.credit, GameLoader:GetGameText("LC_MENU_REBUILD_MODULE_NAME_" .. partData.type))
          GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_REBUILD_DONE_DIRECTLY_BUTTON"), notification)
        end
      end
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_NOT_ENOUGH_CREDIT"))
    end
  elseif finishType == 2 then
    local content = {}
    content.type = partData.type
    content.item_id = 2519
    NetMessageMgr:SendMsg(NetAPIList.remodel_speed_req.Code, content, nil, false, nil)
  end
end
function GameUIRebuildFleet:ShowHelpOthersWindow()
  DebugOut("ShowHelpOthersWindow")
  NetMessageMgr:SendMsg(NetAPIList.remodel_help_info_req.Code, nil, self.RequestModelHelpInfoCallback, true, nil)
end
function GameUIRebuildFleet.RequestModelHelpInfoCallback(msgtype, content)
  DebugOut("RequestModelHelpInfoCallback:")
  DebugOut(msgtype)
  DebugTable(content)
  DebugTable(content.info)
  if msgtype and msgtype == NetAPIList.remodel_help_info_ack.Code then
    DebugOut("RequestModelHelpInfoCallback/true")
    GameGlobalData.GlobalData.remodel_help = {}
    GameGlobalData:UpdateGameData("remodel_help", content.info)
    GameUIRebuildFleet.IsShowHelpOthersWindow = true
    GameUIRebuildFleet._UpdateAccelerateList()
    GameUIFactory:GetFlashObject():InvokeASCallback("_root", "ShowHelpOthersWindow")
    return true
  end
  return false
end
function GameUIRebuildFleet:AskForHelp()
  DebugOut("GameUIRebuildFleet/AskForHelp")
  NetMessageMgr:SendMsg(NetAPIList.remodel_help_req.Code, nil, nil, false, nil)
  GameTip:Show(GameLoader:GetGameText("LC_MENU_REBUILD_ASK_HELP_SUCCESS_LEVEL"))
  self.myAccelerateBarVisible = false
  GameUIRebuildFleet.myAccelerateBarTime = os.time() + 300
  GameUIRebuildFleet:UpdateAccelerateBtn()
  local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
  if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP] then
    FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP)
  else
  end
end
