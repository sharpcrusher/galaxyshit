local GameObjectWelcome = LuaObjectManager:GetLuaObject("GameObjectWelcome")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameUIFestival = LuaObjectManager:GetLuaObject("GameUICommonEvent")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateStore = GameStateManager.GameStateStore
local GameStateArena = GameStateManager.GameStateArena
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
local GameStateDaily = GameStateManager.GameStateDaily
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local k_tempImageName = "welcome_banner.png"
local m_types = {
  k_none = -1,
  k_vip = 0,
  k_ally = 1,
  k_shop = 2,
  k_wd = 3,
  k_colonial = 4,
  k_normalActivety = 5,
  k_festiveActivety = 6
}
local defaultDisType = 6
function GameObjectWelcome:OnFSCommand(cmd, arg)
  if cmd == "close" then
    function self.m_closeCallback()
      GameStateManager:GetCurrentGameState():EraseObject(self)
      GameStateMainPlanet:CheckShowQuestInMainPlanet()
    end
    if self._welcomeScreenList[self._currentIndex - 1] and self._welcomeScreenList[self._currentIndex - 1].type == "HTTP" then
      ext.webview.DestroyWebview()
    end
    self:maskWelecomeScreen(self._welcomeScreenList[self._currentIndex - 1])
    if self._welcomeScreenList[self._currentIndex] then
      self:SetBasicData()
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetContentMove", false)
    end
  elseif cmd == "enter" then
    do
      local index = tonumber(arg)
      local screenData = self._welcomeScreenList[index]
      if screenData and screenData.type == "HTTP" then
        ext.webview.DestroyWebview()
      end
      if self:IsCanEnterWelcomeDis(screenData) then
        function self.m_closeCallback()
          GameStateManager:GetCurrentGameState():EraseObject(self)
          self:EnterWelcomeDis(screenData)
        end
        self:GetFlashObject():InvokeASCallback("_root", "SetContentMove", false)
      else
        local info = GameLoader:GetGameText(self:GetEnterWelcomeDisFailedTextID(screenData))
        GameTip:Show(info)
      end
      self:maskWelecomeScreen(self._welcomeScreenList[self._currentIndex - 1])
    end
  elseif cmd == "contentSize" then
    local x, y, width, height = unpack(LuaUtils:string_split(arg, ":"))
    self._webViewX = tonumber(x)
    self._webViewY = tonumber(y)
    self._webViewWidth = tonumber(width)
    self._webViewHeight = tonumber(height)
  elseif cmd == "close_menu" and self.m_closeCallback then
    self.m_closeCallback()
  end
end
function GameObjectWelcome:CreateWebView(url, x, y, width, height)
  DebugOut("ajjhsaadj:", x, y, width, height)
  local result = ext.webview.OpenWebview(url, x, y, width, height)
  if result then
    DebugOut("webView Create Success")
  end
end
function GameObjectWelcome:EnterWelcomeDis(content)
  local disType = content.func_name
  DebugOut("EnterWelcomeDis disType", disType)
  if disType == "pay" then
    GameVip:showVip(false)
  elseif disType == "vip" then
    GameVip:showVip(false)
  elseif disType == "alliance" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAlliance)
  elseif disType == "store" then
    GameStateManager:SetCurrentGameState(GameStateStore)
  elseif disType == "mysterystore" then
    GameGoodsList:SetEnterStoreType(2)
    GameStateManager:SetCurrentGameState(GameStateStore)
  elseif disType == "salestore" then
    GameGoodsList:SetEnterStoreType(3)
    GameStateManager:SetCurrentGameState(GameStateStore)
  elseif disType == "firstcharge" then
    GameStateManager:GetCurrentGameState():AddObject(GameUIFirstCharge)
  elseif disType == "glc" then
    GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.wdc)
    GameStateManager:SetCurrentGameState(GameStateArena)
  elseif disType == "subtask" then
    GameQuestMenu.gotoTabTag = GameQuestMenu.ACTIVITY_QUEST
    GameQuestMenu:ShowQuestMenu()
  elseif disType == "wd" then
    GameObjectMainPlanet:BuildingClicked("world_domination")
  elseif disType == "wormhole" then
    GameObjectClimbTower:preRequestInstanceData(0)
  elseif disType == "calendar" then
    GameHelper:SetEnterPage(2)
    GameStateManager:SetCurrentGameState(GameStateDaily)
  elseif disType == "dailytask" then
    GameHelper:SetEnterPage(1)
    GameStateManager:SetCurrentGameState(GameStateDaily)
  elseif disType == "activity" then
    GameUIBarLeft:OnFSCommand("enterFestivalReleased", "")
  elseif disType == "wve" then
    WVEGameManeger:StartWVEGame()
  elseif disType == m_types.k_festiveActivety then
    GameUIFestival:ShowEvent(GameUIFestival.DetailType.FESTIVAL)
  end
end
function GameObjectWelcome:IsCanEnterWelcomeDis(content)
  local disType = content.func_name
  if disType == "pay" then
    return true
  elseif disType == "vip" then
    return true
  elseif disType == "alliance" then
    return true
  elseif disType == "store" then
    return true
  elseif disType == "mysterystore" then
    return true
  elseif disType == "salestore" then
    return true
  elseif disType == "firstcharge" then
    return true
  elseif disType == "glc" then
    return true
  elseif disType == "subtask" then
    return true
  elseif disType == "wd" then
    return true
  elseif disType == "wormhole" then
    return true
  elseif disType == "calendar" then
    return true
  elseif disType == "dailytask" then
    return true
  elseif disType == "activity" then
    return true
  elseif disType == "wve" then
    return true
  elseif disType == m_types.k_festiveActivety then
    return ...
  end
  return false
end
function GameObjectWelcome:GetEnterWelcomeDisFailedTextID(content)
  local disType = content.func_name
  if disType == m_types.k_vip then
    return "LC_ALERT_battle_level_limit"
  elseif disType == m_types.k_ally then
    return "LC_ALERT_battle_level_limit"
  elseif disType == m_types.k_shop then
    return "LC_ALERT_battle_level_limit"
  elseif disType == m_types.k_wd then
    return "LC_MENU_CANT_USE_FEATURE"
  elseif disType == m_types.k_colonial then
    return "LC_ALERT_battle_level_limit"
  elseif disType == m_types.k_normalActivety then
    return "LC_ALERT_battle_level_limit"
  elseif disType == m_types.k_festiveActivety then
    return "LC_MENU_CANT_USE_FEATURE"
  end
  return ""
end
function GameObjectWelcome:ShowWelcome()
  GameStateManager:GetCurrentGameState():AddObject(self)
end
function GameObjectWelcome:CheckShowWelcome(content)
  local GameUIRechargePush = LuaObjectManager:GetLuaObject("GameUIRechargePush")
  local needPop = false
  for _, v in pairs(content.png_list) do
    local pngName = v .. ".png"
    if not DynamicResDownloader:IfResExsit("data2/" .. pngName) then
      DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.WELCOME_PIC, nil, nil)
    end
  end
  for k, v in pairs(content.screen_items or {}) do
    local screenData = {}
    screenData.id = tonumber(v.id)
    screenData.leftTime = v.left_time
    screenData.type = v.type
    screenData.content = v.content
    screenData.order = v.order
    screenData.func_name = v.func_name
    screenData.titleText = GameLoader:GetGameText("LC_MENU_WELCOME_TITLE_" .. v.id)
    screenData.descText = GameLoader:GetGameText("LC_MENU_WELCOME_DESC_" .. v.id)
    if screenData.type == "PNG" then
      local pngName = screenData.content
      pngName = pngName .. ".png"
      screenData.content = pngName
      if DynamicResDownloader:IfResExsit("data2/" .. pngName) then
        ext.UpdateAutoUpdateFile(pngName, 0, 0, 0)
        needPop = true
        self._welcomeScreenList[#self._welcomeScreenList + 1] = screenData
      end
    elseif screenData.type == "HTTP" then
      self._welcomeScreenList[#self._welcomeScreenList + 1] = screenData
      needPop = true
    end
  end
  if needPop then
    GameUIRechargePush:WelcomePopReceived(true)
    GameNotice:AddNotice("Welcome")
  else
    GameUIRechargePush:WelcomePopReceived(false)
  end
end
function GameObjectWelcome:OnAddToGameState()
  if not self:GetFlashObject() then
    DebugOut("GameObjectWelcome:OnAddToGameState")
    self:LoadFlashObject()
    self:GetFlashObject():InvokeASCallback("_root", "GetWebViewSize")
    self:SetBasicData()
    GameTimer:Add(function()
      if GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectWelcome) then
        GameObjectWelcome:OnTimeChange()
        return 1000
      end
      return nil
    end, 1000)
    self:GetFlashObject():InvokeASCallback("_root", "SetContentMove", true)
  end
end
function GameObjectWelcome:SetBasicData()
  local obj = self:GetFlashObject()
  local data
  local screenData = self._welcomeScreenList[self._currentIndex]
  if screenData then
    data = {}
    data.currentIndex = self._currentIndex
    data.leftTimeText = GameUtils:formatTimeStringAsPartion(screenData.leftTime)
    data.titleText = screenData.titleText
    data.descText = screenData.descText
    if screenData.type == "PNG" then
      local pngName = screenData.content
      obj:ReplaceTexture(k_tempImageName, pngName)
    elseif screenData.type == "HTTP" then
      data.content = screenData.content .. "/" .. string.lower(GameSettingData.Save_Lang) .. "/index.html"
      self:CreateWebView(data.content, self._webViewX, self._webViewY, self._webViewWidth, self._webViewHeight)
    end
    self._currentIndex = self._currentIndex + 1
    data.isShowEnter = self:IsCanEnterWelcomeDis(screenData)
    data.type = screenData.type
  end
  obj:InvokeASCallback("_root", "SetContent", data)
  self:OnTimeChange()
end
function GameObjectWelcome:OnTimeChange()
  local data = {}
  data.tipsText = ""
  data.isEnd = false
  local screenData = self._welcomeScreenList[self._currentIndex - 1]
  if screenData == nil then
    return
  end
  local leftTime = 0
  if screenData.leftTime ~= -1 then
    data.showTime = true
    leftTime = screenData.leftTime - (os.time() - GameObjectWelcome.receiveDataTime)
    if leftTime < 0 then
      leftTime = 0
    end
    if leftTime == 0 then
      data.tipsText = GameLoader:GetGameText("LC_MENU_EVENT_OVER_BUTTON")
      data.isEnd = true
    else
      data.tipsText = GameUtils:formatTimeString(leftTime)
    end
    data.isShowEnter = self:IsCanEnterWelcomeDis(screenData)
  else
    data.tipsText = ""
    data.isEnd = false
    data.showTime = false
    data.isShowEnter = self:IsCanEnterWelcomeDis(screenData)
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "OnTimeChange", data)
  end
end
function GameObjectWelcome:OnInitGame()
  self._welcomeScreenList = {}
  self._currentIndex = 1
  self._webViewX = 0
  self._webViewY = 0
  self._webViewWidth = 0
  self._webViewHeight = 0
end
function GameObjectWelcome:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameObjectWelcome:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
end
function GameObjectWelcome:SendWelcomeReq()
  DebugOut("GameObjectWelcome:SendWelcomeReq")
  local LoginInfo = GameUtils:GetLoginInfo()
  local data = {
    logic_id = LoginInfo.ServerInfo.logic_id,
    language = string.lower(GameSettingData.Save_Lang)
  }
  NetMessageMgr:SendMsg(NetAPIList.welcome_screen_req.Code, data, GameObjectWelcome.WelcomeInfoCallback, false, nil)
end
function GameObjectWelcome.WelcomeInfoCallback(msgType, content)
  if msgType == NetAPIList.welcome_screen_ack.Code then
    DebugOut("GameObjectWelcome.WelcomeInfoCallback")
    DebugTable(content)
    GameObjectWelcome.receiveDataTime = os.time()
    GameObjectWelcome:CheckShowWelcome(content)
    GameNotice:CheckFetchInfoCallback()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.welcome_screen_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameObjectWelcome:maskWelecomeScreen(screenData)
  DebugOut("GameObjectWelcome:maskWelecomeScreen", screenData.id)
  local reqData = {}
  reqData.id = screenData.id
  NetMessageMgr:SendMsg(NetAPIList.welcome_mask_req.Code, reqData, nil, false, nil)
end
