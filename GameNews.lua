local GameNews = LuaObjectManager:GetLuaObject("GameNews")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
GameNews.SignCanNotReceive = 0
GameNews.SignCanReceive = 1
GameNews.SignReceived = 2
GameNews.ACTIVITY_STS_NO = 1
GameNews.ACTIVITY_STS_RUNNING = 2
GameNews.ACTIVITY_STS_NOT_BEGIN = 3
GameNews.ACTIVITY_STS_END = 4
GameNews.useFakeData = false
GameNews.Link = ""
function GameNews:OnInitGame()
  GameNews.currentLab = ""
  GameNews.showContent = false
end
function GameNews:OnAddToGameState(...)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
end
function GameNews:OnEraseFromGameState()
  GameNews.currentLab = ""
  GameNews.showContent = false
  self:UnloadFlashObject()
end
function GameNews:ShowNews(label)
  DebugNews("GameNews:ShowNews self.currentLab: ", self.currentLab, " label: ", label)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameNews:AddToGameState()
  if self.currentLab == label then
    return
  end
  if label == "news" then
    GameNews:InitNewsList()
    if GameNews.newslist then
      GameNews:ShowNewsList()
      GameNews.currentLab = "news"
      GameNews:AddToGameState()
    else
      self:RequestNews()
    end
  end
end
function GameNews:AddToGameState()
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameNews) then
    GameStateManager:GetCurrentGameState():AddObject(GameNews)
    if not GameNews:GetFlashObject() then
      GameNews:LoadFlashObject()
    end
    self:GetFlashObject():InvokeASCallback("_root", "moveIn")
    local signText = GameLoader:GetGameText("LC_MENU_TITLE_SIGN")
    local activityText = GameLoader:GetGameText("LC_MENU_CHAR_ACTIVITY")
    local newsText = GameLoader:GetGameText("LC_MENU_CHAR_NEWS")
    self:GetFlashObject():InvokeASCallback("_root", "initTabText", signText, activityText, newsText)
  end
end
function GameNews:RequestNews()
  local requestType = "servers/" .. GameUtils:GetActiveServerInfo().id .. "/server_notice"
  local requestParam = {
    lang = GameSettingData.Save_Lang
  }
  GameUtils:HTTP_SendRequest(requestType, requestParam, GameNews.RequestNewsCallback, true, GameUtils.httpRequestFailedCallback, "GET", "notencrypt")
end
function GameNews.RequestNewsCallback(responseContent)
  DebugNews("RequestNewsCallback")
  DebugNewsTable(responseContent)
  local tmp = responseContent
  GameNews.newslist = {}
  for i, v in pairs(tmp) do
    table.insert(GameNews.newslist, 1, v)
  end
  GameNews:ShowNewsList()
  GameNews.currentLab = "news"
  GameNews:AddToGameState()
  return true
end
function GameNews.sortSignItem(v1, v2)
  if v1 and v2 and v1.days < v2.days then
    return true
  end
  return false
end
function GameNews.RequestNewsErrCallback(responseContent)
  DebugNewsTable(responseContent)
end
function GameNews:InitNewsList()
  self:GetFlashObject():InvokeASCallback("_root", "showNewsList")
end
function GameNews:ShowNewsList()
  for k, v in pairs(GameNews.newslist) do
    self:GetFlashObject():InvokeASCallback("_root", "addNewslistItem", k)
  end
end
function GameNews:SetNewsListItem(itemId)
  local item = self.newslist[itemId]
  if item then
    local name, status, isReaded = item.title, false, false
    local viewedNoticeId = GameGlobalData.GlobalData.notice_and_act
    if viewedNoticeId then
      for k, v in pairs(viewedNoticeId.notices) do
        if tonumber(v) == GameUtils:GetShortActivityID(item.id) then
          isReaded = true
        end
      end
    else
      isReaded = true
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetNewsListItem", itemId, name, status, isReaded)
  end
end
function GameNews:setNoticeIsRead(itemId)
  local readID = GameUtils:GetShortActivityID(itemId)
  local param = {type = 1, id = itemId}
  NetMessageMgr:SendMsg(NetAPIList.notice_set_read_req.Code, param, GameNews.SetNoticeIsReadCallback, false)
end
function GameNews.SetNoticeIsReadCallback(msgType, content)
  if msgType == NetAPIList.gateway_info_ntf.Code then
    GameGlobalData.RefreshNewsAndAct(content)
    return true
  end
  return false
end
function GameNews:showNewsContent(itemId)
  local item
  if self.currentLab == "news" then
    item = self.newslist[itemId]
    local isReaded = false
    local viewedNoticeId = GameGlobalData.GlobalData.notice_and_act
    if viewedNoticeId then
      for k, v in pairs(viewedNoticeId.notices) do
        if tonumber(v) == GameUtils:GetShortActivityID(item.id) then
          isReaded = true
        end
      end
    else
      isReaded = true
    end
    if not isReaded then
      GameNews:setNoticeIsRead(item.id)
    end
  else
    item = self.activityList[itemId]
  end
  if item then
    local title = item.title
    local content = item.content
    if item.notice_type and string.find(item.notice_type, "announce_close_server") then
      loadstring(item.content)()
      content = notice.contentText
    end
    local startTime = string.sub(item.start_time, 1, 10)
    local endTime = string.sub(item.end_time, 1, 10)
    local showText = ""
    local isShow = false
    DebugOut("item out")
    DebugTable(item)
    if item.link and item.link ~= "" then
      DebugOut("link...", item.link)
      isShow = true
      GameNews.Link = item.link
      showText = item.link_btn_content
    end
    self:GetFlashObject():InvokeASCallback("_root", "showNewsContent", title, content, startTime, endTime, isShow, showText)
  end
end
function GameNews:Update(dt)
  self:GetFlashObject():Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate")
end
function GameNews:OnFSCommand(cmd, arg)
  if cmd == "CloseContent" then
    if self.currentLab == "news" then
      GameNews.showContent = false
      self:GetFlashObject():InvokeASCallback("_root", "contentReturn", true)
      GameNews:InitNewsList()
      GameNews:ShowNewsList()
    else
      self:GetFlashObject():InvokeASCallback("_root", "contentReturn", false)
    end
  end
  if cmd == "seeMore" then
    DebugOut(" fuck gotoSeeMore", GameNews.Link)
    ext.http.openURL(GameNews.Link)
  end
  if cmd == "showNewsDetail" then
    GameNews.showContent = true
    local itemId = tonumber(arg)
    self:showNewsContent(itemId)
  end
  if cmd == "close_menu" then
    GameStateManager:GetCurrentGameState():EraseObject(GameNews)
  end
  if cmd == "updateNewList" then
    local itemId = tonumber(arg)
    self:SetNewsListItem(itemId)
  end
  if cmd == "shownews" then
    self:ShowNews("news")
  end
  if cmd == "releaseNewsListItem" then
  end
end
if AutoUpdate.isAndroidDevice then
  function GameNews.OnAndroidBack()
    if GameNews.showContent then
      GameNews:OnFSCommand("CloseContent")
    else
      GameNews:GetFlashObject():InvokeASCallback("_root", "moveOut")
    end
  end
end
