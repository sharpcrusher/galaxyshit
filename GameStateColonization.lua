local GameUIColonial = LuaObjectManager:GetLuaObject("GameUIColonial")
local GameUISlaveSelect = LuaObjectManager:GetLuaObject("GameUISlaveSelect")
local GameUIColonialMenu = LuaObjectManager:GetLuaObject("GameUIColonialMenu")
local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameStateColonization = GameStateManager.GameStateColonization
GameStateColonization.directlyTo = {}
GameStateColonization.directlyTo.type = {
  unknown = 0,
  select_slave = 1,
  colonial = 2
}
GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
GameStateColonization.lastState = nil
GameStateColonization.FightType = {
  colonial = 1,
  save = 2,
  help = 3,
  independent = 4
}
GameStateColonization.IdentityType = {
  unknown = -1,
  slave = 1,
  freedom = 2,
  colonist = 3
}
GameStateColonization.TimesType = {
  colonist = 1,
  action_playful = 2,
  action_tricky = 3,
  rescue = 4,
  help = 5,
  independent = 7
}
GameStateColonization.curExpInfo = nil
GameStateColonization.curTimesInfo = nil
function GameStateColonization:InitGameState()
end
function GameStateColonization:OnFocusGain(previousState)
  DebugColonial("GameStateColonization:OnFocusGain", previousState)
  if GameStateColonization.lastState == nil then
    assert(false)
  end
  GameObjectBattleMapBG:LoadBGMap(1, 1)
  local bgPosX = -269
  local bgPosY = 0
  local offsetX = -4
  local offsetY = 0
  self:AddObject(GameObjectBattleMapBG)
  if GameStateColonization.directlyTo.locate == GameStateColonization.directlyTo.type.select_slave then
    self:AddObject(GameUISlaveSelect)
  else
    self:AddObject(GameUIColonial)
  end
  GameUIColonialMenu:LoadFlashObject()
end
function GameStateColonization:OnFocusLost(newState)
  DebugColonial("GameStateColonization:OnFocusLost")
  self:EraseObject(GameUIColonial)
  self:EraseObject(GameObjectBattleMapBG)
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIMessageDialog) then
    GameStateManager.GameStateGlobalState:EraseObject(GameUIMessageDialog)
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonialMenu) then
    GameUIColonialMenu:UnloadFlashObject()
    self:EraseObject(GameUIColonialMenu)
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUISlaveSelect) then
    self:EraseObject(GameUISlaveSelect)
  end
end
function GameStateColonization:GetPreState()
  return self.lastState
end
function GameStateColonization:enterSlaveSelect()
  self:EraseObject(GameUIColonial)
  self:AddObject(GameUISlaveSelect)
end
function GameStateColonization:backToColonial()
  self:EraseObject(GameUISlaveSelect)
  self:AddObject(GameUIColonial)
end
function GameStateColonization:GetIdentityStrByPosition(position)
  local identity = " "
  if position == GameStateColonization.IdentityType.freedom then
    identity = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_INDEPENDENT_TITLE")
  elseif position == GameStateColonization.IdentityType.colonist then
    identity = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_RULER_TITLE")
  elseif position == GameStateColonization.IdentityType.slave then
    identity = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_COLONY_TITLE")
  end
  return identity
end
function GameStateColonization:GetIconStrByPosition(userInfo)
  if userInfo == nil or userInfo.position == GameStateColonization.IdentityType.freedom then
    return "0"
  else
    return "" .. userInfo.icon
  end
end
function GameStateColonization:GetUserInfo(userInfo)
  local avatar, name, status, lv, force, alliance, icon = "male", "", "", "", "", "", "0"
  if userInfo ~= nil or LuaUtils:table_empty(userInfo) then
    avatar = GameUtils:GetPlayerAvatarWithSex(userInfo.sex, userInfo.main_fleet_level)
    if userInfo.icon ~= 0 and userInfo.icon ~= 1 then
      avatar = GameDataAccessHelper:GetFleetAvatar(userInfo.icon, userInfo.main_fleet_level)
    end
    name = GameUtils:GetUserDisplayName(userInfo.name)
    icon = GameStateColonization:GetIconStrByPosition(userInfo)
    lv = GameLoader:GetGameText("LC_MENU_Level") .. userInfo.level
    force = GameUtils.numberConversion(userInfo.force)
    alliance = userInfo.alliance_name
    status = GameStateColonization:GetIdentityStrByPosition(userInfo.position)
  end
  return avatar, name, status, lv, force, alliance, icon
end
function GameStateColonization:MakeMsgs(oriMsg, master, slave, third, conq, color)
  if not oriMsg then
    return ""
  end
  local msg = oriMsg
  if color then
    msg = string.gsub(msg, "<master_name>", "<font color='#FFBF00'>" .. GameUtils:GetUserDisplayName(master) .. "</font>")
    msg = string.gsub(msg, "<slave_name>", "<font color='#FFBF00'>" .. GameUtils:GetUserDisplayName(slave) .. "</font>")
    msg = string.gsub(msg, "<third_name>", "<font color='#FFBF00'>" .. GameUtils:GetUserDisplayName(third) .. "</font>")
  else
    msg = string.gsub(msg, "<master_name>", GameUtils:GetUserDisplayName(master))
    msg = string.gsub(msg, "<slave_name>", GameUtils:GetUserDisplayName(slave))
    msg = string.gsub(msg, "<third_name>", GameUtils:GetUserDisplayName(third))
  end
  msg = string.gsub(msg, "<conq_num>", conq)
  return msg
end
function GameStateColonization:GetTimesString(times_type)
  if GameStateColonization.curTimesInfo == nil then
    return "0/0"
  end
  for i, v in ipairs(GameStateColonization.curTimesInfo.times) do
    if v.type == times_type then
      if times_type == GameStateColonization.TimesType.colonist then
        return v.max_times - v.times .. "/5"
      elseif times_type == GameStateColonization.TimesType.rescue then
        return v.max_times - v.times .. "/3"
      else
        return v.max_times - v.times .. "/" .. v.max_times
      end
    end
  end
  return "0/0"
end
function GameStateColonization:GetActiveTimes(times_type)
  if GameStateColonization.curTimesInfo == nil then
    return 0
  end
  for i, v in ipairs(GameStateColonization.curTimesInfo.times) do
    if v.type == times_type then
      return v.max_times - v.times
    end
  end
  return 0
end
function GameStateColonization:GetActionTitleString(times_type)
  if times_type == GameStateColonization.TimesType.colonist then
    return ...
  elseif times_type == GameStateColonization.TimesType.action_playful then
    return ...
  elseif times_type == GameStateColonization.TimesType.action_tricky then
    return ...
  elseif times_type == GameStateColonization.TimesType.rescue then
    return ...
  elseif times_type == GameStateColonization.TimesType.help then
    return ...
  elseif times_type == GameStateColonization.TimesType.independent then
    return ...
  else
    return ""
  end
end
function GameStateColonization.ColonyExpChangeNotifyHandler(content)
  DebugColonial("ColonyExpChangeNotifyHandler")
  DebugColonialTable(content)
  GameStateColonization.curExpInfo = LuaUtils:table_rcopy(content)
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonial) then
    GameUIColonial:SetExpInfo()
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUISlaveSelect) then
    GameUISlaveSelect:SetExpInfo()
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonialMenu) then
    GameUIColonialMenu:SetExpInfo()
  end
end
function GameStateColonization.ColonyTimesChangeNotifyHandler(content)
  DebugColonial("ColonyTimesChangeNotifyHandler")
  DebugColonialTable(content)
  GameStateColonization.curTimesInfo = LuaUtils:table_rcopy(content)
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonial) then
    GameUIColonial:SetTimesInfo()
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUISlaveSelect) then
    GameUISlaveSelect:SetTimesInfo()
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonialMenu) then
    GameUIColonialMenu:SetTimesInfo()
  end
end
