TPFirebase = {}
local seperator = "|"
local insertKeyValue = function(targetTable, key, value)
  table.insert(targetTable, key)
  table.insert(targetTable, value)
end
local function appendUserProperty(paramTable)
  local userInfo = GameGlobalData:GetUserInfo()
  if userInfo ~= nil then
    insertKeyValue(paramTable, "user_name", userInfo.name)
    insertKeyValue(paramTable, "server_id", userInfo.server)
    insertKeyValue(paramTable, "user_id", userInfo.player_id)
    insertKeyValue(paramTable, "account", userInfo.passport)
  end
  local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
  if GameHelper ~= nil then
    local currentForce = GameHelper:GetFleetsForce()
    insertKeyValue(paramTable, "power", currentForce)
  end
  local resource = GameGlobalData:GetData("resource")
  if resource ~= nil then
    insertKeyValue(paramTable, "credit", resource.credit)
    insertKeyValue(paramTable, "cubit", resource.money)
  end
  insertKeyValue(paramTable, "svn", TPFirebase.GetBuildVersion())
  local ret = table.concat(paramTable, seperator)
  return ret
end
function TPFirebase.LogEvent(eventName, ...)
