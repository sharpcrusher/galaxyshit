local GameUITap4funAccount = LuaObjectManager:GetLuaObject("GameUITap4funAccount")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
function GameUITap4funAccount:OnFSCommand(cmd, arg)
  if cmd == "on_erase" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif cmd == "login_account" then
    self:Tap4funAccountLogin()
  elseif cmd == "send_isAgree" then
    self._isAgree = tonumber(arg)
  elseif cmd == "register_account" then
    self:Tap4funAccountRegister()
  elseif cmd == "tryGetPassword" then
    ext.http.openURL("https://www.tap4fun.com/?forgot")
  elseif cmd == "send_text_passport" then
    self._AccountPassport = arg
  elseif cmd == "send_text_password" then
    self._AccountPassword = arg
  elseif cmd == "send_text_password_confirm" then
    self._AccountPasswordConfirm = arg
  end
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
end
function GameUITap4funAccount:OnAddToGameState(game_state)
  self:LoadFlashObject()
  self:UpdateData()
end
function GameUITap4funAccount:OnEraseFromGameState(game_state)
  self:UnloadFlashObject()
end
function GameUITap4funAccount:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateRead")
  self:GetFlashObject():Update(dt)
end
function GameUITap4funAccount:MoveIn(nameInterface)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn", nameInterface)
end
function GameUITap4funAccount:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameUITap4funAccount:UpdateData()
  if GameSettingData.Tap4funAccount then
    self:MoveIn("login")
  else
    self:MoveIn("account")
  end
end
function GameUITap4funAccount:CheckAccountPassport(passport)
end
function GameUITap4funAccount:Tap4funAccountLogin()
  DebugOut("passport:" .. GameUITap4funAccount._AccountPassport)
  DebugOut("password:" .. GameUITap4funAccount._AccountPassword)
  local tap4fun_account = {
    passport = GameUITap4funAccount._AccountPassport,
    password = GameUITap4funAccount._AccountPassword
  }
  if self:CheckAccountFormat(tap4fun_account) then
    GameWaiting:ShowLoadingScreen()
    GameUtils:Tap4funAccountValidate({
      passport = GameUITap4funAccount._AccountPassport,
      password = GameUITap4funAccount._AccountPassword
    }, self.LoginSuccessCallback)
  end
end
function GameUITap4funAccount:Tap4funAccountRegister()
  if self._isAgree == 0 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PROTOCOL_CONFIRM_ALERT_CHAR"))
    return
  end
  if not GameUITap4funAccount._AccountPassport or GameUITap4funAccount._AccountPassport == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CAN_NOT_EMPTY_CHAR"))
    return
  end
  if not GameUITap4funAccount._AccountPassword or GameUITap4funAccount._AccountPassword == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CAN_NOT_EMPTY_CHAR"))
    return
  end
  if not GameUITap4funAccount._AccountPasswordConfirm or GameUITap4funAccount._AccountPasswordConfirm == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CONFIRM_EMPTY_CHAR"))
    return
  end
  if GameUITap4funAccount._AccountPasswordConfirm ~= GameUITap4funAccount._AccountPassword then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CONFIRM_ERRO_CHAR"))
    return
  end
  local tap4fun_account = {
    passport = GameUITap4funAccount._AccountPassport,
    password = GameUITap4funAccount._AccountPassword
  }
  if self:CheckAccountFormat(tap4fun_account) then
    GameWaiting:ShowLoadingScreen()
    GameUtils:Tap4funAccountRegister(tap4fun_account, self.RegisterSuccessCallback)
  end
end
function GameUITap4funAccount:CheckAccountFormat(tap4fun_account)
  local ret = true
  local start_pos, end_pos, length
  start_pos, end_pos = string.find(tap4fun_account.passport, "%w+@%w+%.%w%w+")
  length = string.len(tap4fun_account.passport)
  DebugOut("passport=" .. tap4fun_account.passport .. ", start_pos=" .. (start_pos == nil and "nil" or start_pos) .. ", end_pos=" .. (end_pos == nil and "nil" or end_pos) .. ", length=" .. length)
  if start_pos == nil or start_pos ~= 1 or end_pos ~= length then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_WRONG_MAIL_ADD_CHAR"))
    ret = false
  end
  length = string.len(tap4fun_account.password)
  if length < 6 or length > 12 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_LENGTH_ERRO_CHAR"))
    ret = false
  end
  return ret
end
function GameUITap4funAccount.BindPlayerFailedCallback(content)
end
function GameUITap4funAccount.AccountInfoCallback(content)
  if GameUITap4funAccount._AccountPassport and GameUITap4funAccount._AccountPassword then
    GameSettingData.Tap4funAccount = {
      passport = GameUITap4funAccount._AccountPassport,
      password = GameUITap4funAccount._AccountPassword
    }
    GameUtils:SaveSettingData()
    GameUITap4funAccount._AccountPassport = nil
    GameUITap4funAccount._AccountPassword = nil
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameSetting) then
      GameSetting:UpdateUserIDSettingData()
    end
  end
  GameUtils:Tap4funAccountBind(GameSettingData.Tap4funAccount, GameUITap4funAccount.BindSuccessCallback)
end
function GameUITap4funAccount.BindSuccessCallback(content)
  DebugOutPutTable(content, "bindCallbackContent")
  GameWaiting:HideLoadingScreen()
  local function onInfoOver()
    GameUITap4funAccount:MoveOut()
    GameUtils:RestartGame(100)
  end
  GameTip:Show(GameLoader:GetGameText("LC_MENU_BINDING_ACCOUNT_SUCCESS_CHAR"), 3000, onInfoOver)
end
function GameUITap4funAccount.RegisterSuccessCallback(content)
  DebugOutPutTable(content, "RegisterCallbackContent")
  GameWaiting:HideLoadingScreen()
  if content.is_valid then
    local function onInfoOver()
      GameWaiting:ShowLoadingScreen()
      GameUtils:UpdateServerList(GameUITap4funAccount.AccountInfoCallback, {
        passport = GameUITap4funAccount._AccountPassport,
        password = GameUITap4funAccount._AccountPassword
      })
    end
    GameTip:Show(GameLoader:GetGameText("LC_MENU_REG_SUCCESS_CHAR"), 3000, onInfoOver)
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_MAIL_REGISTERED_CHAR"))
  end
end
function GameUITap4funAccount.LoginSuccessCallback(content)
  DebugOutPutTable(content, "ValidCallbackContent")
  GameWaiting:HideLoadingScreen()
  if content.is_valid then
    local function onInfoOver()
      GameWaiting:ShowLoadingScreen()
      GameUtils:UpdateServerList(GameUITap4funAccount.AccountInfoCallback, {
        passport = GameUITap4funAccount._AccountPassport,
        password = GameUITap4funAccount._AccountPassword
      })
    end
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CHEAK_RIGHT_CHAR"), 3000, onInfoOver)
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_WRONG_USER_NAME_CHAR"))
  end
end
function GameUITap4funAccount:RegisterCommandCallback(command, callback)
  if not self._CommandList then
    self._CommandList = {}
  end
  local newCommandCallback = {}
  newCommandCallback._command = command
  newCommandCallback._callback = callback
  table.insert(self._CommandList, newCommandCallback)
end
function GameUITap4funAccount:ExecuteCommandCallback(command, arg)
  if not self._CommandList then
    return false
  end
  local result = false
  local numCommandCallback = #self._CommandList
  for index = 1, numCommandCallback do
    local CommandCallback = self._CommandList[1]
    table.remove(self._CommandList, 1)
    if CommandCallback._command == command then
      CommandCallback._callback(arg)
      result = true
    else
      table.insert(self._CommandList, CommandCallback)
    end
  end
  return result
end
