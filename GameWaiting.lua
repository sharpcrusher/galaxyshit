local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
GameWaiting.refrence = 0
function GameWaiting:OnAddToGameState()
  self:LoadFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "playStart")
end
function GameWaiting:OnEraseFromeGameState()
  self:UnloadFlashObject()
end
function GameWaiting:ShowLoadingScreen(clearRefrence)
  if clearRefrence then
    self.refrence = 0
  end
  if self.refrence == 0 then
    GameStateGlobalState:AddObject(self)
  end
  self.refrence = self.refrence + 1
end
function GameWaiting:HideLoadingScreen()
  self.refrence = self.refrence - 1
  if self.refrence < 0 then
    self.refrence = 0
  end
  if self.refrence == 0 then
    GameStateGlobalState:EraseObject(self)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameWaiting.OnAndroidBack()
  end
end
