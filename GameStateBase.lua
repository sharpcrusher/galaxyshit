local LuaObjectManager = LuaObjectManager
GameStateBase = {}
GameStateBase.CAMMANDTYPE = {ADD = 1, ERASE = 2}
function GameStateBase:GetObjectListFromType(objectType)
  if objectType == LuaObjectManager.ObjectType.BACKGROUNDOBJ then
    return self.m_bgObjectList
  elseif objectType == LuaObjectManager.ObjectType.MENU or objectType == LuaObjectManager.ObjectType.MODELMENU then
    return self.m_menuObjectList
  else
    GameUtils:Error("invalid type")
    return nil
  end
end
function GameStateBase:InitMember()
  self.m_bgObjectList = {}
  self.m_bgObjectList._locked = false
  self.m_menuObjectList = {}
  self.m_menuObjectList._locked = false
  self.m_cammandList = {}
  self.m_previousState = nil
  self.stateName = ""
end
function GameStateBase:LockObjectList(object_list)
  DebugOut("lock object list - ", object_list)
  assert(not object_list._locked)
  object_list._locked = true
end
function GameStateBase:UnlockObjectList(object_list)
  DebugOut("unlock object list - ", object_list)
  assert(object_list._locked)
  object_list._locked = false
end
function GameStateBase:PrintAllObject()
  DebugState(" ---------- DebugOut allObjects in current gamestate -----------")
  for i, v in ipairs(self.m_menuObjectList) do
    DebugState("MENU: " .. v.m_objectName)
  end
  for i, v in ipairs(self.m_bgObjectList) do
    DebugState("BG: " .. v.m_objectName)
  end
  DebugState(" ---------- end -----------")
end
function GameStateBase:AddObjectInternal(obj)
  local objType = obj:GetObjectType()
  local objectList = self:GetObjectListFromType(objType)
  if objectList ~= nil then
    GameStateBase:LockObjectList(objectList)
    for i, v in ipairs(objectList) do
      if v == obj then
        table.remove(objectList, i)
        break
      end
    end
    table.insert(objectList, obj)
    if AutoUpdate.isAndroidDevice and obj.OnAndroidBack then
      GameAndroidBackManager:AddCallback(obj.OnAndroidBack)
    end
    self:PrintAllObject()
    if obj.OnAddToGameState ~= nil then
      obj:OnAddToGameState(self)
    end
    AutoUpdateInBackground:SaveBGFileList()
    AutoUpdateInBackground:SaveHRFileList()
    GameStateBase:UnlockObjectList(objectList)
  end
end
function GameStateBase:AddObject(obj)
  if DebugConfig.isAssertAddObject then
    assert(not self:IsObjectInState(obj))
  end
  local cammand = {}
  cammand.Type = self.CAMMANDTYPE.ADD
  cammand.Object = obj
  table.insert(self.m_cammandList, cammand)
end
function GameStateBase:IsObjectInState(obj)
  if not obj then
    return false
  end
  local objectList = self:GetObjectListFromType(obj:GetObjectType())
  if objectList ~= nil then
    for i, v in ipairs(objectList) do
      if v == obj then
        return true
      end
    end
  end
  return false
end
function GameStateBase:EraseObjectInternal(obj)
  DebugOut("GameStateBase:EraseObjectInternal", obj:GetFlashObjectClassName())
  local objectList = self:GetObjectListFromType(obj:GetObjectType())
  if objectList ~= nil then
    GameStateBase:LockObjectList(objectList)
    local find_object = false
    for i, v in ipairs(objectList) do
      if v == obj then
        find_object = true
        table.remove(objectList, i)
        if AutoUpdate.isAndroidDevice and obj.OnAndroidBack then
          GameAndroidBackManager:RemoveCallback(obj.OnAndroidBack)
        end
        if FREE_LAZY_LOAD_IMAGE_WHEN_OBJECT_ERASE_FROM_STATE then
          DebugOut("free object " .. obj.m_objectName)
          if obj:GetFlashObject() then
            DebugOut("RejectLazyLoadTexture " .. obj.m_objectName)
            obj:GetFlashObject():RejectLazyLoadTexture()
          end
        end
        if obj.OnEraseFromGameState ~= nil then
          obj:OnEraseFromGameState(self)
        end
        GameStateBase.needCollect = true
        self:PrintAllObject()
        break
      end
    end
    if not find_object then
      DebugOut("EraseObject:object is not in list, objectName is ", obj.m_objectName)
    end
    GameStateBase:UnlockObjectList(objectList)
  end
end
function GameStateBase:EraseObject(obj)
  local cammand = {}
  cammand.Type = self.CAMMANDTYPE.ERASE
  cammand.Object = obj
  table.insert(self.m_cammandList, cammand)
end
function GameStateBase:Init()
  GameUtils:Warning("child class shoudl overide GameStateBase:Init() to do initialization")
end
function GameStateBase:DoUpdate(dt, objectList)
  if objectList == nil then
    return
  end
  for i, v in ipairs(objectList) do
    if v.Update ~= nil then
      v:Update(dt)
    elseif v:GetFlashObject() ~= nil then
      v:GetFlashObject():Update(dt)
    end
  end
  if GameStateBase.needCollect then
    collectgarbage("collect")
    GameStateBase.needCollect = false
  end
end
function GameStateBase:AfterStateGain()
  for i, v in ipairs(self.m_menuObjectList) do
    if v.OnParentStateGetFocus then
      v:OnParentStateGetFocus()
    end
  end
  for i, v in ipairs(self.m_bgObjectList) do
    if v.OnParentStateGetFocus then
      v:OnParentStateGetFocus()
    end
  end
end
function GameStateBase:ForceCompleteCammandList()
  if #self.m_cammandList == 0 then
    return
  end
  for i, v in ipairs(self.m_cammandList) do
    if v.Type == self.CAMMANDTYPE.ADD then
      self:AddObjectInternal(v.Object)
    elseif v.Type == self.CAMMANDTYPE.ERASE then
      self:EraseObjectInternal(v.Object)
    else
      GameUtils:Error("Update Cammand: invalid type")
    end
  end
  self.m_cammandList = {}
end
function GameStateBase:Update(dt)
  if #self.m_cammandList ~= 0 then
    local cammand = self.m_cammandList[1]
    if cammand.Type == self.CAMMANDTYPE.ADD then
      self:AddObjectInternal(cammand.Object)
      table.remove(self.m_cammandList, 1)
    elseif cammand.Type == self.CAMMANDTYPE.ERASE then
      self:EraseObjectInternal(cammand.Object)
      table.remove(self.m_cammandList, 1)
    else
      GameUtils:Error("Update Cammand: invalid type")
    end
  end
  self:DoUpdate(dt, self.m_menuObjectList)
  self:DoUpdate(dt, self.m_bgObjectList)
end
function GameStateBase:DoRender(objectList)
  if objectList == nil then
    return
  end
  local size = #objectList
  for i = 1, size do
    local obj = objectList[i]
    if obj.Render ~= nil then
      obj:Render()
    elseif obj:GetFlashObject() ~= nil then
      obj:GetFlashObject():Render()
    end
  end
end
function GameStateBase:Render()
  self:DoRender(self.m_bgObjectList)
  self:DoRender(self.m_menuObjectList)
end
function GameStateBase:HandleTouchEvent(x, y, objectList, eventType)
  if objectList == nil then
    return false
  end
  local isModel = false
  local size = #objectList
  for i = size, 1, -1 do
    local v = objectList[i]
    isModel = v:GetObjectType() == LuaObjectManager.ObjectType.MODELMENU
    local handleFunc, handleObj
    if eventType == 0 then
      if v.OnTouchPressed ~= nil then
        handleFunc = v.OnTouchPressed
        handleObj = v
      elseif v:GetFlashObject() then
        handleFunc = v:GetFlashObject().OnTouchPressed
        handleObj = v:GetFlashObject()
      end
    elseif eventType == 1 then
      if v.OnTouchMoved ~= nil then
        handleFunc = v.OnTouchMoved
        handleObj = v
      elseif v:GetFlashObject() then
        handleFunc = v:GetFlashObject().OnTouchMoved
        handleObj = v:GetFlashObject()
      end
    elseif eventType == 2 then
      if v.OnTouchReleased ~= nil then
        handleFunc = v.OnTouchReleased
        handleObj = v
      elseif v:GetFlashObject() then
        handleFunc = v:GetFlashObject().OnTouchReleased
        handleObj = v:GetFlashObject()
      end
    else
      error("invalid type")
    end
    if handleFunc and handleFunc(handleObj, x, y) or isModel then
      return true
    end
  end
  return isModel
end
function GameStateBase:HandleMultiTouchEvent(x, y, objectList, eventType)
  if objectList == nil then
    return false
  end
  local isModel = false
  local size = #objectList
  for i = size, 1, -1 do
    local v = objectList[i]
    isModel = v:GetObjectType() == LuaObjectManager.ObjectType.MODELMENU
    if isModel then
      return true
    end
  end
  return isModel
end
function GameStateBase:OnTouchPressed(x, y)
  if self:HandleTouchEvent(x, y, self.m_menuObjectList, 0) then
    return true
  end
  if self:HandleTouchEvent(x, y, self.m_bgObjectList, 0) then
    return true
  end
  return false
end
function GameStateBase:OnTouchMoved(x, y)
  if self:HandleTouchEvent(x, y, self.m_menuObjectList, 1) then
    return true
  end
  if self:HandleTouchEvent(x, y, self.m_bgObjectList, 1) then
    return true
  end
  return false
end
function GameStateBase:OnTouchReleased(x, y)
  if self:HandleTouchEvent(x, y, self.m_menuObjectList, 2) then
    return true
  end
  if self:HandleTouchEvent(x, y, self.m_bgObjectList, 2) then
    return true
  end
  return false
end
function GameStateBase:OnMultiTouchBegin(x1, y1, x2, y2)
  if self:HandleMultiTouchEvent(x, y, self.m_menuObjectList, 0) then
    return true
  end
  if self:HandleMultiTouchEvent(x, y, self.m_bgObjectList, 0) then
    return true
  end
  return false
end
function GameStateBase:OnMultiTouchMove(x1, y1, x2, y2)
  if self:HandleMultiTouchEvent(x, y, self.m_menuObjectList, 1) then
    return true
  end
  if self:HandleMultiTouchEvent(x, y, self.m_bgObjectList, 1) then
    return true
  end
  return false
end
function GameStateBase:OnMultiTouchEnd(x1, y1, x2, y2)
  if self:HandleMultiTouchEvent(x, y, self.m_menuObjectList, 2) then
    return true
  end
  if self:HandleMultiTouchEvent(x, y, self.m_bgObjectList, 2) then
    return true
  end
  return false
end
function GameStateBase:OnFocusGain(previousState)
  GameUtils:Warning("GameStateBase:OnFocusGain is called, child class should override this function")
end
function GameStateBase:OnFocusLost(nextState)
  GameUtils:Warning("GameStateBase:OnFocusLost is called, child class should override this function")
end
