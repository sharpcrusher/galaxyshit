local GameStateToprank = GameStateManager.GameStateToprank
local GameObjectToprank = LuaObjectManager:GetLuaObject("GameObjectToprank")
function GameStateToprank:OnFocusGain(previousState)
  self:AddObject(GameObjectToprank)
  function self.onQuit()
    GameStateManager:SetCurrentGameState(previousState)
  end
end
function GameStateToprank:OnFocusLost()
  self:EraseObject(GameObjectToprank)
end
function GameStateToprank:Quit()
  self.onQuit()
end
