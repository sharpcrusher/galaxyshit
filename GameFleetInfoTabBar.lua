local GameFleetInfoTabBar = LuaObjectManager:GetLuaObject("GameFleetInfoTabBar")
local GameStateFleetInfo = GameStateManager.GameStateFleetInfo
local GameMail = LuaObjectManager:GetLuaObject("GameMail")
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local GameUIPrestige = LuaObjectManager:GetLuaObject("GameUIPrestige")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local QuestTutorialEnhance_second = TutorialQuestManager.QuestTutorialEnhance_second
function GameFleetInfoTabBar:Init(initIndex)
  initIndex = initIndex or 1
  if initIndex == -1 then
    initIndex = 1
  end
  GameFleetInfoTabBar:OnParentStateGetFocus()
end
function GameFleetInfoTabBar:IsPrestigeEnabled()
  if immanentversion == 1 then
    local resource = GameGlobalData:GetData("resource")
    if resource.prestige > 0 then
      return true
    end
  elseif immanentversion == 2 and (QuestTutorialPrestige:IsActive() or QuestTutorialPrestige:IsFinished()) then
    return true
  end
  return false
end
function GameFleetInfoTabBar:hideSecondTab()
  self:GetFlashObject():InvokeASCallback("_root", "setSecondTabVisible", false)
end
function GameFleetInfoTabBar:CheckShowBtnGP(isShow)
  DebugOut("CheckShowBtnGP:", IPlatformExt.isAchievement_support())
  if IPlatformExt.isAchievement_support() and isShow then
    self:GetFlashObject():InvokeASCallback("_root", "SetBtnGPVisible", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetBtnGPVisible", false)
  end
end
function GameFleetInfoTabBar:OnParentStateGetFocus()
  if not GameFleetInfoTabBar:GetFlashObject() then
    GameFleetInfoTabBar:LoadFlashObject()
  end
  if QuestTutorialEquip:IsActive() then
    if not GameItemBag.itemInBag[1] and not GameItemBag.itemInBag[2] then
      DebugOut("QuestTutorialEquip to ShowTutorialAnimTip")
      GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
    else
      GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
    end
    local state = "true" .. "\001"
    for i = 2, 3 do
      state = state .. "false" .. "\001"
    end
    self:GetFlashObject():InvokeASCallback("_root", "setTabEnable", state)
  else
    GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "HideTutorialAnimTip")
  end
  if immanentversion == 2 then
    local prestige_state = false
    if QuestTutorialPrestige:IsActive() or QuestTutorialPrestige:IsFinished() then
      prestige_state = true
    end
    self:GetFlashObject():InvokeASCallback("_root", "enableTabItem", 4, prestige_state)
    self:GetFlashObject():InvokeASCallback("_root", "enableTabItem", 3, prestige_state)
  end
  local pve_progress = GameGlobalData:GetData("progress")
  self:GetFlashObject():InvokeASCallback("_root", "HideTutorialPrestige")
  DebugOut("_LOG\226\128\148\226\128\148\226\128\148\226\128\148>")
  DebugTable(pve_progress)
  if QuestTutorialPrestige:IsActive() then
    DebugOut("\232\191\155\230\157\165\228\186\134")
    self:GetFlashObject():InvokeASCallback("_root", "enableTabItem", 4, true)
    if immanentversion == 2 then
      GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "showTutorialPrestige_1")
    elseif immanentversion == 1 then
      GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
    end
  elseif immanentversion == 2 then
    GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "HideTutorialPrestige")
    if 1 < pve_progress.act or 1 < pve_progress.chapter then
      self:GetFlashObject():InvokeASCallback("_root", "enableTabItem", 4, true)
    end
  elseif immanentversion == 1 then
    GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "showTutorialPrestige", false)
    if QuestTutorialPrestige:IsFinished() or 1 < pve_progress.chapter or 1 < pve_progress.adv_chapter or pve_progress.finish_count > 5 then
      self:GetFlashObject():InvokeASCallback("_root", "enableTabItem", 4, true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "enableTabItem", 4, false)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "ShowTabBar")
end
function GameFleetInfoTabBar:OnAddToGameState()
  GameFleetInfoTabBar.needPromptFacebook = false
  local resource = GameGlobalData:GetData("resource")
  if resource.prestige > 0 and immanentversion == 1 then
    self:EnableTabItem(4, true)
  end
end
function GameFleetInfoTabBar:EnableTabItem(index_item, enable)
  self:GetFlashObject():InvokeASCallback("_root", "enableTabItem", index_item, enable)
end
function GameFleetInfoTabBar:OnFSCommand(cmd, arg)
  DebugOut("GameFleetInfoTabBar:OnFSCommand ", cmd)
  if cmd == "Return_Btn_Clicked" then
    if QuestTutorialPrestige:IsActive() and immanentversion == 2 then
      GameUIPrestige:SetPrestigeTutorialFinish()
    else
      if GameFleetInfoTabBar.closeAnimTip == "equip" then
        AddFlurryEvent("CloseFirstEquipUI", {}, 1)
        AddFlurryEvent("TutorialCloseFleets", {}, 2)
        GameFleetInfoTabBar.closeAnimTip = nil
      elseif GameFleetInfoTabBar.closeAnimTip == "prestige" then
        AddFlurryEvent("ClosePrestigeUI", {}, 1)
        GameFleetInfoTabBar.closeAnimTip = nil
      end
      DebugOut("--------------> FlashObject(): mainMoveOut")
      self:GetFlashObject():InvokeASCallback("_root", "mainMoveOut")
    end
  end
  if cmd == "animation_over_moveout" then
    DebugOut("animation_over_moveout ----> fuck")
    if GameFleetInfoTabBar.finishEquipTutorialSecond then
      AddFlurryEvent("TutorialItem_CloseFleet", {}, 2)
    end
    GameFleetInfoTabBar.finishEquipTutorialSecond = nil
    if QuestTutorialEnhance_second:IsActive() and immanentversion == 2 then
      GameStateFleetInfo.previousState = nil
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    else
      if GameStateFleetInfo.previousState == GameStateManager.GameStateFormation then
        GameStateManager.GameStateFormation:PreparePrestigeBack()
      elseif GameStateFleetInfo.previousState == GameStateManager.GameStatePlayerMatrix then
        GameStateManager.GameStatePlayerMatrix:PreparePrestigeBack()
      else
        local testThis = false
        if QuestTutorialPrestige:IsActive() and not QuestTutorialPrestige:IsFinished() or testThis then
          QuestTutorialPrestige:SetFinish(true)
          if Facebook:IsFacebookEnabled() then
            GameFleetInfoTabBar.needPromptFacebookStory = true
          else
            GameFleetInfoTabBar.needPromptFacebookStory = false
          end
          DebugOut("should show")
        end
      end
      GameStateManager:SetCurrentGameState(GameStateFleetInfo.previousState)
      GameStateFleetInfo.previousState = nil
      local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
      if GameUIPrestigeRankUp.mIsGotoDetail then
        GameUIPrestigeRankUp:BackFromDetail()
      end
    end
    self.lastTab = nil
  end
  if cmd == "selectTab" then
    local index = tonumber(arg)
    if GameStateFleetInfo.currentTab ~= index then
      GameStateFleetInfo:SetTab(index)
      self:SelectTab(index)
    end
    DebugOut("-----fuck --- the  world ", immanentversion)
    if immanentversion == 2 then
      if index == 4 and not QuestTutorialPrestige:IsFinished() and QuestTutorialPrestige:IsActive() then
        self:GetFlashObject():InvokeASCallback("_root", "HideTutorialPrestige")
      end
    elseif immanentversion == 1 and index == 4 and QuestTutorialPrestige:IsActive() then
      AddFlurryEvent("ClickPrestigeTab", {}, 1)
      self:GetFlashObject():InvokeASCallback("_root", "showTutorialPrestige", false)
      GameFleetInfoTabBar.closeAnimTip = "prestige"
      DebugOut("im == 1 ShowTutorialAnimTip")
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAnimTip")
    end
  elseif cmd == "btn_help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_PRESTIGE_HELP"))
  end
  if cmd == "btn_GP" then
    DebugOut("click btn_GP")
    if IPlatformExt.isAchievement_support() then
      IPlatformExt.show_Achievement()
    end
  end
end
function GameFleetInfoTabBar:SelectTab(index)
  self:GetFlashObject():InvokeASCallback("_root", "selectTab", index)
end
if AutoUpdate.isAndroidDevice then
  function GameFleetInfoTabBar.OnAndroidBack()
    if GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_help then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "hideHelp")
      end
    else
      GameFleetInfoTabBar:GetFlashObject():InvokeASCallback("_root", "HideTabBar")
    end
  end
end
