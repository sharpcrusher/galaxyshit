local GameStateFpsFte = GameStateManager.GameStateFpsFte
local GameUIFpsFte = LuaObjectManager:GetLuaObject("GameUIFpsFte")
local GameObjectShakeScreen = LuaObjectManager:GetLuaObject("GameObjectShakeScreen")
function GameStateFpsFte:InitGameState()
end
function GameStateFpsFte:OnFocusGain(previousState)
  self.m_preState = previousState
  DebugOut("heeeee")
  self:AddObject(GameUIFpsFte)
  self:AddObject(GameObjectShakeScreen)
  GameObjectShakeScreen:LoadFlashObject()
  GameStateFpsFte.bgTouchid = nil
  GameStateFpsFte.commonShootTouchid = nil
  GameStateFpsFte.testresume = nil
end
function GameStateFpsFte:OnFocusLost(newState)
  self.m_preState = nil
  self:EraseObject(GameUIFpsFte)
  local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
  self:EraseObject(GameUIBattleResult)
  self:EraseObject(GameObjectShakeScreen)
end
function GameStateFpsFte:GetPreState()
  return self.m_preState
end
function GameStateFpsFte:OnCommonTouchEvent(x, y, touchid, ntype)
  if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_init or GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_skill then
    return
  end
  if not GameUIFpsFte:GetFlashObject() then
    return
  end
  if ntype == 0 then
    local hitobj = GameUIFpsFte:HitTest(x, y)
    if hitobj == "commonShoot" then
      if GameUIFpsFte.tutoStep == GameUIFpsFte.tutoStep_move then
        return
      end
      if not GameUIFpsFte.canshoot then
        return
      end
      GameStateFpsFte.commonShootTouchid = touchid
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      if GameTip._ActiveTip then
        GameTip:OnFSCommand("clicked")
      end
      if not GameUIFpsFte.isShooting then
        GameUIFpsFte:OnFSCommand("CommonFire", "start")
      end
    elseif hitobj == "bg" then
      GameStateFpsFte.bgTouchid = touchid
      x, y = GameUtils:ConvertScreenToFlash(x, y, GameUIFpsFte.ScreenParam[1], GameUIFpsFte.ScreenParam[2])
      GameStateFpsFte.m_lastx = x
      GameStateFpsFte.m_lasty = y
    end
  elseif ntype == 1 then
    if GameStateFpsFte.bgTouchid == touchid then
      GameStateFpsFte:DoMove(x, y)
    elseif GameStateFpsFte.commonShootTouchid == touchid then
      local hitobj = GameUIFpsFte:HitTest(x, y)
      if hitobj ~= "commonShoot" then
        GameUIFpsFte:OnFSCommand("CommonFire", "end")
        GameStateFpsFte.commonShootTouchid = nil
      end
    end
  elseif ntype == 2 then
    if GameStateFpsFte.bgTouchid == touchid then
      GameUIFpsFte:StopMoveBg()
      GameStateFpsFte.bgTouchid = nil
    end
    if GameStateFpsFte.commonShootTouchid == touchid then
      GameUIFpsFte:OnFSCommand("CommonFire", "end")
      GameStateFpsFte.commonShootTouchid = nil
    end
  end
end
function GameStateFpsFte:DoMove(x, y)
  if not GameUIFpsFte:GetFlashObject() then
    return
  end
  x, y = GameUtils:ConvertScreenToFlash(x, y, GameUIFpsFte.ScreenParam[1], GameUIFpsFte.ScreenParam[2])
  local xdiff = x - GameStateFpsFte.m_lastx
  local ydiff = y - GameStateFpsFte.m_lasty
  GameStateFpsFte.m_lastx = x
  GameStateFpsFte.m_lasty = y
  GameUIFpsFte:StartMoveBg(xdiff, ydiff)
end
