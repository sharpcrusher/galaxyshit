local Keys = GameData.Spell.Keys
Keys[1] = {
  spell_id = 1,
  spell_name = "\228\184\187\232\167\146\229\136\157\229\167\139\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "photonMissile",
  DAMAGE = 100,
  order = 1,
  icon = "2.0"
}
Keys[2] = {
  spell_id = 2,
  spell_name = "\228\184\187\232\167\146\231\172\172\228\186\140\230\138\128\232\131\189(\232\140\131\229\155\1801\239\188\1404\239\188\1407)",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 79,
  order = 2,
  icon = "2.0"
}
Keys[3] = {
  spell_id = 3,
  spell_name = "\228\184\187\232\167\146\231\172\1723\230\138\128\232\131\189(\232\140\131\229\155\1801\239\188\1402\239\188\1403)",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 66,
  order = 3,
  icon = "2.0"
}
Keys[4] = {
  spell_id = 4,
  spell_name = "\233\135\141\229\138\155\233\153\183\233\152\177",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 4,
  icon = "2.0"
}
Keys[5] = {
  spell_id = 5,
  spell_name = "\233\135\141\229\138\155\231\130\174",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 120,
  order = 5,
  icon = "2.0"
}
Keys[6] = {
  spell_id = 6,
  spell_name = "\233\171\152\232\131\189\231\178\146\229\173\144\231\130\174",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 66,
  order = 6,
  icon = "2.0"
}
Keys[7] = {
  spell_id = 7,
  spell_name = "\230\136\152\229\156\186\233\129\174\230\150\173",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "photonMissile",
  DAMAGE = 66,
  order = 7,
  icon = "2.0"
}
Keys[8] = {
  spell_id = 8,
  spell_name = "\228\188\189\233\169\172\231\129\188\231\131\167",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterB",
  DAMAGE = 99,
  order = 8,
  icon = "2.0"
}
Keys[9] = {
  spell_id = 9,
  spell_name = "\229\155\160\233\153\128\231\189\151\231\159\169\233\152\181",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indraBarrageB",
  DAMAGE = 58,
  order = 9,
  icon = "2.0"
}
Keys[10] = {
  spell_id = 10,
  spell_name = "\229\133\137\229\173\144\229\175\188\229\188\185",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "photonMissile",
  DAMAGE = 99,
  order = 10,
  icon = "2.0"
}
Keys[11] = {
  spell_id = 11,
  spell_name = "\230\173\163\231\148\181\229\173\144\231\130\174",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 66,
  order = 11,
  icon = "2.0"
}
Keys[12] = {
  spell_id = 12,
  spell_name = "\231\129\171\229\138\155\229\142\139\229\136\182",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "photonMissile",
  DAMAGE = 100,
  order = 12,
  icon = "2.0"
}
Keys[13] = {
  spell_id = 13,
  spell_name = "\231\155\184\232\189\172\231\167\187\232\163\133\231\148\178",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 0,
  order = 13,
  icon = "2.0"
}
Keys[14] = {
  spell_id = 14,
  spell_name = "\231\169\186\233\151\180\233\153\183\233\152\177",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 15,
  icon = "2.0"
}
Keys[15] = {
  spell_id = 15,
  spell_name = "\230\176\162\229\188\185",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "photonMissile",
  DAMAGE = 100,
  order = 15,
  icon = "2.0"
}
Keys[16] = {
  spell_id = 16,
  spell_name = "\231\173\137\231\166\187\229\173\144\229\134\178\229\135\187",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[17] = {
  spell_id = 17,
  spell_name = "\233\135\143\229\173\144\233\148\129\229\174\154",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gamma",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[18] = {
  spell_id = 18,
  spell_name = "\230\173\163\231\148\181\229\173\144\231\130\174",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "photonMissile",
  DAMAGE = 66,
  order = 100,
  icon = "2.0"
}
Keys[19] = {
  spell_id = 19,
  spell_name = "\232\163\133\231\148\178\231\134\148\232\154\128",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "photonMissile",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[20] = {
  spell_id = 20,
  spell_name = "\231\155\184\232\189\172\231\167\187\231\159\169\233\152\181",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[21] = {
  spell_id = 21,
  spell_name = "\232\180\168\233\135\143\231\159\169\233\152\181",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[22] = {
  spell_id = 22,
  spell_name = "\233\135\141\229\138\155\233\153\183\233\152\177",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[23] = {
  spell_id = 23,
  spell_name = "\228\188\189\233\169\172\229\176\132\231\186\191\231\130\174",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[24] = {
  spell_id = 24,
  spell_name = "\231\173\137\231\166\187\229\173\144\229\134\178\229\135\187",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[25] = {
  spell_id = 25,
  spell_name = "\230\173\163\231\148\181\229\173\144\231\130\174",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "photonMissile",
  DAMAGE = 66,
  order = 100,
  icon = "2.0"
}
Keys[26] = {
  spell_id = 26,
  spell_name = "\231\129\171\229\138\155\229\142\139\229\136\182",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[27] = {
  spell_id = 27,
  spell_name = "\232\163\133\231\148\178\231\136\134\231\160\180",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[28] = {
  spell_id = 28,
  spell_name = "\231\155\184\232\189\172\231\167\187\231\159\169\233\152\181",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[29] = {
  spell_id = 29,
  spell_name = "\231\129\171\229\138\155\229\142\139\229\136\182-II",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 150,
  order = 100,
  icon = "2.0"
}
Keys[30] = {
  spell_id = 30,
  spell_name = "\233\135\143\229\173\144\233\148\129\229\174\154-II",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 150,
  order = 100,
  icon = "2.0"
}
Keys[31] = {
  spell_id = 31,
  spell_name = "\233\135\143\229\173\144\233\148\129\229\174\154",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gamma",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[32] = {
  spell_id = 32,
  spell_name = "\232\163\133\231\148\178\231\136\134\231\160\180-II",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 66,
  order = 100,
  icon = "2.0"
}
Keys[33] = {
  spell_id = 33,
  spell_name = "\229\156\163\232\175\158\230\184\133\230\128\146",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 20,
  order = 100,
  icon = "2.0"
}
Keys[35] = {
  spell_id = 35,
  spell_name = "\233\162\132\229\159\139\230\138\128\232\131\1891",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[36] = {
  spell_id = 36,
  spell_name = "\230\178\131\229\176\148\231\167\145\229\164\171\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 99,
  order = 100,
  icon = "4.0"
}
Keys[37] = {
  spell_id = 37,
  spell_name = "\233\162\132\229\159\139\230\138\128\232\131\1893",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 20,
  order = 100,
  icon = "2.0"
}
Keys[38] = {
  spell_id = 38,
  spell_name = "\233\152\191\230\150\175\229\138\160\229\190\183\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[39] = {
  spell_id = 39,
  spell_name = "\233\162\132\229\159\139\230\138\128\232\131\1895",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 25,
  order = 100,
  icon = "3.0"
}
Keys[40] = {
  spell_id = 40,
  spell_name = "WD2\230\156\159\232\139\177\233\155\132EN\231\178\146\229\173\144\231\130\174",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[41] = {
  spell_id = 41,
  spell_name = "\232\149\190\229\168\156",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "1.0"
}
Keys[42] = {
  spell_id = 42,
  spell_name = "134\233\162\132\229\159\139\229\190\152\229\190\138\232\128\133",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[43] = {
  spell_id = 43,
  spell_name = "\230\172\167\231\177\179\232\140\132\239\188\141\231\148\181\231\163\129\233\156\135\232\141\161\230\179\162",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 120,
  order = 100,
  icon = "2.0"
}
Keys[44] = {
  spell_id = 44,
  spell_name = "601\230\180\187\229\138\168",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 0,
  order = 0,
  icon = "2.0"
}
Keys[45] = {
  spell_id = 45,
  spell_name = "135\229\144\184\232\161\128\232\144\157\232\142\137",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[46] = {
  spell_id = 46,
  spell_name = "137\229\190\152\229\190\138\232\128\133",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[47] = {
  spell_id = 47,
  spell_name = "\233\146\155\233\147\172\231\131\136\231\132\176\231\130\174",
  range_type = 2,
  EFFECT = "indra_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[48] = {
  spell_id = 48,
  spell_name = "\230\157\168\229\168\129\229\136\169\230\138\164\229\141\171\232\128\133",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "normal_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[49] = {
  spell_id = 49,
  spell_name = "\229\165\179\231\142\139\231\154\132\232\175\133\229\146\146",
  range_type = 2,
  EFFECT = "black_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[50] = {
  spell_id = 50,
  spell_name = "\230\158\151\230\152\142\231\190\142\229\190\152\229\190\138\232\128\133",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[51] = {
  spell_id = 51,
  spell_name = "\232\147\157\232\137\178\230\157\168\229\168\129\229\136\169",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "normal_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[52] = {
  spell_id = 52,
  spell_name = "\232\147\157\232\137\178\232\137\190\231\142\155\229\165\179\231\142\139",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "black_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[53] = {
  spell_id = 53,
  spell_name = "\232\147\157\232\137\178\230\158\151\230\152\142\231\190\142",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[54] = {
  spell_id = 54,
  spell_name = "\230\150\176\231\190\142\229\165\179",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[55] = {
  spell_id = 55,
  spell_name = "\228\184\185\229\176\188\230\150\175\233\163\142\230\154\180",
  range_type = 1,
  EFFECT = "missile",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[56] = {
  spell_id = 56,
  spell_name = "\232\181\164\229\133\137\233\149\173\229\176\132\231\130\174",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[57] = {
  spell_id = 57,
  spell_name = "\233\155\182\229\188\143\233\134\137\229\133\137\229\188\185",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[58] = {
  spell_id = 58,
  spell_name = "\231\165\158\228\185\139\229\186\135\230\138\164",
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[59] = {
  spell_id = 59,
  spell_name = "\229\134\176\229\176\129\229\133\137\230\157\159",
  range_type = 1,
  EFFECT = "missile",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[60] = {
  spell_id = 60,
  spell_name = "\232\157\153\232\157\160\228\190\160",
  range_type = 1,
  EFFECT = "gravityB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[61] = {
  spell_id = 61,
  spell_name = "\233\187\145\230\154\151\228\190\181\232\162\173",
  range_type = 2,
  EFFECT = "gammaBlasterC",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[62] = {
  spell_id = 62,
  spell_name = "\230\179\149\232\128\129\231\154\132\231\165\157\231\166\143/\232\175\133\229\146\146",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[63] = {
  spell_id = 63,
  spell_name = "\229\144\141\231\136\181\230\138\164\231\155\190",
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[64] = {
  spell_id = 64,
  spell_name = "\232\135\180\229\145\189\231\142\169\231\172\145",
  range_type = 1,
  EFFECT = "normal_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[65] = {
  spell_id = 65,
  spell_name = "\229\185\189\231\129\181\232\131\189\233\135\143",
  range_type = 4,
  EFFECT = "grim_reap",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[66] = {
  spell_id = 66,
  spell_name = "\229\164\141\228\187\135\232\132\137\229\134\178",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[67] = {
  spell_id = 67,
  spell_name = "\231\190\189\231\191\188\230\137\147\229\135\187",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[68] = {
  spell_id = 68,
  spell_name = "\233\163\142\230\154\180\229\174\136\230\138\164",
  range_type = 1,
  EFFECT = "normal2",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[69] = {
  spell_id = 69,
  spell_name = "\231\187\157\229\175\185\229\134\176\229\134\187",
  range_type = 3,
  EFFECT = "frost_vertical",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[70] = {
  spell_id = 70,
  spell_name = "\233\187\145\230\154\151\229\143\152\232\186\171",
  range_type = 1,
  EFFECT = "snowball_single",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[71] = {
  spell_id = 71,
  spell_name = "\233\187\145\230\154\151\229\156\163\232\175\158\232\128\129\228\186\186",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[72] = {
  spell_id = 72,
  spell_name = "\229\186\159\229\188\131\230\138\128\232\131\189\239\188\136\230\173\164ID\232\183\179\232\191\135\239\188\137",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[73] = {
  spell_id = 73,
  spell_name = "\232\135\180\229\145\189\230\137\147\229\135\187",
  range_type = 1,
  EFFECT = "EMP_new01_single",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[74] = {
  spell_id = 74,
  spell_name = "\233\187\145\230\154\151\229\133\137\231\142\175",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[75] = {
  spell_id = 75,
  spell_name = "\233\187\145\230\154\151\230\179\189\230\139\137\229\155\190\231\154\132\230\138\128\232\131\189",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[76] = {
  spell_id = 76,
  spell_name = "\233\187\145\230\154\151\230\157\168\229\168\129\229\136\169\231\154\132\230\138\128\232\131\189",
  range_type = 1,
  EFFECT = "normal_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[77] = {
  spell_id = 77,
  spell_name = "\230\154\151\229\189\177\229\133\137\230\157\159",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[78] = {
  spell_id = 78,
  spell_name = "\233\187\145\230\154\151\233\163\142\230\154\180\228\184\185\229\176\188\230\150\175",
  range_type = 1,
  EFFECT = "missile",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[79] = {
  spell_id = 79,
  spell_name = "160\239\188\136\231\154\132\230\138\128\232\131\189\239\188\137",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[80] = {
  spell_id = 80,
  spell_name = "\233\187\145\230\154\151\233\190\153\233\170\145\229\163\171\228\188\138\232\146\153",
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[81] = {
  spell_id = 81,
  spell_name = "162\239\188\136\231\154\132\230\138\128\232\131\189\239\188\137",
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[82] = {
  spell_id = 82,
  spell_name = "\230\158\129\229\133\137\229\176\132\231\186\191",
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[83] = {
  spell_id = 83,
  spell_name = "\232\135\180\229\145\189\231\130\184\229\188\185",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[84] = {
  spell_id = 84,
  spell_name = "\229\147\136\231\137\185\230\155\188",
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[85] = {
  spell_id = 85,
  spell_name = "\228\188\175\231\145\158\230\150\175\231\154\132\230\138\128\232\131\189",
  range_type = 1,
  EFFECT = "gravityB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[86] = {
  spell_id = 86,
  spell_name = "\230\175\129\231\129\173\233\159\179\231\172\166",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[87] = {
  spell_id = 87,
  spell_name = "168\229\156\159\229\133\131\231\180\160\239\188\136\231\154\132\230\138\128\232\131\189\239\188\137",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[88] = {
  spell_id = 88,
  spell_name = "169\230\156\168\229\133\131\231\180\160\239\188\136\231\154\132\230\138\128\232\131\189\239\188\137",
  range_type = 2,
  EFFECT = "gammaBlasterB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[89] = {
  spell_id = 89,
  spell_name = "170\230\176\180\229\133\131\231\180\160\239\188\136\231\154\132\230\138\128\232\131\189\239\188\137",
  range_type = 20,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[90] = {
  spell_id = 90,
  spell_name = "171\230\132\159\230\129\169\232\138\130",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[91] = {
  spell_id = 91,
  spell_name = "\231\180\171\232\137\178B\231\186\167\229\164\154\231\177\179\233\135\140\229\174\137\231\154\132\230\138\128\232\131\189",
  range_type = 3,
  EFFECT = "normal_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[92] = {
  spell_id = 92,
  spell_name = "\233\135\145\229\133\131\231\180\160\231\154\132\230\138\128\232\131\189",
  range_type = 1,
  EFFECT = "photonMissile",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[93] = {
  spell_id = 93,
  spell_name = "149\231\129\171\229\133\131\231\180\160\239\188\136\230\175\129\231\129\173\232\128\133\239\188\137",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[94] = {
  spell_id = 94,
  spell_name = "149FACEBOOK\230\180\187\229\138\168\239\188\136\230\137\147\229\135\187\232\128\133\239\188\137",
  range_type = 2,
  EFFECT = "black_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[95] = {
  spell_id = 95,
  spell_name = "\229\129\143\232\189\172\229\138\155\229\156\186",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10095] = {
  spell_id = 10095,
  spell_name = "\229\129\143\232\189\172\229\138\155\229\156\186",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11095] = {
  spell_id = 11095,
  spell_name = "\229\129\143\232\189\172\229\138\155\229\156\186",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12095] = {
  spell_id = 12095,
  spell_name = "\229\129\143\232\189\172\229\138\155\229\156\186",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[96] = {
  spell_id = 96,
  spell_name = "\230\176\180\229\133\131\231\180\160\229\164\141\229\136\182\228\184\141\230\136\144\229\138\159\231\154\132\233\187\152\232\174\164\230\138\128\232\131\189",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[97] = {
  spell_id = 97,
  spell_name = "\229\145\168\232\142\137\232\142\137\231\154\132\230\138\128\232\131\189",
  range_type = 2,
  EFFECT = "DoubleColor",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[98] = {
  spell_id = 98,
  spell_name = "\229\188\149\229\175\188\229\137\175\229\174\152",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 12,
  icon = "2.0"
}
Keys[99] = {
  spell_id = 99,
  spell_name = "13\230\156\159WD\230\138\128\232\131\189",
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[100] = {
  spell_id = 100,
  spell_name = "\227\128\144\230\136\152\229\137\141\231\137\185\230\174\138\229\164\132\231\144\134\230\138\128\232\131\189\227\128\145\229\138\160\229\156\163\232\175\158buff\231\148\168",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[101] = {
  spell_id = 101,
  spell_name = "\227\128\144\230\136\152\229\137\141\231\137\185\230\174\138\229\164\132\231\144\134\230\138\128\232\131\189\227\128\145\229\138\160\230\174\150\230\176\145\232\128\133buff\231\148\168",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[102] = {
  spell_id = 102,
  spell_name = "\227\128\144\230\136\152\229\137\141\231\137\185\230\174\138\229\164\132\231\144\134\230\138\128\232\131\189\227\128\145\229\138\1602\230\156\136\233\162\132\231\149\153\232\139\177\233\155\132\233\128\154\231\148\168buff\231\148\168",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[103] = {
  spell_id = 103,
  spell_name = "151\229\141\138\230\156\186\230\162\176\232\132\184\231\148\183",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[104] = {
  spell_id = 104,
  spell_name = "152\233\187\145\228\186\186\229\163\174\230\177\137\239\188\136\230\138\164\229\141\171\232\128\133\239\188\137",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[105] = {
  spell_id = 105,
  spell_name = "152\230\152\134\232\153\171\239\188\136\229\190\152\229\190\138\232\128\133\239\188\137",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[106] = {
  spell_id = 106,
  spell_name = "\230\178\153\230\155\188\229\165\179\231\142\139\239\188\136\230\175\129\231\129\173\232\128\133\239\188\137",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[107] = {
  spell_id = 107,
  spell_name = "\232\142\171\230\139\137\229\133\139-WD\229\135\143\228\188\164",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[108] = {
  spell_id = 108,
  spell_name = "154\239\188\136\231\170\129\229\135\187\232\128\133\229\137\175\229\174\152\239\188\140ID:184\239\188\137",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[109] = {
  spell_id = 109,
  spell_name = "\230\157\176\229\190\183-0\233\152\182",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 0,
  icon = "0.0"
}
Keys[10109] = {
  spell_id = 10109,
  spell_name = "\230\157\176\229\190\183-1\233\152\182",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 0,
  icon = "0.0"
}
Keys[11109] = {
  spell_id = 11109,
  spell_name = "\230\157\176\229\190\183-3\233\152\182",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 0,
  icon = "0.0"
}
Keys[12109] = {
  spell_id = 12109,
  spell_name = "\230\157\176\229\190\183-5\233\152\182",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 0,
  icon = "0.0"
}
Keys[13109] = {
  spell_id = 13109,
  spell_name = "\230\157\176\229\190\183-7\233\152\182",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 0,
  icon = "0.0"
}
Keys[14109] = {
  spell_id = 14109,
  spell_name = "\230\157\176\229\190\183-10\233\152\182",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 0,
  icon = "0.0"
}
Keys[110] = {
  spell_id = 110,
  spell_name = "WD",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[111] = {
  spell_id = 111,
  spell_name = "\230\178\131\231\153\187",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[112] = {
  spell_id = 112,
  spell_name = "155gacha\229\188\149\229\175\188\232\139\177\233\155\132\239\188\136215\239\188\137",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 20,
  order = 100,
  icon = "2.0"
}
Keys[113] = {
  spell_id = 113,
  spell_name = "\231\177\179\229\168\133",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 0,
  order = 0,
  icon = "2.0"
}
Keys[114] = {
  spell_id = 114,
  spell_name = "WD-\231\167\145\233\154\134",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "indra_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[115] = {
  spell_id = 115,
  spell_name = "156\229\137\175\229\174\152\239\188\140ID190",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[116] = {
  spell_id = 116,
  spell_name = "\233\187\145\230\154\151\232\190\190\228\188\166\239\188\140ID191",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterC",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[117] = {
  spell_id = 117,
  spell_name = "\233\187\145\230\154\151\233\163\142\230\154\180\229\141\161\230\143\144\228\186\154\239\188\140ID192",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[118] = {
  spell_id = 118,
  spell_name = "\233\187\145\230\154\151\228\188\175\231\145\158\230\150\175\239\188\140ID193",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[119] = {
  spell_id = 119,
  spell_name = "\233\187\145\230\154\151\229\147\136\231\137\185\230\155\188\239\188\140ID194",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[120] = {
  spell_id = 120,
  spell_name = "\233\187\145\230\154\151\230\154\151\231\129\181\229\129\135\233\157\162\239\188\140ID195",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[121] = {
  spell_id = 121,
  spell_name = "\233\187\145\230\154\151\230\178\153\230\155\188\229\165\179\231\142\139\239\188\140ID196",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[122] = {
  spell_id = 122,
  spell_name = "158\232\144\165\230\148\182\229\190\152\229\190\138\232\128\133\239\188\140ID197",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "EMP_new01_single",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[123] = {
  spell_id = 123,
  spell_name = "\228\186\154\231\137\185\230\139\137\230\150\175",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "black_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[124] = {
  spell_id = 124,
  spell_name = "\229\133\168\229\177\143\229\165\182",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[125] = {
  spell_id = 125,
  spell_name = "\233\152\191\229\188\151\233\155\183\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[126] = {
  spell_id = 126,
  spell_name = "\231\142\155\229\167\172",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[10126] = {
  spell_id = 10126,
  spell_name = "\231\142\155\229\167\172",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[11126] = {
  spell_id = 11126,
  spell_name = "\231\142\155\229\167\172",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[12126] = {
  spell_id = 12126,
  spell_name = "\231\142\155\229\167\172",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[13126] = {
  spell_id = 13126,
  spell_name = "\231\142\155\229\167\172",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[127] = {
  spell_id = 127,
  spell_name = "\233\155\183\230\179\189",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[128] = {
  spell_id = 128,
  spell_name = "\239\188\140ID303",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[129] = {
  spell_id = 129,
  spell_name = "\239\188\140ID304",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[130] = {
  spell_id = 130,
  spell_name = "\239\188\140ID305",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[131] = {
  spell_id = 131,
  spell_name = "S\233\151\170\231\148\181",
  spell_desc = nil,
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[132] = {
  spell_id = 132,
  spell_name = "WD\229\138\160\232\161\128",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[133] = {
  spell_id = 133,
  spell_name = "\229\184\131\233\178\129\232\175\186",
  spell_desc = nil,
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[134] = {
  spell_id = 134,
  spell_name = "\228\184\135\229\156\163\232\138\130\229\190\152\229\190\138\232\128\133\239\188\140ID309",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "IndraD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[135] = {
  spell_id = 135,
  spell_name = "\239\188\140ID310",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[136] = {
  spell_id = 136,
  spell_name = "\239\188\140ID311",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10136] = {
  spell_id = 10136,
  spell_name = "\239\188\140ID311",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11136] = {
  spell_id = 11136,
  spell_name = "\239\188\140ID311",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12136] = {
  spell_id = 12136,
  spell_name = "\239\188\140ID311",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[13136] = {
  spell_id = 13136,
  spell_name = "\239\188\140ID311",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[137] = {
  spell_id = 137,
  spell_name = "\229\141\129\229\173\151\229\134\176\229\134\187",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "1.0"
}
Keys[138] = {
  spell_id = 138,
  spell_name = "\228\184\135\229\156\163\232\138\130\229\190\152\229\190\138\232\128\133\239\188\140ID309\229\133\179\232\129\148\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 0,
  order = 0,
  icon = "3.0"
}
Keys[139] = {
  spell_id = 139,
  spell_name = "\229\133\139\232\142\177\229\176\148",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10139] = {
  spell_id = 10139,
  spell_name = "\229\133\139\232\142\177\229\176\148",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11139] = {
  spell_id = 11139,
  spell_name = "\229\133\139\232\142\177\229\176\148",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12139] = {
  spell_id = 12139,
  spell_name = "\229\133\139\232\142\177\229\176\148",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[140] = {
  spell_id = 140,
  spell_name = "\229\184\149\228\188\189",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[141] = {
  spell_id = 141,
  spell_name = "\232\142\142\232\142\137",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10141] = {
  spell_id = 10141,
  spell_name = "\232\142\142\232\142\137",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11141] = {
  spell_id = 11141,
  spell_name = "\232\142\142\232\142\137",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12141] = {
  spell_id = 12141,
  spell_name = "\232\142\142\232\142\137",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[13141] = {
  spell_id = 13141,
  spell_name = "\232\142\142\232\142\137",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[142] = {
  spell_id = 142,
  spell_name = "\239\188\140ID316",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[143] = {
  spell_id = 143,
  spell_name = "\230\173\187\228\186\161\229\128\146\232\174\161\230\151\182\229\133\179\232\129\148\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[144] = {
  spell_id = 144,
  spell_name = "\233\187\145\230\154\151\233\152\191\231\180\162",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[145] = {
  spell_id = 145,
  spell_name = "\233\187\145\230\154\151\231\177\179\229\168\133\230\138\128\232\131\189\233\162\132\229\159\139",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 0,
  order = 0,
  icon = "2.0"
}
Keys[146] = {
  spell_id = 146,
  spell_name = "\233\187\145\230\154\151\231\167\145\233\154\134",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "indra_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[147] = {
  spell_id = 147,
  spell_name = "6\230\156\136S\230\180\187\229\138\168",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "EMP_new01_single",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[10147] = {
  spell_id = 10147,
  spell_name = "6\230\156\136S\230\180\187\229\138\168",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "EMP_new01_single",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[11147] = {
  spell_id = 11147,
  spell_name = "6\230\156\136S\230\180\187\229\138\168",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "EMP_new01_single",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[12147] = {
  spell_id = 12147,
  spell_name = "6\230\156\136S\230\180\187\229\138\168",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "EMP_new01_single",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[13147] = {
  spell_id = 13147,
  spell_name = "6\230\156\136S\230\180\187\229\138\168",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "EMP_new01_single",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[148] = {
  spell_id = 148,
  spell_name = "3\229\145\168\229\185\180\230\175\129\231\129\173",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10148] = {
  spell_id = 10148,
  spell_name = "3\229\145\168\229\185\180\230\175\129\231\129\173",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11148] = {
  spell_id = 11148,
  spell_name = "3\229\145\168\229\185\180\230\175\129\231\129\173",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12148] = {
  spell_id = 12148,
  spell_name = "3\229\145\168\229\185\180\230\175\129\231\129\173",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[149] = {
  spell_id = 149,
  spell_name = "\233\187\145\230\154\151\229\184\131\233\178\129\232\175\186",
  spell_desc = nil,
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[150] = {
  spell_id = 150,
  spell_name = "\233\187\145\230\154\151\230\157\176\232\165\191",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[151] = {
  spell_id = 151,
  spell_name = "\228\185\148\229\168\133",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[10151] = {
  spell_id = 10151,
  spell_name = "\228\185\148\229\168\133",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[11151] = {
  spell_id = 11151,
  spell_name = "\228\185\148\229\168\133",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[12151] = {
  spell_id = 12151,
  spell_name = "\228\185\148\229\168\133",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[13151] = {
  spell_id = 13151,
  spell_name = "\228\185\148\229\168\133",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[152] = {
  spell_id = 152,
  spell_name = "\239\188\140ID325",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[153] = {
  spell_id = 153,
  spell_name = "8\230\156\136S",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 2,
  icon = "2.0"
}
Keys[10153] = {
  spell_id = 10153,
  spell_name = "8\230\156\136S",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 2,
  icon = "2.0"
}
Keys[11153] = {
  spell_id = 11153,
  spell_name = "8\230\156\136S",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 2,
  icon = "2.0"
}
Keys[12153] = {
  spell_id = 12153,
  spell_name = "8\230\156\136S",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 2,
  icon = "2.0"
}
Keys[154] = {
  spell_id = 154,
  spell_name = "\239\188\140ID327",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[155] = {
  spell_id = 155,
  spell_name = "\239\188\140ID328",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[156] = {
  spell_id = 156,
  spell_name = "\239\188\140ID329",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[157] = {
  spell_id = 157,
  spell_name = "\239\188\140ID330",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[158] = {
  spell_id = 158,
  spell_name = "\239\188\140ID331",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[159] = {
  spell_id = 159,
  spell_name = "\239\188\140ID332",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[160] = {
  spell_id = 160,
  spell_name = "\239\188\140ID333",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[161] = {
  spell_id = 161,
  spell_name = "\239\188\140ID334",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[162] = {
  spell_id = 162,
  spell_name = "\239\188\140ID335",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 0,
  order = 0,
  icon = "0.0"
}
Keys[163] = {
  spell_id = 163,
  spell_name = "5\230\156\136WD",
  range_type = 2,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[201] = {
  spell_id = 201,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[202] = {
  spell_id = 202,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 99,
  order = 100,
  icon = "2.0"
}
Keys[203] = {
  spell_id = 203,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[204] = {
  spell_id = 204,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraB",
  DAMAGE = 99,
  order = 100,
  icon = "2.0"
}
Keys[205] = {
  spell_id = 205,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "FULL_gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[206] = {
  spell_id = 206,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gamma",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[207] = {
  spell_id = 207,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 66,
  order = 100,
  icon = "2.0"
}
Keys[208] = {
  spell_id = 208,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[209] = {
  spell_id = 209,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[210] = {
  spell_id = 210,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[211] = {
  spell_id = 211,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 66,
  order = 100,
  icon = "2.0"
}
Keys[212] = {
  spell_id = 212,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraB",
  DAMAGE = 66,
  order = 100,
  icon = "2.0"
}
Keys[213] = {
  spell_id = 213,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 58,
  order = 100,
  icon = "2.0"
}
Keys[214] = {
  spell_id = 214,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[215] = {
  spell_id = 215,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[216] = {
  spell_id = 216,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[217] = {
  spell_id = 217,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[218] = {
  spell_id = 218,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[219] = {
  spell_id = 219,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 72,
  order = 100,
  icon = "2.0"
}
Keys[220] = {
  spell_id = 220,
  spell_name = "\231\136\172\229\161\148\239\188\140\229\133\141\231\150\171\230\137\147\229\135\187\232\128\133",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 0,
  order = 100,
  icon = "2.0"
}
Keys[221] = {
  spell_id = 221,
  spell_name = "\231\136\172\229\161\148\239\188\140\229\133\141\231\150\171\230\137\147\229\135\187\232\128\133\227\128\129\230\138\164\229\141\171\232\128\133\239\188\136\230\156\141\229\138\161\229\153\168\230\154\130\230\151\182\228\184\141\230\148\175\230\140\129\232\191\153\228\185\136\233\133\141\239\188\137",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 0,
  order = 100,
  icon = "2.0"
}
Keys[222] = {
  spell_id = 222,
  spell_name = "\231\136\172\229\161\148\239\188\140\229\133\141\231\150\171\230\138\164\229\141\171\232\128\133",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 0,
  order = 100,
  icon = "2.0"
}
Keys[223] = {
  spell_id = 223,
  spell_name = "\231\136\172\229\161\148\239\188\140\229\133\141\231\150\171\229\190\152\229\190\138\232\128\133",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 0,
  order = 100,
  icon = "2.0"
}
Keys[224] = {
  spell_id = 224,
  spell_name = "\231\136\172\229\161\148\239\188\140\229\133\141\231\150\171\230\175\129\231\129\173\232\128\133",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 0,
  order = 100,
  icon = "2.0"
}
Keys[225] = {
  spell_id = 225,
  spell_name = "\231\136\172\229\161\148\239\188\140\229\133\141\231\150\171\231\170\129\229\135\187\232\128\133",
  spell_desc = nil,
  range_type = 0,
  EFFECT = "gravity",
  DAMAGE = 0,
  order = 100,
  icon = "2.0"
}
Keys[300] = {
  spell_id = 300,
  spell_name = "test skill",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 99,
  order = 100,
  icon = "2.0"
}
Keys[301] = {
  spell_id = 301,
  spell_name = "\229\165\165\230\156\175\230\150\176\230\152\159(\229\133\168\228\189\147\231\156\169\230\153\149)",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "FULL_EMP",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[302] = {
  spell_id = 302,
  spell_name = "\229\165\165\230\156\175\229\134\178\229\135\187(\229\141\149\228\189\147\231\156\169\230\153\149)",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[401] = {
  spell_id = 401,
  spell_name = "\231\136\172\229\161\1485\227\128\12910\229\133\179boss\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[402] = {
  spell_id = 402,
  spell_name = "\231\136\172\229\161\14815\227\128\12920\229\133\179boss\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[403] = {
  spell_id = 403,
  spell_name = "\231\136\172\229\161\14825\227\128\12930\229\133\179boss\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[404] = {
  spell_id = 404,
  spell_name = "\231\136\172\229\161\14835\229\133\179boss\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[405] = {
  spell_id = 405,
  spell_name = "\228\184\187\232\167\146\233\171\152\231\186\167\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[406] = {
  spell_id = 406,
  spell_name = "\228\184\187\232\167\146\233\171\152\231\186\167\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[407] = {
  spell_id = 407,
  spell_name = "\228\184\187\232\167\146\233\171\152\231\186\167\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[408] = {
  spell_id = 408,
  spell_name = "\228\184\187\232\167\146\233\171\152\231\186\167\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[409] = {
  spell_id = 409,
  spell_name = "\228\184\187\232\167\146\233\171\152\231\186\167\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[410] = {
  spell_id = 410,
  spell_name = "\228\184\187\232\167\146\233\171\152\231\186\167\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "gravity",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[501] = {
  spell_id = 501,
  spell_name = "\231\186\181\229\144\145dot\239\188\140\230\175\143\229\155\158\229\144\136\230\142\1371000hp",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 10,
  order = 3,
  icon = "3.0"
}
Keys[502] = {
  spell_id = 502,
  spell_name = "\229\133\168\228\189\147\230\166\130\231\142\135dot\239\188\140\230\175\143\230\172\161\230\142\13710%hp",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "FULL_EMP",
  DAMAGE = 10,
  order = 4,
  icon = "4.0"
}
Keys[503] = {
  spell_id = 503,
  spell_name = "\229\141\149\228\189\147\230\148\187\229\135\187\239\188\140\229\185\182\229\135\128\229\140\150\231\155\174\230\160\135",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 1500,
  order = 2,
  icon = "2.0"
}
Keys[504] = {
  spell_id = 504,
  spell_name = "\231\186\181\229\144\145\230\148\187\229\135\187\229\185\182\229\135\128\229\140\150\232\162\171\230\148\187\229\135\187\231\155\174\230\160\135\239\188\140\229\185\182\231\187\153\229\183\177\230\150\185\233\154\143\230\156\186\228\184\128\228\184\170\229\141\149\228\189\141\229\162\158\229\138\160100000\230\138\164\231\155\190\239\188\140\230\140\129\231\187\1732\229\155\158\229\144\136",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 2,
  icon = "2.0"
}
Keys[505] = {
  spell_id = 505,
  spell_name = "\229\141\149\228\189\147\230\148\187\229\135\187\239\188\140\229\185\182\230\156\13750%\230\166\130\231\142\135\228\189\191\229\183\177\230\150\1853\228\184\170\229\141\149\228\189\141\232\142\183\229\190\15110000\230\138\164\231\155\190\239\188\140\230\140\129\231\187\1732\229\155\158\229\144\136",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 1,
  icon = "1.0"
}
Keys[506] = {
  spell_id = 506,
  spell_name = "\231\186\181\229\144\145\230\148\187\229\135\187\229\185\182\230\184\133\233\153\164\231\155\174\230\160\135\232\147\132\232\131\189\239\188\140\233\153\132\229\184\16650%\230\166\130\231\142\135\233\148\129\229\174\154\228\184\128\229\155\158\229\144\136\239\188\140\229\185\182\228\189\191\229\183\177\230\150\185\233\154\143\230\156\1862\228\184\170\231\155\174\230\160\135\229\133\141\231\150\171\229\134\176\229\134\1872\229\155\158\229\144\136",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 1,
  icon = "1.0"
}
Keys[507] = {
  spell_id = 507,
  spell_name = "\229\133\168\228\189\147dot\239\188\140\230\175\143\230\172\161\230\142\13720%\230\138\128\232\131\189\228\188\164\229\174\179hp",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "FULL_EMP",
  DAMAGE = 10,
  order = 4,
  icon = "4.0"
}
Keys[508] = {
  spell_id = 508,
  spell_name = "\229\133\168\229\177\143\230\148\187\229\135\187\239\188\140100%\230\138\128\232\131\189\228\188\164\229\174\179\233\162\157\229\164\150\229\138\16050\232\147\132\232\131\189",
  spell_desc = nil,
  range_type = 5,
  EFFECT = "FULL_EMP",
  DAMAGE = 48,
  order = 100,
  icon = "2.0"
}
Keys[1002] = {
  spell_id = 1002,
  spell_name = "\231\187\153\229\183\177\230\150\185\229\133\168\228\189\147\233\154\144\232\186\1711\229\155\158\229\144\136",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 73,
  order = 2,
  icon = "3.0"
}
Keys[1003] = {
  spell_id = 1003,
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[1004] = {
  spell_id = 1004,
  spell_name = "\229\185\189\230\154\151\228\185\139\229\189\177",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "5.0"
}
Keys[1005] = {
  spell_id = 1005,
  spell_name = "\230\179\149\232\128\129\231\154\132\231\165\157\231\166\143/\232\175\133\229\146\146",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[2001] = {
  spell_id = 2001,
  spell_name = "\229\164\141\230\180\187\230\181\139\232\175\149",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[2002] = {
  spell_id = 2002,
  spell_name = "\229\164\154\230\172\161\230\148\187\229\135\187\230\181\139\232\175\149",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[800] = {
  spell_id = 800,
  spell_name = "9\230\156\136wd\230\137\147\229\135\187\232\128\133",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[10800] = {
  spell_id = 10800,
  spell_name = "9\230\156\136wd\230\137\147\229\135\187\232\128\133",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[11800] = {
  spell_id = 11800,
  spell_name = "9\230\156\136wd\230\137\147\229\135\187\232\128\133",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[12800] = {
  spell_id = 12800,
  spell_name = "9\230\156\136wd\230\137\147\229\135\187\232\128\133",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[13800] = {
  spell_id = 13800,
  spell_name = "9\230\156\136wd\230\137\147\229\135\187\232\128\133",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[801] = {
  spell_id = 801,
  spell_name = "9\230\156\136S\229\190\152\229\190\138\232\128\133",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "5.0"
}
Keys[10801] = {
  spell_id = 10801,
  spell_name = "9\230\156\136S\229\190\152\229\190\138\232\128\133",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "5.0"
}
Keys[11801] = {
  spell_id = 11801,
  spell_name = "9\230\156\136S\229\190\152\229\190\138\232\128\133",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "5.0"
}
Keys[12801] = {
  spell_id = 12801,
  spell_name = "9\230\156\136S\229\190\152\229\190\138\232\128\133",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "5.0"
}
Keys[13801] = {
  spell_id = 13801,
  spell_name = "9\230\156\136S\229\190\152\229\190\138\232\128\133",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "5.0"
}
Keys[802] = {
  spell_id = 802,
  spell_name = "\232\142\171\230\139\137\229\133\139-WD\229\135\143\228\188\164",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10802] = {
  spell_id = 10802,
  spell_name = "\232\142\171\230\139\137\229\133\139-WD\229\135\143\228\188\164",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11802] = {
  spell_id = 11802,
  spell_name = "\232\142\171\230\139\137\229\133\139-WD\229\135\143\228\188\164",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12802] = {
  spell_id = 12802,
  spell_name = "\232\142\171\230\139\137\229\133\139-WD\229\135\143\228\188\164",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[13802] = {
  spell_id = 13802,
  spell_name = "\232\142\171\230\139\137\229\133\139-WD\229\135\143\228\188\164",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[803] = {
  spell_id = 803,
  spell_name = "10\230\156\136\230\180\187\229\138\168\231\170\129\229\135\187\232\128\133",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10803] = {
  spell_id = 10803,
  spell_name = "10\230\156\136\230\180\187\229\138\168\231\170\129\229\135\187\232\128\133",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11803] = {
  spell_id = 11803,
  spell_name = "10\230\156\136\230\180\187\229\138\168\231\170\129\229\135\187\232\128\133",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12803] = {
  spell_id = 12803,
  spell_name = "10\230\156\136\230\180\187\229\138\168\231\170\129\229\135\187\232\128\133",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[13803] = {
  spell_id = 13803,
  spell_name = "10\230\156\136\230\180\187\229\138\168\231\170\129\229\135\187\232\128\133",
  range_type = 1,
  EFFECT = "gravityC",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[804] = {
  spell_id = 804,
  spell_name = "\228\184\135\229\156\163\232\138\130\229\190\152\229\190\138\232\128\133",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[10804] = {
  spell_id = 10804,
  spell_name = "\228\184\135\229\156\163\232\138\130\229\190\152\229\190\138\232\128\133",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[11804] = {
  spell_id = 11804,
  spell_name = "\228\184\135\229\156\163\232\138\130\229\190\152\229\190\138\232\128\133",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[12804] = {
  spell_id = 12804,
  spell_name = "\228\184\135\229\156\163\232\138\130\229\190\152\229\190\138\232\128\133",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[13804] = {
  spell_id = 13804,
  spell_name = "\228\184\135\229\156\163\232\138\130\229\190\152\229\190\138\232\128\133",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[805] = {
  spell_id = 805,
  spell_name = "10\230\156\136WD\230\138\164\229\141\171\232\128\133",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10805] = {
  spell_id = 10805,
  spell_name = "10\230\156\136WD\230\138\164\229\141\171\232\128\133",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11805] = {
  spell_id = 11805,
  spell_name = "10\230\156\136WD\230\138\164\229\141\171\232\128\133",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12805] = {
  spell_id = 12805,
  spell_name = "10\230\156\136WD\230\138\164\229\141\171\232\128\133",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[13805] = {
  spell_id = 13805,
  spell_name = "10\230\156\136WD\230\138\164\229\141\171\232\128\133",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[806] = {
  spell_id = 806,
  spell_name = "\233\187\145\230\154\151\233\155\183\230\179\189",
  spell_desc = nil,
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[807] = {
  spell_id = 807,
  spell_name = "11\230\156\136WD\229\190\152\229\190\138\232\128\133\229\134\176\229\134\187\228\184\128\230\142\146",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[810] = {
  spell_id = 810,
  spell_name = "\233\187\145\230\154\151\228\186\154\231\137\185\230\139\137\230\150\175",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "black_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[808] = {
  spell_id = 808,
  spell_name = "\230\132\159\230\129\169\232\138\130",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[809] = {
  spell_id = 809,
  spell_name = "\229\156\163\232\175\158\232\138\130\230\138\128\232\131\189",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[811] = {
  spell_id = 811,
  spell_name = "12\230\156\136WD",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[812] = {
  spell_id = 812,
  spell_name = "\229\129\143\232\189\172\229\138\155\229\156\186",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10812] = {
  spell_id = 10812,
  spell_name = "\229\129\143\232\189\172\229\138\155\229\156\186",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11812] = {
  spell_id = 11812,
  spell_name = "\229\129\143\232\189\172\229\138\155\229\156\186",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12812] = {
  spell_id = 12812,
  spell_name = "\229\129\143\232\189\172\229\138\155\229\156\186",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[1006] = {
  spell_id = 1006,
  spell_name = "1\230\156\136S\233\153\132\229\184\166\233\154\144\232\186\171\230\138\128\232\131\189\239\188\140\231\137\185\230\174\138ID",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[813] = {
  spell_id = 813,
  spell_name = "WD\232\191\158\233\148\129",
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[814] = {
  spell_id = 814,
  spell_name = "2\230\156\136S\229\190\152\229\190\138\229\133\141\230\173\187+\232\147\132\232\131\189",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "1.0"
}
Keys[815] = {
  spell_id = 815,
  spell_name = "\233\187\145\230\154\151\232\142\142\232\142\137",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10815] = {
  spell_id = 10815,
  spell_name = "\233\187\145\230\154\151\232\142\142\232\142\137",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11815] = {
  spell_id = 11815,
  spell_name = "\233\187\145\230\154\151\232\142\142\232\142\137",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[816] = {
  spell_id = 816,
  spell_name = "\228\184\187\232\167\146\230\181\139\232\175\149\230\138\128\232\131\189",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "indra_new1",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[2003] = {
  spell_id = 2003,
  spell_name = "\229\188\185\229\176\132\230\181\139\232\175\149",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[817] = {
  spell_id = 817,
  spell_name = "3\230\156\136\230\137\147\229\135\187\232\128\133",
  range_type = 1,
  EFFECT = "normal2",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[818] = {
  spell_id = 818,
  spell_name = "\233\187\145\230\154\151\229\133\139\232\142\177\229\176\148",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10818] = {
  spell_id = 10818,
  spell_name = "\233\187\145\230\154\151\229\133\139\232\142\177\229\176\148",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11818] = {
  spell_id = 11818,
  spell_name = "\233\187\145\230\154\151\229\133\139\232\142\177\229\176\148",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12818] = {
  spell_id = 12818,
  spell_name = "\233\187\145\230\154\151\229\133\139\232\142\177\229\176\148",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[819] = {
  spell_id = 819,
  spell_name = "3\230\156\136WD\230\137\147\229\135\187\232\128\133",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[820] = {
  spell_id = 820,
  spell_name = "\229\141\161\232\144\157\229\176\148",
  spell_desc = nil,
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[821] = {
  spell_id = 821,
  spell_name = "4\230\156\136WD\229\190\152\229\190\138\232\128\133",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[822] = {
  spell_id = 822,
  spell_name = "\233\187\145\230\154\151\229\164\169\231\169\185",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[10822] = {
  spell_id = 10822,
  spell_name = "\233\187\145\230\154\151\229\164\169\231\169\185",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[11822] = {
  spell_id = 11822,
  spell_name = "\233\187\145\230\154\151\229\164\169\231\169\185",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[12822] = {
  spell_id = 12822,
  spell_name = "\233\187\145\230\154\151\229\164\169\231\169\185",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[13822] = {
  spell_id = 13822,
  spell_name = "\233\187\145\230\154\151\229\164\169\231\169\185",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[823] = {
  spell_id = 823,
  spell_name = "\231\140\171\229\165\179",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[824] = {
  spell_id = 824,
  spell_name = "5\230\156\136S\229\190\152\229\190\138\232\128\133",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[825] = {
  spell_id = 825,
  spell_name = "\230\150\176\230\151\151\232\136\176\230\175\129\231\129\173\232\128\133",
  range_type = 2,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[826] = {
  spell_id = 826,
  spell_name = "\233\187\145\230\154\151\230\180\155\229\133\139",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[827] = {
  spell_id = 827,
  spell_name = "\233\187\155\229\174\137\229\168\156",
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[828] = {
  spell_id = 828,
  spell_name = "S\231\170\129\229\135\187\232\128\133",
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[829] = {
  spell_id = 829,
  spell_name = "\230\150\176wd\229\190\152\229\190\138\229\164\141\230\180\187",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[830] = {
  spell_id = 830,
  spell_name = "\233\187\145\230\154\151\229\134\156\229\156\186\228\184\187",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[831] = {
  spell_id = 831,
  spell_name = "\230\136\144\233\149\191\230\138\164\231\155\190",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[832] = {
  spell_id = 832,
  spell_name = "\229\138\160\229\133\168\228\189\147\231\137\169\230\148\187",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[833] = {
  spell_id = 833,
  spell_name = "\230\150\175\229\161\148\229\133\139",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[834] = {
  spell_id = 834,
  spell_name = "\228\187\165\229\136\169\228\186\154",
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[835] = {
  spell_id = 835,
  spell_name = "\233\187\145\230\154\151\229\183\180\229\190\183",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[836] = {
  spell_id = 836,
  spell_name = "\229\155\155\229\145\168\229\185\180\230\175\129\231\129\173",
  range_type = 4,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[837] = {
  spell_id = 837,
  spell_name = "\229\155\155\229\145\168\229\185\180\230\151\151\232\136\176",
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[838] = {
  spell_id = 838,
  spell_name = "\228\185\157\230\156\136wd",
  range_type = 4,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[839] = {
  spell_id = 839,
  spell_name = "\233\187\145\230\154\151\229\174\137\229\184\131\231\189\151\230\150\175",
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[840] = {
  spell_id = 840,
  spell_name = "\228\185\157\230\156\136s",
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[841] = {
  spell_id = 841,
  spell_name = "10\230\156\136wd",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[842] = {
  spell_id = 842,
  spell_name = "3\230\156\136WD\230\137\147\229\135\187\232\128\133",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[843] = {
  spell_id = 843,
  spell_name = "10\230\156\136s",
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[844] = {
  spell_id = 844,
  spell_name = "11\230\156\136s",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[845] = {
  spell_id = 845,
  spell_name = "11\230\156\136wd",
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[846] = {
  spell_id = 846,
  spell_name = "\229\165\165\228\184\129",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[847] = {
  spell_id = 847,
  spell_name = "\230\173\187\228\186\161\231\136\134\231\130\184\229\133\179\232\129\148id",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[848] = {
  spell_id = 848,
  spell_name = "4\230\156\136WD\229\190\152\229\190\138\232\128\133",
  spell_desc = nil,
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[849] = {
  spell_id = 849,
  spell_name = "\229\156\163\232\175\158\232\138\130",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[850] = {
  spell_id = 850,
  spell_name = "\229\176\164\232\190\190\229\164\167\229\184\136",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[851] = {
  spell_id = 851,
  spell_name = "\233\187\145\230\154\151\230\150\175\229\141\161\229\176\148",
  range_type = 2,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[852] = {
  spell_id = 852,
  spell_name = "1\230\156\136s\232\139\177\233\155\132",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[853] = {
  spell_id = 853,
  spell_name = "1\230\156\136wd",
  range_type = 1,
  EFFECT = "normal2",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[854] = {
  spell_id = 854,
  spell_name = "\233\187\145\230\154\151\233\186\166\229\138\160\231\153\187",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[855] = {
  spell_id = 855,
  spell_name = "\229\168\145\229\168\156",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[856] = {
  spell_id = 856,
  spell_name = "\229\168\145\229\168\156",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[857] = {
  spell_id = 857,
  spell_name = "\229\168\145\229\168\156",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[858] = {
  spell_id = 858,
  spell_name = "\233\146\162\233\170\168",
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[859] = {
  spell_id = 859,
  spell_name = "\229\133\176\230\160\188",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[860] = {
  spell_id = 860,
  spell_name = "\233\187\145\230\154\151\228\187\165\229\136\169\228\186\154",
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[861] = {
  spell_id = 861,
  spell_name = "\231\144\134\230\159\165\229\190\183",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[862] = {
  spell_id = 862,
  spell_name = "4\230\156\136wd",
  range_type = 2,
  EFFECT = "gammaBlasterB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[863] = {
  spell_id = 863,
  spell_name = "\233\187\145\230\154\151\231\189\151\230\129\169",
  range_type = 4,
  EFFECT = "DrainSoul",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[864] = {
  spell_id = 864,
  spell_name = "\229\176\188\229\143\164\230\139\137\230\150\175",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[865] = {
  spell_id = 865,
  spell_name = "\231\169\134\230\180\155\230\162\133\232\140\168",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[866] = {
  spell_id = 866,
  spell_name = "\232\144\168\233\186\166\232\191\170",
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[867] = {
  spell_id = 867,
  spell_name = "\233\187\145\230\154\151\230\150\175\229\134\133\229\133\139",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[868] = {
  spell_id = 868,
  spell_name = "\233\152\191\233\178\129\232\191\170\229\183\180",
  range_type = 3,
  EFFECT = "indraBarrageB",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[869] = {
  spell_id = 869,
  spell_name = "\230\150\175\232\175\186\231\153\187",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[870] = {
  spell_id = 870,
  spell_name = "\233\187\145\230\154\151\229\188\151\231\145\158",
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[871] = {
  spell_id = 871,
  spell_name = "\229\144\137\229\174\137\229\168\156",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[872] = {
  spell_id = 872,
  spell_name = "\232\142\177\232\130\175-\230\148\185\233\128\1600",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10872] = {
  spell_id = 10872,
  spell_name = "\232\142\177\232\130\175-\230\148\185\233\128\1609",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11872] = {
  spell_id = 11872,
  spell_name = "\232\142\177\232\130\175-\230\148\185\233\128\16015",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[12872] = {
  spell_id = 12872,
  spell_name = "\232\142\177\232\130\175-\230\148\185\233\128\16019\239\188\136+T3)",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[873] = {
  spell_id = 873,
  spell_name = "\233\187\145\230\154\151\232\142\142\232\142\137--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10873] = {
  spell_id = 10873,
  spell_name = "\233\187\145\230\154\151\232\142\142\232\142\137--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11873] = {
  spell_id = 11873,
  spell_name = "\233\187\145\230\154\151\232\142\142\232\142\137--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[874] = {
  spell_id = 874,
  spell_name = "\233\187\145\230\154\151\229\134\156\229\156\186\228\184\187--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[10874] = {
  spell_id = 10874,
  spell_name = "\233\187\145\230\154\151\229\134\156\229\156\186\228\184\187--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[11874] = {
  spell_id = 11874,
  spell_name = "\233\187\145\230\154\151\229\134\156\229\156\186\228\184\187--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[875] = {
  spell_id = 875,
  spell_name = "\233\187\145\230\154\151\229\164\169\231\169\185--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[10875] = {
  spell_id = 10875,
  spell_name = "\233\187\145\230\154\151\229\164\169\231\169\185--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[11875] = {
  spell_id = 11875,
  spell_name = "\233\187\145\230\154\151\229\164\169\231\169\185--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[876] = {
  spell_id = 876,
  spell_name = "\233\187\145\230\154\151\230\180\155\229\133\139--\228\188\152\229\140\150\231\137\136",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10876] = {
  spell_id = 10876,
  spell_name = "\233\187\145\230\154\151\230\180\155\229\133\139--\228\188\152\229\140\150\231\137\136",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11876] = {
  spell_id = 11876,
  spell_name = "\233\187\145\230\154\151\230\180\155\229\133\139--\228\188\152\229\140\150\231\137\136",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[877] = {
  spell_id = 877,
  spell_name = "\233\187\145\230\154\151\229\183\180\229\190\183--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[10877] = {
  spell_id = 10877,
  spell_name = "\233\187\145\230\154\151\229\183\180\229\190\183--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[11877] = {
  spell_id = 11877,
  spell_name = "\233\187\145\230\154\151\229\183\180\229\190\183--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[878] = {
  spell_id = 878,
  spell_name = "\232\142\142\232\142\137--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10878] = {
  spell_id = 10878,
  spell_name = "\232\142\142\232\142\137--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11878] = {
  spell_id = 11878,
  spell_name = "\232\142\142\232\142\137--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlaster",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[879] = {
  spell_id = 879,
  spell_name = "\229\134\156\229\156\186\228\184\187--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[10879] = {
  spell_id = 10879,
  spell_name = "\229\134\156\229\156\186\228\184\187--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[11879] = {
  spell_id = 11879,
  spell_name = "\229\134\156\229\156\186\228\184\187--\228\188\152\229\140\150\231\137\136",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[880] = {
  spell_id = 880,
  spell_name = "\229\164\169\231\169\185--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[10880] = {
  spell_id = 10880,
  spell_name = "\229\164\169\231\169\185--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[11880] = {
  spell_id = 11880,
  spell_name = "\229\164\169\231\169\185--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[881] = {
  spell_id = 881,
  spell_name = "\230\180\155\229\133\139--\228\188\152\229\140\150\231\137\136",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10881] = {
  spell_id = 10881,
  spell_name = "\230\180\155\229\133\139--\228\188\152\229\140\150\231\137\136",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11881] = {
  spell_id = 11881,
  spell_name = "\230\180\155\229\133\139--\228\188\152\229\140\150\231\137\136",
  range_type = 1,
  EFFECT = "gravityA",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[882] = {
  spell_id = 882,
  spell_name = "\229\183\180\229\190\183--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[10882] = {
  spell_id = 10882,
  spell_name = "\229\183\180\229\190\183--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[11882] = {
  spell_id = 11882,
  spell_name = "\229\183\180\229\190\183--\228\188\152\229\140\150\231\137\136",
  range_type = 4,
  EFFECT = "indraB",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[883] = {
  spell_id = 883,
  spell_name = "\229\168\156\229\161\148\232\142\142",
  range_type = 7,
  EFFECT = "gravitylightning",
  DAMAGE = 100,
  order = 100,
  icon = "3.0"
}
Keys[884] = {
  spell_id = 884,
  spell_name = "\233\187\145\230\154\151\229\174\136\230\156\155\232\128\133",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[885] = {
  spell_id = 885,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\230\138\181\230\140\1612\230\172\161\228\188\164\229\174\179--\231\159\169\233\152\181\230\138\164\231\155\190",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10885] = {
  spell_id = 10885,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\230\138\181\230\140\1614\230\172\161\228\188\164\229\174\179--\231\159\169\233\152\181\230\138\164\231\155\190",
  range_type = 2,
  EFFECT = "gammaBlasterD",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[886] = {
  spell_id = 886,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\229\175\185\230\137\128\230\156\137\231\148\183\230\128\167\232\167\146\232\137\178\233\128\160\230\136\144\233\162\157\229\164\150\228\188\164\229\174\179--\229\134\176\233\156\156\229\135\139\233\155\182",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10886] = {
  spell_id = 10886,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\229\175\185\230\137\128\230\156\137\231\148\183\230\128\167\232\167\146\232\137\178\233\128\160\230\136\144\233\162\157\229\164\150\228\188\164\229\174\179--\229\134\176\233\156\156\229\135\139\233\155\182",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[887] = {
  spell_id = 887,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\233\154\144\232\186\171--\233\154\144\232\186\171\230\157\128\230\137\139",
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[10887] = {
  spell_id = 10887,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\233\154\144\232\186\171--\233\154\144\232\186\171\230\157\128\230\137\139",
  range_type = 2,
  EFFECT = "gammaBlasterA",
  DAMAGE = 100,
  order = 100,
  icon = "4.0"
}
Keys[888] = {
  spell_id = 888,
  spell_name = "\231\143\141\229\166\174.\231\165\158\229\156\163\230\136\152\230\151\151",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[10888] = {
  spell_id = 10888,
  spell_name = "\231\143\141\229\166\174.\231\165\158\229\156\163\230\136\152\230\151\1512",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[11888] = {
  spell_id = 11888,
  spell_name = "\231\143\141\229\166\174.\231\165\158\229\156\163\230\136\152\230\151\1513",
  range_type = 4,
  EFFECT = "indra_cross_new",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[900] = {
  spell_id = 900,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\230\151\160\233\153\144\229\155\158\232\161\128",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[901] = {
  spell_id = 901,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\230\151\160\233\153\144\229\138\160\231\155\190",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[902] = {
  spell_id = 902,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\230\151\160\233\153\144\229\164\141\230\180\187",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[903] = {
  spell_id = 903,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\229\133\168\228\189\147\229\135\160\231\142\135\230\148\190\233\128\144/\232\161\176\229\188\177/\230\183\183\228\185\177/\229\134\176\229\134\187/\231\156\169\230\153\149",
  range_type = 5,
  EFFECT = "FULL_EMP",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
Keys[904] = {
  spell_id = 904,
  spell_name = "\230\150\176\232\153\171\230\180\158\231\142\169\230\179\149--\229\133\168\228\189\147\230\149\140\228\186\186\228\184\173\230\175\146",
  range_type = 1,
  EFFECT = "gravity",
  DAMAGE = 100,
  order = 100,
  icon = "2.0"
}
