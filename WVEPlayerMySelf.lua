WVEPlayerMySelf = luaClass(WVEPlayer)
function WVEPlayerMySelf:ctor(id, startPoint, endPoint, speed, movedDistance, playerStatus)
  self.mType = EWVEPlayerType.TYPE_MYSELF
  self.mTypeFrame = EWVEPlayerTypeFrame[self.mType]
  self.mMarchID = -1
  self.mPointID = -1
  self.mDieTime = 0
  self.mDieWaitTime = 0
  self.mState = EWVEPlayerState.STATE_PLAYER_UNKNOW
  self.mStartPoint = startPoint
end
function WVEPlayerMySelf:SetUpManager(playerManager)
  self.mPlayerManager = playerManager
end
function WVEPlayerMySelf:UpdateToStatus(playerContent)
  local playerInfo = LuaUtils:string_split(playerContent.pos, "_")
  if playerInfo[2] and (playerInfo[2] == "dot" or playerInfo[2] == "wait") then
    if self.mState == EWVEPlayerState.STATE_PLAYER_STAYINPOINT and self.mPointID == tonumber(playerInfo[3]) then
      return
    end
    self:OnLeaveState(self.mState)
    self.mState = EWVEPlayerState.STATE_PLAYER_STAYINPOINT
    self.mPointID = tonumber(playerInfo[3])
    self.mMarchID = -1
    self:OnEnterState(self.mState)
  elseif playerInfo[2] and playerInfo[2] == "line" then
    if self.mState == EWVEPlayerState.STATE_PLAYER_MARCH and self.mMarchID == tonumber(playerInfo[3]) then
      return
    end
    self:OnLeaveState(self.mState)
    self.mState = EWVEPlayerState.STATE_PLAYER_MARCH
    self.mMarchID = tonumber(playerInfo[3])
    self.mPointID = -1
    self:OnEnterState(self.mState)
  elseif playerInfo[2] and playerInfo[2] == "die" then
    if self.mState == EWVEPlayerState.STATE_PLAYER_DIE then
      return
    end
    self:OnLeaveState(self.mState)
    self.mState = EWVEPlayerState.STATE_PLAYER_DIE
    self.mMarchID = -1
    self.mPointID = -1
    self.mDieWaitTime = tonumber(playerInfo[3])
    self.mDieTime = os.time()
    self.mLastFrameTime = 0
    self:OnEnterState(self.mState)
  end
end
function WVEPlayerMySelf:OnEnterState(state)
  if state == EWVEPlayerState.STATE_PLAYER_STAYINPOINT then
    self.mPlayerManager:PlayerMyselfEnterPoint(self.mPointID)
  elseif state == EWVEPlayerState.STATE_PLAYER_MARCH then
  elseif state == EWVEPlayerState.STATE_PLAYER_DIE then
    self:PlayerDie()
  end
end
function WVEPlayerMySelf:OnLeaveState(state)
  if state == EWVEPlayerState.STATE_PLAYER_STAYINPOINT then
    self.mPlayerManager:PlayerMyselfLeavePoint(self.mPointID)
  elseif state == EWVEPlayerState.STATE_PLAYER_MARCH then
    self.mPlayerManager:PlayerMyselfLeaveMarch(self.mMarchID)
    self.mPlayerManager:RemovePlayer(self.mMarchID)
  elseif state == EWVEPlayerState.STATE_PLAYER_DIE then
    self:PlayerRebuilded()
  end
end
function WVEPlayerMySelf:PlayerDie()
  local timeStr = GameUtils:formatTimeString(self.mDieWaitTime)
  local rebuildPercent = 1
  local dieInfo = {}
  dieInfo.timeStr = timeStr
  dieInfo.rebuildPercent = rebuildPercent
  local diePanel = self.mPlayerManager.mGameManger:CreateInfoPanel(EWVEINFOPANEL_TYPE.PANEL_DIE)
  diePanel:Show(dieInfo)
end
function WVEPlayerMySelf:PlayerRebuilded()
  self.mPlayerManager.mGameManger:DestroyInfoPanel(EWVEINFOPANEL_TYPE.PANEL_DIE)
  local flashObj = self.mPlayerManager.mGameManger:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "MainMC_HideDieInfo")
  end
end
function WVEPlayerMySelf:GetMarchID()
  if self.mState == EWVEPlayerState.STATE_PLAYER_MARCH then
    return self.mMarchID
  end
  return nil
end
function WVEPlayerMySelf:IsMySelfInMarch(marchID)
  if self.mState == EWVEPlayerState.STATE_PLAYER_MARCH and marchID == self.mMarchID then
    return true
  end
  return false
end
function WVEPlayerMySelf:GetMyselfMarchInfo()
  local marchId = self.mMarchID
  for k, v in pairs(self.mPlayerManager.mPlayerList) do
    if v.mID == marchId then
      return v
    end
  end
  return nil
end
function WVEPlayerMySelf:GetPointID()
  if self.mState == EWVEPlayerState.STATE_PLAYER_STAYINPOINT or self.mState == EWVEPlayerState.STATE_PLAYER_WAITMARCH then
    return self.mPointID
  elseif self.mState == EWVEPlayerState.STATE_PLAYER_DIE then
    local mMap = self.mPlayerManager.mGameManger:GetMap()
    DebugOut("GetPointID:playerstate:die", self.mState, ",", mMap)
    if mMap then
      DebugOut(mMap:GetPlyaerBasePointID())
      return (...), mMap, mMap:GetPlyaerBasePointID()
    end
  end
  return nil
end
function WVEPlayerMySelf:PrepareJumpTo(targetPoint)
  self.mTargetPoint = targetPoint
end
function WVEPlayerMySelf:GetJumpTarget()
  return self.mTargetPoint
end
function WVEPlayerMySelf:GetState()
  return self.mState
end
function WVEPlayerMySelf:IsStayOnPoint(pointID)
  if self.mState ~= EWVEPlayerState.STATE_PLAYER_STAYINPOINT and self.mState ~= EWVEPlayerState.STATE_PLAYER_WAITMARCH then
    return false
  end
  return pointID == self.mPointID
end
