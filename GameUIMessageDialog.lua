local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
GameUIMessageDialog.DialogStyle = {}
GameUIMessageDialog.DialogStyle.OK = 1
GameUIMessageDialog.DialogStyle.YesNo = 2
GameUIMessageDialog.DialogStyle.Caption = 3
GameUIMessageDialog.DialogStyle.Green = 4
GameUIMessageDialog.DialogStyle.RushTutorial = 5
GameUIMessageDialog.DialogStyle.Notice = 6
GameUIMessageDialog.DialogStyle.HELP = 7
GameUIMessageDialog.isCheck = 0
GameUIMessageDialog.Usage = "None"
function GameUIMessageDialog:OnFSCommand(cmd, arg)
  if cmd == "clickBtn" then
    self:GetFlashObject():InvokeASCallback("_root", "hideMessageBox")
    if "text_left_button" == arg then
      local temp = self.callback_left
      local temp_args = self.args_left
      self.callback_left = nil
      self.args_left = nil
      self:ExecuteCallback(temp, temp_args)
    elseif "text_right_button" == arg then
      local temp = self.callback_right
      local temp_args = self.args_right
      self.callback_right = nil
      self.args_right = nil
      self:ExecuteCallback(temp, temp_args)
    elseif "ok_button" == arg then
      local temp = self.callback_ok
      local temp_args = self.args_ok
      self.callback_ok = nil
      self.args_ok = nil
      self:ExecuteCallback(temp, temp_args)
    elseif "yes_button" == arg then
      local temp = self.callback_yes
      local temp_args = self.args_yes
      self.callback_yes = nil
      self.args_yes = nil
      self:ExecuteCallback(temp, temp_args)
    elseif "no_button" == arg then
      local temp = self.callback_no
      local temp_args = self.args_no
      self.callback_no = nil
      self.args_no = nil
      self:ExecuteCallback(temp, temp_args)
    end
  elseif cmd == "erase_dialog" or cmd == "close_reward" then
    GameStateGlobalState:EraseObject(self)
  elseif cmd == "PayActionChange" then
    if GameSettingData.EnablePayRemind then
      GameSettingData.EnablePayRemind = false
      local info = GameLoader:GetGameText("LC_MENU_PURCHES_ALERT_CLOSE_CHAR")
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(info)
    else
      GameSettingData.EnablePayRemind = true
    end
    GameUtils:SaveSettingData()
    GameUIMessageDialog:ShowPayAction()
  elseif cmd == "ChangeCheck" then
    GameUIMessageDialog.isCheck = tonumber(arg)
  elseif cmd == "notice_btn" then
    local temp = self.callback_notice
    local temp_args = self.args_notice
    self.callback_notice = nil
    self.args_notice = nil
    self:ExecuteCallback(temp, temp_args)
    if temp_args == "announce_close_server_html" then
      Webview:Destroy()
    end
  end
end
function GameUIMessageDialog:Display(title, info)
  GameUIMessageDialog.isCheck = 0
  GameUIMessageDialog.Usage = title
  local obj_flash = self:GetFlashObject()
  if not obj_flash then
    self:LoadFlashObject()
    obj_flash = self:GetFlashObject()
  end
  obj_flash:InvokeASCallback("_root", "setLocalText", "helpTitle", GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_HELP"))
  obj_flash:InvokeASCallback("_root", "setLocalText", "noticeTitle", GameLoader:GetGameText("LC_MENU_SEQUEL_ANNOUNCEMENT_TITLE"))
  obj_flash:InvokeASCallback("_root", "Display", title, info)
  GameStateGlobalState:AddObject(self)
end
function GameUIMessageDialog:DbgPanel(info)
  local obj_flash = self:GetFlashObject()
  if not obj_flash then
    self:LoadFlashObject()
    obj_flash = self:GetFlashObject()
  end
  obj_flash:InvokeASCallback("_root", "showDbgPanel", info)
  GameStateGlobalState:AddObject(self)
end
function GameUIMessageDialog:GetElementPosAndSize(element)
  local obj_flash = self:GetFlashObject()
  if not obj_flash then
    self:LoadFlashObject()
    obj_flash = self:GetFlashObject()
  end
  local pos = obj_flash:InvokeASCallback("_root", "GetPostion", element)
  return ...
end
function GameUIMessageDialog:CreateWebView(url, x, y, width, height)
  DebugOut("ajjhsaadj:", x, y, width, height)
  Webview:Show(url, x, y, width, height)
end
function GameUIMessageDialog:SetStyle(style)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    self:LoadFlashObject()
    flash_obj = self:GetFlashObject()
  end
  flash_obj:InvokeASCallback("_root", "SetStyle", style)
end
function GameUIMessageDialog:ShowPayAction()
  local payremind = not GameSettingData.EnablePayRemind
  self:GetFlashObject():InvokeASCallback("_root", "showPayAction", payremind)
end
function GameUIMessageDialog:SetLeftTextButton(caption, callback, args)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    self:LoadFlashObject()
    flash_obj = self:GetFlashObject()
  end
  flash_obj:InvokeASCallback("_root", "SetLeftTextButton", caption)
  self.callback_left = callback
  self.args_left = args
end
function GameUIMessageDialog:SetLeftGreenButton(caption, callback, args)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    self:LoadFlashObject()
    flash_obj = self:GetFlashObject()
  end
  flash_obj:InvokeASCallback("_root", "SetLeftGreenButton", caption)
  self.callback_left = callback
  self.args_left = args
end
function GameUIMessageDialog:SetRightTextButton(caption, callback, args)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    self:LoadFlashObject()
    flash_obj = self:GetFlashObject()
  end
  flash_obj:InvokeASCallback("_root", "SetRightTextButton", caption)
  self.callback_right = callback
  self.args_right = args
end
function GameUIMessageDialog:SetYesButton(callback, args)
  local flash_obj = self:GetFlashObject()
  self.callback_yes = callback
  self.args_yes = args
end
function GameUIMessageDialog:SetNoButton(callback, args)
  local flash_obj = self:GetFlashObject()
  self.callback_no = callback
  self.args_no = args
end
function GameUIMessageDialog:SetOkButton(callback, args)
  local flash_obj = self:GetFlashObject()
  self.callback_ok = callback
  self.args_ok = args
end
function GameUIMessageDialog:SetNoticeButton(callback, args)
  local flash_obj = self:GetFlashObject()
  self.callback_notice = callback
  self.args_notice = args
end
function GameUIMessageDialog:ExecuteCallback(callback_func, args)
  if callback_func then
    local param_table = args
    if param_table and type(param_table) == "table" then
      callback_func(unpack(param_table))
    else
      callback_func(param_table)
    end
  end
end
function GameUIMessageDialog:ShowCheckBox(checkBoxText)
  if checkBoxText then
    self:GetFlashObject():InvokeASCallback("_root", "SetCheckBoxText", checkBoxText)
  end
  if GameUIMessageDialog.isCheck == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "ShowCheckBox", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "ShowCheckBox", false)
  end
end
function GameUIMessageDialog:HideCheckBox()
  self:GetFlashObject():InvokeASCallback("_root", "HideCheckBox")
end
function GameUIMessageDialog:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
    self:GetFlashObject():InvokeASCallback("_root", "update", dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIMessageDialog.OnAndroidBack()
    GameUIMessageDialog:OnFSCommand("clickBtn", "no_button")
  end
end
