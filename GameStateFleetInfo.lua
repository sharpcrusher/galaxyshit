local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local GameFleetInfoBag = LuaObjectManager:GetLuaObject("GameFleetInfoBag")
local GameFleetInfoTabBar = LuaObjectManager:GetLuaObject("GameFleetInfoTabBar")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameObjectDragger = LuaObjectManager:GetLuaObject("GameObjectDragger")
local GameObjectDraggerItem = GameObjectDragger:NewInstance("bagItem.tfs", false)
local GameStateFleetInfo = GameStateManager.GameStateFleetInfo
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameCommonBackground = LuaObjectManager:GetLuaObject("GameCommonBackground")
local GameUIAchievement = LuaObjectManager:GetLuaObject("GameUIAchievement")
local GameUIPrestige = LuaObjectManager:GetLuaObject("GameUIPrestige")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameUIFleetGrowUp = LuaObjectManager:GetLuaObject("GameUIFleetGrowUp")
GameStateFleetInfo.CHARACTER = 1
GameStateFleetInfo.FORMATION = 2
GameStateFleetInfo.ACHIEVEMENT = 3
GameStateFleetInfo.PRESTIGE = 4
GameStateFleetInfo.dragItemOffset = -70
SUB_STATE = {STATE_NORMAL = 1, STATE_GROW_UP = 2}
GameStateFleetInfo.mCurState = SUB_STATE.STATE_NORMAL
function GameStateFleetInfo:GoToSubState(subState)
  GameStateFleetInfo:OnLeaveSubState(GameStateFleetInfo.mCurState)
  GameStateFleetInfo.mCurState = subState
  GameStateFleetInfo:OnEnterSubState(GameStateFleetInfo.mCurState)
end
function GameStateFleetInfo:OnEnterSubState(subState)
  if subState == SUB_STATE.STATE_GROW_UP then
    GameStateFleetInfo:AddObject(GameUIFleetGrowUp)
    GameUIFleetGrowUp:Show()
  elseif subState == SUB_STATE.STATE_NORMAL then
    GameFleetInfoUI:UpdateFleetLevelInfo()
    GameStateFleetInfo:EraseObject(GameUIFleetGrowUp)
  end
end
function GameStateFleetInfo:OnLeaveSubState(subState)
end
function GameObjectDraggerItem:OnAddToGameState(state)
  local frame = "item_" .. GameObjectDraggerItem.DraggedItemTYPE
  DebugOut("frame: ", frame)
  self:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
  self:GetFlashObject():InvokeASCallback("_root", "ChangeToBigger")
end
function GameObjectDraggerItem:OnEraseFromGameState(state)
  DebugOut("erase itemDragger from GameFleetInfoBag and GameFleetInfoUI")
  self.DraggedItemID = -1
  self.unloadItem = false
  self.equipItem = false
end
function GameObjectDraggerItem:OnReachedDestPos()
  DebugOut("GameStateFleetInfo OnReachedDestPos: ", self.equipItem, self.unloadItem)
  if self.DraggedItemID and self.DraggedItemID ~= -1 then
    if self.equipItem then
      GameFleetInfoBag:ShouldEquip(self.DraggedItemID)
    elseif self.unloadItem then
      GameFleetInfoUI:UnloadEquip(self.DraggedItemID)
    end
  else
    GameStateFleetInfo:onEndDragItem()
  end
end
function GameStateFleetInfo:onEndDragItem()
  if self.currentTab == self.CHARACTER then
    GameFleetInfoBag:onEndDragItem()
    GameFleetInfoBackground:onEndDragItem()
    GameFleetInfoUI:onEndDragItem()
  end
  GameStateFleetInfo:EraseObject(GameObjectDraggerItem)
end
function GameStateFleetInfo:InitGameState()
  self.currentTab = -1
  self.initTab = -1
end
function GameStateFleetInfo:InitTab(index)
  self.initTab = index
end
function GameStateFleetInfo:SetTab(index)
  if self.currentTab ~= index then
    self:OnChangeTab(self.currentTab, index)
  end
end
function GameStateFleetInfo:OnChangeTab(pre, cur)
  DebugOut("GameStateFleetInfo:OnChangeTab: ", pre, cur)
  self.currentTab = cur
  if pre ~= -1 then
    self:EraseObject(GameFleetInfoTabBar)
    self:EraseObject(GameCommonBackground)
    if pre == self.CHARACTER then
      self:EraseObject(GameFleetInfoBackground)
      self:EraseObject(GameFleetInfoBag)
      self:EraseObject(GameFleetInfoUI)
    elseif pre == self.FORMATION then
    elseif pre == self.ACHIEVEMENT then
      self:EraseObject(GameUIAchievement)
    elseif pre == self.PRESTIGE then
      self:EraseObject(GameUIPrestige)
    end
  end
  if cur ~= -1 then
    self:AddObject(GameCommonBackground)
    if cur == self.CHARACTER then
      self:AddObject(GameFleetInfoBackground)
      self:AddObject(GameFleetInfoBag)
      self:AddObject(GameFleetInfoUI)
    elseif cur == self.FORMATION then
    elseif cur == self.ACHIEVEMENT then
      self:AddObject(GameUIAchievement)
    elseif cur == self.PRESTIGE then
      GameUIPrestige:Show()
      GameUIPrestige:HideTitleBar()
    end
    self:AddObject(GameFleetInfoTabBar)
    GameFleetInfoTabBar:hideSecondTab()
    self:ForceCompleteCammandList()
    GameFleetInfoTabBar:SelectTab(cur)
    if cur == self.ACHIEVEMENT then
      GameFleetInfoTabBar:CheckShowBtnGP(true)
    else
      GameFleetInfoTabBar:CheckShowBtnGP(false)
    end
  end
  self:ForceCompleteCammandList()
end
function GameStateFleetInfo:OnFocusGain(previousState)
  if not self.previousState then
    self.previousState = previousState
  end
  GameFleetInfoTabBar:LoadFlashObject()
  GameFleetInfoBag:LoadFlashObject()
  GameObjectDraggerItem:Init()
  GameFleetInfoTabBar:Init(self.initTab)
  self.initTab = -1
  GameObjectDraggerItem:LoadFlashObject()
end
function GameStateFleetInfo:OnFocusLost(newState)
  self:OnChangeTab(self.currentTab, -1)
  GameObjectDraggerItem:UnloadFlashObject()
  GameFleetInfoTabBar:UnloadFlashObject()
  GameFleetInfoBag:UnloadFlashObject()
end
function GameStateFleetInfo:BeginDragItem(itemId, itemType, initPosX, initPosY, posX, posY)
  DebugOut("BeginDragItem: ", itemId, itemType, posX, posY)
  GameStateManager:GetCurrentGameState():AddObject(GameObjectDraggerItem)
  GameObjectDraggerItem.DraggedItemID = itemId
  GameObjectDraggerItem.DraggedItemTYPE = itemType
  GameObjectDraggerItem:SetDestPosition(initPosX, initPosY)
  GameObjectDraggerItem:BeginDrag(tonumber(self.dragItemOffset), tonumber(self.dragItemOffset), tonumber(posX), tonumber(posY))
  if self.currentTab == self.CHARACTER then
    GameFleetInfoBag:onBeginDragItem(GameObjectDraggerItem)
    GameFleetInfoBackground:onBeginDragItem(GameObjectDraggerItem)
    GameFleetInfoUI:onBeginDragItem(GameObjectDraggerItem)
  end
end
function GameStateFleetInfo:OnTouchPressed(x, y)
  if GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchPressed(self, x, y)
end
function GameStateFleetInfo:OnTouchMoved(x, y)
  if GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchMoved(self, x, y)
  local dragItemId = GameObjectDraggerItem.DraggedItemID
  if dragItemId and dragItemId ~= -1 then
    GameObjectDraggerItem:UpdateDrag()
  end
end
function GameStateFleetInfo:OnTouchReleased(x, y)
  if GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  GameStateBase.OnTouchReleased(self, x, y)
  local dragItemId = GameObjectDraggerItem.DraggedItemID
  if dragItemId and dragItemId ~= -1 then
    if GameFleetInfoBag.dragItem then
      local onequipment = GameFleetInfoUI:GetFlashObject():InvokeASCallback("_root", "IsOnEquipment")
      DebugOut("onequipment:", type(onequipment), onequipment)
      if onequipment == "true" then
        GameObjectDraggerItem.equipItem = true
        local _, indexInList = GameFleetInfoBag:IsEquipmentInList(dragItemId)
        if indexInList then
          local itemType = GameFleetInfoBag.onlyEquip[indexInList].item_type
          local slot = GameDataAccessHelper:GetEquipSlot(itemType)
          local posInfo = GameFleetInfoUI:GetFlashObject():InvokeASCallback("_root", "GetSlotPos", slot)
          DebugOut("posInfo: ", posInfo)
          local ptPos = LuaUtils:string_split(posInfo, "\001")
          GameObjectDraggerItem:SetDestPosition(tonumber(ptPos[1]), tonumber(ptPos[2]))
        end
      end
    elseif GameFleetInfoUI.dragItem then
      local mouseInfo = GameFleetInfoUI:GetFlashObject():InvokeASCallback("_root", "GetMousePos")
      local mousePos = LuaUtils:string_split(mouseInfo, "\001")
      local onlist = GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "IsOnList", tonumber(mousePos[1]), tonumber(mousePos[2]))
      DebugOut("onlist: ", type(onlist), onlist)
      if onlist == "true" then
        GameObjectDraggerItem.unloadItem = true
        GameObjectDraggerItem:SetDestPosition(tonumber(mousePos[1]), tonumber(mousePos[2]))
      end
    end
    if not GameObjectDraggerItem.equipItem and not GameObjectDraggerItem.unloadItem then
      GameObjectDraggerItem.DraggedItemID = -1
    end
    DebugOut("dragItem: ", GameFleetInfoUI.dragItem, GameFleetInfoBag.dragItem)
    GameObjectDraggerItem:StartAutoMove()
  end
end
function GameStateFleetInfo:Update(dt)
  GameStateBase.Update(self, dt)
  if not GameObjectDraggerItem:IsAutoMove() and self.currentTab == self.CHARACTER then
    GameFleetInfoBag:GetFlashObject():InvokeASCallback("_root", "updateEquipGrid", dt)
    GameFleetInfoUI:GetFlashObject():InvokeASCallback("_root", "updateEquipGrid", dt)
  end
end
