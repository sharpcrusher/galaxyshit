local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
local QuestTutorialUseTechLab = TutorialQuestManager.QuestTutorialUseTechLab
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
require("FleetMatrix.tfl")
GameUITechnology.TechType = {}
GameUITechnology.TechType.Basic = 1
GameUITechnology.TechType.Advance = 2
GameUITechnology.TechType.Hardcore = 3
GameUITechnology.TechType.Surreal = 4
GameUITechnology.buildingName = "tech_lab"
function GameUITechnology:OnInitGame()
end
function GameUITechnology:InitFlashObject()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "setLocalText", "tech_type_initial", GameLoader:GetGameText("LC_MENU_TECH_INITIAL"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "tech_type_advanced", GameLoader:GetGameText("LC_MENU_TECH_ADVANCED"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "tech_type_hardcore", GameLoader:GetGameText("LC_MENU_TECH_HARDCORE"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "tech_type_surreal", GameLoader:GetGameText("LC_MENU_TECH_SURREAL"))
end
function GameUITechnology:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "select_tech_item" then
    local index_tech_item = tonumber(arg)
    local tech_info = GameUITechnology:GetTechTable(GameUITechnology._activeTab)[index_tech_item]
    assert(tech_info)
    GameUITechnology:SelectTechItem(index_tech_item)
    GameUITechnology:ShowTechInfo(tech_info)
    if GameUtils:GetTutorialHelp() then
      GameUtils:HideTutorialHelp()
    end
  elseif cmd == "select_tab" then
    self:SelectTab(arg)
  elseif cmd == "update_tech_data" then
    GameUITechnology:UpdateTechItemData(tonumber(arg))
  elseif cmd == "on_clicked_close" then
    GameUITechnology:MoveOut()
  elseif cmd == "animation_over_moveout" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    if self.m_tempStoryWhenClose then
      GameUICommonDialog:PlayStory(self.m_tempStoryWhenClose)
      self.m_tempStoryWhenClose = nil
    end
    if GameUITechnology.battle_failed then
      local param = {cli_data = "technology"}
      NetMessageMgr:SendMsg(NetAPIList.battle_rate_req.Code, param, nil, false, nil)
      GameUITechnology.battle_failed = nil
    end
  elseif cmd == "upgrade_tech_pos" then
    if GameUtils:GetTutorialHelp() then
      local param = LuaUtils:string_split(arg, "\001")
      GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_13"), GameUIMaskLayer.MaskStyle.small)
    end
  elseif cmd == "GoToMasterPage" then
    GameUIMaster.CurrentScope = {
      [1] = -1
    }
    GameUIMaster.CurrentSystemType = GameUIMaster.MasterSystem.TechnologyMaster
    GameUIMaster.CurrentPageType = self:FormatTechTable(self._activeTab)
    GameUIMaster.CurMatrixId = FleetMatrix.cur_index
    GameStateManager:GetCurrentGameState():AddObject(GameUIMaster)
  elseif cmd == "sendQuickUpdate" then
    local level = tonumber(arg)
    GameUITechnology:DoQuickUpdate(level)
  end
end
function GameUITechnology:GetActiveTab()
  return self._activeTab
end
function GameUITechnology:SelectTab(nameTab)
  self._activeTab = nameTab
  if not self._activeTab then
    self._activeTab = "Initial"
  end
  local tech_table = self:GetTechTable(self._activeTab)
  if not tech_table then
    return
  end
  local flash_obj = self:GetFlashObject()
  local numTechItem = 0
  if not tech_table then
    numTechItem = -1
  else
    numTechItem = #tech_table
  end
  if flash_obj ~= nil then
    flash_obj:InvokeASCallback("_root", "selectTab", self._activeTab, numTechItem)
  end
end
function GameUITechnology:SelectTechItem(index_tech)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "selectTechItem", index_tech)
end
function GameUITechnology:UpdateTechItemData(indexTech)
  local flash_obj = self:GetFlashObject()
  local list_tech = self:GetTechTable(self._activeTab)
  local techinfo = list_tech[indexTech]
  if techinfo then
    local tech_name = GameDataAccessHelper:GetTechName(techinfo.id)
    local resource_info = GameGlobalData:GetData("resource")
    local upgrade_enable = techinfo.enabled > 0 and resource_info.technique > techinfo.cost
    flash_obj:InvokeASCallback("_root", "updateTechItem", indexTech, techinfo.id, tech_name, GameLoader:GetGameText("LC_MENU_Level") .. techinfo.level, GameLoader:GetGameText("LC_MENU_Equip_param_" .. tostring(techinfo.attr_id)), tostring(techinfo.total), upgrade_enable)
  else
    flash_obj:InvokeASCallback("_root", "updateTechItem", indexTech, -1, -1, -1, -1)
  end
end
function GameUITechnology:UpgradeTech(indexTech)
  local tech_info = self:GetTechTable(self._activeTab)[indexTech]
  DebugTable(tech_info)
  if tech_info.enabled then
    NetMessageMgr:SendMsg(NetAPIList.techniques_upgrade_req.Code, {
      id = tech_info.id,
      want_level = 1
    }, self.serverCallback, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.techniques_upgrade_req.Code, {
      id = tech_info.id,
      want_level = 1
    }, self.serverCallback, true, nil)
  end
end
function GameUITechnology:UpgradeTechWithID(techID, wantlevel)
  NetMessageMgr:SendMsg(NetAPIList.techniques_upgrade_req.Code, {id = techID, want_level = wantlevel}, self.serverCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.techniques_upgrade_req.Code, {id = techID, want_level = wantlevel}, self.serverCallback, true, nil)
end
function GameUITechnology:ShowTechInfo(tech_data)
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  ItemBox:UpdateTechBoxInfo(tech_data)
  ItemBox:ShowTechBox()
  if QuestTutorialUseTechLab:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
  end
end
function GameUITechnology:UpdateTechPointText()
  local text_techpoint = ""
  local table_resource = GameGlobalData:GetData("resource")
  text_techpoint = GameUtils.numberConversion(table_resource.technique)
  local flash_obj = self:GetFlashObject()
  if flash_obj ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "setTechPoint", text_techpoint)
  end
end
function GameUITechnology:UpdatePowerPoint()
  local powerpoint = 0
  local battle_matrix = {}
  for _, v in ipairs(GameGlobalData:GetData("matrix").cells) do
    battle_matrix[v.cell_id] = v.fleet_identity
  end
  local powerpoint = 0
  for _, commander_id in pairs(battle_matrix) do
    if commander_id > 0 then
      local commander_data = GameGlobalData:GetFleetInfo(commander_id)
      powerpoint = powerpoint + commander_data.force
    end
  end
  DebugOut("ComputeBattleForce", powerpoint)
  if self.lastPowerPoint then
    local valuePowerUp = powerpoint - self.lastPowerPoint
    if valuePowerUp > 0 then
      self:GetFlashObject():InvokeASCallback("_root", "showPowerUpAnimation", valuePowerUp)
    end
  end
  self.lastPowerPoint = powerpoint
  powerpoint = GameUtils.numberConversion(powerpoint)
  self:GetFlashObject():InvokeASCallback("_root", "setPowerPoint", powerpoint)
end
function GameUITechnology:EnterTechnology()
  local building = GameGlobalData:GetBuildingInfo(self.buildingName)
  if building and building.level > 0 then
    GameStateManager:GetCurrentGameState():AddObject(GameUITechnology)
  elseif building and 0 < building.status then
    if GameStateManager:GetCurrentGameState() ~= GameStateMainPlanet then
      GameStateManager:SetCurrentGameState(GameStateMainPlanet)
    end
    GameUIBuilding:DisplayUpgradeDialog(self.buildingName)
  end
end
function GameUITechnology:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameGlobalData:RegisterDataChangeCallback("resource", self.resourceListener)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", self.fleetInfoListener)
  self.lastPowerPoint = nil
  self:UpdateTechPointText()
  self:UpdatePowerPoint()
  self:Visible(false)
  self:ReqTechnology(true)
  if not self:GetActiveTab() then
    self:SelectTab("Initial")
  end
  if QuestTutorialUseTechLab:IsActive() then
    local function callback()
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialUpgrade")
    end
    GameUICommonDialog:PlayStory({100006}, callback)
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
  end
  self:GetFlashObject():InvokeASCallback("_root", "HideTutorialClose")
  self:getUpgradeTechPos()
end
function GameUITechnology:ClearLocalData()
  self._activeTab = nil
  self._TechTable = nil
  self:UnloadFlashObject()
end
function GameUITechnology:OnEraseFromGameState(game_state)
  self:UnloadFlashObject()
  GameStateMainPlanet:CheckShowQuestInMainPlanet()
  GameGlobalData:RemoveDataChangeCallback("resource", self.resourceListener)
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", self.fleetInfoListener)
end
function GameUITechnology:ReqTechnology(isForceRequest)
  if isForceRequest or not self._TechTable then
    local function netFailedCallback()
      NetMessageMgr:SendMsg(NetAPIList.techniques_req.Code, nil, GameUITechnology.serverCallback, false, netFailedCallback)
    end
    NetMessageMgr:SendMsg(NetAPIList.techniques_req.Code, nil, GameUITechnology.serverCallback, false, netFailedCallback)
  end
end
function GameUITechnology:MoveIn()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn")
end
function GameUITechnology:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameUITechnology:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
end
function GameUITechnology:GetTechTable(techType)
  if not self._TechTable then
    return nil
  end
  if techType == "Initial" or techType == 1 then
    return self._TechTable.Initial
  elseif techType == "Advanced" or techType == 2 then
    return self._TechTable.Advanced
  elseif techType == "Hardcore" or techType == 3 then
    return self._TechTable.Hardcore
  elseif techType == "Surreal" or techType == 4 then
    return self._TechTable.Surreal
  else
    return nil
  end
end
function GameUITechnology:FormatTechTable(techType)
  if techType == "Initial" or techType == 1 then
    return GameUIMaster.MasterPage.tech_initial
  elseif techType == "Advanced" or techType == 2 then
    return GameUIMaster.MasterPage.tech_advanced
  elseif techType == "Hardcore" or techType == 3 then
    return GameUIMaster.MasterPage.tech_hardcore
  elseif techType == "Surreal" or techType == 4 then
    return GameUIMaster.MasterPage.tech_dimension
  else
    return GameUIMaster.MasterPage.tech_initial
  end
end
function GameUITechnology:GetCanUpgradeTechType()
  local resource = GameGlobalData:GetData("resource")
  if resource == nil then
    DebugOut("error! resource==nil")
    return nil
  end
  local techTypes = {
    "Surreal",
    "Hardcore",
    "Advanced",
    "Initial"
  }
  for _, v in ipairs(techTypes) do
    local tech_table = self:GetTechTable(v)
    if tech_table then
      for _, w in ipairs(tech_table) do
        if w.enabled == 1 and w.cost < resource.technique then
          return v
        end
      end
    end
  end
  return nil
end
function GameUITechnology:GetTechInfoWithID(techID)
  for _, v in ipairs(self._TechTable) do
    if v.id == techID then
      return v
    end
  end
  return nil
end
function GameUITechnology:GenerateTechTable(techniques)
  DebugOut("GenerateTechTable:techniques")
  DebugTable(techniques)
  self._TechTable = techniques
  self._TechTable.Initial = {}
  self._TechTable.Advanced = {visible = false}
  self._TechTable.Hardcore = {visible = false}
  self._TechTable.Surreal = {visible = false}
  local type_tech = 0
  for _, v in ipairs(techniques) do
    type_tech = tonumber(v.tech_type)
    local techTable = self:GetTechTable(type_tech)
    if techTable ~= nil then
      DebugOut("techTable!=nil")
      if 0 < v.level or 0 < v.enabled then
        table.insert(techTable, v)
        techTable.visible = true
      elseif v.level == 0 and (techTable.nextTargetTech == nil or v.level_limit < techTable.nextTargetTech.level_limit) then
        techTable.nextTargetTech = v
      end
    end
  end
  for _, techTable in pairs(self._TechTable) do
    if techTable.nextTargetTech then
      table.insert(techTable, techTable.nextTargetTech)
      techTable.nextTargetTech = nil
    end
  end
  table.sort(self._TechTable.Initial, function(a, b)
    return a.id < b.id
  end)
  table.sort(self._TechTable.Advanced, function(a, b)
    return a.id < b.id
  end)
  table.sort(self._TechTable.Hardcore, function(a, b)
    return a.id < b.id
  end)
  table.sort(self._TechTable.Surreal, function(a, b)
    return a.id < b.id
  end)
  DebugOut("after GenerateTechTable:")
  DebugOutPutTable(self._TechTable.Initial, "InitialTech")
  DebugOutPutTable(self._TechTable.Advanced, "AdvancedTech")
  DebugOutPutTable(self._TechTable.Hardcore, "HardcoreTech")
  DebugOutPutTable(self._TechTable.Surreal, "Surreal")
  self:Visible(self._TechTable.Advanced.visible, "Advanced")
  self:Visible(self._TechTable.Hardcore.visible, "Hardcore")
  self:Visible(self._TechTable.Surreal.visible, "Surreal")
end
function GameUITechnology:UpdateGameDataTech(data)
  local tech_update = self:GetTechInfoWithID(data.id)
  tech_update.level = data.level
  tech_update.enabled = data.enabled
  tech_update.total = data.total
  tech_update.cost = data.cost
  tech_update.attr_value = data.attr_value
  tech_update.level_limit = data.level_limit
  tech_update.player_level_limit = data.player_limit_level
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  if GameStateManager:GetCurrentGameState():IsObjectInState(ItemBox) then
    ItemBox:UpdateTechBoxInfo(tech_update)
  end
end
function GameUITechnology:NeedUpdateTechItem()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "needUpdateTechItem")
end
function GameUITechnology:Visible(visible, nameObject)
  nameObject = nameObject or -1
  local flash_obj = self:GetFlashObject()
  if flash_obj ~= nil then
    flash_obj:InvokeASCallback("_root", "setVisible", nameObject, visible)
  end
end
function GameUITechnology:AnimationPowerUp(valuePowerUp)
  self:GetFlashObject():InvokeASCallback("_root", "showPowerUpAnimation", valuePowerUp)
end
function GameUITechnology.serverCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and (content.api == NetAPIList.techniques_req.Code or content.api == NetAPIList.techniques_upgrade_req.Code) then
    if content.code ~= 0 then
      local GameObjectArcaneEnhance = LuaObjectManager:GetLuaObject("GameObjectArcaneEnhance")
      GameObjectArcaneEnhance:CheckIsNeedShowDialog(content.code)
    end
    return true
  end
  if msgtype == NetAPIList.techniques_ack.Code then
    GameUITechnology:GenerateTechTable(content.techniques)
    local flash_obj = GameUITechnology:GetFlashObject()
    if flash_obj ~= nil then
      GameUITechnology:SelectTab(GameUITechnology:GetCanUpgradeTechType())
      GameUITechnology:MoveIn()
    end
    return true
  end
  if msgtype == NetAPIList.techniques_upgrade_ack.Code then
    GameUITechnology:UpdateGameDataTech(content.technique)
    GameUITechnology:NeedUpdateTechItem()
    return true
  end
  return false
end
function GameUITechnology.resourceListener()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUITechnology) then
    GameUITechnology:UpdateTechPointText()
    GameUITechnology:RefreshResource()
  end
  if GameUITechnology then
    GameUITechnology:UpdateTechPointText()
  end
end
function GameUITechnology.fleetInfoListener()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUITechnology) then
    GameUITechnology:UpdatePowerPoint()
  end
end
function GameUITechnology:getUpgradeTechPos()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getUpgradeTechPos")
  end
end
function GameUITechnology:DoQuickUpdate(lv)
  local param = {
    id = GameUITechnology.curTechID,
    want_level = lv
  }
  NetMessageMgr:SendMsg(NetAPIList.techniques_upgrade_req.Code, param, self.serverCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.techniques_upgrade_req.Code, param, self.serverCallback, true, nil)
end
function GameUITechnology:OneKeyUpdate(techid)
  local list_tech = self:GetTechTable(self._activeTab)
  local techinfo = list_tech
  assert(techid)
  for k, v in pairs(list_tech) do
    if v.id == techid then
      techinfo = v
      break
    end
  end
  assert(techinfo, string.format("%d %s", techid, self._activeTab))
  local param = {}
  GameUITechnology.curTechID = techid
  param.equipIcon = "tech_" .. techinfo.id
  param.equipNameText = GameDataAccessHelper:GetTechName(techinfo.id)
  param.equipLevelNow = techinfo.level
  param.levelCanUp = techinfo.max_for_now - techinfo.level
  local resource = GameGlobalData:GetData("resource")
  local tech = resource.technique
  param.userHaveTech = tech
  param.levelupBaseGoldCast = techinfo.every_consume
  local attr_text = GameLoader:GetGameText("LC_MENU_Equip_param_" .. tostring(techinfo.attr_id))
  local attr_basic = techinfo.attr_value
  local attr_add_each = techinfo.attr_value
  param.attrsStr = string.format("%s,%d,%d", attr_text, attr_basic, attr_add_each)
  local gameitem = {item_type = "technique"}
  param.creditCost = GameGlobalData.GetCostCredit_a_b(gameitem)
  param.minCreditCost = GameGlobalData.GetMinCostCredit()
  GameUITechnology:GetFlashObject():InvokeASCallback("_root", "openQuickUpdateUI", param)
end
function GameUITechnology:RefreshResource()
  local resource = GameGlobalData:GetData("resource")
  local tech = resource.technique
  if GameUITechnology:GetFlashObject() then
    GameUITechnology:GetFlashObject():InvokeASCallback("_root", "updateUserGold", tech)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUITechnology.OnAndroidBack()
    GameUITechnology:MoveOut()
  end
end
