local Monsters = GameData.ChapterMonsterAct7.Monsters
Monsters[701001] = {
  ID = 701001,
  durability = 49402,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701002] = {
  ID = 701002,
  durability = 39582,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701003] = {
  ID = 701003,
  durability = 6847,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701004] = {
  ID = 701004,
  durability = 19941,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701005] = {
  ID = 701005,
  durability = 19940,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701006] = {
  ID = 701006,
  durability = 49401,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701007] = {
  ID = 701007,
  durability = 39583,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701008] = {
  ID = 701008,
  durability = 6847,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701009] = {
  ID = 701009,
  durability = 19941,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701010] = {
  ID = 701010,
  durability = 19940,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701011] = {
  ID = 701011,
  durability = 49402,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[701012] = {
  ID = 701012,
  durability = 39583,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701013] = {
  ID = 701013,
  durability = 6847,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701014] = {
  ID = 701014,
  durability = 19941,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[701015] = {
  ID = 701015,
  durability = 19941,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701016] = {
  ID = 701016,
  durability = 73953,
  Avatar = "head45",
  Ship = "ship39"
}
Monsters[701017] = {
  ID = 701017,
  durability = 39583,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701018] = {
  ID = 701018,
  durability = 6847,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701019] = {
  ID = 701019,
  durability = 19941,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701020] = {
  ID = 701020,
  durability = 19941,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701021] = {
  ID = 701021,
  durability = 49402,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701022] = {
  ID = 701022,
  durability = 39582,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701023] = {
  ID = 701023,
  durability = 6847,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701024] = {
  ID = 701024,
  durability = 19941,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701025] = {
  ID = 701025,
  durability = 19941,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701026] = {
  ID = 701026,
  durability = 49402,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701027] = {
  ID = 701027,
  durability = 39580,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701028] = {
  ID = 701028,
  durability = 6846,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701029] = {
  ID = 701029,
  durability = 19941,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701030] = {
  ID = 701030,
  durability = 19940,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701031] = {
  ID = 701031,
  durability = 52474,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701032] = {
  ID = 701032,
  durability = 42040,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701033] = {
  ID = 701033,
  durability = 7256,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701034] = {
  ID = 701034,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[701035] = {
  ID = 701035,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701036] = {
  ID = 701036,
  durability = 78559,
  Avatar = "head41",
  Ship = "ship39"
}
Monsters[701037] = {
  ID = 701037,
  durability = 42040,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[701038] = {
  ID = 701038,
  durability = 7256,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701039] = {
  ID = 701039,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[701040] = {
  ID = 701040,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702001] = {
  ID = 702001,
  durability = 52473,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702002] = {
  ID = 702002,
  durability = 42040,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702003] = {
  ID = 702003,
  durability = 7256,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702004] = {
  ID = 702004,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702005] = {
  ID = 702005,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702006] = {
  ID = 702006,
  durability = 52476,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702007] = {
  ID = 702007,
  durability = 42038,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702008] = {
  ID = 702008,
  durability = 7256,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702009] = {
  ID = 702009,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702010] = {
  ID = 702010,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702011] = {
  ID = 702011,
  durability = 52474,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702012] = {
  ID = 702012,
  durability = 42039,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702013] = {
  ID = 702013,
  durability = 7256,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702014] = {
  ID = 702014,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702015] = {
  ID = 702015,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702016] = {
  ID = 702016,
  durability = 78561,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters[702017] = {
  ID = 702017,
  durability = 42039,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[702018] = {
  ID = 702018,
  durability = 7256,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702019] = {
  ID = 702019,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702020] = {
  ID = 702020,
  durability = 21170,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[702021] = {
  ID = 702021,
  durability = 52474,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702022] = {
  ID = 702022,
  durability = 42039,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702023] = {
  ID = 702023,
  durability = 7256,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702024] = {
  ID = 702024,
  durability = 21170,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702025] = {
  ID = 702025,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702026] = {
  ID = 702026,
  durability = 52474,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702027] = {
  ID = 702027,
  durability = 42039,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702028] = {
  ID = 702028,
  durability = 7256,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702029] = {
  ID = 702029,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702030] = {
  ID = 702030,
  durability = 21169,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702031] = {
  ID = 702031,
  durability = 55546,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702032] = {
  ID = 702032,
  durability = 44496,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702033] = {
  ID = 702033,
  durability = 7666,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702034] = {
  ID = 702034,
  durability = 22398,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702035] = {
  ID = 702035,
  durability = 22398,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702036] = {
  ID = 702036,
  durability = 83169,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters[702037] = {
  ID = 702037,
  durability = 44498,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702038] = {
  ID = 702038,
  durability = 7666,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702039] = {
  ID = 702039,
  durability = 22399,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[702040] = {
  ID = 702040,
  durability = 22398,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[703001] = {
  ID = 703001,
  durability = 35288,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703002] = {
  ID = 703002,
  durability = 26818,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703003] = {
  ID = 703003,
  durability = 7666,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[703004] = {
  ID = 703004,
  durability = 11349,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703005] = {
  ID = 703005,
  durability = 15032,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703006] = {
  ID = 703006,
  durability = 35289,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703007] = {
  ID = 703007,
  durability = 26818,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703008] = {
  ID = 703008,
  durability = 7666,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703009] = {
  ID = 703009,
  durability = 11349,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703010] = {
  ID = 703010,
  durability = 15032,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703011] = {
  ID = 703011,
  durability = 35289,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703012] = {
  ID = 703012,
  durability = 26818,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703013] = {
  ID = 703013,
  durability = 7666,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703014] = {
  ID = 703014,
  durability = 11349,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703015] = {
  ID = 703015,
  durability = 15032,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703016] = {
  ID = 703016,
  durability = 52784,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters[703017] = {
  ID = 703017,
  durability = 26818,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703018] = {
  ID = 703018,
  durability = 7666,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703019] = {
  ID = 703019,
  durability = 11349,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703020] = {
  ID = 703020,
  durability = 15032,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703021] = {
  ID = 703021,
  durability = 35289,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703022] = {
  ID = 703022,
  durability = 26818,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703023] = {
  ID = 703023,
  durability = 7666,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[703024] = {
  ID = 703024,
  durability = 11349,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703025] = {
  ID = 703025,
  durability = 15032,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703026] = {
  ID = 703026,
  durability = 35288,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[703027] = {
  ID = 703027,
  durability = 26817,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703028] = {
  ID = 703028,
  durability = 7666,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703029] = {
  ID = 703029,
  durability = 11349,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703030] = {
  ID = 703030,
  durability = 15032,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703031] = {
  ID = 703031,
  durability = 37236,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703032] = {
  ID = 703032,
  durability = 28292,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703033] = {
  ID = 703033,
  durability = 8076,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703034] = {
  ID = 703034,
  durability = 11964,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703035] = {
  ID = 703035,
  durability = 15852,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703036] = {
  ID = 703036,
  durability = 55702,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters[703037] = {
  ID = 703037,
  durability = 28292,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703038] = {
  ID = 703038,
  durability = 8076,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703039] = {
  ID = 703039,
  durability = 11964,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[703040] = {
  ID = 703040,
  durability = 15851,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704001] = {
  ID = 704001,
  durability = 35291,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704002] = {
  ID = 704002,
  durability = 26737,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704003] = {
  ID = 704003,
  durability = 8075,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704004] = {
  ID = 704004,
  durability = 11963,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704005] = {
  ID = 704005,
  durability = 15851,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704006] = {
  ID = 704006,
  durability = 35292,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[704007] = {
  ID = 704007,
  durability = 26737,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704008] = {
  ID = 704008,
  durability = 8076,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704009] = {
  ID = 704009,
  durability = 11963,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[704010] = {
  ID = 704010,
  durability = 15851,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704011] = {
  ID = 704011,
  durability = 35290,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704012] = {
  ID = 704012,
  durability = 26737,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704013] = {
  ID = 704013,
  durability = 8075,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704014] = {
  ID = 704014,
  durability = 11963,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704015] = {
  ID = 704015,
  durability = 15851,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704016] = {
  ID = 704016,
  durability = 52786,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters[704017] = {
  ID = 704017,
  durability = 26737,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704018] = {
  ID = 704018,
  durability = 8075,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704019] = {
  ID = 704019,
  durability = 11963,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704020] = {
  ID = 704020,
  durability = 15852,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704021] = {
  ID = 704021,
  durability = 35291,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704022] = {
  ID = 704022,
  durability = 26737,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704023] = {
  ID = 704023,
  durability = 8075,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704024] = {
  ID = 704024,
  durability = 11963,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704025] = {
  ID = 704025,
  durability = 15851,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704026] = {
  ID = 704026,
  durability = 35292,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704027] = {
  ID = 704027,
  durability = 26738,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704028] = {
  ID = 704028,
  durability = 8075,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704029] = {
  ID = 704029,
  durability = 11963,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[704030] = {
  ID = 704030,
  durability = 15851,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704031] = {
  ID = 704031,
  durability = 37134,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704032] = {
  ID = 704032,
  durability = 28129,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[704033] = {
  ID = 704033,
  durability = 8485,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704034] = {
  ID = 704034,
  durability = 12577,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704035] = {
  ID = 704035,
  durability = 16670,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704036] = {
  ID = 704036,
  durability = 55552,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters[704037] = {
  ID = 704037,
  durability = 28130,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704038] = {
  ID = 704038,
  durability = 8485,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704039] = {
  ID = 704039,
  durability = 12578,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[704040] = {
  ID = 704040,
  durability = 16671,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705001] = {
  ID = 705001,
  durability = 37135,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705002] = {
  ID = 705002,
  durability = 28130,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705003] = {
  ID = 705003,
  durability = 8485,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705004] = {
  ID = 705004,
  durability = 12578,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705005] = {
  ID = 705005,
  durability = 16670,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705006] = {
  ID = 705006,
  durability = 37134,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705007] = {
  ID = 705007,
  durability = 28131,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705008] = {
  ID = 705008,
  durability = 8485,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705009] = {
  ID = 705009,
  durability = 12578,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705010] = {
  ID = 705010,
  durability = 16670,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705011] = {
  ID = 705011,
  durability = 37135,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705012] = {
  ID = 705012,
  durability = 28130,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[705013] = {
  ID = 705013,
  durability = 8485,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705014] = {
  ID = 705014,
  durability = 12578,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705015] = {
  ID = 705015,
  durability = 16670,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[705016] = {
  ID = 705016,
  durability = 55550,
  Avatar = "head36",
  Ship = "ship39"
}
Monsters[705017] = {
  ID = 705017,
  durability = 28131,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705018] = {
  ID = 705018,
  durability = 8485,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705019] = {
  ID = 705019,
  durability = 12577,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705020] = {
  ID = 705020,
  durability = 16670,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705021] = {
  ID = 705021,
  durability = 37134,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705022] = {
  ID = 705022,
  durability = 28130,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705023] = {
  ID = 705023,
  durability = 8485,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705024] = {
  ID = 705024,
  durability = 12578,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705025] = {
  ID = 705025,
  durability = 16670,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705026] = {
  ID = 705026,
  durability = 37135,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705027] = {
  ID = 705027,
  durability = 28129,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705028] = {
  ID = 705028,
  durability = 8485,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705029] = {
  ID = 705029,
  durability = 12578,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705030] = {
  ID = 705030,
  durability = 16670,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705031] = {
  ID = 705031,
  durability = 40821,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705032] = {
  ID = 705032,
  durability = 30916,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705033] = {
  ID = 705033,
  durability = 9304,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705034] = {
  ID = 705034,
  durability = 13807,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705035] = {
  ID = 705035,
  durability = 18309,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[705036] = {
  ID = 705036,
  durability = 61079,
  Avatar = "head32",
  Ship = "ship39"
}
Monsters[705037] = {
  ID = 705037,
  durability = 30914,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705038] = {
  ID = 705038,
  durability = 9304,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[705039] = {
  ID = 705039,
  durability = 13806,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[705040] = {
  ID = 705040,
  durability = 18309,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[706001] = {
  ID = 706001,
  durability = 40820,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706002] = {
  ID = 706002,
  durability = 30916,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706003] = {
  ID = 706003,
  durability = 9304,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706004] = {
  ID = 706004,
  durability = 13807,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706005] = {
  ID = 706005,
  durability = 18309,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706006] = {
  ID = 706006,
  durability = 40820,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706007] = {
  ID = 706007,
  durability = 30915,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706008] = {
  ID = 706008,
  durability = 9304,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706009] = {
  ID = 706009,
  durability = 13806,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706010] = {
  ID = 706010,
  durability = 18308,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706011] = {
  ID = 706011,
  durability = 40820,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706012] = {
  ID = 706012,
  durability = 30915,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706013] = {
  ID = 706013,
  durability = 9304,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706014] = {
  ID = 706014,
  durability = 13806,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706015] = {
  ID = 706015,
  durability = 18309,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706016] = {
  ID = 706016,
  durability = 61079,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706017] = {
  ID = 706017,
  durability = 30915,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706018] = {
  ID = 706018,
  durability = 9304,
  Avatar = "head37",
  Ship = "ship37"
}
Monsters[706019] = {
  ID = 706019,
  durability = 13806,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706020] = {
  ID = 706020,
  durability = 18309,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706021] = {
  ID = 706021,
  durability = 40819,
  Avatar = "head37",
  Ship = "ship37"
}
Monsters[706022] = {
  ID = 706022,
  durability = 30916,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706023] = {
  ID = 706023,
  durability = 9304,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706024] = {
  ID = 706024,
  durability = 13806,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706025] = {
  ID = 706025,
  durability = 18309,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706026] = {
  ID = 706026,
  durability = 40821,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706027] = {
  ID = 706027,
  durability = 30915,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706028] = {
  ID = 706028,
  durability = 9304,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706029] = {
  ID = 706029,
  durability = 13806,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706030] = {
  ID = 706030,
  durability = 18309,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706031] = {
  ID = 706031,
  durability = 44508,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706032] = {
  ID = 706032,
  durability = 33700,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706033] = {
  ID = 706033,
  durability = 10123,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706034] = {
  ID = 706034,
  durability = 15036,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706035] = {
  ID = 706035,
  durability = 19947,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706036] = {
  ID = 706036,
  durability = 66610,
  Avatar = "head33",
  Ship = "ship39"
}
Monsters[706037] = {
  ID = 706037,
  durability = 33700,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706038] = {
  ID = 706038,
  durability = 10123,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706039] = {
  ID = 706039,
  durability = 15035,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[706040] = {
  ID = 706040,
  durability = 19948,
  Avatar = "head37",
  Ship = "ship39"
}
Monsters[707001] = {
  ID = 707001,
  durability = 44507,
  Avatar = "head44",
  Ship = "ship37"
}
Monsters[707002] = {
  ID = 707002,
  durability = 33701,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707003] = {
  ID = 707003,
  durability = 10123,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707004] = {
  ID = 707004,
  durability = 15035,
  Avatar = "head44",
  Ship = "ship37"
}
Monsters[707005] = {
  ID = 707005,
  durability = 19947,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707006] = {
  ID = 707006,
  durability = 44507,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707007] = {
  ID = 707007,
  durability = 33700,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707008] = {
  ID = 707008,
  durability = 10124,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707009] = {
  ID = 707009,
  durability = 15035,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707010] = {
  ID = 707010,
  durability = 19947,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707011] = {
  ID = 707011,
  durability = 44507,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707012] = {
  ID = 707012,
  durability = 33700,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707013] = {
  ID = 707013,
  durability = 10123,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707014] = {
  ID = 707014,
  durability = 15036,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707015] = {
  ID = 707015,
  durability = 19948,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707016] = {
  ID = 707016,
  durability = 66610,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707017] = {
  ID = 707017,
  durability = 33700,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707018] = {
  ID = 707018,
  durability = 10123,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707019] = {
  ID = 707019,
  durability = 15035,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707020] = {
  ID = 707020,
  durability = 19947,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707021] = {
  ID = 707021,
  durability = 44508,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707022] = {
  ID = 707022,
  durability = 33700,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707023] = {
  ID = 707023,
  durability = 10123,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707024] = {
  ID = 707024,
  durability = 15035,
  Avatar = "head44",
  Ship = "ship37"
}
Monsters[707025] = {
  ID = 707025,
  durability = 19947,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707026] = {
  ID = 707026,
  durability = 44507,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707027] = {
  ID = 707027,
  durability = 33701,
  Avatar = "head44",
  Ship = "ship37"
}
Monsters[707028] = {
  ID = 707028,
  durability = 10123,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707029] = {
  ID = 707029,
  durability = 15035,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707030] = {
  ID = 707030,
  durability = 19947,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707031] = {
  ID = 707031,
  durability = 48194,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707032] = {
  ID = 707032,
  durability = 36486,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707033] = {
  ID = 707033,
  durability = 10943,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707034] = {
  ID = 707034,
  durability = 16264,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707035] = {
  ID = 707035,
  durability = 21586,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707036] = {
  ID = 707036,
  durability = 72138,
  Avatar = "head40",
  Ship = "ship39"
}
Monsters[707037] = {
  ID = 707037,
  durability = 36486,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707038] = {
  ID = 707038,
  durability = 10943,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707039] = {
  ID = 707039,
  durability = 16264,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[707040] = {
  ID = 707040,
  durability = 21586,
  Avatar = "head44",
  Ship = "ship39"
}
Monsters[708001] = {
  ID = 708001,
  durability = 48193,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708002] = {
  ID = 708002,
  durability = 36486,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708003] = {
  ID = 708003,
  durability = 10942,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708004] = {
  ID = 708004,
  durability = 16264,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708005] = {
  ID = 708005,
  durability = 21586,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708006] = {
  ID = 708006,
  durability = 48192,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708007] = {
  ID = 708007,
  durability = 36485,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[708008] = {
  ID = 708008,
  durability = 10942,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708009] = {
  ID = 708009,
  durability = 16264,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708010] = {
  ID = 708010,
  durability = 21586,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[708011] = {
  ID = 708011,
  durability = 48194,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708012] = {
  ID = 708012,
  durability = 36486,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708013] = {
  ID = 708013,
  durability = 10942,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708014] = {
  ID = 708014,
  durability = 16264,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708015] = {
  ID = 708015,
  durability = 21586,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708016] = {
  ID = 708016,
  durability = 72138,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708017] = {
  ID = 708017,
  durability = 36486,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708018] = {
  ID = 708018,
  durability = 10943,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708019] = {
  ID = 708019,
  durability = 16264,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708020] = {
  ID = 708020,
  durability = 21585,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708021] = {
  ID = 708021,
  durability = 48192,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708022] = {
  ID = 708022,
  durability = 36486,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708023] = {
  ID = 708023,
  durability = 10943,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708024] = {
  ID = 708024,
  durability = 16264,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708025] = {
  ID = 708025,
  durability = 21586,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708026] = {
  ID = 708026,
  durability = 48194,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708027] = {
  ID = 708027,
  durability = 36485,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708028] = {
  ID = 708028,
  durability = 10943,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708029] = {
  ID = 708029,
  durability = 16264,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708030] = {
  ID = 708030,
  durability = 21586,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[708031] = {
  ID = 708031,
  durability = 51880,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708032] = {
  ID = 708032,
  durability = 39270,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708033] = {
  ID = 708033,
  durability = 11762,
  Avatar = "head46",
  Ship = "ship37"
}
Monsters[708034] = {
  ID = 708034,
  durability = 17493,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708035] = {
  ID = 708035,
  durability = 23224,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708036] = {
  ID = 708036,
  durability = 77669,
  Avatar = "head38",
  Ship = "ship39"
}
Monsters[708037] = {
  ID = 708037,
  durability = 39271,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708038] = {
  ID = 708038,
  durability = 11762,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708039] = {
  ID = 708039,
  durability = 17493,
  Avatar = "head46",
  Ship = "ship39"
}
Monsters[708040] = {
  ID = 708040,
  durability = 23224,
  Avatar = "head46",
  Ship = "ship39"
}
