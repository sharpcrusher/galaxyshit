local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateLab = GameStateManager.GameStateLab
local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local QuestTutorialMine = TutorialQuestManager.QuestTutorialMine
local QuestTutorialSlot = TutorialQuestManager.QuestTutorialSlot
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIWorldChampion = LuaObjectManager:GetLuaObject("GameUIWorldChampion")
local GameUICrusade = require("data1/GameUICrusade.tfl")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local QuestTutorialInfiniteCosmos = TutorialQuestManager.QuestTutorialInfiniteCosmos
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameStateFairArena = GameStateManager.GameStateFairArena
GameUILab.mTimerWork = false
GameUILab.content = nil
GameUILab.baseTime = 0
local ItemStatusFrameKeys = {
  [1] = "normal",
  [2] = "today_unused",
  [3] = "new_feature",
  [4] = "locked"
}
local ItemStatus = {
  NORMAL = 1,
  TODAY_UNUSED = 2,
  NEW_FEATURE = 3,
  LOCKED = 4,
  NOT_OPEN = 5
}
local m_ListItems = {
  {
    baseKey = 1,
    pos = 1,
    itemKey = "slot",
    status = ItemStatus.LOCKED,
    targetTime = 0
  },
  {
    baseKey = 1,
    pos = 2,
    itemKey = "mining",
    status = ItemStatus.LOCKED,
    targetTime = 0
  },
  {
    baseKey = 2,
    pos = 3,
    itemKey = "infinite_cosmos",
    status = ItemStatus.LOCKED,
    targetTime = 0
  },
  {
    baseKey = 2,
    pos = 4,
    itemKey = "worldboss",
    status = ItemStatus.LOCKED,
    targetTime = 0
  },
  {
    baseKey = 3,
    pos = 5,
    itemKey = "ac",
    status = ItemStatus.LOCKED,
    targetTime = 0
  },
  {
    baseKey = 3,
    pos = 6,
    itemKey = "crusade",
    status = ItemStatus.LOCKED,
    targetTime = 0
  },
  {
    baseKey = 4,
    pos = 7,
    itemKey = "wc",
    status = ItemStatus.NORMAL,
    targetTime = 0
  },
  {
    baseKey = 4,
    pos = 8,
    itemKey = "empire_colosseum",
    status = ItemStatus.NORMAL,
    targetTime = 0
  }
}
function GameUILab:FindItemByKey(itemKey)
  for i, v in ipairs(m_ListItems) do
    if v.itemKey == itemKey then
      return v
    end
  end
  return nil
end
function GameUILab:SetItemStatus(itemKey, status)
  for i, v in ipairs(m_ListItems) do
    if v.itemKey == itemKey then
      v.status = status
      v.targetTime = nil
      DebugOut("v = ")
      DebugTable(v)
    end
  end
end
function GameUILab:FindListItemByBaseKey(baseKey)
  local l, r
  for i, v in ipairs(m_ListItems) do
    if v.baseKey == tonumber(baseKey) then
      if (v.pos + 1) % 2 == 0 then
        l = v
      elseif (v.pos + 1) % 2 == 1 then
        r = v
      end
    end
  end
  return l, r
end
local m_TipText = ""
function GameUILab:OnInitGame()
  DebugOut("GameUILab:OnInitGame")
  GameGlobalData:RegisterDataChangeCallback("modules_status", self.OnItemStatusChange)
end
function GameUILab.OnItemStatusChange()
  local modules = GameGlobalData:GetData("modules_status").modules
  local needUpdate = false
  for _, v in pairs(modules) do
    for _, w in pairs(m_ListItems) do
      if v.name == w.itemKey and v.status == true then
        needUpdate = true
        w.status = w.status == ItemStatus.LOCKED and ItemStatus.NEW_FEATURE or w.status
      end
      if v.name == w.itemKey and v.name == "prime_wve" then
        w.levelEnough = v.status
        if w.levelEnough then
          if 0 == tonumber(w.count_down) then
            w.status = ItemStatus.NORMAL
          elseif 0 < tonumber(w.count_down) then
            w.status = ItemStatus.NORMAL
            w.targetTime = math.abs(tonumber(w.count_down)) + os.time()
          else
            w.targetTime = nil
            w.status = ItemStatus.NOT_OPEN
          end
        else
          w.status = ItemStatus.LOCKED
        end
      end
    end
  end
  if needUpdate and GameUILab.content then
    GameUILab.DexterLibsNotifyHandler(GameUILab.content)
  end
  GameUILab:UpdateInitList()
end
function GameUILab:GetLibsItemByName(name)
  if GameUILab.libs then
    for k, v in pairs(GameUILab.libs or {}) do
      if v.dexter_name == name then
        return v
      end
    end
  end
  return nil
end
function GameUILab.DexterLibsNotifyHandler(content)
  DebugOut("GameUILab.DexterLibsNotifyHandler")
  DebugTable(content)
  GameUILab.content = content
  local libs = content.libs
  GameUILab.libs = libs
  GameUILab.baseTime = os.time()
  local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
  GameObjectMainPlanet:UpdatePrimeWveActivityStatus()
  local modules_status = GameGlobalData:GetData("modules_status")
  if modules_status == nil then
    DebugOut("\229\156\168modules_status \229\136\157\229\167\139\229\140\150\228\185\139\229\137\141\230\148\182\229\136\176 dexter_libs_ntf \230\182\136\230\129\175\239\188\129")
    return
  end
  local modules = modules_status.modules
  local function findModuleByName(name)
    for index, modu in ipairs(modules) do
      if modu.name == name then
        return modu
      end
    end
    return nil
  end
  for _, lib in pairs(libs) do
    for _, item in pairs(m_ListItems) do
      if lib.dexter_name == item.itemKey then
        local found = false
        local aModule = findModuleByName(lib.dexter_name)
        if nil ~= aModule then
          if "prime_wve" == item.itemKey then
            item.levelEnough = aModule.status
          else
            if lib.count_down >= 0 then
              item.targetTime = lib.count_down + os.time()
            end
            if aModule.status == true then
              if 0 < lib.used_cnt then
                item.status = ItemStatus.NORMAL
              else
                item.status = ItemStatus.TODAY_UNUSED
              end
              if "crusade" == item.itemKey then
                if 1 == lib.count_down then
                  item.status = ItemStatus.NORMAL
                else
                  item.status = ItemStatus.NOT_OPEN
                end
              elseif "wc" == item.itemKey then
                if 0 == lib.count_down then
                  item.status = ItemStatus.NORMAL
                elseif lib.count_down > 0 then
                  item.targetTime = lib.count_down + os.time()
                  item.status = ItemStatus.NORMAL
                else
                  item.status = ItemStatus.NOT_OPEN
                end
              elseif "infinite_cosmos" == item.itemKey then
                if 1 == lib.count_down then
                  item.status = ItemStatus.NORMAL
                else
                  item.status = ItemStatus.NOT_OPEN
                end
              elseif "starwar" == item.itemKey then
                if lib.count_down > 0 then
                  item.status = ItemStatus.NORMAL
                else
                  item.status = ItemStatus.NOT_OPEN
                end
              elseif "empire_colosseum" == item.itemKey then
                if 0 == lib.count_down then
                  item.status = ItemStatus.NORMAL
                elseif lib.count_down > 0 then
                  item.targetTime = lib.count_down + os.time()
                  item.status = ItemStatus.NORMAL
                else
                  item.status = ItemStatus.NOT_OPEN
                end
              else
                if "large_map" == item.itemKey then
                  if lib.count_down >= 0 then
                    item.status = ItemStatus.NORMAL
                  else
                    item.status = ItemStatus.NOT_OPEN
                    else
                      item.status = ItemStatus.LOCKED
                    end
                  end
                else
                end
              end
            found = true
          end
        end
        if "prime_wve" == item.itemKey then
          DebugOut("prime_wve:count_down:", lib.count_down)
          item.count_down = lib.count_down
          if item.levelEnough then
            if 0 == tonumber(lib.count_down) then
              item.status = ItemStatus.NORMAL
            elseif 0 < tonumber(lib.count_down) then
              item.status = ItemStatus.NORMAL
              item.targetTime = math.abs(tonumber(lib.count_down)) + os.time()
            else
              item.targetTime = nil
              item.status = ItemStatus.NOT_OPEN
            end
          else
            item.status = ItemStatus.LOCKED
          end
        end
        if not found then
          if "crusade" == item.itemKey then
            if 1 == lib.count_down then
              item.status = ItemStatus.NORMAL
            else
              item.status = ItemStatus.LOCKED
            end
          elseif "wc" == item.itemKey then
            if 0 == lib.count_down then
              item.status = ItemStatus.NORMAL
            elseif lib.count_down > 0 then
              item.targetTime = lib.count_down + os.time()
              item.status = ItemStatus.NORMAL
            else
              item.status = ItemStatus.LOCKED
            end
          elseif "infinite_cosmos" == item.itemKey then
            if 1 == lib.count_down then
              item.status = ItemStatus.NORMAL
            else
              item.status = ItemStatus.LOCKED
            end
          elseif "empire_colosseum" == item.itemKey then
            if 0 == lib.count_down then
              item.status = ItemStatus.NORMAL
            elseif lib.count_down > 0 then
              item.targetTime = lib.count_down + os.time()
              item.status = ItemStatus.NORMAL
            else
              item.status = ItemStatus.NOT_OPEN
            end
          elseif "large_map" == item.itemKey then
            if lib.count_down >= 0 then
              item.status = ItemStatus.NORMAL
            else
              item.status = ItemStatus.LOCKED
            end
          else
            DebugOut("\229\156\168modules_status.modules\232\161\168\228\184\173\230\137\190\228\184\141\229\136\176name=" .. lib.dexter_name .. "\231\154\132\233\161\185\239\188\129\228\184\165\233\135\141\233\148\153\232\175\175")
            DebugTable(modules)
          end
        end
      end
    end
  end
  GameUIBarLeft:UpdateLabBtn()
  GameUILab:UpdateInitList()
end
function GameUILab:OnAddToGameState(game_state)
  DebugOut("GameUILab:OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "tryLuck" then
    local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
    local pos = self:GetFlashObject():InvokeASCallback("_root", "getUIPos", "B_2")
    local pos_s = LuaUtils:string_split(pos, "\001")
    GameUtils:ShowMaskLayer(tonumber(pos_s[1]), tonumber(pos_s[2]), GameUIMaskLayer.ButtonPos.left, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_22"), GameUIMaskLayer.MaskStyle.big)
  end
  if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "getMoney" then
    local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
    local pos = self:GetFlashObject():InvokeASCallback("_root", "getUIPos", "B_1")
    local pos_s = LuaUtils:string_split(pos, "\001")
    local tipsText = GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_20")
    if immanentversion170 == 4 or immanentversion170 == 5 then
      tipsText = GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_20_NEW")
    end
    GameUtils:ShowMaskLayer(tonumber(pos_s[1]), tonumber(pos_s[2]), GameUIMaskLayer.ButtonPos.left, tipsText, GameUIMaskLayer.MaskStyle.big)
  end
  GameUICrusade:Init(self:GetFlashObject())
  if GameStateLab.mCurSubState == STATE_LAB_SUB_STATE.SUBSTATE_CRUSADE then
    GameUICrusade:RequestCheckFirstEnterToday()
  else
    DebugOut("ajksdkahsdk:", GameStateLab.mCurSubState)
    GameStateLab:GoToSubState(GameStateLab.mCurSubState)
  end
end
function GameUILab:EnterLab()
  DebugOut("GameUILab:EnterLab")
  self:Init()
  self:ShowLabMenu()
  if QuestTutorialSlot:IsActive() then
    if immanentversion == 1 then
      GameUICommonDialog:PlayStory({
        100061,
        100062,
        100063
      }, nil)
    elseif immanentversion == 2 then
      GameUICommonDialog:PlayStory({51214}, nil)
    end
  elseif QuestTutorialInfiniteCosmos:IsActive() then
    GameUICommonDialog:PlayStory({1100054}, nil)
  elseif QuestTutorialMine:IsActive() then
    AddFlurryEvent("TutorialMine_EnterMine", {}, 2)
    GameUICommonDialog:PlayStory({100065}, nil)
  end
  if not GameUILab.mTimerWork then
    GameTimer:Add(GameUILab._OnTimerTick, 1000)
    GameUILab.mTimerWork = true
  end
end
function GameUILab:OnEraseFromGameState(game_state)
  GameUICrusade:Destory()
  self:Clear()
  self:UnloadFlashObject()
end
function GameUILab:Init()
  DebugOut("GameUILab:Init")
  math.randomseed(os.time())
  GameUILab:GetFlashObject():InvokeASCallback("_root", "InitList")
end
function GameUILab:Clear()
  GameUILab:GetFlashObject():InvokeASCallback("_root", "ClearList")
end
function GameUILab:ShowLabMenu()
  GameUILab:SortList()
  GameUILab:UpdateInitList()
  GameUILab:UpdateTipText()
  GameUILab:GetFlashObject():InvokeASCallback("_root", "ShowLabMenu")
end
function GameUILab:HideLabMenu()
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
end
function GameUILab:SortList()
  DebugOut("GameUILab:SortList")
  local SortComp = function(a, b)
    return a.pos < b.pos
  end
  table.sort(m_ListItems, SortComp)
end
function GameUILab:FindListItemByItemKey(itemKey)
  for i, v in ipairs(m_ListItems) do
    if v.itemKey == itemKey then
      return v
    end
  end
  return nil
end
function GameUILab:GetTodayUnusedCnt()
  local cnt = GameGlobalData:GetRedPointStatByKey(100000009) + GameGlobalData:GetRedPointStatByKey(100000003)
  return cnt
end
function GameUILab:UpdateInitList()
  if not self:GetFlashObject() then
    return
  end
  GameUILab:GetFlashObject():InvokeASCallback("_root", "ReInitList")
  local baseKeyFlg = {}
  for i, v in pairs(m_ListItems) do
    if not baseKeyFlg[v.baseKey] then
      GameUILab:GetFlashObject():InvokeASCallback("_root", "InsertItem", v.baseKey, v.baseKey)
      baseKeyFlg[v.baseKey] = true
    end
  end
end
function GameUILab:UpdateTipText()
  local curItem = null
  for i, v in ipairs(m_ListItems) do
    if v.status == ItemStatus.NEW_FEATURE then
      curItem = v
      break
    end
  end
  if curItem ~= null then
    m_TipText = GameLoader:GetGameText("LC_STORY_DEXTAR_STORY_" .. curItem.itemKey)
  else
    local x = math.random(1, 7)
    m_TipText = GameLoader:GetGameText("LC_STORY_DEXTAR_STORY_" .. x)
  end
  local LoginInfo = GameUtils:GetLoginInfo()
  local userInfo = LoginInfo.PlayerInfo
  if userInfo ~= nil then
    m_TipText = string.gsub(m_TipText, "<player>", GameUtils:GetUserDisplayName(userInfo.name))
  end
  DebugOut("SetTipText = ", m_TipText)
  GameUILab:GetFlashObject():InvokeASCallback("_root", "SetTipText", m_TipText)
end
function GameUILab:Update(dt)
  local flashObj = self:GetFlashObject()
  if flashObj then
    if GameStateLab.mCurSubState == STATE_LAB_SUB_STATE.SUBSTATE_NORMAL then
      flashObj:InvokeASCallback("_root", "OnUpdateList")
    end
    GameUICrusade:MapFrameUpdate(dt)
    flashObj:Update(dt)
  else
    DebugOut("GameUILab:Update flash is nil.")
  end
end
function GameUILab:UpdateItemBaseData(baseKey)
  local listItemL, listItemR = GameUILab:FindListItemByBaseKey(baseKey)
  local tmpList = {}
  table.insert(tmpList, listItemL)
  table.insert(tmpList, listItemR)
  for k, v in pairs(tmpList) do
    if v then
      local itemPic = v.itemKey
      local statusKey = "locked"
      if v.status ~= ItemStatus.LOCKED then
        statusKey = "normal"
      end
      local isDark = v.status == ItemStatus.LOCKED or v.status == ItemStatus.NOT_OPEN
      local countDown = 0
      local needShowTutorial = false
      if QuestTutorialSlot:IsActive() and v.itemKey == "slot" then
        needShowTutorial = true
      end
      if QuestTutorialInfiniteCosmos:IsActive() and v.itemKey == "infinite_cosmos" then
        needShowTutorial = true
      end
      if QuestTutorialMine:IsActive() and v.itemKey == "mining" then
        needShowTutorial = true
      end
      local indexItemName = ""
      local unlockText = ""
      if v.itemKey == "slot" then
        indexItemName = GameLoader:GetGameText("LC_MENU_SLOT_BUTTON")
        unlockText = GameLoader:GetGameText("LC_MENU_DEXTER_CHAPTER")
        if "" == unlockText or nil == unlockText then
          unlockText = "(capter)"
        end
      elseif v.itemKey == "mining" then
        indexItemName = GameLoader:GetGameText("LC_MENU_MINING_BUTTON")
        unlockText = GameLoader:GetGameText("LC_MENU_DEXTER_CHAPTER")
        if "" == unlockText or nil == unlockText then
          unlockText = "(capter)"
        end
      elseif v.itemKey == "ac" then
        indexItemName = GameLoader:GetGameText("LC_MENU_ARCANE_CELESTIAL_TITLE")
        local playerLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("ac")
        unlockText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevel)
        unlockText = "(" .. unlockText .. ")"
      elseif v.itemKey == "worldboss" then
        indexItemName = GameLoader:GetGameText("LC_MENU_PRIMUS_TITLE")
        local playerLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("worldboss")
        unlockText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevel)
        unlockText = "(" .. unlockText .. ")"
      elseif v.itemKey == "crusade" then
        indexItemName = GameLoader:GetGameText("LC_MENU_LABORATORY_CHAOS_QUASAR")
        local playerLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("crusade")
        unlockText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevel)
        unlockText = "(" .. unlockText .. ")"
      elseif v.itemKey == "wc" then
        indexItemName = GameLoader:GetGameText("LC_MENU_SA_ARENR_TITLE")
      elseif v.itemKey == "infinite_cosmos" then
        indexItemName = GameLoader:GetGameText("LC_MENU_LABORATORY_INFINITE_UNIVERSE")
        local playerLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("infinite_cosmos")
        unlockText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevel)
        unlockText = "(" .. unlockText .. ")"
      elseif v.itemKey == "prime_wve" then
        indexItemName = GameLoader:GetGameText("LC_MENU_LABORATORY_PRIMUS_INVASION")
        local playerLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("prime_wve")
        unlockText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevel)
        unlockText = "(" .. unlockText .. ")"
      elseif v.itemKey == "starwar" then
        indexItemName = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_TITLE")
        local playerLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("starwar")
        unlockText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevel)
        unlockText = "(" .. unlockText .. ")"
      elseif v.itemKey == "empire_colosseum" then
        indexItemName = GameLoader:GetGameText("LC_MENU_EMPIRE_COLOSSEUM_TITLE")
        local playerLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("empire_colosseum")
        unlockText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevel)
        unlockText = "(" .. unlockText .. ")"
      elseif v.itemKey == "large_map" then
        indexItemName = GameLoader:GetGameText("LC_MENU_EMPIRE_COLOSSEUM_TITLE_MAP")
        local playerLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("large_map")
        unlockText = string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevel)
        unlockText = "(" .. unlockText .. ")"
      end
      DebugOut(" unlock Text: " .. unlockText)
      local subIdx = (v.pos + 1) % 2
      local lang = "en"
      if GameSettingData and GameSettingData.Save_Lang then
        lang = GameSettingData.Save_Lang
        if string.find(lang, "ru") == 1 then
          lang = "ru"
        elseif GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese() then
          lang = "zh"
        end
      end
      local hasNews = false
      if v.itemKey == "infinite_cosmos" then
        hasNews = 0 < GameGlobalData:GetRedPointStatByKey(100000009)
      elseif v.itemKey == "empire_colosseum" then
        hasNews = 0 < GameGlobalData:GetRedPointStatByKey(100000003)
      end
      GameUILab:GetFlashObject():InvokeASCallback("_root", "UpdateItem", v.baseKey, itemPic, statusKey, isDark, needShowTutorial, indexItemName, subIdx, unlockText, lang, hasNews)
      GameUILab:UpdateItemCDTime(v.itemKey)
    end
  end
end
function GameUILab:RefreshRedPoint()
  if self:GetFlashObject() then
    local infinite_cosmos_hasNews = GameGlobalData:GetRedPointStatByKey(100000009) > 0
    local empire_colosseum_hasNews = 0 < GameGlobalData:GetRedPointStatByKey(100000003)
    self:GetFlashObject():InvokeASCallback("_root", "RefreshRedPoint", infinite_cosmos_hasNews, empire_colosseum_hasNews)
  end
end
function GameUILab:UpdateItemCDTime(itemKey)
  local listItem = GameUILab:FindListItemByItemKey(itemKey)
  local lefttime = 0
  if listItem.targetTime then
    lefttime = listItem.targetTime - os.time()
  end
  local timeString = -1
  if lefttime <= 0 then
    lefttime = -1
    listItem.targetTime = nil
  else
    timeString = GameUtils:formatTimeString(lefttime)
  end
  local subIdx = (listItem.pos + 1) % 2
  local flash_obj = GameUILab:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setItemCDTime", listItem.baseKey, timeString, subIdx)
  end
end
function GameUILab:OnFSCommand(cmd, arg)
  if cmd == "needUpdateItem" then
    local itemKey = arg
    GameUILab:UpdateItemBaseData(itemKey)
  elseif cmd == "MenuItemReleased" then
    local itemKey = arg
    if itemKey == "coming" then
      GameStateManager:SetCurrentGameState(GameStateFairArena)
    end
    local listItem = GameUILab:FindListItemByItemKey(itemKey)
    if listItem ~= nil then
      if listItem.status == ItemStatus.LOCKED or listItem.status == ItemStatus.NOT_OPEN then
        m_TipText = GameLoader:GetGameText("LC_STORY_DEXTAR_STORY_LOCKED")
        GameUILab:GetFlashObject():InvokeASCallback("_root", "SetTipText", m_TipText)
        local lockTipStr = ""
        if itemKey == "slot" or itemKey == "wc" then
          lockTipStr = GameLoader:GetGameText("LC_MENU_DEXTER_INFO_5")
        elseif itemKey == "mining" then
          lockTipStr = GameLoader:GetGameText("LC_MENU_DEXTER_INFO_1")
          if "" == lockTipStr or nil == lockTipStr then
            lockTipStr = "you need xxxxxxx to unlock this function"
          end
        elseif itemKey == "ac" or itemKey == "worldboss" or itemKey == "crusade" or itemKey == "infinite_cosmos" or itemKey == "prime_wve" or itemKey == "starwar" or itemKey == "empire_colosseum" or itemKey == "large_map" then
          local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel(itemKey)
          local playerLevelNow = GameGlobalData:GetData("levelinfo").level
          if listItem.status == ItemStatus.NOT_OPEN then
            lockTipStr = GameLoader:GetGameText("LC_MENU_DEXTER_INFO_3")
            if "" == lockTipStr or nil == lockTipStr then
              lockTipStr = "this activity invailable now"
            end
          else
            if "infinite_cosmos" == itemKey then
              lockTipStr = GameLoader:GetGameText("LC_MENU_DEXTER_INFO_4")
            else
              lockTipStr = GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2")
            end
            if "" == lockTipStr or nil == lockTipStr then
              lockTipStr = "player level %d need for this function"
            end
            lockTipStr = string.format(lockTipStr, playerLevelNeed)
          end
        end
        if "" ~= lockTipStr then
          local GameTip = LuaObjectManager:GetLuaObject("GameTip")
          GameTip:Show(lockTipStr)
        end
      else
        if listItem.status == ItemStatus.NEW_FEATURE then
          listItem.status = ItemStatus.TODAY_UNUSED
          GameUILab:UpdateInitList()
        end
        if itemKey == "slot" then
          if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "tryLuck" then
            GameUtils:HideTutorialHelp()
          end
          if listItem.status == ItemStatus.NOT_OPEN then
            return
          end
          if QuestTutorialSlot:IsActive() then
            AddFlurryEvent("TutorialSlot_UseSlot", {}, 2)
          end
          GameStateManager:GetCurrentGameState():EraseObject(self)
          local GameStateGacha = GameStateManager.GameStateGacha
          GameStateManager:SetCurrentGameState(GameStateGacha)
        elseif itemKey == "mining" then
          if listItem.status == ItemStatus.NOT_OPEN then
            return
          end
          if QuestTutorialMine:IsActive() then
            AddFlurryEvent("TutorialMine_EnterMine", {}, 2)
          end
          GameStateManager:GetCurrentGameState():EraseObject(self)
          local GameStateMineMap = GameStateManager.GameStateMineMap
          GameStateManager:SetCurrentGameState(GameStateMineMap)
        elseif itemKey == "ac" then
          if listItem.status == ItemStatus.NOT_OPEN then
            return
          end
          local GameStateArcane = GameStateManager.GameStateArcane
          GameStateManager:SetCurrentGameState(GameStateArcane)
        elseif itemKey == "worldboss" then
          if listItem.targetTime then
            local countDownTimeText = GameUtils:formatTimeString(listItem.targetTime - os.time())
            m_TipText = string.format(GameLoader:GetGameText("LC_STORY_DEXTER_PRIMUS_LOCKING"), countDownTimeText)
            GameUILab:GetFlashObject():InvokeASCallback("_root", "SetTipText", m_TipText)
          else
            if listItem.status == ItemStatus.NOT_OPEN then
              return
            end
            GameStateManager:GetCurrentGameState():EraseObject(self)
            GameStateManager:SetCurrentGameState(GameStateManager.GameStateWorldBoss)
          end
        elseif itemKey == "crusade" then
          if listItem.status == ItemStatus.NOT_OPEN then
            return
          end
          GameUICrusade.mFromLab = true
          GameUICrusade:RequestCheckFirstEnterToday()
        elseif itemKey == "wc" then
          if listItem.targetTime then
            local countDownTimeText = GameUtils:formatTimeString(listItem.targetTime - os.time())
            m_TipText = string.format(GameLoader:GetGameText("LC_MENU_SA_LOCKING_DESC"), countDownTimeText)
            GameUILab:GetFlashObject():InvokeASCallback("_root", "SetTipText", m_TipText)
          else
            if listItem.status == ItemStatus.NOT_OPEN then
              return
            end
            GameUIWorldChampion:RequestEnterWC()
          end
        elseif itemKey == "infinite_cosmos" then
          if listItem.status == ItemStatus.NOT_OPEN then
            return
          end
          local instanceActivityList = GameUIActivityNew:getInstanceData()
          if instanceActivityList then
            GameObjectClimbTower:gotoInstanceSelectScreen(instanceActivityList)
          else
            GameUIActivityNew:RequestActivity(true, GameUILab.gotoInstanceLater)
          end
        elseif itemKey == "prime_wve" then
          DebugOut("WVE select")
          if listItem.targetTime then
            DebugOut("targetTime:" .. listItem.targetTime)
            local countDownTimeText = GameUtils:formatTimeString(listItem.targetTime - os.time())
            m_TipText = string.format(GameLoader:GetGameText("LC_MENU_WVE_LOCKING_DESC"), countDownTimeText)
            GameUILab:GetFlashObject():InvokeASCallback("_root", "SetTipText", m_TipText)
          elseif listItem.status == ItemStatus.NOT_OPEN then
            GameUILab:GetFlashObject():InvokeASCallback("_root", "SetTipText", GameLoader:GetGameText("LC_MENU_WVE_LOCKING_DESC_2"))
          else
            WVEGameManeger:StartWVEGame()
          end
        elseif itemKey == "starwar" then
          if listItem.status == ItemStatus.NOT_OPEN then
            return
          end
          GameStateManager:GetCurrentGameState():EraseObject(self)
          GameStateManager:SetCurrentGameState(GameStateManager.GameStateMiningWorld)
          local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
          GameUIMiningWorld.enterDir = 1
          GameUIMiningWorld:Show()
        elseif itemKey == "empire_colosseum" then
          if listItem.status == ItemStatus.NOT_OPEN then
            return
          end
          GameStateManager:SetCurrentGameState(GameStateFairArena)
        elseif itemKey == "large_map" then
          if listItem.status == ItemStatus.NOT_OPEN then
            return
          end
          GameStateManager:SetCurrentGameState(GameStateManager.GameStateLargeMap)
        elseif itemKey == "coming" then
        end
      end
    end
  elseif cmd == "close" then
    GameUILab:HideLabMenu()
  elseif cmd == "Remove_LabMenu" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  else
    GameUICrusade:OnFSCommand(cmd, arg)
  end
end
function GameUILab._OnTimerTick()
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLab and GameStateLab.mCurSubState == STATE_LAB_SUB_STATE.SUBSTATE_NORMAL then
    for _, v in pairs(m_ListItems) do
      if v.targetTime then
        GameUILab:UpdateItemCDTime(v.itemKey)
      end
    end
    return 1000
  end
  GameUILab.mTimerWork = false
  return nil
end
if AutoUpdate.isAndroidDevice then
  function GameUILab.OnAndroidBack()
    if GameUIWDStuff.menuType.menu_type_help == GameUIWDStuff.currentMenu then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "hideHelp")
      end
    else
      GameUILab:HideLabMenu()
    end
  end
end
function GameUILab.gotoInstanceLater()
  local instanceActivityList = GameUIActivityNew:getInstanceData()
  if instanceActivityList then
    GameObjectClimbTower:gotoInstanceSelectScreen(instanceActivityList)
  else
    DebugOut("GameUILab:gotoInstanceLater error:no instanceActivityList")
  end
end
