local GameStateStarCraft = GameStateManager.GameStateStarCraft
local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
GameStateStarCraft.mStarCraftLeftTime = 0
GameStateStarCraft.mStartCraftFetchTime = 0
GameStateStarCraft.mCurrentMapVersion = -1
GameStateStarCraft.mStarCraftActive = false
GameStateStarCraft.mTeleportPrice = 12
GameStateStarCraft.mRevivePrice = 100
GameStateStarCraft.nextState = nil
GameStateStarCraft.forceOutTimer = nil
function GameStateStarCraft:InitGameState()
end
function GameStateStarCraft:OnFocusGain(previousState)
  self.lastState = previousState
  self:AddObject(GameUIStarCraftMap)
  self:AddObject(GameUIBarRight)
end
function GameStateStarCraft:OnFocusLost(newState)
  GameStateStarCraft.nextState = newState
  self:EraseObject(GameUIStarCraftMap)
  self:EraseObject(GameUIBarRight)
end
function GameStateStarCraft:EnterSubMenu()
  self:EraseObject(GameUIBarRight)
end
function GameStateStarCraft:LeaveSubMenu()
  self:AddObject(GameUIBarRight)
end
function GameStateStarCraft:ForceOutTimer()
  if GameStateManager:GetCurrentGameState() == GameStateStarCraft then
    GameStateStarCraft.forceOutTimer = nil
    GameUIStarCraftMap:ForceOut()
  end
  return nil
end
