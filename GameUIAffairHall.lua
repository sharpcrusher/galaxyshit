local GameUIAffairHall = LuaObjectManager:GetLuaObject("GameUIAffairHall")
local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local QuestTutorialUseAffairs = TutorialQuestManager.QuestTutorialUseAffairs
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local QuestTutorialInfiniteCosmos = TutorialQuestManager.QuestTutorialInfiniteCosmos
GameUIAffairHall.buildingName = "affairs_hall"
GameUIAffairHall.FourthOfficer = {}
GameUIAffairHall.FourthOfficer.customText = ""
GameUIAffairHall.FourthOfficer.frame = "temp"
GameUIAffairHall.FourthOfficer.disabled = false
GameUIAffairHall.FourthOfficer.type = ""
GameUIAffairHall.FourthGameItem = {}
function GameUIAffairHall:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("resource", GameUIAffairHall.resourceChangeCallback)
  GameGlobalData:RegisterDataChangeCallback("item_count", GameUIAffairHall.ItemsChangeCallback)
end
function GameUIAffairHall.ItemsChangeCallback()
  if GameUIAffairHall.FourthOfficer.type == "krypton" or GameUIAffairHall.FourthOfficer.type == "item" then
    if GameUIAffairHall.officers ~= nil then
      GameUIAffairHall.FourthOfficer.customText = GameUtils.numberConversion(GameGlobalData:GetItemCount(GameUIAffairHall.officers[4].ext_num))
    end
    if GameUIAffairHall:GetFlashObject() then
      GameUIAffairHall:GetFlashObject():InvokeASCallback("_root", "SetTitleFourthOfficerIconAndText", GameUIAffairHall.FourthOfficer.frame, GameUIAffairHall.FourthOfficer.customText)
    end
  end
end
function GameUIAffairHall.resourceChangeCallback()
  if GameUIAffairHall:GetFlashObject() then
    local res = GameGlobalData:GetData("resource")
    local creditText, moneyText, prestigeText, techText = GameUtils.numberConversion(res.credit), GameUtils.numberConversion(res.money), GameUtils.numberConversion(res.prestige), GameUtils.numberConversion(res.technique)
    GameUIAffairHall:GetFlashObject():InvokeASCallback("_root", "RefreshResource", creditText, moneyText, prestigeText, techText, GameUIAffairHall.FourthOfficer.disabled)
    if GameUIAffairHall.FourthOfficer.type ~= "krypton" and GameUIAffairHall.FourthOfficer.type ~= "item" and res[GameUIAffairHall.FourthOfficer.type] ~= nil and GameUIAffairHall.FourthOfficer.type ~= "" then
      GameUIAffairHall.FourthOfficer.customText = GameUtils.numberConversion(res[GameUIAffairHall.FourthOfficer.type])
      if GameUIAffairHall:GetFlashObject() then
        GameUIAffairHall:GetFlashObject():InvokeASCallback("_root", "SetTitleFourthOfficerIconAndText", GameUIAffairHall.FourthOfficer.frame, GameUIAffairHall.FourthOfficer.customText)
      end
    end
  end
end
function GameUIAffairHall:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:ShowMenu()
  self:GetFlashObject():InvokeASCallback("_root", "EnableTipClose", false)
  self:GetFlashObject():InvokeASCallback("_root", "EnableTip", false)
  local descText = GameLoader:GetGameText("LC_MENU_AFFAIRS_DESC")
  self:GetFlashObject():InvokeASCallback("_root", "setAffairsDesc", descText)
  if QuestTutorialUseAffairs:IsActive() then
    local function callback()
      self:GetFlashObject():InvokeASCallback("_root", "EnableTip", true)
    end
    if immanentversion == 1 then
      GameUICommonDialog:PlayStory({100007, 100008}, callback)
    elseif immanentversion == 2 then
      GameUICommonDialog:PlayStory({100007}, callback)
    end
  end
end
function GameUIAffairHall:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIAffairHall:ShowMenu()
  self:GetFlashObject():InvokeASCallback("_root", "ShowAffair")
  GameUIAffairHall.resourceChangeCallback()
end
function GameUIAffairHall:HideMenu()
  self:GetFlashObject():InvokeASCallback("_root", "HideAffair")
end
function GameUIAffairHall:RequestInfo()
  local building = GameGlobalData:GetBuildingInfo(self.buildingName)
  if building and building.level > 0 then
    NetMessageMgr:SendMsg(NetAPIList.officers_req.Code, null, self.officerListCallback, true)
  elseif building and 0 < building.status then
    GameUIBuilding:DisplayUpgradeDialog(self.buildingName)
  end
end
function GameUIAffairHall.officerListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.officers_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.officers_ack.Code then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAffairInfo)
    local buildInfo = GameGlobalData:GetBuildingInfo(GameUIAffairHall.buildingName)
    GameUIAffairHall.officers = content.officers
    DebugOut("GameUIAffairHall.officerListCallback")
    DebugTable(GameUIAffairHall.officers)
    for k, v in pairs(GameUIAffairHall.officers) do
      local disabled = false
      local freeText = GameLoader:GetGameText("LC_MENU_CAPTION_FREE") .. " (" .. v.free_time .. ")"
      local textDisabled = ""
      local rewardsTypeText = ""
      if v.level_limit == 0 then
        textDisabled = GameLoader:GetGameText("LC_MENU_AFFAIRS_HALL_NOT_ACTIVE")
        if v.id == 4 then
          GameUIAffairHall.FourthOfficer.disabled = true
          GameUIAffairHall.resourceChangeCallback()
        end
      else
        textDisabled = string.format(GameLoader:GetGameText("LC_MENU_AFFAIRS_HALL_LEVEL_ACTIVE"), v.level_limit)
        if v.id == 4 then
          GameUIAffairHall.FourthOfficer.disabled = false
          GameUIAffairHall.resourceChangeCallback()
        end
      end
      local disabled = buildInfo.level < v.level_limit
      rewardsTypeText = GameHelper:GetAwardTypeText(v.loot_type, v.ext_num)
      local frame = ""
      if v.id == 4 then
        GameUIAffairHall.FourthOfficer.type = v.loot_type
        local game_item = {}
        game_item.item_type = v.loot_type
        game_item.number = v.ext_num
        game_item.no = 1
        game_item.level = 1
        GameUIAffairHall.FourthGameItem = game_item
        frame = GameHelper:GetCommonIconFrame(game_item, GameUIAffairHall.replaceCommonIcon)
        GameUIAffairHall.FourthOfficer.frame = frame
        if v.loot_type == "krypton" or v.loot_type == "item" then
          local customText = GameGlobalData:GetItemCount(v.ext_num)
          GameUIAffairHall.FourthOfficer.customText = customText
          GameUIAffairHall.ItemsChangeCallback()
        else
          local res = GameGlobalData:GetData("resource")
          local fourthText = GameUtils.numberConversion(res[v.loot_type])
          GameUIAffairHall.FourthOfficer.customText = fourthText
          GameUIAffairHall.resourceChangeCallback()
        end
      end
      GameUIAffairHall:GetFlashObject():InvokeASCallback("_root", "setCollectData", v.id, v.loot_value, v.loot_type, rewardsTypeText, frame)
      GameUIAffairHall:GetFlashObject():InvokeASCallback("_root", "UpdateAffairOfficerInfo", v.id, v.price, disabled, textDisabled, v.free_time, freeText, v.level_limit)
    end
    return true
  end
  return false
end
function GameUIAffairHall.replaceCommonIcon()
  GameUIAffairHall.FourthOfficer.frame = GameHelper:GetCommonIconFrame(GameUIAffairHall.FourthGameItem, nil)
  if GameUIAffairHall and GameUIAffairHall:GetFlashObject() then
    GameUIAffairHall:GetFlashObject():InvokeASCallback("_root", "SetFourthIcon", GameUIAffairHall.FourthOfficer.frame)
  end
end
function GameUIAffairHall.buyOfficerCallback(msgType, content)
  if 0 ~= content.code and nil ~= content.code then
    assert(content.code ~= 0)
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  end
  if msgType == NetAPIList.officer_buy_ack.Code then
    local changeContent = content.officer
    local buildInfo = GameGlobalData:GetBuildingInfo(GameUIAffairHall.buildingName)
    local disabled = buildInfo.level < changeContent.level_limit
    local freeText = GameLoader:GetGameText("LC_MENU_CAPTION_FREE") .. " (" .. changeContent.free_time .. ")"
    if changeContent.level_limit == 0 then
      local textDisabled = GameLoader:GetGameText("LC_MENU_AFFAIRS_HALL_NOT_ACTIVE")
    else
      local textDisabled = string.format(GameLoader:GetGameText("LC_MENU_AFFAIRS_HALL_LEVEL_ACTIVE"), changeContent.level_limit)
    end
    local disabled = buildInfo.level < changeContent.level_limit
    GameUIAffairHall:GetFlashObject():InvokeASCallback("_root", "UpdateAffairOfficerInfo", changeContent.id, changeContent.price, disabled, textDisabled, changeContent.free_time, freeText, changeContent.level_limit)
    for k, v in pairs(GameUIAffairHall.officers) do
      if v.id == changeContent.id then
        GameUIAffairHall.officers[k] = changeContent
        break
      end
    end
    if GameUIAffairHall.onBuyOfficerOver then
      GameUIAffairHall.onBuyOfficerOver(changeContent.id, changeContent.loot_value, changeContent.strike, changeContent.loot_type, disabled)
    end
    return true
  end
  return false
end
function GameUIAffairHall.GotoBuyCreditObject()
  GameVip:showVip(false)
end
function GameUIAffairHall:OnFSCommand(cmd, arg)
  if "close_menu" == cmd then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  elseif "Return_Btn_Clicked" == cmd then
    self:HideMenu()
  elseif "buy_button_clicked" == cmd then
    if QuestTutorialUseAffairs:IsActive() then
      if immanentversion == 2 then
        local function callback()
          QuestTutorialUseAffairs:SetFinish(true)
          self:GetFlashObject():InvokeASCallback("_root", "EnableTip", false)
          self:GetFlashObject():InvokeASCallback("_root", "EnableTipClose", true)
        end
        GameUICommonDialog:PlayStory({100008}, callback)
      elseif immanentversion == 1 then
        QuestTutorialUseAffairs:SetFinish(true)
        self:GetFlashObject():InvokeASCallback("_root", "EnableTip", false)
        self:GetFlashObject():InvokeASCallback("_root", "EnableTipClose", true)
        if not QuestTutorialInfiniteCosmos:IsActive() and not QuestTutorialInfiniteCosmos:IsFinished() then
          QuestTutorialInfiniteCosmos:SetActive(true)
          local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
          GameUIBarLeft:ShowIsNewFunctionInLab()
        end
      end
    end
    local officerId = tonumber(arg)
    self:BuyOfficer(officerId)
  end
end
function GameUIAffairHall:BuyOfficer(officerId)
  for k, v in pairs(GameUIAffairHall.officers) do
    if v.id == officerId then
      if v.free_time > 0 then
        self:RequestBuyOfficer(officerId)
        break
      end
      do
        local contentText = string.format(GameLoader:GetGameText("LC_MENU_AFFAIRS_PAY_CREDIT"), v.price)
        GameUtils:CreditCostConfirm(contentText, function()
          GameUIAffairHall:RequestBuyOfficer(officerId)
        end, false)
      end
      break
    end
  end
end
function GameUIAffairHall:RequestBuyOfficer(officerId)
  local officer_buy_req_content = {id = officerId}
  NetMessageMgr:SendMsg(NetAPIList.officer_buy_req.Code, officer_buy_req_content, GameUIAffairHall.buyOfficerCallback, true, nil)
  function self.onBuyOfficerOver(type_id, value, strike, loot_type, disabled)
    GameUIAffairHall:GetFlashObject():InvokeASCallback("_root", "showCollectInfo", type_id, value * strike, strike, loot_type, GameUIAffairHall.FourthOfficer.frame, disabled)
    GameUIAffairHall.onBuyOfficerOver = nil
  end
end
function GameUIAffairHall:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIAffairHall.OnAndroidBack()
    GameUIAffairHall:HideMenu()
  end
end
