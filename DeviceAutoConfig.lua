DeviceAutoConfig = {
  m_isInfoSent = {},
  m_infoType = {
    "BattlePlayLoadTime",
    "BattlePlayPreLoadTime",
    "BattlePlayAllLoadTime",
    "BattlePlayAverageFPS",
    "MainPlanetAverageFPS"
  },
  BattlePlayLoadTime = {},
  BattlePlayPreLoadTime = {},
  BattlePlayAllLoadTime = {},
  BattlePlayAverageFPS = {},
  MainPlanetAverageFPS = {},
  m_callArray = {},
  m_lastTime = 0,
  m_curTime = 0
}
local k_IsAssert = AutoUpdate.isDeveloper
function DeviceAutoConfig.SendDeviceInfo()
  if DeviceAutoConfig.m_isInfoSent.main then
    return
  end
  DeviceAutoConfig.m_isInfoSent.main = true
  local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
  local deviceFullName = ext.GetDeviceFullName()
  local deviceGPURendererName = ext.GetGpuRender()
  local deviceGPUVendorName = ext.GetGpuVendor()
  local deviceCPUHZ = ext.GetCpuMaxReq()
  local deviceMaxMemory = ext.GetAndroidDeviceTotalMem()
  deviceCPUHZ = tonumber(deviceCPUHZ)
  deviceMaxMemory = tonumber(deviceMaxMemory)
  GameSetting:SetDeviceFullName(deviceFullName)
  GameSetting:SetGPURenderer(deviceGPURendererName)
  GameSetting:SetGPUVendor(deviceGPUVendorName)
  GameSetting:SetCPUHZ(deviceCPUHZ)
  GameSetting:SetMaxMemory(deviceMaxMemory)
  GameUtils:SaveSettingData()
  local client_device_info = {
    device_full_name = deviceFullName,
    device_real_name = "",
    gpu_renderer = deviceGPURendererName,
    gpu_vendor = deviceGPUVendorName,
    cpu_hz = deviceCPUHZ,
    max_memory = deviceMaxMemory,
    udid = ext.GetIOSOpenUdid()
  }
  DebugTable(client_device_info)
  local function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.client_effect_req.Code, client_device_info, DeviceAutoConfig.NetCallbackSendDeviceInfo, false, nil)
  end
  netFailedCallback()
end
function DeviceAutoConfig.NetCallbackSendDeviceInfo(msgType, content)
  if msgType == NetAPIList.client_effect_ack.Code then
    SKIP_BATTLE_BG_PRELOAD = not content.preload_battle
    SKIP_BATTLE_PLAY_PRELOAD = not content.preload_battle_bg
    USE_SIMPLE_BATTLE_BG = not content.use_complex_bg
    USE_DEFAULT_EFFECT = not content.use_complex_battle_effect
    DebugOut("NetCallbackSendDeviceInfo")
    DebugTable(content)
    return true
  end
  return false
end
function DeviceAutoConfig.SendStatisticsInfo(infoName, infoValue)
  local fullName = infoName
  if DeviceAutoConfig.m_isInfoSent[fullName] then
    return
  end
  DeviceAutoConfig.m_isInfoSent[fullName] = true
  if type(infoValue) == "number" then
    infoValue = "" .. infoValue
  end
  local deviceType = ext.GetDeviceFullName()
  if deviceType == "android" then
    deviceType = ext.GetDeviceVersion()
    print("Device Real Name: ", deviceType)
  end
  local fps = 0
  if fullName == "MainPlanetAverageFPS" then
    fps = tonumber(infoValue)
  end
  local client_fps_info = {
    name = fullName,
    value = infoValue,
    udid = ext.GetIOSOpenUdid(),
    user_id = tonumber(ext.T4FGetUserID()) or 0,
    created_at = "",
    ip = "",
    device_type = deviceType,
    fps = fps or 0
  }
  local function netFailedCallback()
    NetMessageMgr:SendMsg(NetAPIList.client_fps_req.Code, client_fps_info, nil, false, netFailedCallback)
  end
  netFailedCallback()
end
function DeviceAutoConfig.StartStatisticsInfo(infoName)
  local fullName = infoName
  local infoTable = DeviceAutoConfig[fullName]
  if infoTable then
    infoTable.m_isStart = true
    if k_IsAssert then
      assert(not DeviceAutoConfig.m_callArray[fullName])
    end
    infoTable.InitCall()
    if infoTable.UpdateCall then
      DeviceAutoConfig.m_callArray[fullName] = infoTable.UpdateCall
    end
  end
end
function DeviceAutoConfig.EndStatisticsInfo(infoName)
  local fullName = infoName
  local infoTable = DeviceAutoConfig[fullName]
  if infoTable and infoTable.m_isStart then
    infoTable.m_isStart = false
    if DeviceAutoConfig.m_callArray[fullName] then
      DeviceAutoConfig.m_callArray[fullName] = nil
    end
    local infoValue = infoTable.GetValue()
    DeviceAutoConfig.SendStatisticsInfo(fullName, infoValue)
  end
end
function DeviceAutoConfig.UpdateStatistics()
  DeviceAutoConfig.m_curTime = ext.getSysTime()
  local dt = DeviceAutoConfig.m_curTime - DeviceAutoConfig.m_lastTime
  for _, callUpdate in pairs(DeviceAutoConfig.m_callArray) do
    callUpdate(dt)
  end
  DeviceAutoConfig.m_lastTime = DeviceAutoConfig.m_curTime
end
function DeviceAutoConfig.BattlePlayLoadTime.InitCall()
  DeviceAutoConfig.BattlePlayLoadTime.m_startTime = ext.getSysTime()
end
function DeviceAutoConfig.BattlePlayLoadTime.GetValue()
  return ext.getSysTime() - DeviceAutoConfig.BattlePlayLoadTime.m_startTime
end
function DeviceAutoConfig.BattlePlayPreLoadTime.InitCall()
  DeviceAutoConfig.BattlePlayPreLoadTime.m_startTime = ext.getSysTime()
end
function DeviceAutoConfig.BattlePlayPreLoadTime.GetValue()
  return ext.getSysTime() - DeviceAutoConfig.BattlePlayPreLoadTime.m_startTime
end
function DeviceAutoConfig.BattlePlayAllLoadTime.InitCall()
  DeviceAutoConfig.BattlePlayAllLoadTime.m_startTime = ext.getSysTime()
end
function DeviceAutoConfig.BattlePlayAllLoadTime.GetValue()
  return ext.getSysTime() - DeviceAutoConfig.BattlePlayAllLoadTime.m_startTime
end
function DeviceAutoConfig.BattlePlayAverageFPS.InitCall()
  DeviceAutoConfig.BattlePlayAverageFPS.m_frameCount = 0
  DeviceAutoConfig.BattlePlayAverageFPS.m_allDT = 0
end
function DeviceAutoConfig.BattlePlayAverageFPS.UpdateCall(dt)
  DeviceAutoConfig.BattlePlayAverageFPS.m_frameCount = DeviceAutoConfig.BattlePlayAverageFPS.m_frameCount + 1
  DeviceAutoConfig.BattlePlayAverageFPS.m_allDT = DeviceAutoConfig.BattlePlayAverageFPS.m_allDT + dt
end
function DeviceAutoConfig.BattlePlayAverageFPS.GetValue()
  return (...), DeviceAutoConfig.BattlePlayAverageFPS.m_frameCount * 1 / DeviceAutoConfig.BattlePlayAverageFPS.m_allDT * 1000 + 0.5, DeviceAutoConfig.BattlePlayAverageFPS.m_allDT
end
function DeviceAutoConfig.MainPlanetAverageFPS.InitCall()
  DeviceAutoConfig.MainPlanetAverageFPS.m_frameCount = 0
  DeviceAutoConfig.MainPlanetAverageFPS.m_allDT = 0
end
function DeviceAutoConfig.MainPlanetAverageFPS.UpdateCall(dt)
  DeviceAutoConfig.MainPlanetAverageFPS.m_frameCount = DeviceAutoConfig.MainPlanetAverageFPS.m_frameCount + 1
  DeviceAutoConfig.MainPlanetAverageFPS.m_allDT = DeviceAutoConfig.MainPlanetAverageFPS.m_allDT + dt
end
function DeviceAutoConfig.MainPlanetAverageFPS.GetValue()
  return (...), DeviceAutoConfig.MainPlanetAverageFPS.m_frameCount * 1 / DeviceAutoConfig.MainPlanetAverageFPS.m_allDT * 1000 + 0.5, DeviceAutoConfig.MainPlanetAverageFPS.m_allDT
end
