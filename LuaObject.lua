local LuaObjectTracking = {
  m_trackIndex = {},
  m_trackCount = 0,
  m_needOutput = false
}
LuaObjectManager = {
  ObjectType = {
    UNKNOW = 1,
    BACKGROUNDOBJ = 2,
    MENU = 3,
    MODELMENU = 4
  },
  m_tracking = LuaObjectTracking
}
LuaObjectManager.LuaObjectBase = {
  m_objectName = nil,
  m_flashFileName = nil,
  m_ObjectType = LuaObjectManager.ObjectType.UNKNOW,
  m_renderFx = nil,
  m_derivedInstCT = 0
}
function LuaObjectManager:GetLuaObject(objectName)
  if LuaObjectManager[objectName] == nil then
    GameUtils:Error(objectName .. " is not registered ")
    return nil
  end
  return LuaObjectManager[objectName]
end
function LuaObjectManager:RegisterLuaObject(objectName, objectType, flashFileName, alwaysload)
  assert(LuaObjectManager[objectName] == nil)
  local newClass = {}
  local metaTable = {}
  function metaTable.__index(t, k)
    local ret = LuaObjectManager.LuaObjectBase[k]
    newClass[k] = ret
    return ret
  end
  setmetatable(newClass, metaTable)
  newClass.m_objectName = objectName
  newClass.m_ObjectType = objectType
  newClass.m_alwaysload = alwaysload
  if flashFileName ~= nil then
    newClass.m_flashFileName = flashFileName
  end
  LuaObjectManager[objectName] = newClass
  if self.m_tracking then
    self.m_tracking.m_trackCount = LuaObjectManager.m_tracking.m_trackCount + 1
    self.m_tracking.m_trackIndex[objectName] = LuaObjectManager.m_tracking.m_trackCount
  end
end
function LuaObjectManager:OnInitGame()
  for k, v in pairs(self) do
    if type(v) == "table" and v.OnInitGame then
      DebugOut("on init game!!!")
      v:OnInitGame()
    end
  end
  if self.m_tracking ~= nil and (immanentversion == 1 or not ext.is16x9()) then
    self.m_tracking = nil
  end
end
function LuaObjectManager:GetFlashObjectSuperTableName()
  return "_AllFlashClasses"
end
function LuaObjectManager:OnFlashObjectDestroy(instName)
  DebugOut("-------fuck onFlashObjectDestroy ", instName)
  local flashObjectSuperTable = _G[LuaObjectManager:GetFlashObjectSuperTableName()]
  if flashObjectSuperTable[instName] ~= nil then
    flashObjectSuperTable[instName] = nil
  else
    _G[instName] = nil
  end
end
function LuaObjectManager:LoadFlashFileName(objectName, flashV1, flashV2)
  local gameObject = LuaObjectManager:GetLuaObject(objectName)
  if immanentversion == 1 then
    DebugOut("ChangeFlashFileName " .. gameObject.m_flashFileName .. " -> " .. flashV1)
    gameObject:ChangeFlashFile(flashV1)
  elseif immanentversion == 2 then
    DebugOut("ChangeFlashFileName " .. gameObject.m_flashFileName .. " -> " .. flashV2)
    gameObject:ChangeFlashFile(flashV2)
  else
    assert(false)
  end
end
local LuaObjectBase = LuaObjectManager.LuaObjectBase
function LuaObjectBase:NewInstance(flashFileName, alwaysLoad)
  assert(flashFileName ~= nil)
  self.m_derivedInstCT = self.m_derivedInstCT + 1
  local newObject = {}
  setmetatable(newObject, {__index = self})
  if flashFileName ~= nil then
    newObject.m_flashFileName = flashFileName
  end
  newObject.m_alwaysload = alwaysLoad
  if newObject.m_alwaysload then
    newObject:LoadFlashObject()
  end
  return newObject
end
function LuaObjectBase:ChangeFlashFile(flashFileName)
  assert(not self.m_renderFx)
  self.m_flashFileName = flashFileName
end
function LuaObjectBase:GetObjectType()
  return self.m_ObjectType
end
function LuaObjectBase:GetFlashObjectClassName()
  return self.m_objectName .. "_F" .. self.m_derivedInstCT
end
function LuaObjectBase:GetFlashObject()
  return self.m_renderFx
end
function LuaObjectBase:UnloadFlashObject()
  if not self.m_alwaysload then
    self.m_renderFx = nil
  end
end
function LuaObjectBase:LoadFlashObject()
  if self:GetFlashObject() then
    GameUtils:Error("flash object was loaded already" .. self.m_flashFileName)
    return
  end
  self:RegisterFlashObject()
  local flashObjectSuperTable = _G[LuaObjectManager:GetFlashObjectSuperTableName()]
  self.m_renderFx = flashObjectSuperTable[self:GetFlashObjectClassName()]:new()
  if ext.is2x1 and ext.is2x1() and not AutoUpdate.isAndroidDevice and self.m_flashFileName ~= "IphoneXCover.tfs" then
    local width = ext.SCREEN_WIDTH
    local height = ext.SCREEN_HEIGHT
    local x_start = math.floor(width * 92 / 2436)
    local usewidth = math.floor(width * 2252 / 2436)
    DebugOut("flash:", self.m_flashFileName)
    DebugOut("2x1LoadFlash", x_start, usewidth, width, height)
    self:GetFlashObject():Load("data2/" .. self.m_flashFileName, ext.isLangFR(), x_start, 0, usewidth, height)
  else
    DebugOut("flash:", self.m_flashFileName)
    self:GetFlashObject():Load("data2/" .. self.m_flashFileName, ext.isLangFR())
  end
  if self.InitFlashObject then
    self:InitFlashObject()
  end
end
function LuaObjectBase:RegisterFlashObject()
  DebugOut("register flash Object " .. self:GetFlashObjectClassName())
  ext.RegisteFlashClass(self:GetFlashObjectClassName(), LuaObjectManager:GetFlashObjectSuperTableName())
  local flashObjectSuperTable = _G[LuaObjectManager:GetFlashObjectSuperTableName()]
  flashObjectSuperTable[self:GetFlashObjectClassName()].fscommand = function(sender, cmd, args)
    DebugOut("sender " .. self:GetFlashObjectClassName() .. " cmd: " .. tostring(cmd) .. " args: " .. tostring(args))
    if cmd == "CUSTOM_PLAYSFX" then
      local oldsound = args
      if GameSettingData and GameSettingData.Save_Lang then
        lang = GameSettingData.Save_Lang
        if string.find(lang, "ru") == 1 then
          args = string.gsub(args, "%.", "_ru.")
          DebugOut("advcd:", args, ext.crc32.crc32("sound/" .. args))
          if ext.crc32.crc32("sound/" .. args) and ext.crc32.crc32("sound/" .. args) ~= "" then
            oldsound = args
          end
        end
      end
      GameUtils:PlaySound(oldsound)
      return
    elseif cmd == "CUSTOM_PLAYMUSIC" then
      GameUtils:PlayMusic(args)
      return
    end
    if GameUtils:OnFSCommand(cmd, args, self) then
      return
    end
    if self.OnFSCommand then
      self:OnFSCommand(cmd, args)
    else
      GameUtils:Warning("there is no OnFSCommand function in class " .. self.m_objectName)
    end
  end
end
function LuaObjectManager:OutputObjectIndex()
  if self.m_tracking == nil or self.m_tracking.m_needOutput == false then
    return
  end
  file = io.open("../../../objectindex.lua", "w+")
  file:write("objectindex = {\n")
  for k, v in pairs(self.m_tracking.m_trackIndex) do
    file:write("\t[" .. v .. "] \t= ")
    file:write("\t\t'" .. k .. "',\n")
  end
  file:write("}\n")
  file:close()
end
function LuaObjectManager:setTrackingState(enable)
  if enable then
    if self.m_tracking == nil and ext.is16x9() then
      self.m_tracking = LuaObjectTracking
    end
  elseif self.m_tracking ~= nil then
    self.m_tracking = nil
  end
end
