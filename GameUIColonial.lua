local GameStateColonization = GameStateManager.GameStateColonization
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIColonial = LuaObjectManager:GetLuaObject("GameUIColonial")
local GameUISlaveSelect = LuaObjectManager:GetLuaObject("GameUISlaveSelect")
local GameUIColonialMenu = LuaObjectManager:GetLuaObject("GameUIColonialMenu")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
GameUIColonial.moveOutReasonType = {
  unknown = -1,
  exit = 0,
  identityChange = 1,
  enterSlaveSelect = 2
}
GameUIColonial.ScreenLayerType = {
  unknown = -1,
  slave = 0,
  freedom = 1,
  colonist = 2
}
GameUIColonial.curScreenLayerType = GameUIColonial.ScreenLayerType.unknown
GameUIColonial.curColonyInfo = nil
GameUIColonial.fetchUserInfoTime = 0
GameUIColonial.curIndependentFightUserInfo = nil
GameUIColonial.LogStack = {}
GameUIColonial.LogStack.acceptAllianceChannel = true
GameUIColonial.LogStack.max_size = 200
GameUIColonial.LogStack.logs = {}
GameUIColonial.k_UpdateReapFreq = 5000
GameUIColonial.m_updateReapTime = 0
GameUIColonial.LogMsgType = {}
GameUIColonial.LogMsgType["10101"] = "LC_MENU_EVENT_COLONIAL_SLAVE_ATK_SUCSS_MESSAGE"
GameUIColonial.LogMsgType["10102"] = "LC_MENU_EVENT_COLONIAL_SLAVE_DEF_FAIL_MESSAGE"
GameUIColonial.LogMsgType["30102"] = "LC_MENU_EVENT_COLONIAL_ALLY_SLAVE_DEF_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20101"] = "LC_MENU_EVENT_COLONIAL_SLAVE_ATK_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20103"] = "LC_MENU_EVENT_COLONIAL_SLAVE_ATK_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20105"] = "LC_MENU_EVENT_COLONIAL_SLAVE_ATK_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20102"] = "LC_MENU_EVENT_COLONIAL_SLAVE_DEF_SUCSS_MESSAGE"
GameUIColonial.LogMsgType["40104"] = "LC_MENU_EVENT_COLONIAL_ALLY_SLAVE_DEF_SUCSS_MESSAGE"
GameUIColonial.LogMsgType["40107"] = "LC_MENU_EVENT_COLONIAL_ALLY_SLAVE_DEF_SUCSS_MESSAGE"
GameUIColonial.LogMsgType["40102"] = "LC_MENU_EVENT_COLONIAL_ALLY_SLAVE_DEF_SUCSS_MESSAGE"
GameUIColonial.LogMsgType["10201"] = "LC_MENU_EVENT_COLONIAL_YOU_LIB_SLAVE_SUCSS_MESSAGE"
GameUIColonial.LogMsgType["10202"] = "LC_MENU_EVENT_COLONIAL_MASTER_DEF_FAIL_MESSAGE"
GameUIColonial.LogMsgType["10203"] = "LC_MENU_EVENT_COLONIAL_SLAVE_SAVED_MESSAGE"
GameUIColonial.LogMsgType["30203"] = "LC_MENU_EVENT_COLONIAL_ALLY_SLAVE_SAVED_MESSAGE"
GameUIColonial.LogMsgType["20201"] = "LC_MENU_EVENT_COLONIAL_THIRD_SAVE_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20202"] = "LC_MENU_EVENT_COLONIAL_MASTER_ANTI_THIRD_SUCESS_MESSAGE"
GameUIColonial.LogMsgType["20203"] = "LC_MENU_EVENT_COLONIAL_SLAVE_SAVED_FAIL_MESSAGE"
GameUIColonial.LogMsgType["40203"] = "LC_MENU_EVENT_COLONIAL_ALLY_HELP_SUCESS_MESSAGE"
GameUIColonial.LogMsgType["10401"] = "LC_MENU_EVENT_COLONIAL_SLAVE_INDEPEN_SUCESS_MESSAGE"
GameUIColonial.LogMsgType["10402"] = "LC_MENU_EVENT_COLONIAL_MASTER_ANTI_INDEPEN_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20401"] = "LC_MENU_EVENT_COLONIAL_SLAVE_INDEPEN_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20402"] = "LC_MENU_EVENT_COLONIAL_MASTER_ANTI_INDEPEN_SUCESS_MESSAGE"
GameUIColonial.LogMsgType["10303"] = "LC_MENU_EVENT_COLONIAL_SLAVE_HELPED_SUCESS_MESSAGE"
GameUIColonial.LogMsgType["10301"] = "LC_MENU_EVENT_COLONIAL_THIRD_HELP_SUCESS_MESSAGE"
GameUIColonial.LogMsgType["10302"] = "LC_MENU_EVENT_COLONIAL_MASTER_ANTI_HELP_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20303"] = "LC_MENU_EVENT_COLONIAL_SLAVE_HELPED_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20301"] = "LC_MENU_EVENT_COLONIAL_THIRD_HELP_FAIL_MESSAGE"
GameUIColonial.LogMsgType["20302"] = "LC_MENU_EVENT_COLONIAL_MASTER_ANTI_HELP_SUCESS_MESSAGE"
GameUIColonial.LogMsgType["10701"] = "LC_MENU_EVENT_COLONIAL_REAP_MESSAGE"
GameUIColonial.LogMsgType["10702"] = "LC_MENU_EVENT_COLONIAL_REAP_MESSAGE_MONEY"
GameUIColonial.LogMsgType["10801"] = "LC_MENU_EVENT_COLONIAL_REQUIRE_%d_MESSAGE"
GameUIColonial.LogMsgType["10811"] = "LC_MENU_EVENT_COLONIAL_REQUIRE_%d_MESSAGE_MONEY"
GameUIColonial.LogMsgType["10802"] = "LC_MENU_EVENT_COLONIAL_REQUIRED_%d_MESSAGE"
GameUIColonial.LogMsgType["10901"] = "LC_MENU_EVENT_COLONIAL_PROTEST_%d_MESSAGE"
GameUIColonial.LogMsgType["10911"] = "LC_MENU_EVENT_COLONIAL_PROTEST_%d_MESSAGE_MONEY"
GameUIColonial.LogMsgType["10902"] = "LC_MENU_EVENT_COLONIAL_PROTESTED_%d_MESSAGE"
GameUIColonial.LogMsgType["10501"] = "LC_MENU_EVENT_COLONIAL_ABANDON_MESSAGE"
GameUIColonial.LogMsgType["10502"] = "LC_MENU_EVENT_COLONIAL_ABANDONED_MESSAGE"
GameUIColonial.LogMsgType["10601"] = "LC_MENU_EVENT_COLONIAL_SLAVE_FREE_MESSAGE"
GameUIColonial.LogMsgType["10602"] = "LC_MENU_EVENT_COLONIAL_MASTER_FREE_MESSAGE"
GameUIColonial.LogMsgType["10105"] = "LC_MENU_EVENT_COLONIAL_THIRD_ATK_MASTER_MESSAGE"
GameUIColonial.LogMsgType["10104"] = "LC_MENU_EVENT_COLONIAL_THIRD_ATK_MASTER_SUCCESS_FOR_MASTER_MESSAGE"
GameUIColonial.LogMsgType["10103"] = "LC_MENU_EVENT_COLONIAL_SNATCH_SUCSS_CHAR"
GameUIColonial.LogMsgType["10106"] = "LC_MENU_EVENT_COLONIAL_THIRD_ATK_OVER_LEVEL_MASTER_MESSAGE"
GameUIColonial.LogMsgType["10107"] = "LC_MENU_EVENT_COLONIAL_THIRD_ATK_MASTER_SUCCESS_FOR_OVER_LEVEL_MASTER_MESSAGE"
GameUIColonial.LogMsgType["10108"] = "LC_MENU_EVENT_COLONIAL_SNATCH_SUCSS_OVER_LEVEL_SLAVE_FREE_MESSAGE"
function GameUIColonial:OnInitGame()
  DebugColonial("GameUIColonial:OnInitGame()")
  GameGlobalData:RegisterDataChangeCallback("vipinfo", self.onLeveOrVIPChanged)
  GameGlobalData:RegisterDataChangeCallback("levelinfo", self.onLeveOrVIPChanged)
end
function GameUIColonial:Init()
  if GameUIColonial.curColonyInfo == nil then
    GameUIColonial.curScreenLayerType = GameUIColonial.ScreenLayerType.freedom
  else
    GameUIColonial.curScreenLayerType = self:GetScreenLayerByIdentity()
  end
end
function GameUIColonial:InitFlashObject()
  self:GetFlashObject():InvokeASCallback("_root", "onFlashObjectLoaded")
end
function GameUIColonial:OnAddToGameState()
  DebugFestival("GameUIColonial:OnAddToGameState()")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameGlobalData:RegisterDataChangeCallback("newChatTotalCount", GameUIColonial.UpdateChatButtonStatus)
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  if chatCount and chatCount > 0 then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  end
  self:ForceFreshEXP()
  self:Init()
  self:MoveIn(GameUIColonial.curScreenLayerType)
end
function GameUIColonial:OnEraseFromGameState()
  DebugFestival("GameUIColonial:OnEraseFromGameState()")
  self:UnloadFlashObject()
  GameUIColonial.lastFrameCDState[1] = "unknown"
  GameUIColonial.lastFrameCDState[2] = "unknown"
  GameUIColonial.lastFrameCDState[3] = "unknown"
  GameGlobalData:RemoveDataChangeCallback("newChatTotalCount", GameUIColonial.UpdateChatButtonStatus)
end
function GameUIColonial:MoveIn(screenType)
  self:InitScreen()
  self:UpdateChatMessage("")
  self:SetHeadInfo()
  self:SetExpInfo()
  self:SetTimesInfo()
  self:SetChannelCheckbox()
  self:SyncItem()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveIn", screenType)
end
if AutoUpdate.isAndroidDevice then
  function GameUIColonial.OnAndroidBack()
    if GameUIColonialMenu.IsShowChildWindow then
      GameUIColonialMenu:OnFSCommand("auto_close_menu")
    else
      GameUIColonial:MoveOut(GameUIColonial.moveOutReasonType.exit)
    end
  end
end
function GameUIColonial:MoveOut(reason)
  self.moveOutReason = reason
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut", GameUIColonial.curScreenLayerType)
end
function GameUIColonial:UpdateChatMessage(htmlText)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setChatContent", GameUIColonial.curScreenLayerType, htmlText)
  end
end
function GameUIColonial:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    if self.m_updateReapTime > self.k_UpdateReapFreq then
      self.m_updateReapTime = 0
    end
    self.m_updateReapTime = self.m_updateReapTime + dt
    self:UpdateCDTime()
    self:UpdateExpInSlaveHead()
    local vipTime = 0
    local vipTimeStr
    if GameVipDetailInfoPanel.mTmpInfoFetchTime and 0 < GameVipDetailInfoPanel.mTmpInfoFetchTime then
      vipTime = GameVipDetailInfoPanel.mTmpInfo.left_time - (os.time() - GameVipDetailInfoPanel.mTmpInfoFetchTime)
    end
    if vipTime > 0 then
      vipTimeStr = GameUtils:formatTimeString(vipTime)
    end
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", GameUIColonial.curScreenLayerType, dt, vipTimeStr)
    flash_obj:Update(dt)
  end
end
function GameUIColonial:SetExpInfo()
  if GameUIColonial.curScreenLayerType == GameUIColonial.ScreenLayerType.unknown then
    return false
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local expStr = ""
    if GameStateColonization.curExpInfo ~= nil then
      expStr = GameUtils.numberConversion(GameStateColonization.curExpInfo.ntf.experience) .. "/" .. GameUtils.numberConversion(GameStateColonization.curExpInfo.ntf.max_experience)
    end
    local levelInfo = GameGlobalData:GetData("levelinfo")
    if levelInfo.level == GameGlobalData.max_level then
      flash_obj:InvokeASCallback("_root", "setColonyRewordText", GameUIColonial.curScreenLayerType, GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_DAILY_MONEY_CHAR"))
    end
    flash_obj:InvokeASCallback("_root", "setExpText", GameUIColonial.curScreenLayerType, expStr)
    if GameUIColonial.curScreenLayerType == GameUIColonial.ScreenLayerType.slave then
      expStr = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_EXP_INCOME_CHAR")
      if levelInfo.level == GameGlobalData.max_level then
        expStr = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_MONEY_INCOME_CHAR")
      end
      local exp = "<font color='#FF0000'>" .. GameStateColonization.curExpInfo.ntf.producted_experience .. "</font>"
      expStr = string.format(expStr, exp)
      flash_obj:InvokeASCallback("_root", "SetProductedExp", expStr)
    end
  end
  return true
end
function GameUIColonial:SetTimesInfo()
  DebugOut("GameUIColonial:SetTimesInfo()")
  if GameUIColonial.curScreenLayerType == GameUIColonial.ScreenLayerType.unknown then
    return false
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local timesStr = "0/0"
    local titleStr = ""
    if GameUIColonial.curColonyInfo ~= nil then
      if GameUIColonial.curColonyInfo.info.position == GameStateColonization.IdentityType.slave then
        timesStr = GameStateColonization:GetTimesString(GameStateColonization.TimesType.action_tricky)
        titleStr = GameStateColonization:GetActionTitleString(GameStateColonization.TimesType.action_tricky)
      elseif GameUIColonial.curColonyInfo.info.position == GameStateColonization.IdentityType.freedom then
        timesStr = GameStateColonization:GetTimesString(GameStateColonization.TimesType.colonist)
        titleStr = GameStateColonization:GetActionTitleString(GameStateColonization.TimesType.colonist)
      else
        timesStr = GameStateColonization:GetTimesString(GameStateColonization.TimesType.colonist)
        titleStr = GameStateColonization:GetActionTitleString(GameStateColonization.TimesType.colonist)
      end
    end
    flash_obj:InvokeASCallback("_root", "setTimesText", GameUIColonial.curScreenLayerType, titleStr, timesStr)
  end
  return true
end
function GameUIColonial:OnFSCommand(cmd, arg)
  DebugColonial("colonial command: ", cmd, arg)
  local flash_obj = self:GetFlashObject()
  if cmd == "close_menu" then
    DebugOut("close_menu")
    self:MoveOut(GameUIColonial.moveOutReasonType.exit)
  elseif cmd == "move_out_finish" then
    if self.moveOutReason == GameUIColonial.moveOutReasonType.exit then
      GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
      GameStateManager:SetCurrentGameState(GameStateColonization:GetPreState())
    elseif self.moveOutReason == GameUIColonial.moveOutReasonType.identityChange then
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "hideLayer", GameUIColonial.curScreenLayerType)
      end
      local newLayer = self:GetScreenLayerByIdentity()
      GameUIColonial.curScreenLayerType = newLayer
      self:MoveIn(GameUIColonial.curScreenLayerType)
    elseif self.moveOutReason == GameUIColonial.moveOutReasonType.enterSlaveSelect then
      GameStateColonization:enterSlaveSelect()
    end
    self.moveOutReason = GameUIColonial.moveOutReasonType.unknown
  elseif cmd == "PopupChatWindow" then
    self:HideChatIcon()
    GameStateColonization:AddObject(GameUIChat)
    GameUIChat:SelectChannel("world")
  elseif cmd == "select_slave" then
    GameStateColonization:enterSlaveSelect()
  elseif cmd == "select_planet" then
    local index = tonumber(arg)
    if index > #self.curColonyInfo.info.users then
      GameStateColonization:enterSlaveSelect()
    else
      local userInfo = GameUIColonial.curColonyInfo.info.users[index]
      GameUIColonialMenu:SetSlaveLayerInfo(userInfo, GameUIColonial.fetchUserInfoTime)
      GameUIColonialMenu:ShowSlaveLayer()
    end
  elseif cmd == "OnPlayChatBarTextComplete" then
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "OnPlayChatBarTextComplete", GameUIColonial.curScreenLayerType)
    end
  elseif cmd == "checkbox_released" then
    self.LogStack.acceptAllianceChannel = not self.LogStack.acceptAllianceChannel
    self:SetChannelCheckbox()
  elseif cmd == "update_message_item" then
    self:UpdateLogItem(tonumber(arg))
  elseif cmd == "select_message" then
    local index = tonumber(arg)
    local logInfo = GameUIColonial.LogStack.logs[index]
    if logInfo.type == 30102 then
      if GameUIColonial.curColonyInfo.info.position == GameStateColonization.IdentityType.slave then
        GameTip:Show(GameLoader:GetGameText("LC_ALERT_colony_actual_not_expect_one"))
        return
      end
      self.waitSavedUserID = logInfo.slave_id
      self.waitSavedUserName = logInfo.slave_name
      local playerList = {}
      table.insert(playerList, logInfo.master_id)
      local requestParam = {user_ids = playerList}
      DebugColonialTable(requestParam)
      NetMessageMgr:SendMsg(NetAPIList.colony_players_req.Code, requestParam, GameUIColonial.FetchMasterCallback, true, nil)
    end
  elseif cmd == "slave_protest_released" then
    local userInfo = GameUIColonial.curColonyInfo.info.users[1]
    local lefttimes = "10/10"
    GameUIColonialMenu:SetColonistLayerInfo(userInfo, lefttimes)
    GameUIColonialMenu:ShowColonistLayer()
  elseif cmd == "slave_independ_released" then
    GameUIColonialMenu.curIndependentFightUserInfo = LuaUtils:table_rcopy(GameUIColonial.curColonyInfo.info.users[1])
    local requestParam = {
      fight_type = GameStateColonization.FightType.independent,
      def_id = GameUIColonialMenu.curIndependentFightUserInfo.player_id,
      def_pos = GameUIColonialMenu.curIndependentFightUserInfo.position,
      third_id = ""
    }
    DebugColonialTable(requestParam)
    if GameStateManager:GetCurrentGameState().fightRoundData then
      GameStateManager:GetCurrentGameState().fightRoundData = nil
    end
    NetMessageMgr:SendMsg(NetAPIList.colony_challenge_req.Code, requestParam, GameUIColonial.IndependentCallback, true, nil)
  elseif cmd == "slave_help_released" then
    local data_alliance = GameGlobalData:GetData("alliance")
    if data_alliance.id == "" then
      local msg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_ASK_HELP_NO_ALIANCE_ALERT")
      GameTip:Show(msg)
    else
      local userInfo = GameUIColonial.curColonyInfo.info.users[1]
      GameUIColonialMenu:SetHelpLayerInfo(userInfo)
      GameUIColonialMenu:ShowHelpLayer()
    end
  end
end
function GameUIColonial:HideChatIcon()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "hideChatIcon", GameUIColonial.curScreenLayerType)
  end
end
function GameUIColonial:ShowChatIcon()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "showChatIcon", GameUIColonial.curScreenLayerType)
  end
end
function GameUIColonial:InitScreen()
  if GameUIColonial.curScreenLayerType == GameUIColonial.ScreenLayerType.slave then
    self:InitSlaveScreen()
  elseif GameUIColonial.curScreenLayerType == GameUIColonial.ScreenLayerType.freedom then
    self:InitFreedomScreen()
  elseif GameUIColonial.curScreenLayerType == GameUIColonial.ScreenLayerType.colonist then
    self:InitColonistScreen()
  end
end
function GameUIColonial:InitFreedomScreen()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setFreedomPlanet")
  end
end
function GameUIColonial:InitSlaveScreen()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local name, sex, identity, lv = "", "", "", ""
    local userInfo = GameUIColonial.curColonyInfo.info.users[1]
    name = GameUtils:GetUserDisplayName(userInfo.name)
    local avatar = ""
    DebugOut("here Tp:", userInfo.icon, userInfo.main_fleet_level, userInfo.sex)
    if userInfo.icon ~= 0 and userInfo.icon ~= 1 then
      avatar = GameDataAccessHelper:GetFleetAvatar(userInfo.icon, userInfo.main_fleet_level)
    else
      avatar = GameUtils:GetPlayerAvatarWithSex(userInfo.sex, userInfo.main_fleet_level)
    end
    lv = GameLoader:GetGameText("LC_MENU_Level") .. userInfo.level
    identity = GameStateColonization:GetIdentityStrByPosition(userInfo.position)
    flash_obj:InvokeASCallback("_root", "setSlavePlanet", name, avatar, identity, lv)
  end
end
function GameUIColonial:InitColonistScreen()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local name, sex, identity, lv, force, alliance, planet, exp, withCD = "", "", "", "", "", "", "", "", ""
    local userList = GameUIColonial.curColonyInfo.info.users
    local _end = math.min(3, #userList)
    for i = 1, _end do
      local sexInt, nameStr, statusStr, lvStr, forceStr, allianceStr, iconStr = GameStateColonization:GetUserInfo(userList[i])
      name = name .. nameStr .. "\001"
      sex = sex .. sexInt .. "\001"
      lv = lv .. lvStr .. "\001"
      force = force .. forceStr .. "\001"
      alliance = alliance .. allianceStr .. "\001"
      identity = identity .. statusStr .. "\001"
      planet = planet .. iconStr .. "\001"
      exp = exp .. userList[i].product.experience + math.floor(userList[i].product.speed * (os.time() - userList[i].product.fetch_os_time) / 60) .. "\001"
      withCD = withCD .. GameUIColonialMenu:CheckUserInCD(userList[i], GameUIColonial.fetchUserInfoTime) .. "\001"
    end
    DebugColonial("GameUIColonial:InitColonistScreen()")
    DebugColonial(name, sex, identity, lv, planet, exp, withCD)
    local productText = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_EXP_PRODUCT_CHAR")
    local levelInfo = GameGlobalData:GetData("levelinfo")
    if levelInfo.level == GameGlobalData.max_level then
      productText = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_MONEY_PRODUCT_CHAR")
    end
    flash_obj:InvokeASCallback("_root", "setColonialPlanet", name, sex, lv, planet, exp, withCD, productText)
  end
end
GameUIColonial.lastFrameCDState = {}
GameUIColonial.lastFrameCDState[1] = "unknown"
GameUIColonial.lastFrameCDState[2] = "unknown"
GameUIColonial.lastFrameCDState[3] = "unknown"
function GameUIColonial:UpdateCDTime()
  if GameUIColonial.curScreenLayerType ~= GameUIColonial.ScreenLayerType.colonist then
    return
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local userList = GameUIColonial.curColonyInfo.info.users
    local _end = math.min(3, #userList)
    for i = 1, _end do
      local cdTime = GameUIColonialMenu:CheckUserInCD(userList[i], GameUIColonial.fetchUserInfoTime)
      local frameName = "disable"
      if cdTime == "none" then
        frameName = "active"
      end
      if GameUIColonial.lastFrameCDState[i] ~= frameName then
        self:InitScreen()
        GameUIColonial.lastFrameCDState[i] = frameName
      else
        flash_obj:InvokeASCallback("_root", "setColonialPlanetCD", i - 1, cdTime)
      end
    end
  end
end
function GameUIColonial:ForceFreshEXP()
  self.m_updateReapTime = self.k_UpdateReapFreq
end
function GameUIColonial:IsFreshEXP()
  return self.m_updateReapTime > self.k_UpdateReapFreq
end
function GameUIColonial:ResetFreshEXP()
  self.m_updateReapTime = 0
end
function GameUIColonial:UpdateExpInSlaveHead()
  if GameUIColonial.curScreenLayerType ~= GameUIColonial.ScreenLayerType.colonist then
    return
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj and self:IsFreshEXP() then
    local userList = GameUIColonial.curColonyInfo.info.users
    local _end = math.min(3, #userList)
    for i = 1, _end do
      local curExp = userList[i].product.experience + math.floor(userList[i].product.speed * (os.time() - userList[i].product.fetch_os_time) / 60)
      local maxExp = userList[i].product.max_experience
      if curExp > maxExp then
        curExp = maxExp
      end
      local cdTime = GameUIColonialMenu:CheckUserInCD(userList[i], GameUIColonial.fetchUserInfoTime)
      local isInCDFrame = cdTime ~= "none"
      local productText = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_EXP_PRODUCT_CHAR")
      local levelInfo = GameGlobalData:GetData("levelinfo")
      if levelInfo.level == GameGlobalData.max_level then
        productText = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_MONEY_PRODUCT_CHAR")
      end
      flash_obj:InvokeASCallback("_root", "setColonialPlanetExp", i - 1, isInCDFrame, curExp, productText)
    end
  end
end
function GameUIColonial:SetHeadInfo()
  local userInfo = GameGlobalData:GetData("userinfo")
  local name_display = GameUtils:GetUserDisplayName(userInfo.name)
  local sex = GameGlobalData:GetUserInfo().sex
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curMatrixIndex] or 1, player_main_fleet.level)
  local vipInfo = GameGlobalData:GetData("vipinfo")
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local identityStr = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_INDEPENDENT_TITLE")
  if GameUIColonial.curColonyInfo ~= nil then
    identityStr = GameStateColonization:GetIdentityStrByPosition(GameUIColonial.curColonyInfo.info.position)
  end
  local flash_obj = GameUIColonial:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setHeadAreaInfo", GameUIColonial.curScreenLayerType, name_display, player_avatar, levelInfo.level, curLevel, identityStr)
  end
end
function GameUIColonial:GetScreenLayerByIdentity()
  if GameUIColonial.curColonyInfo == nil or GameUIColonial.curColonyInfo.info.position == GameStateColonization.IdentityType.slave then
    return GameUIColonial.ScreenLayerType.slave
  elseif GameUIColonial.curColonyInfo.info.position == GameStateColonization.IdentityType.freedom then
    return GameUIColonial.ScreenLayerType.freedom
  else
    return GameUIColonial.ScreenLayerType.colonist
  end
end
function GameUIColonial:SetChannelCheckbox()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "setChannelCheckbox", GameUIColonial.curScreenLayerType, GameUIColonial.LogStack.acceptAllianceChannel)
  end
end
function GameUIColonial:SyncItem()
  if self:GetFlashObject() then
    DebugColonial("SyncItem")
    DebugColonialTable(GameUIColonial.LogStack.logs)
    self:GetFlashObject():InvokeASCallback("_root", "syncListboxItemCount", GameUIColonial.curScreenLayerType, #GameUIColonial.LogStack.logs)
  end
end
function GameUIColonial:UpdateLogItem(indexItem)
  local message_info = self.LogStack.logs[indexItem]
  DebugColonial("UpdateLogItem")
  DebugColonial(indexItem)
  DebugColonialTable(message_info)
  local flash_obj = self:GetFlashObject()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if flash_obj and message_info then
    local orimsg = ""
    if GameUIColonial.LogMsgType["" .. message_info.type] ~= nil then
      local text_name = GameUIColonial.LogMsgType["" .. message_info.type]
      if message_info.type == 10801 then
        if levelInfo.level == GameGlobalData.max_level then
          text_name = GameUIColonial.LogMsgType["" .. message_info.type + 10]
        end
        text_name = string.format(text_name, tonumber(message_info.params[1]))
        orimsg = GameLoader:GetGameText(text_name)
        orimsg = string.format(orimsg, message_info.params[2])
        DebugOut("UpdateLogItem 10801 text_name = ", text_name)
      elseif message_info.type == 10802 then
        text_name = string.format(text_name, tonumber(message_info.params[1]))
        orimsg = GameLoader:GetGameText(text_name)
      elseif message_info.type == 10901 then
        if levelInfo.level == GameGlobalData.max_level then
          text_name = GameUIColonial.LogMsgType["" .. message_info.type + 10]
        end
        text_name = string.format(text_name, tonumber(message_info.params[1]) - 100)
        orimsg = GameLoader:GetGameText(text_name)
        orimsg = string.format(orimsg, message_info.params[2])
        DebugOut("UpdateLogItem 10901 text_name = ", text_name)
      elseif message_info.type == 10701 then
        if levelInfo.level == GameGlobalData.max_level then
          text_name = GameUIColonial.LogMsgType["" .. message_info.type + 1]
          text_name = string.format(text_name, tonumber(message_info.params[1]))
          DebugOut("UpdateLogItem 10701 text_name = ", text_name)
        end
        orimsg = GameLoader:GetGameText(text_name)
        orimsg = string.format(orimsg, message_info.params[1])
        DebugOut("UpdateLogItem orimsg = ", orimsg)
      elseif message_info.type == 10902 then
        text_name = string.format(text_name, tonumber(message_info.params[1]) - 100)
        orimsg = GameLoader:GetGameText(text_name)
      else
        orimsg = GameLoader:GetGameText(text_name)
        if #message_info.params > 0 then
          orimsg = string.format(orimsg, message_info.params[1])
        end
      end
    else
      DebugColonial("no message + ", message_info.type)
      orimsg = "no message"
    end
    orimsg = GameStateColonization:MakeMsgs(orimsg, message_info.master_name, message_info.slave_name, message_info.third_name, "", true)
    flash_obj:InvokeASCallback("_root", "setMessageItem", GameUIColonial.curScreenLayerType, indexItem, orimsg, message_info.type == 30102, GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_LIBERATION_BUTTON"))
  end
end
function GameUIColonial.onLeveOrVIPChanged()
  local flash_obj = GameUIColonial:GetFlashObject()
  if flash_obj then
    GameUIColonial.SetHeadInfo()
  end
end
function GameUIColonial.ColonyInfoNotifyHandler(content)
  DebugColonial("ColonyInfoNotifyHandler")
  DebugColonialTable(content)
  local curOSTime = os.time()
  GameUIColonial.curColonyInfo = LuaUtils:table_rcopy(content)
  GameUIColonial.fetchUserInfoTime = curOSTime
  if GameUIColonial.curColonyInfo.info.position == GameStateColonization.IdentityType.colonist then
    for i, v in ipairs(GameUIColonial.curColonyInfo.info.users) do
      v.product.fetch_os_time = curOSTime
    end
  end
  if GameStateManager:GetCurrentGameState() == GameStateColonization then
    local flash_obj = GameUIColonial:GetFlashObject()
    if flash_obj ~= nil then
      if GameUIColonial.curScreenLayerType ~= GameUIColonial:GetScreenLayerByIdentity() then
        GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
        GameStateManager:ReEnterCurrentGameState()
      else
        GameUIColonial:InitScreen()
        if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIColonialMenu) then
          GameUIColonialMenu:ChangeUserInfo()
        end
      end
    end
  end
end
function GameUIColonial.ColonyLogChangeNotifyHandler(content)
  DebugColonial("ColonyLogChangeNotifyHandler")
  DebugColonialTable(content)
  if content.type == nil then
    DebugOut("content.type empty")
  elseif content.type == 1 then
    DebugOut("refresh log")
    GameUIColonial.LogStack.logs = {}
  elseif content.type == 2 then
    DebugOut("insert log")
  end
  local needSort = false
  if #GameUIColonial.LogStack.logs == 0 then
    needSort = true
  end
  for i, v in ipairs(content.logs) do
    if GameUIColonial.LogMsgType["" .. v.type] ~= nil and (v.catalog == 0 or v.catalog == 1 and GameUIColonial.LogStack.acceptAllianceChannel == true) then
      if v.catalog == 1 then
        v.type = v.type + 20000
      end
      table.insert(GameUIColonial.LogStack.logs, 1, v)
    end
  end
  DebugColonial("After Insert, total count: ", #GameUIColonial.LogStack.logs)
  DebugColonialTable(GameUIColonial.LogStack.logs)
  while #GameUIColonial.LogStack.logs > GameUIColonial.LogStack.max_size do
    table.remove(GameUIColonial.LogStack.logs, #GameUIColonial.LogStack.logs)
  end
  DebugColonial("After remove")
  DebugColonialTable(GameUIColonial.LogStack.logs)
  do
    local SortCallback = function(a, b)
      return a.time < b.time
    end
    table.sort(GameUIColonial.LogStack.logs, SortCallback)
  end
  DebugColonial("After sort")
  DebugColonialTable(GameUIColonial.LogStack.logs)
  GameUIColonial:SyncItem()
end
function GameUIColonial.IndependentCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_challenge_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_challenge_ack.Code then
    DebugColonial("GameUIColonial.IndependentCallback")
    DebugColonial(msgType)
    DebugColonialTable(content)
    DebugColonialTable(GameUIColonialMenu.curIndependentFightUserInfo)
    do
      local window_type = ""
      GameUIBattleResult:SetFightReport(content.report, nil, nil)
      if content.result == 1 then
        GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
        local sex, name, status, lv, force, alliance, icon = GameStateColonization:GetUserInfo(GameUIColonialMenu.curIndependentFightUserInfo)
        local oriMsg = GameLoader:GetGameText("LC_MENU_EVENT_COLONIAL_INDEPEND_SUCSS_CHAR")
        oriMsg = GameStateColonization:MakeMsgs(oriMsg, name, "", "", "", false)
        icon = "0"
        GameUIBattleResult:updateColonialWinInfo(icon, name, lv, oriMsg)
        window_type = "colonial_win"
        GameUIBattleResult.colonialName = name
      else
        GameStateColonization.directlyTo.locate = GameStateColonization.directlyTo.type.colonial
        window_type = "colonial_lose"
      end
      local lastGameState = GameStateManager:GetCurrentGameState()
      if #content.report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
        content.report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
      end
      GameStateBattlePlay.curBattleType = "UIColonial"
      GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, content.report)
      GameStateBattlePlay:RegisterOverCallback(function()
        GameStateManager:SetCurrentGameState(lastGameState)
        GameUIBattleResult:LoadFlashObject()
        GameUIBattleResult:AnimationMoveIn(window_type, false, oriMsg, FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_FIGHT_WIN)
        lastGameState:AddObject(GameUIBattleResult)
      end)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
      GameUIColonial:SetTimesInfo()
      return true
    end
  end
  return false
end
function GameUIColonial.FetchMasterCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.colony_players_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
  elseif msgType == NetAPIList.colony_players_ack.Code then
    DebugColonial("GameUIColonial.FetchMasterCallback")
    DebugColonial(msgType)
    DebugColonialTable(content)
    if 0 < #content.users then
      GameUIColonialMenu:SetSaveLayerInfo(content.users[1], GameUIColonial.waitSavedUserID, GameUIColonial.waitSavedUserName)
      GameUIColonialMenu:ShowSaveLayer()
    end
    return true
  end
  return false
end
function GameUIColonial.UpdateChatButtonStatus()
  DebugOut("GameUIColonial UpdateChatButtonStatus")
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  DebugOut(chatCount)
  local flash_obj = GameUIColonial:GetFlashObject()
  if flash_obj == nil then
    return
  end
  if chatCount and chatCount > 0 then
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  else
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", false)
  end
end
