ext.RegisteFlashClass("FlashLogoSplash_t")
LogoSplash = {m_curLogoIdx = 0, m_renderFx = nil}
local LogoSplash = LogoSplash
if ext.SNSdkType == "kakao" then
  LogoSplash.m_logoList = {}
else
  LogoSplash.m_logoList = {}
end
function LogoSplash:Update(dt)
  if not self.m_renderFx then
    return
  end
  self.m_renderFx:Update(dt)
end
function LogoSplash:Render()
  if not self.m_renderFx then
    return
  end
  self.m_renderFx:Render()
end
function FlashLogoSplash_t:fscommand(cmd, arg)
  if cmd == "OnLogoPlayEnd" and not LogoSplash:_Next() then
    collectgarbage("collect")
  end
end
function LogoSplash:Start()
  self:_Next()
end
function LogoSplash:IsRunning()
  return self.m_renderFx ~= nil
end
function LogoSplash:_Next()
  self.m_renderFx = nil
  self.m_curLogoIdx = self.m_curLogoIdx + 1
  if self.m_curLogoIdx > #self.m_logoList then
    return false
  end
  local logoName = self.m_logoList[self.m_curLogoIdx]
  self.m_renderFx = FlashLogoSplash_t:new()
  self.m_renderFx:Load(string.format("data2/%s.tfs", logoName), ext.isLangFR())
  return true
end
