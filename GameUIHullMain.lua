local GameStateHull = GameStateManager.GameStateHull
local GameUIHullMain = LuaObjectManager:GetLuaObject("GameUIHullMain")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameStateEquipEnhance = LuaObjectManager:GetLuaObject("GameStateEquipEnhance")
local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local TutorialQuestManager = TutorialQuestManager
local QuestTutorialHullFunction = TutorialQuestManager.QuestTutorialHullFunction
local QuestTutorialHullEquip = TutorialQuestManager.QuestTutorialHullEquip
GameUIHullMain.hullListBag = {}
GameUIHullMain.grid_capacity = 100
GameUIHullMain.hasGetHullList = false
local mCurrentFuncTypeIndex = 0
local HULL_BAG_ITEM_STATUS_NORMAL = 0
local HULL_BAG_ITEM_STATUS_CANGROW = 1
local HULL_BAG_ITEM_STATUS_SELECTED = 2
local HULL_BAG_ITEM_STATUS_LOCKED = 3
local mCurrentChoosedHulls = {
  0,
  0,
  0,
  0
}
local mHullToBeUpGrade = {}
local mHasShowFleetBackGround = false
local mHasGetCardItemFirst = false
local isBatchDecompose = false
local function SetIsBatchDecompose(batchDecompose)
  isBatchDecompose = batchDecompose
end
local function IsBatchDecompose()
  return isBatchDecompose
end
local batchDecomposeSelection = {}
local function ClearBatchDecomposeSelection()
  batchDecomposeSelection = {}
end
local function GetBatchDecomposeSelection()
  return batchDecomposeSelection
end
local function IsSelectedInBatchDecompose(index)
  if IsBatchDecompose() then
    for j = 1, #batchDecomposeSelection do
      if index == batchDecomposeSelection[j] then
        return true, j
      end
    end
  end
  return false, nil
end
local function ResetBatchDecompose()
  SetIsBatchDecompose(false)
  ClearBatchDecomposeSelection()
  local flashObj = GameUIHullMain:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetBatchDecompose", false)
  end
end
local getRankColor = function(rankId)
  if rankId == 5 then
    return "green"
  elseif rankId == 12 then
    return "blue"
  elseif rankId == 98 then
    return "purple"
  elseif rankId == 99 then
    return "red"
  elseif rankId == 100 then
    return "golden"
  end
  return "blue"
end
function getHullColor(colorId)
  if colorId == 1 then
    return "green"
  elseif colorId == 2 then
    return "blue"
  elseif colorId == 3 then
    return "purple"
  elseif colorId == 6 then
    return "red"
  elseif colorId == 5 then
    return "golden"
  end
  return "blue"
end
local check_grow_RULL = {
  [1] = {
    1,
    1,
    2,
    0
  },
  [2] = {
    2,
    2,
    2,
    0
  },
  [3] = {
    1,
    3,
    1,
    0
  },
  [4] = {
    2,
    4,
    2,
    50
  },
  [5] = {
    1,
    4,
    1,
    100
  },
  [6] = {
    2,
    6,
    1,
    500
  },
  [7] = {
    2,
    6,
    1,
    1000
  },
  [8] = {
    1,
    4,
    2,
    2000
  },
  [9] = {
    1,
    4,
    1,
    3000
  }
}
function GameUIHullMain:OnInitGame()
end
function GameUIHullMain:InitFlashObject()
  local flash_obj = self:GetFlashObject()
  local text1 = GameLoader:GetGameText("LC_HULL_Hull_build")
  local text2 = GameLoader:GetGameText("LC_HULL_Universe_Exploration_Title")
  local text3 = GameLoader:GetGameText("LC_HULL_Hull_Improvement_Title")
  local text4 = GameLoader:GetGameText("LC_HULL_Hull_Improvement_tips")
  local text5 = GameLoader:GetGameText("LC_HULL_Hull_backpack_Title")
  text2 = string.gsub(text2, "!@#", "\n")
  text3 = string.gsub(text3, "!@#", "\n")
  text5 = string.gsub(text5, "!@#", "\n")
  flash_obj:InvokeASCallback("_root", "setLocalText", text1, text2, text3, text4, text5)
  SetIsBatchDecompose(false)
  local txtBatchDecompose = GameLoader:GetGameText("LC_MENU_AUTO_DECOMPOSE")
  local txtDecompose = GameLoader:GetGameText("LC_MENU_DECOMPOSE_CHAR")
  local txtCancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
  self:GetFlashObject():InvokeASCallback("_root", "SetBatchDecomposeText", txtBatchDecompose, txtDecompose, txtCancel)
end
function GameUIHullMain:OnAddToGameState()
  self:LoadFlashObject()
  GameTimer:Add(self._OnTimerTick, 1000)
end
function GameUIHullMain:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIHullMain._OnTimerTick()
  local flashObj = GameUIHullMain:GetFlashObject()
  if nil == flashObj then
    return 1000
  end
  for k, v in pairs(GameUIHullMain._HullDrawCardTable or {}) do
    local data = {}
    if v.isActivity then
      local Aleft = v.activityTime - (os.time() - v.baseTime)
      if Aleft < 0 then
        Aleft = 0
      end
      data.isActivity = v.isActivity
      data.activityTime = GameUtils:formatTimeStringAsPartion2(Aleft)
    end
    local Fleft = v.nextFreeTime - (os.time() - v.baseTime)
    if Fleft < 0 then
      Fleft = 0
    end
    if Fleft == 0 then
      data.isFree = true
      data.freeTip = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_FREE_TIME") .. v.leftFreeCount
    else
      data.isFree = false
      data.freeTip = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_COLDDOWN") .. GameUtils:formatTimeStringAsPartion2(Fleft)
    end
    if 0 >= v.leftFreeCount then
      data.freeTip = ""
      data.isFree = false
    end
    data.freeText = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_FREE")
    GameUIHullMain:GetFlashObject():InvokeASCallback("_root", "updateItemTime", k, data)
  end
  return 1000
end
function GameUIHullMain:SetCurrentTab(tabType)
  local flashObj = self:GetFlashObject()
  if nil == flashObj then
    return
  end
  if tabType == "hull_card" then
    if mHasGetCardItemFirst == false then
      GameUIHullMain:UpdateGameData()
    end
    mCurrentFuncTypeIndex = 1
    self:RefreshCurrency()
    flashObj:InvokeASCallback("_root", "SetDisplayTab", 1)
  elseif tabType == "hull_manager" then
    mCurrentFuncTypeIndex = 2
    GameUIHullMain:ResetCurrentChoosedHulls()
    GameUIHullMain:ResetHullBagListStatus(true)
    flashObj:InvokeASCallback("_root", "SetDisplayTab", 2)
    GameUIHullMain:SetHullBagList()
    GameUIHullMain:SetHullGrowChooseUI()
    GameFleetInfoBackground:SetRootVisible(true)
    if false == GameUIHullMain.hasGetHullList then
      GameUIHullMain:CallForHullList()
    end
  elseif tabType == "hull_bag" then
    ResetBatchDecompose()
    mCurrentFuncTypeIndex = 3
    if false == GameUIHullMain.hasGetHullList then
      GameUIHullMain:CallForHullList()
    else
      GameUIHullMain:SortHullList2()
      flashObj:InvokeASCallback("_root", "SetDisplayTab", 3)
      GameUIHullMain:SetHullBagList2()
      GameFleetInfoBackground:SetRootVisible(false)
    end
  end
  GameStateHull:OnTabChange(mCurrentFuncTypeIndex)
  if mCurrentFuncTypeIndex == 2 then
    GameFleetInfoBackground:ClearShowOneFleet()
  end
end
function GameUIHullMain:RefreshCurrency()
  local flashObj = self:GetFlashObject()
  if nil == flashObj then
    return
  end
  local resource = GameGlobalData:GetData("resource")
  local currency_credit = resource.credit
  local currency_hull_1 = GameGlobalData:GetItemCount(21812)
  local currency_hull_2 = GameGlobalData:GetItemCount(21811)
  flashObj:InvokeASCallback("_root", "SetCurrency", GameUtils.numberConversion(currency_credit), GameUtils.numberConversion(currency_hull_1), GameUtils.numberConversion(currency_hull_2))
end
function GameUIHullMain:OnFSCommand(cmd, arg)
  if cmd == "on_clicked_close" then
    GameStateHull:Quit()
  elseif cmd == "tabChanged" then
    local numArg = tonumber(arg)
    if numArg == mCurrentFuncTypeIndex then
      return
    end
    if arg == "1" then
      GameUIHullMain:SetCurrentTab("hull_card")
    elseif arg == "2" then
      GameUIHullMain:SetCurrentTab("hull_manager")
    elseif arg == "3" then
      GameUIHullMain:SetCurrentTab("hull_bag")
    end
  elseif cmd == "update_list_item" then
    self:updateListItem(tonumber(arg))
  elseif cmd == "show_chance" then
    local clickId = tonumber(arg)
    local drawCardInfo = GameUIHullMain:GetHullDrawCardInfoById(clickId)
    if nil ~= drawCardInfo then
      GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
      local helpText = string.gsub(GameLoader:GetGameText(drawCardInfo.chanceText), "!@#", "\n")
      GameUIWDStuff:SetHelpText(helpText)
    end
  elseif cmd == "buy_one" then
    local clickId = tonumber(arg)
    local drawCardInfo = GameUIHullMain:GetHullDrawCardInfoById(clickId)
    local freeCount = 0
    local freeCDTime = 0
    if nil ~= drawCardInfo then
      freeCount = drawCardInfo.leftFreeCount
      freeCDTime = drawCardInfo.nextFreeTime - (os.time() - drawCardInfo.baseTime)
    end
    if freeCount > 0 and freeCDTime <= 0 then
      self:DrawCardReq(1, clickId, 1)
    else
      self:DrawCardReq(2, clickId, 1)
    end
  elseif cmd == "buy_ten" then
    self:DrawCardReq(2, tonumber(arg), 10)
  elseif cmd == "ShowDetail" then
    local itemType, index = unpack(LuaUtils:string_split(arg, ":"))
    if itemType == "item" then
      DebugOut(self.curRewards, index)
      DebugTable(self.curRewards)
      local item = self.curRewards[tonumber(index)]
      item.cnt = 1
      ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    elseif itemType == "hull" then
      GameUIHullMain:ShowClickRewardHull(self.curRewards[tonumber(index)])
    end
  elseif cmd == "close_all" then
    GameUIHullMain.curShowUI = "main"
  elseif cmd == "close_buy" then
    GameUIHullMain.curShowUI = "main"
  elseif cmd == "NeedUpdateHullBagListItem" then
    GameUIHullMain:UpdateHullBagListItem(tonumber(arg))
  elseif cmd == "NeedUpdateHullBagListItem2" then
    GameUIHullMain:UpdateHullBagListItem2(tonumber(arg))
  elseif cmd == "clickHullBagList" then
    local idx = tonumber(arg)
    local clickedItem = GameUIHullMain.hullListBag[idx]
    if 0 >= mCurrentChoosedHulls[1] and 10 <= clickedItem.level then
      GameUIGlobalScreen:ShowAlert("error", 653, nil)
      return
    end
    GameUIHullMain:DealHullBagItemClicked(clickedItem, idx)
  elseif cmd == "clickHullBagList2" then
    local idx = tonumber(arg)
    local clickedItem = GameUIHullMain.hullListBag[idx]
    if IsBatchDecompose() then
      if clickedItem.is_equipon == 1 then
        GameUIHullMain:DealClickItemInBag2(clickedItem, idx, true)
        return
      end
      local isSelected, key = IsSelectedInBatchDecompose(idx)
      if isSelected then
        table.remove(batchDecomposeSelection, key)
      else
        table.insert(batchDecomposeSelection, idx)
      end
      self:GetFlashObject():InvokeASCallback("_root", "SetChooseBoxStatus", idx, not isSelected)
      return
    end
    GameUIHullMain:DealClickItemInBag2(clickedItem, idx, true)
  elseif cmd == "clickChoosedList" then
    local choosedIdx = tonumber(arg)
    local choosedItemId = mCurrentChoosedHulls[choosedIdx]
    local choosedItem, bagIdx = GameUIHullMain:GetHullItemInBagById(choosedItemId)
    GameUIHullMain:DealHullBagItemClicked(choosedItem, bagIdx)
  elseif cmd == "on_clickedConfirmHullGrow" then
    GameUIHullMain:CallHullUpGradeReq()
  elseif cmd == "on_clickedHullBagHelp" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    local helpText = GameLoader:GetGameText("LC_HULL_Hull_Improvement_help")
    helpText = string.gsub(helpText, "!@#", "\n")
    GameUIWDStuff:SetHelpText(helpText)
  elseif cmd == "on_clickedHullBag2Help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    local helpText = GameLoader:GetGameText("LC_HULL_Hull_backpack_help")
    helpText = string.gsub(helpText, "!@#", "\n")
    GameUIWDStuff:SetHelpText(helpText)
  elseif cmd == "on_clicked_decompose" then
    local hullId = tonumber(arg)
    GameUIHullMain:CallDecomposeHullItem(hullId)
  elseif cmd == "clicked_hull_corner" then
    local idx = tonumber(arg)
    local clickedItem = GameUIHullMain.hullListBag[idx]
    GameUIHullMain:DealClickItemInBag2(clickedItem, idx, true)
  elseif cmd == "clicked_batch_decompose" then
    SetIsBatchDecompose(true)
    ClearBatchDecomposeSelection()
    local cnt = math.ceil(GameUIHullMain.grid_capacity / 4)
    for index = 1, cnt do
      GameUIHullMain:UpdateHullBagListItem2(index)
    end
  elseif cmd == "clicked_decompose" then
    GameUIHullMain:CallBatchDecompose()
  elseif cmd == "clicked_cancel" then
    ResetBatchDecompose()
    local hullCount = #GameUIHullMain.hullListBag
    self:GetFlashObject():InvokeASCallback("_root", "ResetBatchDecomposeChooseBox", hullCount, false)
  else
    TPLog("GameUIHullMain:OnFSCommand not processed:", cmd, arg)
  end
end
function GameUIHullMain:CallBatchDecompose()
  TPLog("CallBatchDecompose")
  if not IsBatchDecompose() then
    return
  end
  if #batchDecomposeSelection <= 0 then
    GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_AUTO_DECOMPOSE_STARSHIP_1"))
    return
  end
  local hulls = {}
  local selectSlotCount = #batchDecomposeSelection
  for i = 1, selectSlotCount do
    local slot = batchDecomposeSelection[i]
    local item = GameUIHullMain.hullListBag[slot]
    if nil ~= item then
      local hullId = item.id
      table.insert(hulls, hullId)
    end
  end
  GameFleetEquipment.zhuangbei:requestBreakHulls(hulls)
end
function GameUIHullMain:ClearLocalData()
  mHasGetCardItemFirst = false
end
function GameUIHullMain:updateListItem(itemIndex)
  local itemdata = self._HullDrawCardTable[itemIndex]
  if itemdata then
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(itemdata.bgImg, DynamicResDownloader.resType.WELCOME_PIC)
    DebugOut("GameUIHullMain:updateListItem:", localPath, DynamicResDownloader:IsPngCrcCorrect(localPath), ext.crc32.crc32(localPath), type(ext.crc32.crc32(localPath)))
    if ext.crc32.crc32(localPath) ~= "" then
      itemdata.imgExist = true
    else
      local extendInfo = {}
      extendInfo.img = itemdata.bgImg
      extendInfo.itemIndex = itemIndex
      DynamicResDownloader:AddDynamicRes(itemdata.bgImg, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIHullMain.DynamicBackgroundCallback)
      itemdata.imgExist = false
    end
    local extInfo_buy = {}
    extInfo_buy.index = itemIndex
    extInfo_buy.frame = GameHelper:GetAwardTypeIconFrameName(itemdata.baseData.buy_item.item_type, itemdata.baseData.buy_item.number, itemdata.baseData.buy_item.no)
    itemdata.buyItemFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(itemdata.baseData.buy_item, extInfo_buy, GameUIHullMain.buyIconCallBack)
    local extInfo_one = {}
    extInfo_one.index = itemIndex
    extInfo_one.frame = GameHelper:GetAwardTypeIconFrameName(itemdata.baseData.buy_price_one.item_type, itemdata.baseData.buy_price_one.number, itemdata.baseData.buy_price_one.no)
    itemdata.priceOneFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(itemdata.baseData.buy_price_one, extInfo_one, GameUIHullMain.priceOneCallBack)
    local extInfo_ten = {}
    extInfo_ten.index = k
    extInfo_ten.frame = GameHelper:GetAwardTypeIconFrameName(itemdata.baseData.buy_price_ten.item_type, itemdata.baseData.buy_price_ten.number, itemdata.baseData.buy_price_ten.no)
    itemdata.priceTenFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(itemdata.baseData.buy_price_ten, extInfo_ten, GameUIHullMain.priceTenCallBack)
    itemdata.showTutorial = false
    if itemdata.baseData.buy_price_one.number == 21811 and QuestTutorialHullFunction:IsActive() then
      itemdata.showTutorial = true
    end
    self:GetFlashObject():InvokeASCallback("_root", "updateListItem", itemIndex, itemdata)
    GameUIHullMain._OnTimerTick()
  end
end
function GameUIHullMain.DynamicBackgroundCallback(extinfo)
  if GameUIHullMain:GetFlashObject() then
    GameUIHullMain:GetFlashObject():InvokeASCallback("_root", "setItemBg", extinfo.itemIndex, extinfo.img)
  end
end
function GameUIHullMain:GetItemDataById(id)
  for k, v in ipairs(self._HullDrawCardTable or {}) do
    if v.id == id then
      return v, k
    end
  end
  return nil
end
function GameUIHullMain:UpdateGameData()
  local param = {ui_type = 1}
  NetMessageMgr:SendMsg(NetAPIList.hull_draw_list_req.Code, param, self.HullDrawListItemCallBack, true, nil)
end
function GameUIHullMain.HullDrawListItemCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.hull_draw_list_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgtype == NetAPIList.hull_draw_list_ack.Code then
    mHasGetCardItemFirst = true
    GameUIHullMain:GenerateDrawCardList(content)
    return true
  end
  return false
end
function GameUIHullMain:GetItemByID(id)
  for k, v in pairs(self._HullDrawCardTable or {}) do
    if v.id == id then
      return v
    end
  end
  return nil
end
function GameUIHullMain:GenerateDrawCardList(content)
  local data = {}
  for k, v in ipairs(content.ui_data_list or {}) do
    if v.open_status > 0 then
      local item = GameUIHullMain:GenerateDrawCardItemData(v)
      data[#data + 1] = item
    end
  end
  self._HullDrawCardTable = data
  self:UpdateDrawCardList()
end
function GameUIHullMain:GenerateDrawCardItemData(itemdata)
  local v = itemdata
  local item = {}
  item.buyItemFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
  item.buyNumber = GameHelper:GetAwardCount(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
  item.priceOneFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
  item.priceOneNumber = GameHelper:GetAwardCount(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
  item.priceTenFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
  item.priceTenNumber = GameHelper:GetAwardCount(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
  item.baseTime = os.time()
  item.nextFreeTime = v.free_count_colddown
  item.leftFreeCount = v.free_count
  item.desc = GameLoader:GetGameText(v.disc_key)
  item.nextDesc = GameLoader:GetGameText(v.name)
  item.activityTime = v.activity_time
  item.isActivity = v.is_activity
  item.id = v.pool_id
  item.buyText = GameLoader:GetGameText("LC_MENU_WELFAREE_PURCHASE_BUTTON")
  item.buyOneText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_PURCHASE"), "<number1>", "1")
  item.buyTenText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_PURCHASE"), "<number1>", "10")
  item.freeText = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_FREE")
  item.loadText = GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR")
  item.bgImg = v.pict_name
  item.chanceText = v.chance_list_key
  item.baseData = v
  item.imgExist = true
  return item
end
function GameUIHullMain:GetHullDrawCardInfoById(Id)
  for i = 1, #self._HullDrawCardTable do
    if self._HullDrawCardTable[i].id == Id then
      return self._HullDrawCardTable[i]
    end
  end
  return nil
end
function GameUIHullMain:UpdateDrawCardList()
  local flashObj = self:GetFlashObject()
  if flashObj == nil then
    return
  end
  flashObj:InvokeASCallback("_root", "initListBox", #self._HullDrawCardTable)
end
function GameUIHullMain:DrawCardReq(drawtype, id, count)
  local function callback()
    local param = {}
    param.draw_type = drawtype
    param.draw_times = count
    param.pool_id = id
    NetMessageMgr:SendMsg(NetAPIList.hull_draw_req.Code, param, self.DrawCardCallBack, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.hull_draw_req.Code, param, self.DrawCardCallBack, true, nil)
  end
  callback()
end
function GameUIHullMain.DrawCardCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.hull_draw_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.hull_draw_ack.Code then
    DebugTable(content)
    if QuestTutorialHullFunction:IsActive() then
      QuestTutorialHullFunction:SetFinish(true)
      QuestTutorialHullEquip:SetActive(true)
      local param = {}
      param.id = 1
      NetMessageMgr:SendMsg(NetAPIList.change_leader_req.Code, param, nil, false)
    end
    GameUIHullMain:RefreshCurrency()
    local id = content.pool_id
    local itemData, index = GameUIHullMain:GetItemDataById(id)
    local item = GameUIHullMain:GenerateDrawCardItemData(content.new_ui_info)
    if itemData then
      DebugTable(item)
      GameUIHullMain._HullDrawCardTable[index] = item
    end
    if 1 < #content.item_list then
      GameUIHullMain:SetTenReward(item, content.item_list)
    else
      GameUIHullMain:SetOneReward(item, content.item_list)
    end
    GameUIHullMain:updateListItem(index)
    return true
  end
  return false
end
function GameUIHullMain:SetOneReward(itemData, rewards)
  local v = rewards[1]
  local data = {}
  data.id = itemData.id
  data.price = itemData.priceOneNumber
  data.priceIcon = itemData.priceOneFrame
  data.buytip = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_PURCHASE_MORE"), "<number1>", "1")
  data.reward = {}
  data.reward.number = v.number
  data.reward.itemType = v.item_type
  if v.item_type == "hull" then
    data.reward.ship = GameDataAccessHelper:GetCommanderVesselsImage(v.number, 1)
    if v.number == 1 then
      data.reward.nameText = GameLoader:GetGameText("LC_FLEET_TYPE_6")
    else
      data.reward.nameText = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(v.number, 1))
    end
    data.reward.job = GameDataAccessHelper:GetCommanderAbility(v.number).vessels
    data.reward.rankFrame = v.level
    data.reward.rankColor = getHullColor(GameDataAccessHelper:GetCommanderAbility(v.number, v.level).COLOR)
  elseif v.item_type == "item" then
    local extInfo = {}
    extInfo.iconframe = "item_" .. v.number
    extInfo.index = 1
    data.reward.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, extInfo, GameUIHullMain.dynamicDownCallBack)
    data.reward.nameText = GameHelper:GetAwardTypeText(v.item_type, v.number)
    data.reward.Count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
  end
  data.reward.type = v.item_type
  local redlist = {}
  redlist[1] = data.reward
  GameUIHullMain.curRewards = redlist
  if QuestTutorialHullEquip:IsActive() then
    data.showTips = true
  else
    data.showTips = false
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetOneRewardInfo", data)
    if QuestTutorialHullEquip:IsActive() then
      flashObj:InvokeASCallback("_root", "ShowTopBarPageClose")
    end
  end
  GameUIHullMain.curShowUI = "buyOne"
end
function GameUIHullMain:SetTenReward(itemData, rewards)
  local data = {}
  data.id = itemData.id
  data.price = itemData.priceTenNumber
  data.priceIcon = itemData.priceTenFrame
  data.buytip = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_PURCHASE_MORE"), "<number1>", "10")
  data.rewards = {}
  for k, v in ipairs(rewards or {}) do
    local item = {}
    item.number = v.number
    if v.item_type == "hull" then
      item.ship = GameDataAccessHelper:GetCommanderVesselsImage(v.number, 1)
      if v.number == 1 then
        item.nameText = GameLoader:GetGameText("LC_FLEET_TYPE_6")
      else
        item.nameText = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(v.number, 1))
      end
      item.job = GameDataAccessHelper:GetCommanderAbility(v.number).vessels
      item.rankFrame = v.level
      item.itemType = v.item_type
      item.rankColor = getHullColor(GameDataAccessHelper:GetCommanderAbility(v.number, v.level).COLOR)
    elseif v.item_type == "item" then
      local extInfo = {}
      extInfo.iconframe = "item_" .. v.number
      extInfo.index = k
      item.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, extInfo, GameUIHullMain.dynamicDownCallBack)
      item.nameText = GameHelper:GetAwardTypeText(v.item_type, v.number)
      item.Count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
      item.itemType = v.item_type
    end
    item.type = v.item_type
    data.rewards[#data.rewards + 1] = item
  end
  GameUIHullMain.curRewards = data.rewards
  if QuestTutorialHullEquip:IsActive() then
    data.showTips = true
  else
    data.showTips = false
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetTenRewardInfo", data)
  if QuestTutorialHullEquip:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTopBarPageClose")
  end
  GameUIHullMain.curShowUI = "buyTen"
end
function GameUIHullMain.dynamicDownCallBack(extInfo)
  if GameUIHullMain:GetFlashObject() then
    GameUIHullMain:GetFlashObject():InvokeASCallback("_root", "SetRewardIcon", extInfo.index, extInfo.iconframe)
  end
end
function GameUIHullMain:CallForHullList()
  NetMessageMgr:SendMsg(NetAPIList.hull_list_req.Code, nil, self.HullListCallBack, true, nil)
end
function GameUIHullMain.HullListCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.hull_list_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgtype == NetAPIList.hull_list_ack.Code then
    GameUIHullMain.hasGetHullList = true
    GameUIHullMain.hullListBag = LuaUtils:table_values(content.hull_list)
    GameUIHullMain.grid_capacity = content.grid_capacity
    if mCurrentFuncTypeIndex == 2 then
      GameUIHullMain:ResetHullBagListStatus(true)
      GameUIHullMain:GetFlashObject():InvokeASCallback("_root", "SetDisplayTab", 2)
      GameUIHullMain:SetHullBagList()
      GameUIHullMain:SetHullGrowChooseUI()
      GameFleetInfoBackground:SetRootVisible(true)
    elseif mCurrentFuncTypeIndex == 3 then
      GameUIHullMain:SortHullList2()
      GameUIHullMain:GetFlashObject():InvokeASCallback("_root", "SetDisplayTab", 3)
      GameUIHullMain:SetHullBagList2()
      GameFleetInfoBackground:SetRootVisible(false)
      GameUIHullMain:CloseHullItemDetailPop()
    end
    return true
  end
  return false
end
function GameUIHullMain:ResetHullBagListStatus(resort)
  for _, v in pairs(GameUIHullMain.hullListBag) do
    if true == GameUIHullMain:CheckHullCanGrow(v) then
      v.status = HULL_BAG_ITEM_STATUS_CANGROW
    else
      v.status = HULL_BAG_ITEM_STATUS_NORMAL
    end
  end
  if true == resort then
    GameUIHullMain:SortHullList()
  end
end
function GameUIHullMain:CheckHullCanGrow(hullInfo)
  local id = hullInfo.id
  local starLv = hullInfo.level
  local fleetId = hullInfo.fleet_configid
  local job = hullInfo.vessel
  if starLv < 1 or starLv > 9 then
    return false
  end
  local conditions = check_grow_RULL[starLv]
  local conditionType = conditions[1]
  local conditionStarLv = conditions[2]
  local conditionCount = conditions[3]
  local conditionItemCount = conditions[4]
  local specialItemHasCount = GameGlobalData:GetItemCount(21920)
  if conditionItemCount > specialItemHasCount then
    return false
  end
  local count = 0
  if conditionType == 1 then
    for k, v in ipairs(GameUIHullMain.hullListBag) do
      if v.id ~= id and v.fleet_configid == fleetId and v.level == conditionStarLv then
        count = count + 1
      end
    end
  elseif conditionType == 2 then
    for k, v in ipairs(GameUIHullMain.hullListBag) do
      if v.id ~= id and v.vessel == job and v.level == conditionStarLv then
        count = count + 1
      end
    end
  elseif conditionType == 3 then
    for k, v in ipairs(GameUIHullMain.hullListBag) do
      if v.id ~= id and v.level == conditionStarLv then
        count = count + 1
      end
    end
  end
  if conditionCount <= count then
    return true
  else
    return false
  end
end
function GameUIHullMain:SortHullList()
  local function sortFunc(a, b)
    if nil == a or nil == b or nil == a.status or nil == b.status or nil == a.rank or nil == b.rank then
      return false
    end
    if a.status == HULL_BAG_ITEM_STATUS_CANGROW and b.status ~= HULL_BAG_ITEM_STATUS_CANGROW then
      return true
    elseif a.status ~= HULL_BAG_ITEM_STATUS_CANGROW and b.status == HULL_BAG_ITEM_STATUS_CANGROW then
      return false
    elseif a.rank > b.rank then
      return true
    elseif a.rank < b.rank then
      return false
    elseif a.level > b.level then
      return true
    elseif a.level < b.level then
      return false
    elseif a.level == 10 and b.level ~= 10 then
      return true
    else
      if a.fleet_configid > b.fleet_configid then
        return true
      end
      return false
    end
  end
  table.sort(GameUIHullMain.hullListBag, sortFunc)
end
function GameUIHullMain:SortHullListChoosed()
  local function sortFunc3(a, b)
    if nil == a or nil == b or nil == a.status or nil == b.status or nil == a.rank or nil == b.rank then
      return false
    end
    if a.status == HULL_BAG_ITEM_STATUS_SELECTED and b.status ~= HULL_BAG_ITEM_STATUS_SELECTED then
      return true
    elseif a.status ~= HULL_BAG_ITEM_STATUS_SELECTED and b.status == HULL_BAG_ITEM_STATUS_SELECTED then
      return false
    elseif a.status == HULL_BAG_ITEM_STATUS_NORMAL and b.status ~= HULL_BAG_ITEM_STATUS_NORMAL then
      return true
    elseif a.status ~= HULL_BAG_ITEM_STATUS_NORMAL and b.status == HULL_BAG_ITEM_STATUS_NORMAL then
      return false
    elseif a.rank > b.rank then
      return true
    elseif a.rank < b.rank then
      return false
    elseif a.fleet_configid > b.fleet_configid then
      return true
    else
      return false
    end
  end
  table.sort(GameUIHullMain.hullListBag, sortFunc3)
end
function GameUIHullMain:SortHullList2()
  local sortFunc2 = function(a, b)
    if nil == a or nil == b or nil == a.rank or nil == b.rank then
      return false
    end
    if a.rank > b.rank then
      return true
    elseif a.rank < b.rank then
      return false
    elseif a.level > b.level then
      return true
    elseif a.level < b.level then
      return false
    elseif a.fleet_configid > b.fleet_configid then
      return true
    else
      return false
    end
  end
  table.sort(GameUIHullMain.hullListBag, sortFunc2)
end
function GameUIHullMain:GetHullItemInBagById(id)
  if nil == id then
    return nil, 0
  end
  local index = 0
  for _, v in pairs(GameUIHullMain.hullListBag) do
    index = index + 1
    if v.id == id then
      return v, index
    end
  end
  return nil, 0
end
function GameUIHullMain:SetHullBagList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    local cnt = math.ceil(GameUIHullMain.grid_capacity / 4)
    DebugOut("GameUIHullMain:SetBagList()" .. tostring(cnt))
    flashObj:InvokeASCallback("_root", "SetHullBagList", cnt)
  end
  GameUIHullMain:ClearHullChooseMainFleetShow()
end
function GameUIHullMain:SetHullBagList2()
  local flashObj = self:GetFlashObject()
  if flashObj then
    local specialItemHasCount = GameGlobalData:GetItemCount(21920)
    local cnt = math.ceil(GameUIHullMain.grid_capacity / 4)
    DebugOut("GameUIHullMain:SetBagList2()" .. tostring(cnt))
    flashObj:InvokeASCallback("_root", "SetHullBagList2", cnt, tostring(specialItemHasCount))
  end
end
function GameUIHullMain:SetHullGrowChooseUI(conditionType, specialItemCount, specialItemHasCount)
  local flashObj = self:GetFlashObject()
  if nil == flashObj then
    return
  end
  if conditionType == nil then
    conditionType = 0
  end
  local targetStarLevel = 0
  local targetRank = "green"
  local targetVessel = 0
  local fourItem = {}
  local hasSetSpecialItem = false
  for i = 1, 4 do
    local frame = ""
    local level = 0
    local rank = "green"
    local vessels = 0
    local itemRealStr = ""
    local itemReqStr = ""
    local item, bagIdx = GameUIHullMain:GetHullItemInBagById(mCurrentChoosedHulls[i])
    if nil ~= item then
      frame = GameDataAccessHelper:GetCommanderVesselsImage(item.fleet_configid, 1)
      level = item.level
      vessels = item.vessel
      rank = getRankColor(item.rank)
      if i == 1 then
        targetStarLevel = level
        targetRank = rank
        targetVessel = vessels
      end
    end
    if mCurrentChoosedHulls[i] == 0 then
      frame = "empty"
    elseif mCurrentChoosedHulls[i] == -1 then
      frame = "wait"
      if i > 1 and targetStarLevel > 0 and targetStarLevel < 10 then
        level = check_grow_RULL[targetStarLevel][2]
        if conditionType == 2 then
          rank = "blue"
        else
          rank = targetRank
        end
        vessels = targetVessel
      end
    elseif mCurrentChoosedHulls[i] == 3 then
      frame = "item"
      if specialItemHasCount < specialItemCount then
        itemRealStr = "<font color='#FF0000'>" .. specialItemHasCount .. "</font>"
      else
        itemRealStr = "<font color='#28D159'>" .. specialItemHasCount .. "</font>"
      end
      itemReqStr = "" .. specialItemCount
    end
    local one = {
      fr = frame,
      lv = level,
      rk = rank,
      job = vessels,
      itemreal = itemRealStr,
      itemreq = itemReqStr
    }
    fourItem[i] = one
  end
  flashObj:InvokeASCallback("_root", "SetHullGrowChoosed", fourItem[1], fourItem[2], fourItem[3], fourItem[4], conditionType)
end
function GameUIHullMain:RefreshHullBagList(onlyInBag)
  local cnt = math.ceil(GameUIHullMain.grid_capacity / 4)
  if true == onlyInBag then
    cnt = math.ceil(#GameUIHullMain.hullListBag / 4)
  end
  if cnt > 0 then
    for k = 1, cnt do
      GameUIHullMain:UpdateHullBagListItem(k)
    end
  end
end
function GameUIHullMain:ResetCurrentChoosedHulls()
  mCurrentChoosedHulls = {
    0,
    0,
    0,
    0
  }
end
function GameUIHullMain:UpdateHullBagListItem(itemId)
  local baseIdx = tonumber(itemId)
  local fourItem = {}
  for k = 1, 4 do
    local frame = ""
    local level = 0
    local rank = "green"
    local vessels = ""
    local idx = (baseIdx - 1) * 4 + k
    local item = GameUIHullMain.hullListBag[idx]
    if item then
      frame = GameDataAccessHelper:GetCommanderVesselsImage(item.fleet_configid, 1)
      level = item.level
      vessels = item.vessel
      rank = getRankColor(item.rank)
    else
      frame = "empty"
      level = 0
      vessels = 0
      rank = 0
    end
    local isHide = false
    if idx > GameUIHullMain.grid_capacity then
      isHide = true
    end
    local canGrow = false
    local isSelected = false
    local isLocked = false
    if nil ~= item then
      if item.status == HULL_BAG_ITEM_STATUS_CANGROW then
        canGrow = true
        isSelected = false
        isLocked = false
      elseif item.status == HULL_BAG_ITEM_STATUS_SELECTED then
        canGrow = false
        isSelected = true
        isLocked = false
      elseif item.status == HULL_BAG_ITEM_STATUS_LOCKED then
        canGrow = false
        isSelected = false
        isLocked = true
      end
    end
    local one = {
      isHide = isHide,
      fr = frame,
      lv = level,
      rk = rank,
      job = vessels,
      bGrow = canGrow,
      bSelected = isSelected,
      bLocked = isLocked
    }
    fourItem[k] = one
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateHullBagListItem", itemId, fourItem[1], fourItem[2], fourItem[3], fourItem[4])
  end
end
function GameUIHullMain:DealHullBagItemClicked(clickedItem, bagIndex)
  if nil == clickedItem then
    return
  end
  local uiItemIndex = math.ceil(bagIndex / 4)
  local id = clickedItem.id
  local starLv = clickedItem.level
  local fleetId = clickedItem.fleet_configid
  local job = clickedItem.vessel
  local conditions = {}
  local conditionType = 0
  local conditionStarLv = 0
  local conditionCount = 0
  local specialItemCount = 0
  local specialItemHasCount = 0
  if 0 < mCurrentChoosedHulls[1] then
    local selectedMainItem, bagIdx = GameUIHullMain:GetHullItemInBagById(mCurrentChoosedHulls[1])
    if nil ~= selectedMainItem then
      local starLv = selectedMainItem.level
      conditions = check_grow_RULL[starLv]
      conditionType = conditions[1]
      conditionStarLv = conditions[2]
      conditionCount = conditions[3]
      specialItemCount = conditions[4]
      specialItemHasCount = GameGlobalData:GetItemCount(21920)
    end
  end
  if clickedItem.status == HULL_BAG_ITEM_STATUS_CANGROW or clickedItem.status == HULL_BAG_ITEM_STATUS_NORMAL then
    if mCurrentChoosedHulls[1] == 0 then
      local starLv = clickedItem.level
      conditions = check_grow_RULL[starLv]
      conditionType = conditions[1]
      conditionStarLv = conditions[2]
      conditionCount = conditions[3]
      specialItemCount = conditions[4]
      specialItemHasCount = GameGlobalData:GetItemCount(21920)
      mCurrentChoosedHulls[1] = id
      for kk = 2, conditionCount + 1 do
        mCurrentChoosedHulls[kk] = -1
      end
      for kk = conditionCount + 2, 4 do
        if kk == conditionCount + 2 and specialItemCount > 0 then
          mCurrentChoosedHulls[kk] = 3
        else
          mCurrentChoosedHulls[kk] = 0
        end
      end
      for _, v in pairs(GameUIHullMain.hullListBag) do
        v.status = HULL_BAG_ITEM_STATUS_LOCKED
        if v.id == clickedItem.id then
          v.status = HULL_BAG_ITEM_STATUS_SELECTED
        elseif conditionType == 1 then
          if v.fleet_configid == fleetId and v.level == conditionStarLv then
            v.status = HULL_BAG_ITEM_STATUS_NORMAL
          end
        elseif conditionType == 2 then
          if v.vessel == job and v.level == conditionStarLv then
            v.status = HULL_BAG_ITEM_STATUS_NORMAL
          end
        elseif conditionType == 3 and v.level == conditionStarLv then
          v.status = HULL_BAG_ITEM_STATUS_NORMAL
        end
      end
      GameUIHullMain:SortHullListChoosed()
      GameUIHullMain:RefreshHullBagList(true)
      GameUIHullMain:RefreshHullChooseMainFleet(fleetId)
      GameUIHullMain:ScrollToTop()
    else
      local setSlot = -1
      for k = 2, conditionCount + 1 do
        if mCurrentChoosedHulls[k] == -1 then
          mCurrentChoosedHulls[k] = id
          setSlot = k
          break
        end
      end
      if setSlot ~= -1 then
        clickedItem.status = HULL_BAG_ITEM_STATUS_SELECTED
      end
      GameUIHullMain:UpdateHullBagListItem(uiItemIndex)
    end
  elseif clickedItem.status == HULL_BAG_ITEM_STATUS_SELECTED then
    if id == mCurrentChoosedHulls[1] then
      for k = 1, 4 do
        mCurrentChoosedHulls[k] = 0
      end
      GameUIHullMain:ResetHullBagListStatus(true)
      GameUIHullMain:RefreshHullBagList(true)
      GameUIHullMain:ClearHullChooseMainFleetShow()
    else
      for k = 2, conditionCount + 1 do
        if mCurrentChoosedHulls[k] == id then
          mCurrentChoosedHulls[k] = -1
          break
        end
      end
      clickedItem.status = HULL_BAG_ITEM_STATUS_NORMAL
      GameUIHullMain:UpdateHullBagListItem(uiItemIndex)
    end
  end
  GameUIHullMain:SetHullGrowChooseUI(conditionType, specialItemCount, specialItemHasCount)
end
function GameUIHullMain:RefreshHullChooseMainFleet(fleetId)
  local shipIdStr = GameDataAccessHelper:GetCommanderVesselsImage(fleetId, 1)
  GameFleetInfoBackground:OnlyShowOneFleetById(shipIdStr)
  local fleet_name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(fleetId, 1))
  if fleetId == 1 then
    fleet_name = GameLoader:GetGameText("LC_NPC_NPC_New")
  end
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(fleetId, 1)
  local vesselsType = GameDataAccessHelper:GetCommanderVesselsType(fleetId, 1)
  local fleetColor = GameDataAccessHelper:GetCommanderColorFrame(fleetId, 1)
  local flashObj = self:GetFlashObject()
  if nil ~= flashObj then
    flashObj:InvokeASCallback("_root", "setFleetHullUpGradeChoosed", fleet_name, iconFrame, vesselsType, fleetColor)
  end
end
function GameUIHullMain:ClearHullChooseMainFleetShow()
  GameFleetInfoBackground:ClearShowOneFleet()
  local flashObj = self:GetFlashObject()
  if nil ~= flashObj then
    flashObj:InvokeASCallback("_root", "clearFleetHullUpGradeChoosed")
  end
end
function GameUIHullMain:CallHullUpGradeReq()
  local toUpGradeId = mCurrentChoosedHulls[1]
  if toUpGradeId == nil or toUpGradeId <= 0 then
    return
  end
  local item, bagIdx = GameUIHullMain:GetHullItemInBagById(toUpGradeId)
  if nil ~= item then
    mHullToBeUpGrade = {}
    mHullToBeUpGrade.fleetId = item.fleet_configid
    mHullToBeUpGrade.starLv = item.level
    mHullToBeUpGrade.job = item.vessel
  else
    return
  end
  local material_list = {}
  for i = 2, 4 do
    local value = mCurrentChoosedHulls[i]
    if value > 0 and value ~= 3 then
      table.insert(material_list, value)
    end
  end
  local param = {}
  param.id = toUpGradeId
  param.material_id_list = material_list
  NetMessageMgr:SendMsg(NetAPIList.hull_upgrade_req.Code, param, self.HullUpGradeCallBack, true, nil)
end
function GameUIHullMain.HullUpGradeCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.hull_upgrade_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  if msgtype == NetAPIList.hull_upgrade_ack.Code and content.errorcode == 0 then
    GameUIHullMain:ResetHullBagListStatus(true)
    GameUIHullMain:RefreshHullBagList(true)
    GameUIHullMain:ClearHullChooseMainFleetShow()
    GameUIHullMain:ResetCurrentChoosedHulls()
    GameUIHullMain:SetHullGrowChooseUI()
    GameUIHullMain.hasGetHullList = true
    GameUIHullMain:ShowHullUpGradeInfo()
    return true
  end
  return false
end
function GameUIHullMain:ShowHullUpGradeInfo()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  if mHullToBeUpGrade == nil then
    return
  end
  local tparam = {}
  tparam.shipframe = GameDataAccessHelper:GetCommanderVesselsImage(mHullToBeUpGrade.fleetId, 1)
  tparam.shipname = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(mHullToBeUpGrade.fleetId, 1))
  if mHullToBeUpGrade.fleetId == 1 then
    tparam.shipname = GameLoader:GetGameText("LC_NPC_NPC_New")
  end
  tparam.preStar = mHullToBeUpGrade.starLv
  tparam.newStar = mHullToBeUpGrade.starLv + 1
  tparam.job = mHullToBeUpGrade.job
  tparam.propparam = GameFleetEquipment:GetHullPropChangeInfo(mHullToBeUpGrade.job, tparam.preStar)
  flash:InvokeASCallback("_root", "setGradeUpSuccessHullDisplay", tparam)
  flash:InvokeASCallback("_root", "InitHullUpgradeSuccess", {
    listparam = tparam.propparam
  })
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  flash:InvokeASCallback("_root", "showPop_HullUpGradeSuccess", {lang = lang})
  mHullToBeUpGrade = nil
end
function GameUIHullMain:UpdateSingleHullInfo(hullInfo)
  if nil == hullInfo then
    return
  end
  local bfind = false
  for k, v in ipairs(GameUIHullMain.hullListBag) do
    if v.id == hullInfo.id then
      v.level = hullInfo.level
      v.fleet_configid = hullInfo.fleet_configid
      v.is_equipon = hullInfo.is_equipon
      v.rank = hullInfo.rank
      v.vessel = hullInfo.vessel
      v.prams = hullInfo.prams
      bfind = true
      return
    end
  end
  if false == bfind then
    table.insert(GameUIHullMain.hullListBag, hullInfo)
  end
end
function GameUIHullMain.UpdateHullListChange(content)
  local function removeHullById(toRemoveId)
    for k, v in ipairs(GameUIHullMain.hullListBag) do
      if v.id == toRemoveId then
        table.remove(GameUIHullMain.hullListBag, k)
        return
      end
    end
  end
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateEquipEnhance then
    GameFleetEquipment.zhuangbei:DoHullListChange(content)
  end
  if GameUIHullMain.hasGetHullList == false then
    return
  end
  local deleteCnt = #content.remove_ids
  if deleteCnt > 0 then
    GameUIHullMain:PlayDecomposeAnimInBag()
  end
  for i = 1, deleteCnt do
    local toRemoveId = content.remove_ids[i]
    removeHullById(toRemoveId)
  end
  for i = 1, #content.update_hulls do
    GameUIHullMain:UpdateSingleHullInfo(content.update_hulls[i])
  end
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateHull then
    if mCurrentFuncTypeIndex == 2 then
      GameUIHullMain:ResetCurrentChoosedHulls()
      GameUIHullMain:ResetHullBagListStatus(true)
      GameUIHullMain:SetHullBagList()
      GameUIHullMain:SetHullGrowChooseUI()
    elseif mCurrentFuncTypeIndex == 3 then
      GameUIHullMain:SortHullList2()
      GameUIHullMain:SetHullBagList2()
    end
  end
end
function GameUIHullMain:UpdateHullBagListItem2(itemId)
  local baseIdx = tonumber(itemId)
  local fourItem = {}
  for k = 1, 4 do
    local frame = ""
    local level = 0
    local rank = "green"
    local vessels = ""
    local idx = (baseIdx - 1) * 4 + k
    local item = GameUIHullMain.hullListBag[idx]
    if item then
      frame = GameDataAccessHelper:GetCommanderVesselsImage(item.fleet_configid, 1)
      level = item.level
      vessels = item.vessel
      rank = getRankColor(item.rank)
    else
      frame = "empty"
      level = 0
      vessels = 0
      rank = 0
    end
    local isHide = false
    if idx > GameUIHullMain.grid_capacity then
      isHide = true
    end
    local isSelected = IsSelectedInBatchDecompose(idx)
    local isBatchDecompose = IsBatchDecompose()
    if item and item.is_equipon == 1 then
      isSelected = false
      isBatchDecompose = false
    end
    local one = {
      isHide = isHide,
      fr = frame,
      lv = level,
      rk = rank,
      job = vessels,
      bSelected = isSelected,
      bIsBatchDecompose = isBatchDecompose
    }
    fourItem[k] = one
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateHullBagListItem2", itemId, fourItem[1], fourItem[2], fourItem[3], fourItem[4])
  end
end
function GameUIHullMain:DealClickItemInBag2(clickedItem, bagIndex, showCompose)
  local isEquiped = clickedItem.is_equipon
  local rank = getRankColor(clickedItem.rank)
  local shipframe = GameDataAccessHelper:GetCommanderVesselsImage(clickedItem.fleet_configid, 1)
  local shipName = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(clickedItem.fleet_configid, 1))
  if clickedItem.fleet_configid == 1 then
    shipName = GameLoader:GetGameText("LC_NPC_NPC_New")
  end
  local showtext1 = GameLoader:GetGameText("LC_HULL_Hull_item_des")
  showtext1 = string.gsub(showtext1, "<NPC_NAME>", shipName)
  local showtext2 = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), 1)
  local equipText = GameLoader:GetGameText("LC_MENU_NEW_FLEET_NO_EQUIPMENT")
  if isEquiped > 0 then
    equipText = GameLoader:GetGameText("LC_MENU_NEW_FLEET_EQUIPED")
  end
  local shipInfo = {
    id = clickedItem.id,
    fr = shipframe,
    lv = clickedItem.level,
    rk = rank,
    job = clickedItem.vessel
  }
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowHullDetailPop", shipInfo, shipName, showtext1, showtext2, equipText, showCompose)
  end
end
function GameUIHullMain:ShowClickRewardHull(clickedReward)
  local isEquiped = 0
  local shipframe = GameDataAccessHelper:GetCommanderVesselsImage(clickedReward.number, 1)
  local shipName = ""
  if clickedReward.number == 1 then
    shipName = GameLoader:GetGameText("LC_FLEET_TYPE_6")
  else
    shipName = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(clickedReward.number, 1))
  end
  local showtext1 = GameLoader:GetGameText("LC_HULL_Hull_item_des")
  showtext1 = string.gsub(showtext1, "<NPC_NAME>", shipName)
  local showtext2 = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), 1)
  local equipText = GameLoader:GetGameText("LC_MENU_NEW_FLEET_NO_EQUIPMENT")
  if isEquiped > 0 then
    equipText = GameLoader:GetGameText("LC_MENU_NEW_FLEET_EQUIPED")
  end
  local shipInfo = {
    id = clickedReward.number,
    fr = shipframe,
    lv = clickedReward.rankFrame,
    rk = clickedReward.rankColor,
    job = clickedReward.job
  }
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowHullDetailPop", shipInfo, shipName, showtext1, showtext2, equipText, false)
  end
end
local mDecomposeRow = -1
local mDecomposeCol = -1
function GameUIHullMain:CallDecomposeHullItem(hullId)
  local choosedItem, bagIdx = GameUIHullMain:GetHullItemInBagById(hullId)
  if bagIdx > 0 then
    mDecomposeRow = math.ceil(bagIdx / 4)
    mDecomposeCol = bagIdx - (mDecomposeRow - 1) * 4
  end
  GameFleetEquipment.zhuangbei:requestBreakHull(hullId, choosedItem.level)
end
function GameUIHullMain:GetHullUpGradeSpecialItemCount(targetLv)
  local retValue = 0
  for i = 1, #check_grow_RULL do
    if i < targetLv then
      retValue = retValue + check_grow_RULL[i][4]
    end
  end
  return retValue
end
function GameUIHullMain:CloseHullItemDetailPop()
  local flashObj = GameUIHullMain:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideHullDetailPop")
  end
end
function GameUIHullMain:DecomposeHullItemBack()
  ResetBatchDecompose()
  GameUIHullMain:SortHullList2()
  GameUIHullMain:SetHullBagList2()
  GameUIHullMain:CloseHullItemDetailPop()
end
function GameUIHullMain:PlayDecomposeAnimInBag()
  if mDecomposeRow >= 0 and mDecomposeCol >= 0 then
    local flashObj = GameUIHullMain:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "PlayHullItemDecomposeAnim", mDecomposeRow, mDecomposeCol)
    end
  end
  mDecomposeRow = -1
  mDecomposeCol = -1
end
function GameUIHullMain:SetHasShowFleetBackGround(boolVal)
  mHasShowFleetBackGround = boolVal
end
function GameUIHullMain:GetHasShowFleetBackGround()
  return mHasShowFleetBackGround
end
function GameUIHullMain:ScrollToTop()
  local flashObj = GameUIHullMain:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ScrollToTop")
  end
end
