local fakeBattleResult2 = {
  player1 = "vddfv. vbb.s991",
  player1_identity = 1,
  player1_pos = 5,
  player2_identity = 6000203,
  player2_pos = 8,
  player2_avatar = "head13",
  player2 = "battle",
  player1_avatar = "female",
  result = 1,
  rounds = {
    [1] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal_new2",
          def_pos = 2,
          shield = 0,
          atk_acc_change = 25,
          acc = 25,
          intercept = false,
          atk_acc = 25,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 73,
          durability = 67,
          atk_pos = 5,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = true,
      dot = {},
      pos = 5,
      round_cnt = 1
    },
    [2] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 50,
          intercept = false,
          atk_acc = 50,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 22,
          durability = 88,
          atk_pos = 2,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 2,
      round_cnt = 2
    },
    [3] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 75,
          intercept = false,
          atk_acc = 25,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 21,
          durability = 67,
          atk_pos = 5,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 5,
      round_cnt = 3
    },
    [4] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 100,
          intercept = false,
          atk_acc = 25,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 24,
          durability = 43,
          atk_pos = 5,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 8,
      round_cnt = 4
    },
    [5] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 0,
          def_pos = 2,
          sp_id = 2,
          shield = 0,
          buff_effect = 0,
          acc = 75,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 105,
          round_cnt = 0,
          atk_pos = 5,
          crit = false
        },
        [2] = {
          durability = 0,
          def_pos = 5,
          sp_id = 2,
          shield = 0,
          buff_effect = 0,
          acc = 50,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 113,
          round_cnt = 0,
          atk_pos = 5,
          crit = false
        },
        [3] = {
          durability = 0,
          def_pos = 8,
          sp_id = 2,
          shield = 0,
          buff_effect = 0,
          acc = 50,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 189,
          round_cnt = 0,
          atk_pos = 5,
          crit = true
        }
      },
      buff = {},
      player1_action = true,
      dot = {},
      pos = 5,
      round_cnt = 5
    }
  },
  player1_fleets = {
    [1] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 110,
      cur_accumulator = 0,
      max_durability = 110,
      identity = 1,
      pos = 5
    }
  },
  player2_fleets = {
    [1] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 140,
      cur_accumulator = 0,
      max_durability = 140,
      identity = 6000201,
      pos = 2
    },
    [2] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 80,
      cur_accumulator = 0,
      max_durability = 80,
      identity = 6000202,
      pos = 5
    },
    [3] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 80,
      cur_accumulator = 0,
      max_durability = 80,
      identity = 6000203,
      pos = 8
    }
  }
}
local fakeBattleAnim2 = {
  [1] = {
    round_index = 4,
    round_step = 2,
    attack_round_index = -1,
    dialog_id = {1100011},
    befor_dialog_anim = "in skill_hilight",
    hide_anim_in_dialog = false,
    after_dialog_anim = "ou skill_hilight"
  },
  [2] = {
    round_index = 5,
    round_step = 2,
    attack_round_index = -1,
    dialog_id = {1100012},
    befor_dialog_anim = "in skill_hilight",
    hide_anim_in_dialog = false,
    after_dialog_anim = "ou skill_hilight"
  }
}
GameData.fakeBattleAnim2 = fakeBattleAnim2
GameData.fakeBattleResult2 = fakeBattleResult2
