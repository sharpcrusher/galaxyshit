local GameUIStarCraftEnter = LuaObjectManager:GetLuaObject("GameUIStarCraftEnter")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
function GameUIStarCraftEnter:OnInitGame()
end
function GameUIStarCraftEnter:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameTimer:Add(self._OnTimerTick, 1000)
  self:SetBasicData(true)
end
function GameUIStarCraftEnter:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIStarCraftEnter:Show()
  if GameGlobalData.starCraftData and GameGlobalData.starCraftData.type == 2 then
    if GameGlobalData.starCraftData.open == false or GameGlobalData.starCraftData.open and GameGlobalData.starCraftData.status ~= 1 then
      GameStateManager:GetCurrentGameState():AddObject(GameUIStarCraftEnter)
    else
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarCraft)
    end
  else
    local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarCraft)
  end
end
function GameUIStarCraftEnter:OnFSCommand(cmd, arg)
  if cmd == "Close" then
    GameStateManager:GetCurrentGameState():EraseObject(GameUIStarCraftEnter)
  elseif cmd == "TouchHelp" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_TC_HELP_DESC"))
  elseif cmd == "TouchRewardIcon" then
    local poslist = LuaUtils:string_split(arg, "|")
    local itemType = ""
    if not GameHelper:IsResource(GameGlobalData.starCraftData.reward.item_type) then
      itemType = GameGlobalData.starCraftData.reward.number
      if GameGlobalData.starCraftData.reward.item_type == "equip" then
        ItemBox:showItemBox("Equip", nil, itemType, tonumber(poslist[1]), tonumber(poslist[2]), tonumber(poslist[3]), tonumber(poslist[4]), "", nil, "", nil)
      else
        ItemBox:showItemBox("Item", nil, itemType, tonumber(poslist[1]), tonumber(poslist[2]), tonumber(poslist[3]), tonumber(poslist[4]), "", nil, "", nil)
      end
    end
  elseif cmd == "TouchSign" then
    GameUIStarCraftEnter:OnSingUp()
  end
end
function GameUIStarCraftEnter:UpdateData()
  if GameGlobalData.starCraftData and GameGlobalData.starCraftData.type == 1 then
    GameStateManager:GetCurrentGameState():EraseObject(GameUIStarCraftEnter)
  elseif GameGlobalData.starCraftData and GameGlobalData.starCraftData.status == 1 and GameGlobalData.starCraftData.open then
    GameStateManager:GetCurrentGameState():EraseObject(GameUIStarCraftEnter)
  else
    self:SetBasicData(false)
  end
end
function GameUIStarCraftEnter:SetBasicData(movein)
  local data = {}
  local iconName = GameHelper:GetAwardTypeIconFrameName(GameGlobalData.starCraftData.reward.item_type, GameGlobalData.starCraftData.reward.number, GameGlobalData.starCraftData.reward.no)
  local open = GameGlobalData.starCraftData.open
  local status = GameGlobalData.starCraftData.status
  local tipText = GameLoader:GetGameText("LC_MENU_PANDORA_UNSIGNED_TIP")
  data.iconName = iconName
  data.open = open
  data.status = status
  data.tipText = tipText
  data.signupText = GameLoader:GetGameText("LC_MENU_PANDORA_WAR_SIGN")
  data.signupedText = GameLoader:GetGameText("LC_MENU_PANDORA_WAR_SIGNED")
  data.signupnextText = GameLoader:GetGameText("LC_MENU_PANDORA_WAR_SIGN_NEXT")
  data.signupednextText = GameLoader:GetGameText("LC_MENU_PANDORA_WAR_SIGNED_NEXT")
  data.rewardText = GameLoader:GetGameText("LC_MENU_PANDORA_REWARD_PREVIEW")
  self:GetFlashObject():InvokeASCallback("_root", "setBasicData", data, movein)
  DebugOut("GameUIStarCraftEnter:SetBasicData")
  DebugTable(data)
  self:UpdateEndTime()
end
function GameUIStarCraftEnter:UpdateEndTime()
  local leftTime = GameGlobalData.starCraftData.end_time - (os.time() - GameGlobalData.starCraftData.curLocalTime)
  if leftTime < 0 then
    leftTime = 0
  end
  local colorText = ""
  local tipText = ""
  if not GameGlobalData.starCraftData.open then
    colorText = "<font color=#00CC00>%s:</font><font color=#FFC926><number1></font>"
    tipText = GameLoader:GetGameText("LC_MENU_PANDORA_BATTLE_COMING")
  else
    colorText = "<font color=#BDFFFF>%s:</font><font color=#FFC926>" .. GameLoader:GetGameText("LC_MENU_PANDORA_ENDS_TIME") .. "</font>"
    tipText = GameLoader:GetGameText("LC_MENU_PANDORA_ONGOING")
  end
  local timetip = string.format(colorText, tipText)
  timetip = string.gsub(timetip, "<number1>", GameUtils:formatTimeStringAsPartion2(leftTime))
  self:GetFlashObject():InvokeASCallback("_root", "setEndTime", timetip)
end
function GameUIStarCraftEnter:OnSingUp()
  local params = {}
  params.id = GameGlobalData:GetData("userinfo").player_id
  params.logic_id = GameUtils:GetActiveServerInfo().logic_id
  NetMessageMgr:SendMsg(NetAPIList.star_craft_sign_up_req.Code, params, GameUIStarCraftEnter.RequestSignUpCallBack, true, nil)
end
function GameUIStarCraftEnter.RequestSignUpCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.star_craft_sign_up_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      if GameGlobalData.starCraftData.open or GameGlobalData.starCraftData.status == 3 then
        GameGlobalData.starCraftData.status = 2
      else
        GameGlobalData.starCraftData.status = 1
      end
      GameUIStarCraftEnter:SetBasicData(false)
      DebugOut("sign up successful")
    end
    return true
  end
  return false
end
function GameUIStarCraftEnter._OnTimerTick()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIStarCraftEnter) then
    GameUIStarCraftEnter:UpdateEndTime()
    return 1000
  end
  return nil
end
if AutoUpdate.isAndroidDevice then
  function GameUIStarCraftEnter.OnAndroidBack()
    self:GetFlashObject():InvokeASCallback("_root", "move_out")
  end
end
