local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameUIEvent = LuaObjectManager:GetLuaObject("GameUIEvent")
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialsChangeFleets1 = TutorialQuestManager.QuestTutorialsChangeFleets1
local QuestTutorialGetGift = TutorialQuestManager.QuestTutorialGetGift
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local QuestTutorialPveMapPoint = TutorialQuestManager.QuestTutorialPveMapPoint
local QuestTutorialTheFirstEvent = TutorialQuestManager.QuestTutorialTheFirstEvent
local EventType = {}
EventType.get_award = 2
function GameUIEvent._netMessageCallback(msg_type, content)
  if msg_type == NetAPIList.common_ack.Code and content.api == NetAPIList.pve_battle_req.Code then
    local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    GameUIEvent:UnlockInput()
    GameStateManager:GetCurrentGameState():EraseObject(GameUIEvent)
    return true
  elseif msg_type == NetAPIList.event_loot_ack.Code then
    GameUIEvent:ProcessEventData(content)
    GameUIEvent:ContinueLoading()
    if GameUIEvent.event_id == 1001103 then
      if immanentversion == 1 then
        if not QuestTutorialEquip:IsActive() and not QuestTutorialEquip:IsFinished() then
          if immanentversion170 == nil then
            QuestTutorialEquip:SetActive(true)
          end
          GameUIBarRight:CheckQuestTutorial()
        end
      elseif immanentversion == 2 and not QuestTutorialsChangeFleets1:IsFinished() then
        QuestTutorialsChangeFleets1:SetActive(true)
        GameUIBarRight:CheckQuestTutorial()
      end
    elseif GameUIEvent.event_id == 1001105 then
      GameUIEvent.finishSecondBag = true
    end
    return true
  end
  return false
end
function GameUIEvent:OnAddToGameState(parent_state)
  assert(self.event_id)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:UpdateEventInfo()
  self:AnimationEventInfoMovein()
  self.is_active = true
  if not self._randomseed then
    math.randomseed(os.time())
    self._randomseed = true
  end
  GameUIEvent:showTutorialTip()
end
function GameUIEvent:OnEraseFromGameState(parent_state)
  self.event_id = nil
  self.event_data = nil
  self.is_active = nil
  self:UnloadFlashObject()
end
function GameUIEvent:IsActive()
  return self.is_active
end
function GameUIEvent:ContinueLoading()
  self:GetFlashObject():InvokeASCallback("_root", "continueLoading")
end
function GameUIEvent:UnlockInput()
  self:GetFlashObject():InvokeASCallback("_root", "UnlockInput")
end
function GameUIEvent:OnFSCommand(cmd, arg)
  if cmd == "process_event" then
    if self.event_type == 3 then
      GameStateBattleMap:EraseObject(self)
      local GameStateFormation = GameStateManager.GameStateFormation
      GameStateFormation:SetBattleID(GameStateBattleMap.m_currentAreaID, self.m_battleID)
      GameStateManager:SetCurrentGameState(GameStateFormation)
      if QuestTutorialTheFirstEvent:IsActive() then
        QuestTutorialTheFirstEvent:SetFinish(true)
      end
    else
      local areaID, battleID = GameUtils:ResolveBattleID(self.event_id)
      if areaID == 1 and battleID == 1103 then
        AddFlurryEvent("ProcessBattleMapEvent_1_1_1", {}, 1)
      elseif areaID == 1 and battleID == 1105 then
        AddFlurryEvent("ProcessBattleMapEvent_1_1_2", {}, 1)
      elseif areaID == 1 and battleID == 1109 then
        AddFlurryEvent("ProcessBattleMapEvent_1_1_3", {}, 1)
      end
      local battle_request = {
        battle_id = self.event_id,
        action = 1
      }
      NetMessageMgr:SendMsg(NetAPIList.pve_battle_req.Code, battle_request, self._netMessageCallback, false, nil)
    end
    if k_FlurryEvent[self.event_id] then
      AddFlurryEvent(k_FlurryEvent[self.event_id], {}, 2)
    end
  elseif cmd == "renounce_event" then
    local battle_request = {
      battle_id = self.event_id,
      action = 1
    }
    NetMessageMgr:SendMsg(NetAPIList.pve_battle_req.Code, battle_request, self._netMessageCallback, false, nil)
    if k_FlurryEvent[self.event_id] then
      AddFlurryEvent(k_FlurryEvent[self.event_id], {}, 2)
    end
    if QuestTutorialTheFirstEvent:IsActive() then
      QuestTutorialTheFirstEvent:SetFinish(true)
    end
  elseif cmd == "click_ok" then
    local areaID, battleID = GameUtils:ResolveBattleID(self.event_id)
    print("!~ areaID=", areaID, "battleID=", battleID)
    if areaID == 1 and battleID == 1103 then
      AddFlurryEvent("BattleMapEventConfirmReward_1_1_1", {}, 1)
    elseif areaID == 1 and battleID == 1105 then
      AddFlurryEvent("BattleMapEventConfirmReward_1_1_2", {}, 1)
    elseif areaID == 1 and battleID == 1109 then
      AddFlurryEvent("BattleMapEventConfirmReward_1_1_3", {}, 1)
    end
  elseif cmd == "on_erase" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    local function callback()
      GameStateBattleMap:OnBattleOrEventFinish(self.m_battleID)
      local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
    end
    GameStateBattleMap:ReEnter(callback)
  end
end
function GameUIEvent:Visible(is_visible, object_name)
  object_name = object_name or -1
  self:GetFlashObject():InvokeASCallback("_root", "SetVisible", object_name, is_visible)
end
function GameUIEvent:AnimationEventInfoMovein()
  self:GetFlashObject():InvokeASCallback("_root", "AnimationEventInfoMovein")
end
function GameUIEvent:AnimationEventInfoMoveout()
  self:GetFlashObject():InvokeASCallback("_root", "AnimationEventInfoMoveout")
end
function GameUIEvent:AnimationLoadingMovein()
  self:GetFlashObject():InvokeASCallback("_root", "AnimationLoadingMovein")
end
function GameUIEvent:AnimationLoadingMoveout()
  self:GetFlashObject():InvokeASCallback("_root", "AnimationLoadingMoveout")
end
function GameUIEvent:UpdateAwardItem(index_award, award_type, award_text, award_count)
  award_type = award_type or -1
  award_count = award_count or -1
  self:GetFlashObject():InvokeASCallback("_root", "UpdateAwardItem", index_award, award_type, award_text, award_count)
end
function GameUIEvent:UpdateEventInfo()
  local min_event_id = self.battle_info.EventInfo[1]
  local max_event_id = self.battle_info.EventInfo[2]
  if max_event_id then
    self.event_info_id = min_event_id + math.random(0, max_event_id - min_event_id)
  else
    self.event_info_id = min_event_id
  end
  local event_title = GameLoader:GetGameText("LC_EVENT_TITLE_" .. tostring(self.event_info_id))
  local event_desc = GameLoader:GetGameText("LC_EVENT_DES_" .. tostring(self.event_info_id))
  local event_style = -1
  if self.event_type == 3 then
    event_style = "battle"
  end
  DebugOut(event_title, ", ", event_desc, ", ", event_style)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateEventInfo", event_title, event_desc, event_style)
end
function GameUIEvent:UpdateResultInfo(event_success)
  local result_desc
  if event_success then
    result_desc = GameLoader:GetGameText("LC_EVENT_RESULT_" .. tostring(self.event_info_id))
  else
    result_desc = GameLoader:GetGameText("LC_EVENT_FAILED_" .. tostring(self.event_info_id))
  end
  DebugOut(result_desc)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateResultInfo", result_desc)
end
function GameUIEvent:ShowEvent(battle_id, combined_id)
  self.event_id = combined_id
  self.battle_info = GameDataAccessHelper:GetBattleInfo(GameUtils:ResolveBattleID(self.event_id))
  self.m_battleID = battle_id
  self.event_type = self.battle_info.EVENT_TYPE
  GameStateManager:GetCurrentGameState():AddObject(self)
end
function GameUIEvent:ProcessEventData(event_data)
  self.event_data = event_data
  self:UpdateResultInfo(true)
  for index_event = 1, 5 do
    local award_data = event_data.loot[index_event]
    if award_data then
      local item_type = award_data.item_type
      local item_count = award_data.number
      local itemName, icon = GameHelper:GetAwardTypeTextAndIcon(award_data.item_type, award_data.number)
      if item_type == "equip" or item_type == "item" then
        if DynamicResDownloader:IsDynamicStuff(award_data.number, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(award_data.number .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            item_type = "item_" .. award_data.number
          else
            item_type = "temp"
            self:AddDownloadPath(award_data.number, 0, 0)
          end
        else
          item_type = "item" .. "_" .. award_data.number
        end
        itemName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(award_data.number))
        item_count = -1
      end
      self:UpdateAwardItem(index_event, item_type, itemName, item_count)
    else
      self:UpdateAwardItem(index_event, -1, -1, -1)
    end
  end
end
function GameUIEvent:AddDownloadPath(itemID, itemKey, index)
  DebugStore("Add download path", itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function GameUIEvent:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "UpdateFrame", dt)
end
function GameUIEvent:showTutorialTip(...)
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  flash_obj:InvokeASCallback("_root", "showTutorial", false)
  if self.event_type == 3 and QuestTutorialTheFirstEvent:IsActive() then
    flash_obj:InvokeASCallback("_root", "showTutorial", true)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIEvent.OnAndroidBack()
  end
end
