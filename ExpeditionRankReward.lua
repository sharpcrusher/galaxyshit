local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
GameObjectClimbTower.ExpeditionRankReward = {}
local ExpeditionRankReward = GameObjectClimbTower.ExpeditionRankReward
GameObjectClimbTower.BlackHoleRankReward = {}
local BlackHoleRankReward = GameObjectClimbTower.BlackHoleRankReward
ExpeditionRankReward.isShow = false
function ExpeditionRankReward:Show()
  if GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_EXPEDITION then
    if not ExpeditionRankReward.rewardContent or not ExpeditionRankReward.rewardContent.ranking_awards then
      ExpeditionRankReward:RequestRankRewards()
      return
    end
    self:GetFlashObject():InvokeASCallback("_root", "showRankRewardList", #ExpeditionRankReward.rewardContent.ranking_awards)
    ExpeditionRankReward.isShow = true
  elseif GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_BLACKHOLE then
    if not BlackHoleRankReward.rewardContent or not BlackHoleRankReward.rewardContent.ranking_awards then
      ExpeditionRankReward:RequestRankRewards()
      return
    end
    self:GetFlashObject():InvokeASCallback("_root", "showRankRewardList", #BlackHoleRankReward.rewardContent.ranking_awards)
    ExpeditionRankReward.isShow = true
  end
end
function ExpeditionRankReward:IsShow()
  return ExpeditionRankReward.isShow
end
function ExpeditionRankReward:RequestRankRewards()
  DebugOut("ExpeditionRankReward:RequestRankRewards()")
  local param = {
    dungeon_categor = GameObjectClimbTower.category
  }
  NetMessageMgr:SendMsg(NetAPIList.expedition_rank_awards_req.Code, param, ExpeditionRankReward.RequestRankRewardsCallBack, true, nil)
end
function ExpeditionRankReward.RequestRankRewardsCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.expedition_rank_awards_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.expedition_rank_awards_ack.Code then
    DebugOut("ExpeditionRankReward.RequestRankRewardsCallBack")
    DebugTable(content)
    if GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_EXPEDITION then
      ExpeditionRankReward.rewardContent = content
    elseif GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_BLACKHOLE then
      BlackHoleRankReward.rewardContent = content
    end
    local SortCallback = function(a, b)
      if a.id < b.id then
        return true
      end
    end
    if GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_EXPEDITION then
      table.sort(ExpeditionRankReward.rewardContent.ranking_awards, SortCallback)
    elseif GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_BLACKHOLE then
      table.sort(BlackHoleRankReward.rewardContent.ranking_awards, SortCallback)
    end
    ExpeditionRankReward:Show()
    return true
  end
  return false
end
function ExpeditionRankReward:GetFlashObject()
  return (...), GameObjectClimbTower
end
function ExpeditionRankReward:Hide()
  ExpeditionRankReward:GetFlashObject():InvokeASCallback("_root", "hideRewardMenu")
end
function ExpeditionRankReward:OnFSCommand(cmd, arg)
  if "btn_RankReward_exp" == cmd then
    ExpeditionRankReward:Show()
  elseif "UpdateRankRewardItem" == cmd then
    ExpeditionRankReward:UpdateRankRewardItem(tonumber(arg))
  elseif "closeRewardMenu" == cmd then
    ExpeditionRankReward.isShow = false
  end
end
function ExpeditionRankReward.UpdateRankRewardItemIcon(extInfo)
  DebugOut("UpdateRankRewardItemIcon")
  DebugTable(extInfo)
  extInfo.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo, nil, nil)
  ExpeditionRankReward:GetFlashObject():InvokeASCallback("_root", "UpdateRankRewardItemIcon", extInfo)
end
function ExpeditionRankReward:UpdateRankRewardItem(indexItem)
  local DataString = -1
  local ItemInfo, rankRewards
  if GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_EXPEDITION then
    rankRewards = ExpeditionRankReward.rewardContent.ranking_awards
  elseif GameObjectClimbTower.category == GameUIActivityNew.category.TYPE_BLACKHOLE then
    rankRewards = BlackHoleRankReward.rewardContent.ranking_awards
  end
  if indexItem > 0 and indexItem <= #rankRewards then
    ItemInfo = rankRewards[indexItem]
  end
  if ItemInfo then
    local DataTable = {}
    table.insert(DataTable, ItemInfo.begin_rank)
    table.insert(DataTable, ItemInfo.end_rank)
    table.insert(DataTable, #ItemInfo.awards)
    for i = 1, #ItemInfo.awards do
      local extInfo = ItemInfo.awards[i]
      extInfo.indexItem = indexItem
      extInfo.mcIndex = i
      table.insert(DataTable, GameHelper:GetAwardTypeIconFrameNameSupportDynamic(ItemInfo.awards[i], extInfo, ExpeditionRankReward.UpdateRankRewardItemIcon))
      table.insert(DataTable, GameHelper:GetAwardCount(ItemInfo.awards[i].item_type, ItemInfo.awards[i].number, ItemInfo.awards[i].no))
    end
    DataString = table.concat(DataTable, "\001")
  end
  DebugOut("UpdateRankRewardItem ", indexItem, DataString)
  self:GetFlashObject():InvokeASCallback("_root", "setRankRewardItemData", indexItem, DataString)
end
