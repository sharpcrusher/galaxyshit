require("DummyGameItem.tfl")
DummyItemBag = luaClass(nil)
function DummyItemBag:ctor(bagTitleType)
  self.mBagTitleType = bagTitleType
  self.mLastItemIndex = 0
  self.mItemList = {}
end
function DummyItemBag:AddItem(dummyItem)
  self.mLastItemIndex = self.mLastItemIndex + 1
  self.mItemList[self.mLastItemIndex] = dummyItem
end
function DummyItemBag:SetItem(itemRealPos, dummyItem)
  for i, v in ipairs(self.mItemList) do
    if v.pos == itemRealPos then
      self.mItemList[i] = dummyItem
      return
    end
  end
end
function DummyItemBag:RemoveItem(dummyItem)
end
function DummyItemBag:UpdateItem(dummyItem)
end
function DummyItemBag:RefreshBag()
  local index = 1
  DebugOut("RefreshBag table = ")
  DebugTable(self.mItemList)
  while index <= #self.mItemList do
    DebugOut("RefreshBag index = ", index)
    if self.mItemList[index].mNotInRealBag then
      table.remove(self.mItemList, index)
    else
      index = index + 1
    end
  end
  self.mLastItemIndex = #self.mItemList
  self:SortItems()
  local destTable = {}
  for i, v in ipairs(self.mItemList) do
    table.insert(destTable, v)
  end
  self.mItemList = destTable
end
function DummyItemBag:SortItems()
  local sortFunc = function(a, b)
    return a.mSubType > b.mSubType
  end
  table.sort(self.mItemList, sortFunc)
end
function DummyItemBag:GetItemsCount()
  return #self.mItemList
end
function DummyItemBag:GetDummyItem(itemIndex)
  return self.mItemList[itemIndex]
end
function DummyItemBag:ClearBag()
  self.mLastItemIndex = 0
  self.mItemList = {}
end
function DummyItemBag:IsItemInBag(itemRealPos)
  for i, v in ipairs(self.mItemList) do
    if v.pos == itemRealPos then
      return true
    end
  end
  return false
end
function DummyItemBag:GetDummyItemByID(itemRealPos)
  for i, v in ipairs(self.mItemList) do
    if v.pos == itemRealPos then
      return v
    end
  end
  return nil
end
function DummyItemBag:ProcessItemsInbag(realBag, clearBag)
  if clearBag then
    self:ClearBag()
  end
  local realKeyValueBag = {}
  for k, v in pairs(realBag) do
    local dummyItem = self:GetDummyItemByID(v.pos)
    if v.title_type == self.mBagTitleType then
      realKeyValueBag[v.pos] = v
      if dummyItem then
        DebugTable(dummyItem)
        dummyItem.item_type = v.item_type
        dummyItem.mGridType = v.grid_type
        dummyItem.mTitleType = v.title_type
        dummyItem.mSubType = v.sub_type
        dummyItem.pos = v.pos
        dummyItem.cnt = v.cnt
      else
        local newDummyItem = DummyGameItem.new(v.item_id, v.item_type, v.grid_type, v.title_type, v.sub_type, v.pos, v.cnt)
        self:AddItem(newDummyItem)
      end
    end
  end
  for iDummy, vDummy in ipairs(self.mItemList) do
    if not realKeyValueBag[vDummy.pos] then
      self.mItemList[iDummy].mNotInRealBag = true
    else
      self.mItemList[iDummy].mNotInRealBag = false
    end
  end
end
function DummyItemBag:updateDummyBags(itemsChanged)
  for k, v in pairs(itemsChanged) do
    local dummyItem = self:GetDummyItemByID(v.pos)
    if v.item_type == 0 then
      if dummyItem then
        dummyItem.mNotInRealBag = true
      end
    elseif v.title_type == self.mTitleType then
      if dummyItem then
        dummyItem.item_type = v.item_type
        dummyItem.mGridType = v.grid_type
        dummyItem.mTitleType = v.title_type
        dummyItem.mSubType = v.sub_type
        dummyItem.pos = v.pos
        dummyItem.cnt = v.cnt
      else
        local newDummyItem = DummyGameItem.new(v.item_id, v.item_type, v.grid_type, v.title_type, v.sub_type, v.pos, v.cnt)
        self:AddItem(newDummyItem)
      end
    end
  end
end
function DummyItemBag:GetSize()
  return #self.mItemList
end
function DummyItemBag:GetLastPosInDummyBag()
  return self.mLastItemIndex
end
