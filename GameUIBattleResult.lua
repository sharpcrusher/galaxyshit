local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameStateRecruit = GameStateManager.GameStateRecruit
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local AlertDataList = AlertDataList
local GameGlobalData = GameGlobalData
local GameLoader = GameLoader
local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
local GameStateKrypton = GameStateManager.GameStateKrypton
local GameStateArcane = GameStateManager.GameStateArcane
local GameStateFormation = GameStateManager.GameStateFormation
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local QuestTutorialEnhance_third = TutorialQuestManager.QuestTutorialEnhance_third
local QuestTutorialRechargePower = TutorialQuestManager.QuestTutorialRechargePower
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local QuestTutorialBattleFailed = TutorialQuestManager.QuestTutorialBattleFailed
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local QuestTutorialMakeUserLevel10 = TutorialQuestManager.QuestTutorialMakeUserLevel10
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
local GameUIArcaneEnemyInfo = LuaObjectManager:GetLuaObject("GameUIArcaneEnemyInfo")
local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameTimer = GameTimer
local GameStateInstance = GameStateManager.GameStateInstance
local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameFleetNewEnhance = LuaObjectManager:GetLuaObject("GameFleetNewEnhance")
local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameObjectPlayerMatrix = LuaObjectManager:GetLuaObject("GameObjectPlayerMatrix")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameStateFactory = GameStateManager.GameStateFactory
local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
local GameArtifact = LuaObjectManager:GetLuaObject("GameArtifact")
local GameStateTacticsCenter = GameStateManager.GameStateTacticsCenter
local RefineDataManager = LuaObjectManager:GetLuaObject("RefineDataManager")
local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
local QuestTutorialGetGift = TutorialQuestManager.QuestTutorialGetGift
local QuestTutorialPveMapPoint = TutorialQuestManager.QuestTutorialPveMapPoint
local percent = 0
local maxPercent = 0
local gainedExpValue = 0
local currentExpValue = 0
GameUIBattleResult.m_combinedBattleID = -1
GameUIBattleResult.m_battleID = -1
GameUIBattleResult.m_areaID = -1
GameUIBattleResult.m_accomplishLevel = -1
local isConditionNotMeet = false
GameUIBattleResult.battleState = false
GameUIBattleResult.mShareArenaWinChecked = false
GameUIBattleResult.mShareMineWinChecked = false
GameUIBattleResult.mShareStoryChecked = {}
GameUIBattleResult.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN] = true
GameUIBattleResult.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN] = true
GameUIBattleResult.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_WIN] = true
GameUIBattleResult.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_FIGHT_WIN] = true
GameUIBattleResult.mCurStoryType = nil
GameUIBattleResult.closeCallBack = nil
GameUIBattleResult.failed_list = nil
function GameUIBattleResult:SetCombinedBattleID(combined_id)
  if combined_id and combined_id > 0 then
    local area_id, battle_id = GameUtils:ResolveBattleID(combined_id)
    self.m_battleID = battle_id
    self.m_areaID = area_id
    self.m_combinedBattleID = combined_id
  else
    self.m_combinedBattleID = -1
    self.m_battleID = -1
    self.m_areaID = -1
  end
  DebugOut("\230\137\147\229\141\176SetCombinedBattleID", self.m_combinedBattleID)
end
function GameUIBattleResult:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
  end
end
function GameUIBattleResult:CheckFlashObject()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  return (...), self
end
function GameUIBattleResult:SortAwardList(windowType, awardDataTable)
  if windowType == "battle_win" or windowType == "challenge_win" then
    do
      local itemPriority = {
        credit = 1,
        prestige = 2,
        money = 3,
        exp = 4,
        item = 5
      }
      DebugOut("GameUIBattleResult:SortAwardList")
      DebugTable(awardDataTable)
      local function compFun(v1, v2)
        local prioV1 = itemPriority[v1.itemType]
        local prioV2 = itemPriority[v2.itemType]
        if prioV1 ~= nil and prioV2 ~= nil then
          return prioV1 < prioV2
        elseif prioV2 == nil then
          return true
        else
          return false
        end
      end
      table.sort(awardDataTable, compFun)
    end
  end
  return awardDataTable
end
function GameUIBattleResult:UpdateAwardList(windowType, awardDataTable)
  DebugOut("UpdateAwardList")
  GameUtils:printTable(awardDataTable)
  local sortedAwardDataTable = self:SortAwardList(windowType, awardDataTable)
  GameUIBattleResult.awardDataTable = sortedAwardDataTable
  GameUtils:printTable(sortedAwardDataTable)
  for indexAward = 1, 3 do
    local awardData = sortedAwardDataTable[indexAward]
    if awardData then
      if awardData.isDirectSet ~= nil and awardData.isDirectSet then
        self:CheckFlashObject():InvokeASCallback("_root", "setAwardItem", windowType, indexAward, awardData.itemIcon, awardData.itemDesc, awardData.itemNumber, GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese())
      else
        self:UpdateAwardItem(windowType, indexAward, awardData.itemType, awardData.itemNumber)
      end
    else
      self:UpdateAwardItem(windowType, indexAward, -1, -1)
    end
  end
end
function GameUIBattleResult:GetItemTextAndNum2(item_type, item_text)
  local itemType_tmp = item_type
  if item_type == "item" then
    if DynamicResDownloader:IsDynamicStuff(item_text, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(item_text .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        itemType_tmp = "item_" .. item_text
      else
        itemType_tmp = "temp"
        GameUIActivity:AddDownloadPath(item_text, 0, 0)
      end
    else
      itemType_tmp = "item_" .. item_text
    end
    if tonumber(item_text) > 100 and tonumber(item_text) < 199 then
      item_text = GameLoader:GetGameText("LC_ITEM_BLUEPRINT_CHAR")
    else
      item_text = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(item_text))
    end
  elseif item_type == "equip" then
    itemType_tmp = "item_" .. item_text
    if tonumber(item_text) > 100000 and tonumber(item_text) < 199999 then
      item_text = GameLoader:GetGameText("LC_ITEM_EQUIP_TYPE_SLOT" .. GameDataAccessHelper:GetEquipSlot(item_text))
    else
      item_text = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(item_text))
    end
  elseif item_type == "money" then
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_CUBIT")
  elseif item_type == "vip_exp" then
    item_text = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR")
  elseif item_type == "ac_supply" then
    item_text = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR")
  elseif item_type == "laba_supply" then
    item_text = GameLoader:GetGameText("LC_MENU_DEXTER_COIN")
  elseif item_type == "credit" then
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  elseif item_type == "technique" then
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_TECHPOINT")
  elseif item_type == "expedition" then
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_EXPEDITION")
  elseif item_type == "prestige" then
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_PRESTIGE")
  elseif item_type == "lepton" then
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_LEPTON")
  elseif item_type == "quark" then
    item_text = GameLoader:GetGameText("LC_MENU_QUARK_CHAR")
  elseif item_type == "brick" then
    item_text = GameLoader:GetGameText("LC_MENU_BRICK_CHAR")
  elseif item_type == "wd_supply" then
    item_text = GameLoader:GetGameText("LC_MENU_WD_SUPPLY_CHAR")
  elseif item_type == "mine_digg_licence_one" then
    item_text = GameLoader:GetGameText("LC_MENU_MINE_CHANCE_EVENT")
  elseif item_type == "mine_fight_licence_one" then
    item_text = GameLoader:GetGameText("LC_MENU_PLUNDER_CHANCE_EVENT")
  elseif item_type == "krypton" then
    item_text = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(item_text))
  elseif item_type == "crusade_repair" then
    item_text = GameLoader:GetGameText("LC_MENU_TBC_REPAIR_ENERGY")
  else
    item_text = item_type
  end
  return item_text, itemType_tmp
end
function GameUIBattleResult:GetItemTextAndNum3(item_type, no, number)
  local number_text = 1
  local itemType_tmp = item_type
  local item_text = ""
  if item_type == "item" then
    itemType_tmp = "item_" .. number
    if tonumber(number) > 100 and tonumber(number) < 199 then
      item_text = GameLoader:GetGameText("LC_ITEM_BLUEPRINT_CHAR")
    else
      item_text = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(number))
    end
    number_text = no
  elseif item_type == "equip" then
    itemType_tmp = "item_" .. number
    if tonumber(number) > 100000 and tonumber(number) < 199999 then
      item_text = GameLoader:GetGameText("LC_ITEM_EQUIP_TYPE_SLOT" .. GameDataAccessHelper:GetEquipSlot(number))
    else
      item_text = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(number))
    end
    number_text = no
  elseif item_type == "money" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_CUBIT")
  elseif item_type == "vip_exp" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR")
  elseif item_type == "ac_supply" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR")
  elseif item_type == "laba_supply" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_DEXTER_COIN")
  elseif item_type == "credit" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  elseif item_type == "technique" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_TECHPOINT")
  elseif item_type == "expedition" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_EXPEDITION")
  elseif item_type == "prestige" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_PRESTIGE")
  elseif item_type == "kenergy" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_krypton_energy")
  elseif item_type == "lepton" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_LEPTON")
  elseif item_type == "quark" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_QUARK_CHAR")
  elseif item_type == "brick" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_BRICK_CHAR")
  elseif item_type == "wd_supply" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_WD_SUPPLY_CHAR")
  elseif item_type == "mine_digg_licence_one" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_MINE_CHANCE_EVENT")
  elseif item_type == "mine_fight_licence_one" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_PLUNDER_CHANCE_EVENT")
  elseif item_type == "krypton" then
    number_text = no
    item_text = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(item_text))
  elseif item_type == "tc_point" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_TC_POINT_LABEL")
  elseif item_type == "crusade_repair" then
    item_text = GameLoader:GetGameText("LC_MENU_TBC_REPAIR_ENERGY")
    number_text = number
  else
    item_text = item_type
  end
  return item_text, number_text, itemType_tmp
end
function GameUIBattleResult:GetItemTextAndNum(item_type, item_text)
  DebugOut("GetItemTextAndNum " .. item_type .. " item_text " .. item_text)
  local number_text = 1
  local itemType_tmp = item_type
  if item_type == "item" then
    itemType_tmp = "item_" .. item_text
    if tonumber(item_text) > 100 and tonumber(item_text) < 199 then
      item_text = GameLoader:GetGameText("LC_ITEM_BLUEPRINT_CHAR")
    else
      item_text = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(item_text))
    end
  elseif item_type == "equip" then
    itemType_tmp = "item_" .. item_text
    if tonumber(item_text) > 100000 and tonumber(item_text) < 199999 then
      item_text = GameLoader:GetGameText("LC_ITEM_EQUIP_TYPE_SLOT" .. GameDataAccessHelper:GetEquipSlot(item_text))
    else
      item_text = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(item_text))
    end
  elseif item_type == "wd_point" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_MERIT")
  elseif item_type == "money" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_CUBIT")
  elseif item_type == "vip_exp" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR")
  elseif item_type == "ac_supply" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR")
  elseif item_type == "laba_supply" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_DEXTER_COIN")
  elseif item_type == "credit" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  elseif item_type == "technique" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_TECHPOINT")
  elseif item_type == "expedition" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_EXPEDITION")
  elseif item_type == "prestige" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_PRESTIGE")
  elseif item_type == "lepton" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_LOOT_LEPTON")
  elseif item_type == "quark" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_QUARK_CHAR")
  elseif item_type == "krypton" then
    item_text = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(item_text))
  elseif item_type == "crusade_repair" then
    number_text = item_text
    item_text = GameLoader:GetGameText("LC_MENU_TBC_REPAIR_ENERGY")
  elseif item_type == "tc_point" then
    number_text = number
    item_text = GameLoader:GetGameText("LC_MENU_TC_POINT_LABEL")
  else
    item_text = item_type
  end
  return item_text, number_text, itemType_tmp
end
function GameUIBattleResult:UpdateAwardItem(award_type, award_index, itemType, itemText)
  local item_text, number_text, itemType = GameUIBattleResult:GetItemTextAndNum(itemType, itemText)
  self:CheckFlashObject():InvokeASCallback("_root", "setAwardItem", award_type, award_index, itemType, item_text, number_text, GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese())
end
function GameUIBattleResult:UpdateAwardItem2(award_type, award_index, itemType, itemText, itemNumber)
  local item_text, itemType = GameUIBattleResult:GetItemTextAndNum2(itemType, itemText)
  self:CheckFlashObject():InvokeASCallback("_root", "setAwardItem", award_type, award_index, itemType, item_text, itemNumber, GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese())
end
function GameUIBattleResult:UpdateAwardItem3(award_type, award_index, itemType, no, number)
  local item_text, number_text, itemType = GameUIBattleResult:GetItemTextAndNum3(itemType, no, number)
  self:CheckFlashObject():InvokeASCallback("_root", "setAwardItem", award_type, award_index, itemType, item_text, number_text, GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese())
end
function GameUIBattleResult:updateColonialWinInfo(planet, displayName, lv, msg)
  self:CheckFlashObject():InvokeASCallback("_root", "updateColonialWinInfo", planet, displayName, lv, msg, GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese())
end
function GameUIBattleResult:ShowItemDetial(arg)
  DebugOut("GameUIBattleResult:ShowItemDetial")
  local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
  local awardData = GameUIBattleResult.awardDataTable[arg]
  if not awardData then
    DebugOut("awardData == nil")
    return
  end
  DebugTable(awardData)
  if awardData.isDirectSet ~= nil and awardData.isDirectSet then
    if awardData.itemType == "item" then
      local item = {}
      item.number = awardData.itemID
      item.cnt = awardData.itemNumber
      item.no = awardData.itemNumber
      item.item_type = "item"
      ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    elseif awardData.itemType == "krypton" then
      do
        local item = {}
        item.number = awardData.itemID
        item.level = awardData.itemLevel
        if item.level == 0 then
          item.level = 1
        end
        local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
          local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
          if ret then
            local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
            if tmp == nil then
              return false
            end
            ItemBox:SetKryptonBox(tmp, item.number)
            local operationTable = {}
            operationTable.btnUnloadVisible = false
            operationTable.btnUpgradeVisible = false
            operationTable.btnUseVisible = false
            operationTable.btnDecomposeVisible = false
            ItemBox:ShowKryptonBox(320, 240, operationTable)
          end
          return ret
        end)
        if detail == nil then
        else
          ItemBox:SetKryptonBox(detail, item.number)
          local operationTable = {}
          operationTable.btnUnloadVisible = false
          operationTable.btnUpgradeVisible = false
          operationTable.btnUseVisible = false
          operationTable.btnDecomposeVisible = false
          ItemBox:ShowKryptonBox(320, 240, operationTable)
        end
      end
    end
  elseif awardData.itemType == "item" then
    local item = {}
    if tonumber(awardData.itemDesc) then
      item.number = awardData.itemDesc
    else
      item.number = awardData.itemNumber
    end
    item.cnt = 1
    item.no = 1
    item.item_type = "item"
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
end
function GameUIBattleResult:GetLanguage()
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    elseif GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese() then
      lang = "zh"
    end
  end
  return lang
end
function GameUIBattleResult:ShowArenaWin(dataTable)
  local stringTable = {}
  stringTable[#stringTable + 1] = dataTable.winnerRank
  stringTable[#stringTable + 1] = dataTable.winnerName
  stringTable[#stringTable + 1] = dataTable.winnerLevel
  stringTable[#stringTable + 1] = dataTable.loserRank
  stringTable[#stringTable + 1] = dataTable.loserName
  stringTable[#stringTable + 1] = dataTable.loserLevel
  local dataString = table.concat(stringTable, "\001")
  if dataTable.awardTable then
    for index = 1, 2 do
      local awardinfo = dataTable.awardTable[index]
      local itemType = -1
      local itemDesc = -1
      local itemNumber = -1
      if awardinfo then
        itemType = awardinfo.itemType
        itemDesc = awardinfo.itemDesc
        itemNumber = awardinfo.itemNumber
      end
      if itemType == "equip" or itemType == "item" then
        itemType = "item_" .. itemDesc
        itemDesc = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. tostring(itemDesc))
        itemNumber = -1
      end
      local item_text, number_text = GameUIBattleResult:GetItemTextAndNum(itemType, itemNumber)
      self:GetFlashObject():InvokeASCallback("_root", "setArenaAward", index, itemType, item_text, number_text)
    end
  end
  GameUIBattleResult.loserName = dataTable.loserName
  local shareStoryEnabled = false
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN) then
    shareStoryEnabled = true
  end
  GameUtils:StopMusic()
  GameUtils:PlaySound("GE2_BattleWin.mp3")
  local lang = self:GetLanguage()
  self:GetFlashObject():InvokeASCallback("_root", "showArenaWin", dataString, shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN], lang)
end
function GameUIBattleResult:ShowAdvancedWin(dataTable)
  DebugOut("ShowAdvancedWin = ")
  DebugTable(dataTable)
  if self:GetFlashObject() then
    local lang = self:GetLanguage()
    self:GetFlashObject():InvokeASCallback("_root", "showAdvancedWin", dataTable, lang)
  end
end
function GameUIBattleResult:ShowStarSystemWin(dataTable)
  DebugOut("ShowAdvancedWin = ")
  DebugTable(dataTable)
  if self:GetFlashObject() then
    local lang = self:GetLanguage()
    self:GetFlashObject():InvokeASCallback("_root", "showStarSystemWin", dataTable, lang)
  end
end
function GameUIBattleResult:ShowFPSWin(dataTable)
  DebugOut("ShowFPSWin = ")
  DebugTable(dataTable)
  if self:GetFlashObject() then
    local lang = self:GetLanguage()
    self:GetFlashObject():InvokeASCallback("_root", "ShowFPSWin", dataTable, lang)
  end
end
function GameUIBattleResult:UpdateStarLevel(starLevel)
  if self.m_renderFx ~= nil then
    self.m_renderFx:InvokeASCallback("_root", "UpdateStarLevel", starLevel)
  end
end
function GameUIBattleResult:MoveOutBattleResult()
  DebugOut("MoveOutBattleResult")
  if self.m_renderFx ~= nil then
    self.m_renderFx:InvokeASCallback("_root", "MoveOutBattleResult")
  end
end
local status_table
function GameUIBattleResult:RandomizeImprovement(battle_type)
  if self.m_renderFx ~= nil then
    local random_value = math.random(1, 100)
    local selectedSolution
    local canEnter = ""
    local desText = ""
    local pve_progress = GameGlobalData:GetData("progress")
    status_table = {}
    status_table.formation = "normal"
    if GameGlobalData:BuildingCanEnter(GameStateEquipEnhance.buildingName) then
      status_table.equip = "normal"
    else
      status_table.equip = "disable"
    end
    if GameGlobalData:fix_BuildingCanEnter(GameStateKrypton.buildingName) then
      status_table.krypton = "normal"
    else
      status_table.krypton = "disable"
    end
    if GameGlobalData:fix_BuildingCanEnter(GameStateRecruit.buildingName) then
      status_table.recruit = "normal"
    else
      status_table.recruit = "disable"
    end
    if GameGlobalData:fix_BuildingCanEnter(GameUITechnology.buildingName) then
      status_table.tech = "normal"
    else
      status_table.tech = "disable"
    end
    if GameGlobalData:fix_BuildingCanEnter(GameUIFactory.buildingName) then
      DebugOut("GameUIFactoryenter")
      status_table.factory = "normal"
    else
      status_table.factory = "disable"
    end
    local level_info = GameGlobalData:GetData("levelinfo")
    if level_info.level >= 18 and level_info.level <= 25 then
      selectedSolution = "recruit"
    elseif level_info.level >= 26 and level_info.level <= 30 then
      selectedSolution = "krypton"
    else
      if random_value >= 1 and random_value <= 50 then
        selectedSolution = "equip"
      elseif random_value >= 51 and random_value <= 75 then
        selectedSolution = "krypton"
      elseif random_value >= 76 and random_value <= 90 then
        selectedSolution = "recruit"
      else
        selectedSolution = "tech"
      end
      local progress = GameGlobalData:GetData("progress")
      if progress.act == 1 then
        status_table.equip = "normal"
        selectedSolution = "equip"
      end
    end
    DebugOut("selectedSolution - ", selectedSolution)
    if status_table[selectedSolution] ~= "disable" then
      status_table[selectedSolution] = "recommend"
      desText = GameLoader:GetGameText("LC_MENU_RECOMMEND_" .. string.upper(selectedSolution))
    end
    for k, v in pairs(status_table) do
      self:SetSolutionStatus(k, v, battle_type)
    end
    GameUIBattleResult:UpdatePlayerForece(battle_type, status_table)
    self:CheckFlashObject():InvokeASCallback("_root", "setRecommendDes", desText)
  end
end
function GameUIBattleResult.BattleRateNtfCall(content)
  DebugOut("BattleRateNtfCall")
  DebugTable(content)
  GameUIBattleResult.battlerate = content
  if content.cli_data ~= "" then
    DebugOut("cli_data?")
    GameUIBattleResult:UpdatePlayerForece(1, status_table)
  end
end
function GameUIBattleResult:UpdatePlayerForece(battle_type, status_table)
  DebugOut("UpdatePlayerForece")
  local power_point = 0
  local player_info = GameGlobalData:GetUserInfo()
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local player_avatar = ""
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  if leaderlist and curMatrixIndex then
    player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[curMatrixIndex], player_main_fleet.level)
  else
    player_avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
  end
  local player_name = GameUtils:GetUserDisplayName(player_info.name)
  if battle_type == 1 then
    DebugOut("fuck-battlerate------->")
    DebugTable(GameUIBattleResult.battlerate)
    if GameUIBattleResult.battlerate then
      local rate = GameUIBattleResult.battlerate.player1_rate
      failed_list = {}
      local enhance_text = GameLoader:GetGameText("LC_MENU_TEXT_ENHANCE")
      if status_table.equip ~= "disable" then
        table.insert(failed_list, {
          score = rate.equip,
          name = "equip",
          text = GameLoader:GetGameText("LC_MENU_SLAC_EQUIPMENT_BUTTON"),
          enhance = enhance_text
        })
      end
      if status_table.krypton ~= "disable" then
        table.insert(failed_list, {
          score = rate.krypton,
          name = "krypton",
          text = GameLoader:GetGameText("LC_MENU_SLAC_KRYPTON_BUTTON"),
          enhance = enhance_text
        })
      end
      if status_table.tech ~= "disable" then
        table.insert(failed_list, {
          score = rate.technique,
          name = "technique",
          text = GameLoader:GetGameText("LC_MENU_TECH_BUTTON"),
          enhance = enhance_text
        })
      end
      if status_table.recruit ~= "disable" then
        table.insert(failed_list, {
          score = rate.recruit,
          name = "recruit",
          text = GameLoader:GetGameText("LC_MENU_SLAC_RECRUITMENT_BUTTON"),
          enhance = enhance_text
        })
      end
      if GameFleetNewEnhance:GetArtUpgradeState() == 1 then
        table.insert(failed_list, {
          score = rate.art,
          name = "art",
          text = GameLoader:GetGameText("LC_MENU_SLAC_ART_BUTTON"),
          enhance = enhance_text
        })
      end
      if RefineDataManager:GetIsOpenTacticsCenter() == 1 and GameUtils:IsModuleUnlock("centalsystem") then
        table.insert(failed_list, {
          score = rate.tactical,
          name = "tactical",
          text = GameLoader:GetGameText("LC_MENU_SLAC_TACTICAL_BUTTON"),
          enhance = enhance_text
        })
      end
      local level_info = GameGlobalData:GetData("levelinfo")
      if level_info.level >= 80 then
        table.insert(failed_list, {
          score = rate.medal,
          name = "medal",
          text = GameLoader:GetGameText("LC_MENU_SLAC_MEDAL_BUTTON"),
          enhance = enhance_text
        })
      end
      if immanentversion170 == 4 or immanentversion170 == 5 then
        if status_table.factory ~= "disable" then
          table.insert(failed_list, {
            score = rate.remodel,
            name = "remodel",
            text = GameLoader:GetGameText("LC_MENU_SLAC_REMODEL_BUTTON"),
            enhance = enhance_text
          })
        end
      elseif status_table.factory ~= "disable" and level_info.level >= 30 then
        table.insert(failed_list, {
          score = rate.remodel,
          name = "remodel",
          text = GameLoader:GetGameText("LC_MENU_SLAC_REMODEL_BUTTON"),
          enhance = enhance_text
        })
      end
      local minValue, minName
      for k, v in pairs(failed_list) do
        DebugOut("k=", k, "v=", v)
        if not minValue or minValue > v.score then
          minValue = v.score
          minName = v.name
        end
        DebugOut("minValue=", minValue, "minName=", minName)
      end
      failed_list.min = minName
      local lang = self:GetLanguage()
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "UpdateBattleLostScorepve", failed_list, lang)
      end
    else
      DebugOut("Fuck  \230\156\141\229\138\161\229\153\168\230\178\161\230\142\168\233\128\129\230\136\152\230\150\151\232\175\132\229\136\134\230\149\176\230\141\174\232\191\135\230\157\165")
    end
  elseif battle_type == 2 then
    local lang = self:GetLanguage()
    self:GetFlashObject():InvokeASCallback("_root", "UpdatePlayerInfo_pvp", player_name, player_avatar, power_point)
    self:GetFlashObject():InvokeASCallback("_root", "UpdateBattleLostScorepvp", math.random(1, 100), math.random(1, 100), math.random(1, 100), math.random(1, 100), math.random(1, 100), math.random(1, 100), math.random(1, 100), math.random(1, 100), lang)
  end
end
function GameUIBattleResult:SetSolutionStatus(nameSolution, statusSolution, battle_type)
  self:CheckFlashObject():InvokeASCallback("_root", "setSolutionStatus", nameSolution, statusSolution, battle_type)
end
function GameUIBattleResult:UpdateExperiencePercent(hideExpUp)
  local lastLevelInfo = GameGlobalData:GetLastLevelInfo()
  DebugOut("lastLevelInfo.experience", lastLevelInfo.experience, "lastLevelInfo.uplevel_experience ", lastLevelInfo.uplevel_experience)
  DebugTable(lastLevelInfo)
  local lastExpPercent = math.ceil(lastLevelInfo.experience / lastLevelInfo.uplevel_experience * 100)
  if GameUtils:isPlayerMaxLevel(lastLevelInfo.level) then
    self:CheckFlashObject():InvokeASCallback("_root", "setLastExpPercent", 100)
  else
    self:CheckFlashObject():InvokeASCallback("_root", "setLastExpPercent", lastExpPercent)
  end
  local level_info = GameGlobalData:GetData("levelinfo")
  local exp_percent = 0
  local up_level = 0
  local up_experience = 0
  if level_info ~= nil then
    exp_percent = math.ceil(level_info.experience / level_info.uplevel_experience * 100)
    up_level = level_info.level - lastLevelInfo.level
    up_experience = level_info.experience - lastLevelInfo.experience
  else
    level_info = lastLevelInfo
    exp_percent = lastExpPercent
  end
  DebugOut("up_level", up_level)
  if up_level > 0 then
    up_experience = lastLevelInfo.uplevel_experience - lastLevelInfo.experience + level_info.experience
  end
  if hideExpUp then
    up_experience = 0
  end
  if exp_percent == 0 then
    exp_percent = 1
  end
  DebugOut("up_experience=", up_experience, "level_info.experience ", level_info.experience, "level_info.uplevel_experience ", level_info.uplevel_experience)
  if GameUtils:isPlayerMaxLevel(level_info.level) then
    self:CheckFlashObject():InvokeASCallback("_root", "updateExperiencePercent", 100, 1, 0, GameLoader:GetGameText("LC_MENU_Level") .. level_info.level)
    self:CheckFlashObject():InvokeASCallback("_root", "setExpNumInProcessBar", "-", "-", true)
  else
    self:CheckFlashObject():InvokeASCallback("_root", "updateExperiencePercent", exp_percent, level_info.experience, up_experience, GameLoader:GetGameText("LC_MENU_Level") .. level_info.level)
    self:CheckFlashObject():InvokeASCallback("_root", "setExpNumInProcessBar", level_info.experience, level_info.uplevel_experience, false)
  end
end
function GameUIBattleResult:UpdateAccomplishLevel(accomplish_level)
  self:CheckFlashObject():InvokeASCallback("_root", "updateAccomplishLevel", accomplish_level)
end
function GameUIBattleResult:AnimationMoveIn(window_type, isFinishBattle, battleWinDesc, shareStoryType)
  local shareStoryEnabled = false
  local curStoryChecked = false
  local winDescText = battleWinDesc
  GameUtils:StopMusic()
  if window_type == "battle_win" then
    GameUtils:PlaySound("GE2_BattleWin.mp3")
  elseif window_type == "battle_lose" then
    self:RandomizeImprovement(1)
    GameUtils:PlaySound("GE2_BattleLose.mp3")
  elseif window_type == "challenge_win" then
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMineMap and FacebookGraphStorySharer:IsStoryEnabled(shareStoryType) then
      shareStoryEnabled = true
      DebugOut("shareStoryEnabled = true")
      curStoryChecked = self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN]
    end
    GameUtils:PlaySound("GE2_BattleWin.mp3")
  elseif window_type == "colonial_win" then
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateColonization and FacebookGraphStorySharer:IsStoryEnabled(shareStoryType) then
      GameUIBattleResult.mCurStoryType = shareStoryType
      shareStoryEnabled = true
      curStoryChecked = GameUIBattleResult.mShareStoryChecked[shareStoryType]
    end
    GameUtils:PlaySound("GE2_BattleWin.mp3")
  elseif window_type == "challenge_lose" or window_type == "colonial_lose" then
    self:RandomizeImprovement(1)
    GameUtils:PlaySound("GE2_BattleLose.mp3")
  else
    assert(false)
  end
  local lang = self:GetLanguage()
  self.LastBattleType = window_type
  self:CheckFlashObject():InvokeASCallback("_root", "animationMoveIn", window_type, shareStoryEnabled, curStoryChecked, winDescText, lang)
  self.m_isFinishBattle = isFinishBattle
  GameUIBattleResult:HideTutorialBattleReplayBtn()
  GameUIBattleResult:ShowTipAnimation()
  self:ShowTipAnimationWin()
  DebugOut("fuck GameUIBattleResult.m_combinedBattleID", GameUIBattleResult.m_combinedBattleID)
end
function GameUIBattleResult:ShowTipAnimation()
  if self.LastBattleType == "battle_lose" and GameUIBattleResult.m_combinedBattleID == 1001011 and not TutorialQuestManager.QuestTutorialSecondHeroLoadEQU:IsFinished() then
    TutorialQuestManager.QuestTutorialSecondHeroLoadEQU:SetActive(true)
    self:CheckFlashObject():InvokeASCallback("_root", "showPveEquipmentTipAnimation")
  end
end
function GameUIBattleResult:SetCurrentBattleState(state)
  if state == 0 then
    self.battleState = true
  else
    self.battleState = false
  end
end
function GameUIBattleResult:ShowTipAnimationWin(...)
  self:CheckFlashObject():InvokeASCallback("_root", "isShowTutorialComonOnWin", false)
  if self.LastBattleType == "battle_win" and QuestTutorialPveMapPoint:IsActive() and self.battleState and GameUIBattleResult.m_combinedBattleID == 1001001 then
    self:CheckFlashObject():InvokeASCallback("_root", "isShowTutorialComonOnWin", true)
  end
end
function GameUIBattleResult:HideTutorialBattleReplayBtn()
  DebugOut("GameUIBattleResult:HideTutorialBattleReplayBtn")
  if GameUIBattleResult.m_combinedBattleID == 60001001 or GameUIBattleResult.m_combinedBattleID == 60001002 or GameUIBattleResult.m_combinedBattleID == 60001007 or GameUIBattleResult.m_combinedBattleID == 600010013 then
    self:CheckFlashObject():InvokeASCallback("_root", "hideTutorialBattleReplayBtn")
  end
end
function GameUIBattleResult:OnAddToGameState()
  self:LoadFlashObject()
  math.randomseed(ext.getSysTime())
  if immanentversion == 2 then
    self:DoSpectialTutorial()
  end
end
function GameUIBattleResult:RegisterOverCallback(func, args)
  self._overCallback = {}
  self._overCallback.func = func
  self._overCallback.args = args
end
function GameUIBattleResult:ExecuteOverCallback()
  if self._overCallback then
    local callback = self._overCallback.func
    local args = self._overCallback.args
    if args and #args > 0 then
      callback(unpack(args))
    else
      callback(args)
    end
    self._overCallback = nil
  end
end
function GameUIBattleResult:OnEraseFromGameState()
  if self:GetFlashObject() then
    self:UnloadFlashObject()
  end
  GameUIBattleResult.awardDataTable = nil
  GameUIBattleResult.battlerate = nil
  local music = GameUtils.ActiveMusic or "GE2_Battle.mp3"
  GameUtils.ActiveMusic = ""
  GameUtils:PlayMusic(music)
  if immanentversion == 2 and GameUIEnemyInfo.m_combinedBattleID then
    local flurryName = k_FlurryFinishBattle[GameUIEnemyInfo.m_combinedBattleID]
    if flurryName then
      local infoSend = {}
      local winType = string.lower(self.LastBattleType)
      if string.find(winType, "win") then
        infoSend[1] = 1
      else
        infoSend[1] = 0
      end
      AddFlurryEvent(flurryName, infoSend, 2)
    end
  end
  if immanentversion == 2 then
    GameUIBattleResult:CheckTutorial()
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIArena) then
      GameUIArena.hasCheckBattle = false
      GameUIArena:ChecQuestDialog()
    end
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectAdventure) then
      GameObjectAdventure:checkTutorial()
    end
  end
  if not GameStateManager:GetCurrentGameState() == GameStateBattleMap then
    GameUIPrestigeRankUp:CheckNeedDisplay()
  end
  self:ExecuteOverCallback()
end
function GameUIBattleResult:SetFightReport(report, replayAreaID, replayBattleID, isGroupBattle)
  self.m_fightReport = report
  self.m_replayAreaID = replayAreaID
  self.m_replayBattleID = replayBattleID
  self.isGroupBattle = isGroupBattle
end
function GameUIBattleResult.NetworkCallback(msgType, content)
  DebugOut("fuck GameUIBattleResult network")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fight_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIBattleResult:BeforeOut()
  DebugOut("GameUIBattleResult BeforeOut")
  self:SetFightReport(nil)
  self.isGroupBattle = nil
  GameStateManager:GetCurrentGameState():EraseObject(GameUIBattleResult)
  if GameStateInstance == GameStateManager:GetCurrentGameState() then
    DebugOut("CheckToNextLadder")
    local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
    GameObjectClimbTower.CheckToNextLadder()
  end
  if self.m_isFinishBattle and GameStateBattleMap == GameStateManager:GetCurrentGameState() then
    GameStateBattleMap:OnBattleOrEventFinish(self.m_battleID)
  end
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateArena then
    GameUIArena:SetBreakthoughPanelInfo()
    GameUIArena:ShowBreakthroughPanel()
  end
  if GameUIBattleResult.closeCallBack then
    GameUIBattleResult.closeCallBack()
    GameUIBattleResult.closeCallBack = nil
  end
end
function GameUIBattleResult:DoSpectialTutorial()
  if tonumber(GameUIBattleResult.m_combinedBattleID) == 1001006 and immanentversion == 2 then
    if not TutorialQuestManager.QuestTutorialPrestige:IsActive() and not TutorialQuestManager.QuestTutorialPrestige:IsFinished() then
      TutorialQuestManager.QuestTutorialPrestige:SetActive(true)
      local GameNewMenuItem = LuaObjectManager:GetLuaObject("GameNewMenuItem")
      local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
      GameUIBarRight:CheckQuestTutorial()
      local GameStateBattleMap = GameStateManager.GameStateBattleMap
      GameNewMenuItem:Push("prestige")
    end
  elseif tonumber(GameUIBattleResult.m_combinedBattleID) == 1001011 and immanentversion == 2 then
    local levelInfo = GameGlobalData:GetData("levelinfo")
    if levelInfo.level <= 9 then
      QuestTutorialMakeUserLevel10:SetActive(true)
      QuestTutorialMakeUserLevel10:SetFinish(false)
    elseif not QuestTutorialBuildStar:IsActive() and not QuestTutorialBuildStar:IsFinished() then
      QuestTutorialBuildStar:SetActive(true)
      QuestTutorialMakeUserLevel10:SetActive(false)
      QuestTutorialMakeUserLevel10:SetFinish(true)
    end
  end
end
function GameUIBattleResult:CheckTutorial()
  if tonumber(GameUIBattleResult.m_combinedBattleID) == 60001013 and not QuestTutorialCityHall:IsActive() and not QuestTutorialCityHall:IsFinished() then
    QuestTutorialCityHall:SetActive(true)
  end
end
function GameUIBattleResult:UpdateBattleLostScorepve(Id)
  local itemKey = tonumber(Id)
  local param = {}
  param.itemKey = itemKey
  param.text = failed_list[itemKey].text
  param.name = failed_list[itemKey].name
  param.score = failed_list[itemKey].score
  param.enhance = failed_list[itemKey].enhance
  param.isRed = failed_list.min == param.name
  DebugOut("param")
  DebugOut(failed_list.min)
  DebugTable(param)
  self:GetFlashObject():InvokeASCallback("_root", "setScore_new", param)
end
function GameUIBattleResult:OnFSCommand(cmd, arg)
  if cmd == "Close_Battle_Result" then
    GameStateBattlePlay.isLadderBattle = false
    if self.LastBattleType == "battle_win" and GameUIEnemyInfo.m_combinedBattleID then
      local areaID, battleID = GameUtils:ResolveBattleID(GameUIEnemyInfo.m_combinedBattleID)
      if areaID == 1 and battleID == 1001 then
        AddFlurryEvent("CloseBattleResult_Enemy_1_1_1", {}, 1)
      elseif areaID == 1 and battleID == 1002 then
        AddFlurryEvent("CloseBattleResult_Enemy_1_1_2", {}, 1)
      elseif areaID == 1 and battleID == 1004 then
        AddFlurryEvent("CloseBattleResult_Enemy_1_1_3", {}, 1)
      elseif areaID == 1 and battleID == 1006 then
        AddFlurryEvent("CloseBattleResult_Enemy_1_1_4_Boss", {}, 1)
      elseif areaID == 1 and battleID == 1007 then
        AddFlurryEvent("CloseBattleResult_Enemy_1_1_5", {}, 1)
      elseif areaID == 1 and battleID == 1008 then
        AddFlurryEvent("CloseBattleResult_Enemy_1_1_6", {}, 1)
      elseif areaID == 1 and battleID == 1010 then
        AddFlurryEvent("CloseBattleResult_Enemy_1_1_7", {}, 1)
      elseif areaID == 1 and battleID == 1011 then
        AddFlurryEvent("CloseBattleResult_Enemy_1_1_8_Boss", {}, 1)
      elseif areaID == 1 and battleID == 2001 then
        AddFlurryEvent("CloseBattleResult_Enemy_1_2_1", {}, 1)
      end
    end
    GameUIBattleResult:MoveOutBattleResult()
    if GameStateInstance == GameStateManager:GetCurrentGameState() then
      if self.LastBattleType == "battle_win" then
        if GameObjectClimbTower.nextLadder ~= nil then
          DebugOut("Close_Battle_Result,prepare to exchangeLadder:")
          DebugTable(GameObjectClimbTower.nextLadder)
          GameObjectClimbTower:exchangeLadderInfo(GameObjectClimbTower.nextLadder)
        end
      elseif self.isConditionNotMeet then
        local GameTip = LuaObjectManager:GetLuaObject("GameTip")
        GameTip:Show(GameLoader:GetGameText("LC_ALERT_DUNGEON_LADDER_UNPASS"))
        self.isConditionNotMeet = false
      end
    end
    if self.LastBattleType == "challenge_win" and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLab then
      GameUICrusade:CheckReward()
    end
    self.battleState = false
  elseif cmd == "UpdateBattleLostScorepve" then
    self:UpdateBattleLostScorepve(arg)
  elseif cmd == "onRewardIcons" then
    local tid = LuaUtils:deserializeTable(arg)
    if tid.item_type == "item" then
      tid.cnt = 1
      tid.hideCost = true
      ItemBox:showItemBox("ChoosableItem", tid, tid.number, 320, 480, 200, 200, "", nil, "", nil)
    elseif tid.item_type == "medal_item" then
      tid.quality = tid.quality or 1
      ItemBox:showItemBox("Medal", tid, tid.number, 320, 480, 200, 200, "", nil, "", nil)
    elseif tid.item_type == "medal" then
      ItemBox:ShowMedalDetail(tid.number)
    end
  elseif cmd == "Check_Goto_Enhance" then
    if self.GoToEnhanceType ~= nil then
      if immanentversion == 1 and self.GoToEnhanceType == "Improvement_Recommend_Equipment" and GameUIBattleResult.m_combinedBattleID == 1001011 and TutorialQuestManager.QuestTutorialSecondHeroLoadEQU:IsActive() then
        local fleet_index, can_loadEquip = GameFleetNewEnhance.check_1001011_story()
        GameFleetNewEnhance.m_currentSelectFleetIndex = fleet_index
        if can_loadEquip then
          GameUICommonDialog:PlayStory({100066}, GameFleetNewEnhance.autoUploadEquipAfter_1001011_story)
        else
          GameUICommonDialog:PlayStory({100067}, nil)
        end
      end
      GameUIBattleResult:GoToEnhance(self.GoToEnhanceType)
      TutorialQuestManager.QuestTutorialSecondHeroLoadEQU:SetFinish(true)
      self.GoToEnhanceType = nil
    end
  elseif cmd == "Remove_Battle_Result" then
    self:BeforeOut()
    self.mCurStoryType = nil
  elseif cmd == "win_all_award_click" then
    self:ShowItemDetial(tonumber(arg))
  elseif cmd == "Randomize_Improvement" then
    GameUIBattleResult:RandomizeImprovement(1)
  elseif cmd == "Replay_Battle_Result" then
    do
      local current_gamestate = GameStateManager:GetCurrentGameState()
      local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
      DebugOut("InitBattle:", self.m_replayAreaID, self.m_replayBattleID)
      DebugTable(self.m_fightReport)
      if self.isGroupBattle then
        GameObjectBattleReplay.isGroupBattle = true
        GameObjectBattleReplay.curGroupBattleIndex = 1
        GameObjectBattleReplay.GroupBattleReportArr = self.m_fightReport
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, self.m_fightReport[1].headers[1].report, self.m_replayAreaID, self.m_replayBattleID)
      elseif self.m_fightReport.rounds == nil and self.m_fightReport[1] and self.m_fightReport[1].headers then
        GameObjectBattleReplay.isGroupBattle = true
        GameObjectBattleReplay.curGroupBattleIndex = 1
        GameObjectBattleReplay.GroupBattleReportArr = self.m_fightReport
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, self.m_fightReport[1].headers[1].report, self.m_replayAreaID, self.m_replayBattleID)
      else
        GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_HISTORY, self.m_fightReport, self.m_replayAreaID, self.m_replayBattleID)
      end
      GameStateBattlePlay:RegisterOverCallback(function()
        if current_gamestate == GameStateInstance then
          GameStateInstance:EnterInstance()
        else
          GameStateManager:SetCurrentGameState(current_gamestate)
          if current_gamestate.stateName == "GameStateMiningWorld" then
            GameUIMiningWorld:Show()
            local function callback()
              GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
            end
            GameUIMiningWorld.onAddStateCallBack = callback
            return
          end
        end
        current_gamestate:AddObject(GameUIBattleResult)
      end, nil)
      GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    end
  elseif cmd == "Improvement_Recommned_Main" then
    DebugOut("Improvement_Recommned_Main")
  elseif cmd == "Improvement_Recommend_Equipment" or cmd == "Improvement_Recommend_Tech" or cmd == "Improvement_Recommend_Krypton" or cmd == "Improvement_Recommend_Recruitment" or cmd == "Improvement_Recommend_Formation" or cmd == "Improvement_Recommend_Ship" or cmd == "Improvement_Recommend_Medal" or cmd == "Improvement_Recommend_Matrix" or cmd == "Improvement_Recommend_Remodel" then
    self.GoToEnhanceType = cmd
    self:OnFSCommand("Check_Goto_Enhance")
  elseif cmd == "level_up" then
  elseif cmd == "shareArenaWinCheckboxClicked" then
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN] then
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN] = false
    else
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN] = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN], FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN)
    end
  elseif cmd == "shareChallengeWinCheckboxClicked" then
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMineMap then
      if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN] then
        self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN] = false
      else
        self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN] = true
      end
      local flash_obj = self:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN], FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN)
      end
    end
  elseif cmd == "shareCollionWinCheckboxClicked" then
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateColonization then
      if not self.mCurStoryType then
        return
      end
      if self.mShareStoryChecked[self.mCurStoryType] then
        self.mShareStoryChecked[self.mCurStoryType] = false
      else
        self.mShareStoryChecked[self.mCurStoryType] = true
      end
      local flash_obj = self:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[self.mCurStoryType], self.mCurStoryType)
      end
    end
  elseif cmd == "hideColonialWin" then
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateColonization then
      local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
      if GameUIBattleResult.mCurStoryType and self.mShareStoryChecked[self.mCurStoryType] then
        local extraInfo = {}
        extraInfo.playerName = GameUIBattleResult.colonialName or ""
        FacebookGraphStorySharer:ShareStory(self.mCurStoryType, extraInfo)
        self.mCurStoryType = nil
        GameUIBattleResult.colonialName = nil
        local flurryStr = "none"
        if self.mCurStoryType == FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_WIN then
          flurryStr = "FB_STORY_CHECKED_COLONIAL_WIN"
        elseif self.mCurStoryType == FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_FIGHT_WIN then
          flurryStr = "FB_STORY_CHECKED_SLAVER_FIGHT_WIN"
        end
      elseif GameUIBattleResult.mCurStoryType and FacebookGraphStorySharer:IsStoryEnabled(self.mCurStoryType) then
        local flurryStr = "none"
        if self.mCurStoryType == FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_WIN then
          flurryStr = "FB_STORY_UNCHECKED_COLONIAL_WIN"
        elseif self.mCurStoryType == FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_FIGHT_WIN then
          flurryStr = "FB_STORY_UNCHECKED_SLAVER_FIGHT_WIN"
        end
      end
    end
  elseif cmd == "hideArenaWin" then
    local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    DebugOut("self.mShareStoryChecked = ", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN])
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN] then
      local extraInfo = {}
      extraInfo.playerName = GameUIBattleResult.loserName or ""
      FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN, extraInfo)
    else
    end
  else
    if cmd == "hideWinChallenge" and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMineMap then
      local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
      if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN] then
        local extraInfo = {}
        extraInfo.ownerName = GameUIBattleResult.minerOwnerName or ""
        FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN, extraInfo)
        GameUIBattleResult.minerOwnerName = nil
      end
    else
    end
  end
end
function GameUIBattleResult:SetRewardContents(index)
  local text = ""
  DebugOut("GameUIBattleResult:SetRewardContents")
  GameUtils:printTable(self.rewards)
  for k, v in pairs(self.rewards) do
    if v.level == index then
      text = v.base.number
      if v.base.item_type == "item" then
        if tonumber(v.base.number) > 100000 then
          text = GameLoader:GetGameText("LC_ITEM_EQUIP_TYPE_SLOT" .. GameDataAccessHelper:GetEquipSlot(v.base.number))
        else
          text = GameLoader:GetGameText("LC_ITEM_ITEM_CHAR")
        end
      end
    end
  end
  self:CheckFlashObject():InvokeASCallback("_root", "SetRewardContents", index, text)
end
function GameUIBattleResult:EnhanceCallback()
end
function GameUIBattleResult:GoToEnhance(_type)
  if _type == "Improvement_Recommend_Equipment" then
    GameStateEquipEnhance:EnterEquipEnhance()
    GameStateManager.GameStateEquipEnhance.battle_failed = true
  elseif _type == "Improvement_Recommend_Ship" then
    GameStateEquipEnhance:EnterEquipEnhance()
    GameStateManager.GameStateEquipEnhance.battle_failed = true
  elseif _type == "Improvement_Recommend_Medal" then
    local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
    GameStateEquipEnhance:EnterEquipEnhance()
    if not GameFleetEquipment:GetFlashObject() then
      GameFleetEquipment:LoadFlashObject()
    end
    GameFleetEquipment:SelectFunction("xunzhang")
    GameStateManager.GameStateEquipEnhance.battle_failed = true
  elseif _type == "Improvement_Recommend_Matrix" then
    GameObjectPlayerMatrix:OnFSCommand("btn_save")
    GameStateTacticsCenter.battle_failed = true
  elseif _type == "Improvement_Recommend_Remodel" then
    GameUIFactory.EnterRebuild = true
    GameStateManager:GetCurrentGameState():AddObject(GameUIFactory)
    GameUIFactory.battle_failed = true
  elseif _type == "Improvement_Recommend_Krypton" then
    GameStateKrypton.tab = GameStateKrypton.LAB
    GameStateKrypton:EnterKrypton(GameStateKrypton.LAB)
    GameStateKrypton.battle_failed = true
  elseif _type == "Improvement_Recommend_Recruitment" then
    GameStateManager:SetCurrentGameState(GameStateRecruit)
    GameStateRecruit.battle_failed = true
  elseif _type == "Improvement_Recommend_Tech" then
    GameUITechnology:EnterTechnology()
    GameUITechnology.battle_failed = true
  elseif _type == "Improvement_Recommend_Formation" then
    GameStateManager:GetCurrentGameState():EraseObject(self)
    local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
    local GameStateConfrontation = GameStateManager.GameStateConfrontation
    if GameUIArena:IsActive() or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMineMap or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateColonization or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateWD or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateStarCraft or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateInstance then
      if GameUIArena:IsActive() then
        GameStateManager:GetCurrentGameState():EraseObject(GameUIArena)
      end
      local GameStatePlayerMatrix = GameStateManager.GameStatePlayerMatrix
      GameStateManager:SetCurrentGameState(GameStatePlayerMatrix)
    elseif GameStateManager:GetCurrentGameState() == GameStateConfrontation then
      local GameStateFormation = GameStateManager.GameStateFormation
      local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
      DebugOut("battle id - ", GameObjectAdventure.m_currentBattleID)
      GameStateFormation:SetBattleID(51, GameObjectAdventure.m_currentBattleID)
      GameStateManager:SetCurrentGameState(GameStateFormation)
    elseif GameStateManager:GetCurrentGameState() == GameStateArcane then
      GameStateFormation:SetBattleID(52, GameStateArcane:GetArcaneBattleID())
      GameStateManager:SetCurrentGameState(GameStateFormation)
    elseif GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLab and GameStateManager.GameStateLab:IsInCrusadeState() then
      GameUICrusade:TriggerTarget(GameUICrusade.mLastFightEnemyPos)
    else
      local GameStateFormation = GameStateManager.GameStateFormation
      local GameUIEnemyInfo = LuaObjectManager:GetLuaObject("GameUIEnemyInfo")
      GameStateFormation:SetBattleID(GameStateBattleMap.m_currentAreaID, GameUIEnemyInfo.m_currentBattleID)
      GameStateManager:SetCurrentGameState(GameStateFormation)
    end
  end
end
function GameUIBattleResult:RegisterEraseCallback(func)
end
if AutoUpdate.isAndroidDevice then
  function GameUIBattleResult.OnAndroidBack()
    GameUIBattleResult:OnFSCommand("Close_Battle_Result")
  end
end
function GameUIBattleResult:SetReplayBtnState(enabled)
  local flash = self:CheckFlashObject()
  if flash then
    if self.LastBattleType == "challenge_lose" or self.LastBattleType == "challenge_win" then
      self:CheckFlashObject():InvokeASCallback("_root", "setChallengeReplayBtnState", enabled)
    else
      self:CheckFlashObject():InvokeASCallback("_root", "setReplayBtnState", enabled)
    end
  end
end
function GameUIBattleResult:SetStarWarResult(data)
  local flash = self:CheckFlashObject()
  if flash then
    self:CheckFlashObject():InvokeASCallback("_root", "SetStarWarResult", data)
  end
end
function GameUIBattleResult:SetLargeMapResult(data)
  local flash = self:CheckFlashObject()
  if flash then
    self:CheckFlashObject():InvokeASCallback("_root", "SetLargeMapResult", data)
  end
end
