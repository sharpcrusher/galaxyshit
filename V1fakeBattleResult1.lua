local fakeBattleResult1 = {
  player1 = "vddfv. vbb.s991",
  player1_identity = 1,
  player1_pos = 5,
  player2_identity = 6000001,
  player2_pos = 5,
  player2_avatar = "head13",
  player2 = "battle",
  player1_avatar = "female",
  result = 1,
  rounds = {
    [1] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal_new2",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 25,
          intercept = false,
          atk_acc = 25,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 58,
          durability = 82,
          atk_pos = 5,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = true,
      dot = {},
      pos = 5,
      round_cnt = 1
    },
    [2] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 50,
          intercept = false,
          atk_acc = 50,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 21,
          durability = 79,
          atk_pos = 5,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 5,
      round_cnt = 2
    },
    [3] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal_new2",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 75,
          intercept = false,
          atk_acc = 75,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 66,
          durability = 16,
          atk_pos = 5,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = true,
      dot = {},
      pos = 5,
      round_cnt = 3
    },
    [4] = {
      attacks = {
        [1] = {
          attack_type = 1,
          atk_effect = "normal",
          def_pos = 5,
          shield = 0,
          atk_acc_change = 25,
          acc = 100,
          intercept = false,
          atk_acc = 100,
          shield_damage = 0,
          acc_change = 25,
          hit = true,
          dur_damage = 20,
          durability = 59,
          atk_pos = 5,
          crit = false
        }
      },
      sp_attacks = {},
      buff = {},
      player1_action = false,
      dot = {},
      pos = 5,
      round_cnt = 4
    },
    [5] = {
      attacks = {},
      sp_attacks = {
        [1] = {
          durability = 0,
          def_pos = 5,
          sp_id = 1,
          shield = 0,
          buff_effect = 0,
          acc = 125,
          intercept = false,
          self_cast = false,
          shield_damage = 0,
          acc_change = 25,
          atk_acc_change = -100,
          atk_acc = 0,
          hit = true,
          dur_damage = 117,
          round_cnt = 0,
          atk_pos = 5,
          crit = false
        }
      },
      buff = {},
      player1_action = true,
      dot = {},
      pos = 5,
      round_cnt = 5
    }
  },
  player1_fleets = {
    [1] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 100,
      cur_accumulator = 0,
      max_durability = 100,
      identity = 1,
      pos = 5
    }
  },
  player2_fleets = {
    [1] = {
      max_shield = 0,
      shield = 0,
      cur_durability = 140,
      cur_accumulator = 0,
      max_durability = 140,
      identity = 6000001,
      pos = 5
    }
  }
}
local fakeBattleAnim1 = {
  [1] = {
    round_index = 1,
    round_step = 5,
    attack_round_index = -1,
    dialog_id = {1100007},
    befor_dialog_anim = "in skill_hilight",
    hide_anim_in_dialog = false,
    after_dialog_anim = "ou skill_hilight"
  },
  [2] = {
    round_index = 2,
    round_step = 5,
    attack_round_index = -1,
    dialog_id = {1100008},
    befor_dialog_anim = "in skill_hilight",
    hide_anim_in_dialog = false,
    after_dialog_anim = "ou skill_hilight"
  },
  [3] = {
    round_index = 5,
    round_step = 2,
    attack_round_index = -1,
    dialog_id = {1100009},
    befor_dialog_anim = "in skill_hilight",
    hide_anim_in_dialog = false,
    after_dialog_anim = "ou skill_hilight"
  }
}
GameData.fakeBattleAnim1 = fakeBattleAnim1
GameData.fakeBattleResult1 = fakeBattleResult1
