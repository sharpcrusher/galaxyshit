local GameUIRouletteEvent = LuaObjectManager:GetLuaObject("GameUIRouletteEvent")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIActivityEvent = LuaObjectManager:GetLuaObject("GameObjectActivityEvent")
GameUIRouletteEvent.singleDistance = 36
GameUIRouletteEvent.peekVelocity = 1.08
GameUIRouletteEvent.accelerationDistance = 360
GameUIRouletteEvent.serverRewardTitle = ""
GameUIRouletteEvent.acceleration = 0
GameUIRouletteEvent.remainDistance = 0
GameUIRouletteEvent.beginTime = 0
GameUIRouletteEvent.step = 0
GameUIRouletteEvent.currRotation = 0
GameUIRouletteEvent.isNeedToChangeRoulette = false
GameUIRouletteEvent.startDraw = false
GameUIRouletteEvent.totalInfo = nil
GameUIRouletteEvent.rewardShowItem = nil
GameUIRouletteEvent.rewardHistory = nil
GameUIRouletteEvent.serverRewardHistory = nil
GameUIRouletteEvent.eventTitle = ""
GameUIRouletteEvent.jackPotRefreshTime = 0
GameUIRouletteEvent.accumulated_point = 0
GameUIRouletteEvent.max_point = 0
GameUIRouletteEvent.progress_rewards = {}
function GameUIRouletteEvent:UpdateProgressRewardList(reward_table)
  GameUIRouletteEvent.progress_rewards = reward_table
end
function GameUIRouletteEvent:UpdateProgressReward()
  local status, cur, total = GameUIRouletteEvent:GetProgressRewardInfo()
  self:GetFlashObject():InvokeASCallback("_root", "UpdateAwardProcessBar", status, cur, total, tostring(cur) .. "/" .. tostring(total))
end
function GameUIRouletteEvent:GetProgressRewardInfo()
  local status = 0
  local cur = GameUIRouletteEvent.accumulated_point
  local total = GameUIRouletteEvent.max_point
  if cur >= total then
    status = 1
  end
  return status, cur, total
end
function GameUIRouletteEvent:UpdateAccumulatedPoint(reward, curPoint, maxPoint)
  GameUIRouletteEvent.progress_rewards = reward
  GameUIRouletteEvent.accumulated_point = curPoint
  GameUIRouletteEvent.max_point = maxPoint
end
function GameUIRouletteEvent:GetProgressRewardReq()
  NetMessageMgr:SendMsg(NetAPIList.prize_wheel_get_accumulation_reward_req.Code, {
    wheel_id = self.totalInfo.wheel_id
  }, GameUIRouletteEvent.OnServerProgressRewardAck, true)
end
function GameUIRouletteEvent.OnServerProgressRewardAck(msgType, content)
  local isSuccess = false
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.prize_wheel_get_accumulation_reward_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    isSuccess = true
  elseif msgType == NetAPIList.prize_wheel_get_accumulation_reward_req.Code then
    isSuccess = true
  end
  if isSuccess then
    TPLog("reset cur progress to 0")
    GameUIRouletteEvent.accumulated_point = 0
    GameUIRouletteEvent:UpdateProgressReward()
  end
  return isSuccess
end
function GameUIRouletteEvent:showItemDetailForGameItem(gameItem)
  if GameHelper:IsResource(gameItem.item_type) then
    return
  end
  if gameItem.item_type == "krypton" then
    if gameItem.level == 0 then
      gameItem.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(gameItem.number, gameItem.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(gameItem.number, gameItem.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, gameItem.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, gameItem.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  ItemBox:ShowGameItem(gameItem)
end
function GameUIRouletteEvent:OnInitGame()
end
function GameUIRouletteEvent:Close()
  self:ResetRouletteInfo()
  self:ResetDrawInfo()
  self:ResetEventInfo()
  self:UnloadFlashObject()
end
function GameUIRouletteEvent:ResetDrawInfo()
  self.startDraw = false
  self.step = 0
  self.isNeedToChangeRoulette = false
end
function GameUIRouletteEvent:ResetRouletteInfo()
  self.acceleration = 0
  self.remainDistance = 0
  self.beginTime = 0
end
function GameUIRouletteEvent:ResetEventInfo()
  self.totalInfo = nil
  self.rewardShowItem = nil
  self.rewardHistory = nil
  self.serverRewardHistory = nil
  self.jackPotRefreshTime = 0
  self.currRotation = 0
  GameUIRouletteEvent:UpdateProgressRewardList({})
end
function GameUIRouletteEvent:SetEventTitle(title)
  self.eventTitle = title
end
function GameUIRouletteEvent:SetCurrRotation(angle)
  self.currRotation = angle % 360
end
function GameUIRouletteEvent:OnAddToGameState(new_parent)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if self.totalInfo == nil then
    self:Close()
    return
  end
  self:SetRouletteUI()
  self:SetProgressBar()
  self:SetRouletteButton()
  self:SetRouletteRewardPreview()
  self:SetRouletteWorldRewardPreview()
  self:SetResourceAndIteamNum()
  self:SetUIText()
  self:UpdateProgressReward()
  self:AnimationMoveInAll()
end
function GameUIRouletteEvent:OnEraseFromGameState()
  GameUIRouletteEvent:UnloadFlashObject()
end
function GameUIRouletteEvent:ReqRouletteEventInfo(id)
  NetMessageMgr:SendMsg(NetAPIList.prize_wheel_open_req.Code, {wheel_id = id}, GameUIRouletteEvent.OnRouletteEventCallBack, true)
end
function GameUIRouletteEvent:ReqRewardHistory()
  NetMessageMgr:SendMsg(NetAPIList.prize_wheel_history_list_req.Code, {
    wheel_id = self.totalInfo.wheel_id
  }, GameUIRouletteEvent.OnRewardHistoryCallBack, true)
end
function GameUIRouletteEvent:ReqServerRewardHistory()
  NetMessageMgr:SendMsg(NetAPIList.prize_wheel_jackpot_list_req.Code, {
    wheel_id = self.totalInfo.wheel_id
  }, GameUIRouletteEvent.OnServerRewardHistoryCallBack, true)
end
function GameUIRouletteEvent:ReqDraw(isSingle)
  if not self.startDraw then
    NetMessageMgr:SendMsg(NetAPIList.prize_wheel_play_req.Code, {
      wheel_id = self.totalInfo.wheel_id,
      is_single = isSingle
    }, GameUIRouletteEvent.OnDrawCallBack, true)
  end
end
function GameUIRouletteEvent:AnimationMoveInAll()
  self:GetFlashObject():InvokeASCallback("_root", "AnimationMoveInAll")
end
function GameUIRouletteEvent:SetRouletteUI()
  local isPrime = self.totalInfo.is_jackpot
  local rouletteItems = {}
  if isPrime then
    local cnt = #self.totalInfo.jackpot_whell_items
    for i = 1, cnt do
      local info = self.totalInfo.jackpot_whell_items[i]
      local iteminfo = GameHelper:GetItemInfo(info.award_item)
      local data = {}
      data.cnt = iteminfo.cnt
      data.index = info.index
      data.icon = iteminfo.icon_frame
      data.pic = iteminfo.icon_pic
      table.insert(rouletteItems, data)
    end
  else
    local cnt = #self.totalInfo.prize_wheel_items
    for i = 1, cnt do
      local info = self.totalInfo.prize_wheel_items[i]
      local iteminfo = GameHelper:GetItemInfo(info.award_item)
      local data = {}
      data.cnt = iteminfo.cnt
      data.index = info.index
      data.icon = iteminfo.icon_frame
      data.pic = iteminfo.icon_pic
      table.insert(rouletteItems, data)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetRouletteUI", isPrime, rouletteItems)
  self:GetDynamicIcon()
  self:UpdateDrawButtons()
end
function GameUIRouletteEvent:UpdateDrawButtons()
  local minus = self.totalInfo.progress_need
  local max = self.totalInfo.progress_max
  local isX10Visible = max >= minus * 10
  local isPrime = self.totalInfo.is_jackpot
  if not isPrime then
    isX10Visible = true
  end
  self:GetFlashObject():InvokeASCallback("_root", "UpdateDrawButtons", isX10Visible)
end
function GameUIRouletteEvent:GetDynamicIcon()
  local isPrime = self.totalInfo.is_jackpot
  local rouletteItems = {}
  local items = {}
  local cnt = 0
  if isPrime then
    items = self.totalInfo.jackpot_whell_items
    cnt = #items
  else
    items = self.totalInfo.prize_wheel_items
    cnt = #items
  end
  for i = 1, cnt do
    local itemInfo = items[i]
    local gameItem = itemInfo.award_item
    if DynamicResDownloader:IsDynamicStuffByGameItem(gameItem) then
      local fullFileName = DynamicResDownloader:GetFullName(gameItem.number .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if not DynamicResDownloader:IfResExsit(localPath) then
        local extraInfo = GameHelper:GetItemInfo(gameItem)
        local extInfo = gameItem
        extInfo.cnt = extraInfo.cnt
        extInfo.index = itemInfo.index
        GameHelper:AddDownloadPathForGameItem(gameItem.number, extInfo, self.DynamicIconDownloadCallback)
      end
    end
  end
end
function GameUIRouletteEvent.DynamicIconDownloadCallback(extInfo)
  local self = GameUIRouletteEvent
  if self:GetFlashObject() == nil then
    return
  end
  if self.totalInfo == nil then
    TPLog("error GameUIRouletteEvent.totalInfo is nil")
    return
  end
  local isPrime = self.totalInfo.is_jackpot
  extInfo.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo, nil, nil)
  self:GetFlashObject():InvokeASCallback("_root", "SetRouletteUI", isPrime, {extInfo})
end
function GameUIRouletteEvent:SetProgressBar()
  local percent = math.floor(self.totalInfo.progress_now / self.totalInfo.progress_max * 100)
  self:GetFlashObject():InvokeASCallback("_root", "SetProgressBar", percent)
end
function GameUIRouletteEvent:CheckDrawItemEnough(drawTimes)
  local itemInfo = self.totalInfo.need_item
  local isPrime = self.totalInfo.is_jackpot
  local primeEnergyNeed = self.totalInfo.progress_need * drawTimes
  local primeEnergyOwn = self.totalInfo.progress_now
  local itemsTab = GameGlobalData:GetData("item_count")
  local resourceCount = GameGlobalData:GetData("resource")
  local itemCnt = 0
  if GameHelper:IsResource(itemInfo.item_type) then
    itemCnt = resourceCount[itemInfo.item_type]
  else
    for iItem, vItem in ipairs(itemsTab.items) do
      if vItem.item_id == itemInfo.number then
        itemCnt = vItem.item_no
      end
    end
  end
  if isPrime then
    return drawTimes <= itemCnt and primeEnergyNeed <= primeEnergyOwn
  else
    return drawTimes <= itemCnt
  end
end
function GameUIRouletteEvent:SetRouletteButton()
  self:GetFlashObject():InvokeASCallback("_root", "SetRouletteButton", self:CheckDrawItemEnough(1), self:CheckDrawItemEnough(10))
end
function GameUIRouletteEvent:SetRouletteRewardPreview()
  local rewardHistory = self.totalInfo.normal_history_list
  local cnt = #rewardHistory
  local history = {}
  for i = 1, cnt do
    local info = rewardHistory[i]
    local iteminfo = GameHelper:GetItemInfo(info)
    local data = {}
    data.cnt = iteminfo.cnt
    data.icon = iteminfo.icon_frame
    data.pic = iteminfo.icon_pic
    table.insert(history, data)
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetRouletteRewardPreview", history)
end
function GameUIRouletteEvent:SetRouletteWorldRewardPreview()
  local rewardHistory = self.totalInfo.super_jackpot_list
  local cnt = #rewardHistory
  local history = {}
  for i = 1, cnt do
    local info = rewardHistory[i]
    local iteminfo = GameHelper:GetItemInfo(info.jackpot_award)
    local data = {}
    data.playerName = info.player_name
    data.cnt = iteminfo.cnt
    data.icon = iteminfo.icon_frame
    data.pic = iteminfo.icon_pic
    table.insert(history, data)
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetRouletteWorldRewardPreview", history)
end
function GameUIRouletteEvent:SetUIText()
  local leftBtnText = GameLoader:GetGameText("LC_MENU_Get_reward")
  local rightBtnText = GameLoader:GetGameText("LC_MENU_Get_reward_pro")
  local rewardTitleText = GameLoader:GetGameText("LC_MENU_Get_reward_item")
  local rewardLogText = GameLoader:GetGameText("LC_MENU_Get_item_history")
  local serverRewardLogText = GameLoader:GetGameText("LC_MENU_Get_reward_history")
  local data = {}
  data.eventTitle = self.eventTitle
  data.leftBtnText = leftBtnText
  data.rightBtnText = rightBtnText
  data.rewardTitleText = rewardTitleText
  data.rewardLogText = rewardLogText
  data.serverRewardLogText = serverRewardLogText
  self:GetFlashObject():InvokeASCallback("_root", "SetUIText", data)
end
function GameUIRouletteEvent:SetResourceAndIteamNum()
  local itemInfo = self.totalInfo.need_item
  local itemsTab = GameGlobalData:GetData("item_count")
  local resourceCount = GameGlobalData:GetData("resource")
  local itemCnt = 0
  if GameHelper:IsResource(itemInfo.item_type) then
    itemCnt = resourceCount[itemInfo.item_type]
  else
    for iItem, vItem in ipairs(itemsTab.items) do
      if vItem.item_id == itemInfo.number then
        itemCnt = vItem.item_no
      end
    end
  end
  local creditCnt = resourceCount.credit
  self:GetFlashObject():InvokeASCallback("_root", "SetResourceAndIteamNum", creditCnt, itemCnt)
end
function GameUIRouletteEvent:SetTotalDistance(index)
  local distance = index * self.singleDistance
  local halfDistance = self.singleDistance / 2 - 5
  local dynamicDistance = halfDistance - math.random() * halfDistance * 2
  self.remainDistance = math.random(5, 8) * 360 + distance + dynamicDistance - self.currRotation
end
function GameUIRouletteEvent:SetArrowRotation(angle)
  self:GetFlashObject():InvokeASCallback("_root", "SetArrowRotation", angle, self.totalInfo.is_jackpot)
end
function GameUIRouletteEvent:SetDrawInfo(content)
  self:ResetDrawInfo()
  self:ResetRouletteInfo()
  self.totalInfo.progress_now = content.progress_now
  self.isNeedToChangeRoulette = self.totalInfo.is_jackpot ~= content.is_jackpot
  self.totalInfo.point_index = content.point_index
  local historyCnt = #self.totalInfo.normal_history_list
  while historyCnt >= 4 do
    table.remove(self.totalInfo.normal_history_list, historyCnt)
    historyCnt = #self.totalInfo.normal_history_list
  end
  table.insert(self.totalInfo.normal_history_list, 1, content.award_item)
  self.rewardShowItem = content.award_item
  self:StarDraw()
end
function GameUIRouletteEvent:StarDraw()
  self.startDraw = true
  self.step = 1
  self:SetTotalDistance(self.totalInfo.point_index - 1)
end
function GameUIRouletteEvent:ShowRewardItem()
  if self.rewardShowItem ~= nil then
    local rewardTab = {}
    table.insert(rewardTab, self.rewardShowItem)
    ItemBox:ShowRewardBox(rewardTab)
  end
end
function GameUIRouletteEvent:ShowRewardHistory(isServerLog)
  local rewardHistory = {}
  if isServerLog == 0 then
    rewardHistory = self.rewardHistory
  else
    rewardHistory = self.serverRewardHistory
  end
  local cnt = #rewardHistory
  local hintText = GameLoader:GetGameText("LC_MENU_FST_log")
  self:GetFlashObject():InvokeASCallback("_root", "ShowRewardHistory", cnt, hintText, isServerLog)
end
function GameUIRouletteEvent:SetOneRewardHistory(index, isServerLog)
  if isServerLog == 0 then
    local rewardHistory = self.rewardHistory
    local oneLog = rewardHistory[index]
    if oneLog ~= nil then
      local iteminfo = GameHelper:GetItemInfo(oneLog)
      local logText = GameLoader:GetGameText("LC_MENU_Get_reward_item") .. ": " .. iteminfo.name .. "*" .. iteminfo.cnt
      self:GetFlashObject():InvokeASCallback("_root", "SetOneRewardHistory", index, logText)
    end
  else
    local rewardHistory = self.serverRewardHistory
    local oneLog = rewardHistory[index]
    if oneLog ~= nil then
      local logText = GameLoader:GetGameText("LC_MENU_Get_reward_credit")
      local playerName = oneLog.player_name
      local itemInfo = GameHelper:GetItemInfo(oneLog.jackpot_award)
      logText = string.gsub(logText, "<name>", playerName)
      logText = string.gsub(logText, "<number>", itemInfo.cnt)
      self:GetFlashObject():InvokeASCallback("_root", "SetOneRewardHistory", index, logText)
    end
  end
end
function GameUIRouletteEvent:UpdateJackpotNum()
  if self:GetFlashObject() == nil then
    return
  end
  local currTime = ext.getSysTime()
  if currTime - self.jackPotRefreshTime >= 10000 then
    self.jackPotRefreshTime = currTime
    NetMessageMgr:SendMsg(NetAPIList.prize_wheel_jp_score_req.Code, {
      wheel_id = self.totalInfo.wheel_id
    }, GameUIRouletteEvent.OnUpdateJackpotNumCallBack, false)
  end
end
function GameUIRouletteEvent:UpdateRotation()
  if self:GetFlashObject() == nil then
    return
  end
  if self.step == 1 then
    self.beginTime = ext.getSysTime()
    self.acceleration = GameUIRouletteEvent.peekVelocity * GameUIRouletteEvent.peekVelocity / (2 * self.accelerationDistance)
    self.step = 2
  elseif self.step == 2 then
    local deltaTime = ext.getSysTime() - self.beginTime
    local distance = 0.5 * self.acceleration * deltaTime * deltaTime
    if distance >= self.accelerationDistance then
      distance = self.accelerationDistance
      self.acceleration = -(GameUIRouletteEvent.peekVelocity * GameUIRouletteEvent.peekVelocity) / (2 * self.remainDistance)
      self.beginTime = ext.getSysTime()
      self.step = 3
    end
    self:SetArrowRotation(distance + self.currRotation)
  elseif self.step == 3 then
    local deltaTime = ext.getSysTime() - self.beginTime
    local distance = GameUIRouletteEvent.peekVelocity * deltaTime + 0.5 * self.acceleration * deltaTime * deltaTime + self.accelerationDistance
    local currVelocity = GameUIRouletteEvent.peekVelocity + self.acceleration * deltaTime
    if currVelocity <= 0 then
      distance = self.accelerationDistance + self.remainDistance
      self.step = 4
    end
    self:SetArrowRotation(distance + self.currRotation)
  elseif self.step == 4 then
    self.startDraw = false
    self.step = 0
    self:SetCurrRotation(self.accelerationDistance + self.remainDistance + self.currRotation)
    if self.isNeedToChangeRoulette then
      self.totalInfo.is_jackpot = not self.totalInfo.is_jackpot
      self:SetRouletteUI()
      self:SetCurrRotation(0)
      self:SetArrowRotation(0, true)
      self:SetArrowRotation(0, false)
    end
    self:ShowRewardItem()
    self:SetRouletteRewardPreview()
    self:SetRouletteWorldRewardPreview()
    self:SetProgressBar()
    self:SetResourceAndIteamNum()
    self:SetRouletteButton()
    self:UpdateProgressReward()
  end
end
function GameUIRouletteEvent:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Update(dt)
    flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
    if self.startDraw then
      self:UpdateRotation()
    end
    self:UpdateJackpotNum()
  end
end
function GameUIRouletteEvent.OnRouletteEventCallBack(msgType, content)
  if msgType == NetAPIList.prize_wheel_open_ack.Code then
    GameUIRouletteEvent.totalInfo = content
    GameUIRouletteEvent:UpdateAccumulatedPoint(content.rewards, content.accumulate_point, content.max_point)
    GameStateManager:GetCurrentGameState():AddObject(GameUIRouletteEvent)
    return true
  end
  return false
end
function GameUIRouletteEvent.OnDrawCallBack(msgType, content)
  if msgType == NetAPIList.prize_wheel_play_ack.Code then
    GameUIRouletteEvent:SetDrawInfo(content)
    GameUIRouletteEvent:UpdateAccumulatedPoint(content.rewards, content.accumulate_point, content.max_point)
    GameUIRouletteEvent:UpdateProgressReward()
    return true
  end
  return false
end
function GameUIRouletteEvent.OnRewardHistoryCallBack(msgType, content)
  if msgType == NetAPIList.prize_wheel_history_list_ack.Code then
    GameUIRouletteEvent.rewardHistory = content.history_list
    GameUIRouletteEvent:ShowRewardHistory(0)
    return true
  end
  return false
end
function GameUIRouletteEvent.OnServerRewardHistoryCallBack(msgType, content)
  if msgType == NetAPIList.prize_wheel_jackpot_list_ack.Code then
    GameUIRouletteEvent.serverRewardHistory = content.super_jackpot_list
    GameUIRouletteEvent:ShowRewardHistory(1)
    return true
  end
  return false
end
function GameUIRouletteEvent.OnUpdateJackpotNumCallBack(msgType, content)
  if msgType == NetAPIList.prize_wheel_jp_score_ack.Code then
    if GameUIRouletteEvent.serverRewardTitle == nil or GameUIRouletteEvent.serverRewardTitle == "" then
      GameUIRouletteEvent.serverRewardTitle = GameLoader:GetGameText("LC_MENU_Super_reward")
    end
    GameUIRouletteEvent:GetFlashObject():InvokeASCallback("_root", "OnUpdateJackpotNumCallBack", GameUIRouletteEvent.serverRewardTitle, content.jp_score)
    return true
  end
  return false
end
function GameUIRouletteEvent.ServerRewardLogNtf(content)
  GameUIRouletteEvent.totalInfo.super_jackpot_list = content.super_jackpot_list
end
function GameUIRouletteEvent:AskGotoStore()
  if self.startDraw then
    return
  end
  local GameGoodsList = LuaObjectManager:GetLuaObject("GameGoodsList")
  GameGoodsList:SetEnterStoreType(1)
  GameGoodsList:SetExitCallbackOnce(function()
    self:SetRouletteButton()
    self:SetResourceAndIteamNum()
  end)
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateStore)
end
function GameUIRouletteEvent:OnFSCommand(cmd, arg)
  if cmd == "close_all" then
    self:Close()
  elseif cmd == "one_draw" then
    if self:CheckDrawItemEnough(1) then
      self:ReqDraw(true)
    else
      self:AskGotoStore()
    end
  elseif cmd == "ten_draw" then
    if self:CheckDrawItemEnough(10) then
      self:ReqDraw(false)
    else
      self:AskGotoStore()
    end
  elseif cmd == "show_rewardHistory" then
    self:ReqRewardHistory()
  elseif cmd == "refresh_historyLog" then
    local param = LuaUtils:string_split(arg, "\001")
    local index = tonumber(param[1])
    local isServerLog = tonumber(param[2])
    self:SetOneRewardHistory(index, isServerLog)
  elseif cmd == "show_serverRewardHistory" then
    self:ReqServerRewardHistory()
  elseif cmd == "show_guide" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(string.gsub(GameLoader:GetGameText(self.totalInfo.prize_desc), "!@#", "\n"))
  elseif cmd == "needUpdateBoxRewardItems" then
    local item = GameUIRouletteEvent.progress_rewards[tonumber(arg)]
    local iteminfo = GameHelper:GetItemInfo(item)
    local info = {
      m_icon = iteminfo.icon_frame,
      pic = iteminfo.icon_pic
    }
    info.num = GameHelper:GetAwardCountWithX(item.item_type, item.number, item.no)
    self:GetFlashObject():InvokeASCallback("_root", "setBoxAwardListItems", tonumber(arg), info)
  elseif cmd == "showBoxItemDetail" then
    local item = GameUIRouletteEvent.progress_rewards[tonumber(arg)]
    GameUIRouletteEvent:showItemDetailForGameItem(item)
  elseif cmd == "TreasureBoxClicked" then
    TPLog("TreasureBoxClicked", GameUIRouletteEvent.progress_rewards)
    self:GetFlashObject():InvokeASCallback("_root", "showBoxDetail", #GameUIRouletteEvent.progress_rewards)
  elseif cmd == "getBoxReward" then
    GameUIRouletteEvent:GetProgressRewardReq()
  else
    TPLog("OnFSCommand", cmd, arg)
  end
end
