local GameStateWorldChampion = GameStateManager.GameStateWorldChampion
local GameUIWorldChampion = LuaObjectManager:GetLuaObject("GameUIWorldChampion")
STATE_WC_SUB_STATE = {
  SUB_STATE_BEFORE = -1,
  SUB_STATE_ENROLL = 1,
  SUB_STATE_MASS_ELECTION = 2,
  SUB_STATE_PROMOTION = 3,
  SUB_STATE_UNKNOW = 4
}
GameStateWorldChampion.mCurSubState = STATE_WC_SUB_STATE.SUB_STATE_UNKNOW
function GameStateWorldChampion:OnFocusGain(previousState)
  DebugOut("GameStateWorldChampion:OnFocusGain")
  self.lastState = previousState
  self:AddObject(GameUIWorldChampion)
end
function GameStateWorldChampion:OnFocusLost(newState)
  DebugOut("GameStateWorldChampion:OnFocusLost")
  GameStateWorldChampion.nextState = newState
  self.mCurSubState = STATE_WC_SUB_STATE.SUB_STATE_UNKNOW
  self:EraseObject(GameUIWorldChampion)
end
function GameStateWorldChampion:GotoSubState(subState)
  if self.mCurSubState == subState then
    return
  end
  self:OnLeaveSubState(self.mCurSubState)
  self.mCurSubState = subState
  self:OnEnterSubState(self.mCurSubState)
end
function GameStateWorldChampion:OnEnterSubState(subState)
  DebugOut("GameStateWorldChampion:OnEnterSubState:", subState)
  if GameStateWorldChampion.mCurSubState == STATE_WC_SUB_STATE.SUB_STATE_BEFORE then
    GameUIWorldChampion:EnterBeforeUI()
  elseif GameStateWorldChampion.mCurSubState == STATE_WC_SUB_STATE.SUB_STATE_ENROLL then
    GameUIWorldChampion:EnterEnrollUI()
  elseif GameStateWorldChampion.mCurSubState == STATE_WC_SUB_STATE.SUB_STATE_MASS_ELECTION then
    GameUIWorldChampion:EnterRoundUI()
  elseif GameStateWorldChampion.mCurSubState == STATE_WC_SUB_STATE.SUB_STATE_PROMOTION then
    GameUIWorldChampion:EnterPromotionUI()
  end
end
function GameStateWorldChampion:OnLeaveSubState(subState)
end
