local GameUIEventGift = LuaObjectManager:GetLuaObject("GameUIEventGift")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameStateGlobalState = GameStateManager.GameStateGlobalState
function GameUIEventGift:Display(idSendTo, serverId)
  self.idSendTo = idSendTo
  self.serverId = serverId
  local flash = self:GetFlashObject()
  if not flash then
    self:LoadFlashObject()
    flash = self:GetFlashObject()
  end
  flash:InvokeASCallback("_root", "onLuaCommand", "StartShow")
  flash:InvokeASCallback("_root", "onLuaCommand", "RefreshText", {
    title = GameLoader:GetGameText("LC_MENU_SEND_GIFT_INVENTORY_TITLE"),
    selectGift = GameLoader:GetGameText("LC_MENU_SEND_GIFT_INVENTORY_TIP"),
    send = GameLoader:GetGameText("LC_MENU_SEND_GIFT_INVENTORY_BUTTON")
  })
  GameStateGlobalState:AddObject(self)
  self.canChooseGiftItem = nil
  self.bagItems = nil
  self:RequestGiftList()
  self:RequestItemList()
end
function GameUIEventGift:RequestGiftList()
  NetMessageMgr:SendMsg(NetAPIList.give_present_config_list_req.Code, {
    activity_name = "send_gift_valentine2021"
  }, function(msgType, content)
    if msgType == NetAPIList.give_present_config_list_ack.Code then
      self.canChooseGiftItem = content.give_present_config_list
      self:CheckRefreshList()
      return true
    end
    return false
  end, true)
end
function GameUIEventGift:RequestItemList()
  local bag_req_content = {
    owner = GameGlobalData:GetData("userinfo").player_id,
    bag_type = LuaObjectManager:GetLuaObject("GameFleetInfoBag").BAG_OWNER_USER,
    pos = -1
  }
  NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, function(msgType, content)
    if msgType == NetAPIList.bag_ack.Code then
      self.bagItems = content.grids
      self:CheckRefreshList()
      return true
    end
    return false
  end, true)
end
function GameUIEventGift:CheckRefreshList()
  if self.isMoveInOver and self.canChooseGiftItem and self.bagItems then
    local giftItems = {}
    for _, v in ipairs(self.bagItems) do
      for _, gift in ipairs(self.canChooseGiftItem) do
        if v.item_type == gift.gift_item then
          table.insert(giftItems, {
            id = v.item_type,
            num = v.cnt,
            name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. v.item_type)
          })
        end
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "onLuaCommand", "RefreshList", giftItems)
  end
end
function GameUIEventGift:Update(dt)
  local flash = self:GetFlashObject()
  if flash then
    flash:Update(dt)
    flash:InvokeASCallback("_root", "update", dt)
  end
end
function GameUIEventGift:OnFSCommand(cmd, arg)
  if cmd == "move_in_over" then
    self.isMoveInOver = true
    self:CheckRefreshList()
  elseif cmd == "move_out_over" then
    GameStateGlobalState:EraseObject(self)
  elseif cmd == "onClickSend" then
    do
      local itemId = tonumber(arg)
      local id
      for _, v in ipairs(self.canChooseGiftItem) do
        if v.gift_item == itemId then
          id = v.id
        end
      end
      local content = {
        id = id,
        target_player_id = tonumber(self.idSendTo),
        target_server_id = tonumber(self.serverId)
      }
      local itemName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemId)
      NetMessageMgr:SendMsg(NetAPIList.give_present_req.Code, content, function(msgType, content)
        if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.give_present_req.Code then
          if content.code ~= 0 then
            self:GetFlashObject():InvokeASCallback("_root", "onLuaCommand", "StartHide")
            local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
            GameUIGlobalScreen:ShowAlert("error", content.code, nil)
            return true
          end
          self:GetFlashObject():InvokeASCallback("_root", "onLuaCommand", "StartHide")
          local str = GameLoader:GetGameText("LC_MENU_SEND_GIFT_SEND_SUCCESS")
          str = string.gsub(str, "<itemname>", itemName)
          GameUtils:ShowTips(str)
          return true
        end
        return false
      end, true, function()
        GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_SEND_GIFT_SEND_FAIL"))
        self:GetFlashObject():InvokeASCallback("_root", "onLuaCommand", "StartHide")
      end)
    end
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIEventGift.OnAndroidBack()
    local flash = self:GetFlashObject()
    flash:InvokeASCallback("_root", "onLuaCommand", "StartHide")
  end
end
