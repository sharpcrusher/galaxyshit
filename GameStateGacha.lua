local GameStateGacha = GameStateManager.GameStateGacha
local GameObjectGacha = LuaObjectManager:GetLuaObject("GameObjectGacha")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
function GameStateGacha:OnFocusGain(statePrevious)
  GameStateGacha:AddObject(GameObjectGacha)
  function self.onQuitState()
    GameStateManager:SetCurrentGameState(statePrevious)
  end
  local function netCallGachaInfo()
    NetMessageMgr:SendMsg(NetAPIList.laba_info_req.Code, nil, self.NetCallbackGachaInfo, true, netCallGachaInfo)
  end
  netCallGachaInfo()
end
function GameStateGacha:OnFocusLost(stateNew)
  self:EraseObject(GameObjectGacha)
end
function GameStateGacha:QuitState()
  self.onQuitState()
end
function GameStateGacha.NetCallbackGachaInfo(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.laba_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgType == NetAPIList.laba_ntf.Code then
    if content.rate ~= 0 and tonumber(content.pos1) ~= 0 and tonumber(content.pos2) ~= 0 and tonumber(content.pos3) ~= 0 then
      GameObjectGacha.UserGachaData = content
      GameObjectGacha:SetAwardData(content)
      GameObjectGacha:UpdateResultData()
      GameObjectGacha:ShowTimesBox()
    else
      GameObjectGacha:ShowItemsBox()
    end
    return true
  end
  return false
end
