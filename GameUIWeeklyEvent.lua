local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIActivityEvent = LuaObjectManager:GetLuaObject("GameObjectActivityEvent")
GameUIWeeklyEvent = {}
GameUIWeeklyEvent.weeklyEventInfo = nil
GameUIWeeklyEvent.weeklyChestInfo = {}
GameUIWeeklyEvent.weeklyChestRewardInfo = {}
GameUIWeeklyEvent.weeklyChestRewardItemInfo = nil
GameUIWeeklyEvent.weeklyMissionInfo = {}
GameUIWeeklyEvent.weeklyTopThreeInfo = {}
GameUIWeeklyEvent.weeklyEventRankInfo = nil
GameUIWeeklyEvent.weeklyEventRankRewardInfo = nil
GameUIWeeklyEvent.mFlashObj = nil
GameUIWeeklyEvent.subCategory = 0
GameUIWeeklyEvent.category = 0
function GameUIWeeklyEvent:Init(flashObj, category, subCategory, weeklyEventInfo)
  self.mFlashObj = flashObj
  self:SetWeeklyEventInfo(weeklyEventInfo)
  self.category = category
  self.subCategory = subCategory
end
function GameUIWeeklyEvent:Close()
  self.weeklyEventInfo = nil
  self.weeklyChestInfo = {}
  self.weeklyChestRewardInfo = {}
  self.weeklyChestRewardItemInfo = nil
  self.weeklyMissionInfo = {}
  self.weeklyTopThreeInfo = {}
  self.weeklyEventRankInfo = nil
  self.weeklyEventRankRewardInfo = nil
  self.mFlashObj = nil
  self.subCategory = 0
  self.category = 0
end
function GameUIWeeklyEvent:GetFlashObj()
  return self.mFlashObj
end
function GameUIWeeklyEvent:SetWeeklyChestInfo(info)
  if info == nil then
    return
  end
  self.weeklyEventInfo.box_info = info
  self.weeklyChestInfo = {}
  self.weeklyChestRewardInfo = {}
  for k, v in ipairs(info.progress_awards or {}) do
    local data = {}
    data.score = v.box_score
    data.stat = v.status
    table.insert(self.weeklyChestInfo, data)
    table.insert(self.weeklyChestRewardInfo, v.awards)
  end
end
function GameUIWeeklyEvent:SetWeeklyMissionInfo(info)
  if info == nil then
    return
  end
  local gotoBtnStr = GameLoader:GetGameText("LC_MENU_TC_MOVE_BUTTON")
  self.weeklyEventInfo.tasks_list = info
  self.weeklyMissionInfo = {}
  for k, v in ipairs(info or {}) do
    local data = {}
    data.missionText = GameLoader:GetGameText(v.desc_key)
    data.missionPts = v.task_score
    data.btnText = gotoBtnStr
    data.cmd = v.jump
    table.insert(self.weeklyMissionInfo, data)
  end
end
function GameUIWeeklyEvent:SetWeeklyTopThreeInfo(info)
  if info == nil then
    return
  end
  self.weeklyEventInfo.top3_info = info
  self.weeklyTopThreeInfo = {}
  for k, v in ipairs(info.top_list or {}) do
    local data = {}
    data.playerName = v.name
    data.playerLV = v.level
    data.playerScore = v.score
    data.rankNum = v.rank
    table.insert(self.weeklyTopThreeInfo, data)
  end
end
function GameUIWeeklyEvent:SetWeeklyEventInfo(info)
  if info == nil then
    return
  end
  self.weeklyEventInfo = info
  self:SetWeeklyChestInfo(info.box_info)
  self:SetWeeklyMissionInfo(info.tasks_list)
  self:SetWeeklyTopThreeInfo(info.top3_info)
end
function GameUIWeeklyEvent:MoveIn()
  if self.mFlashObj then
    self.mFlashObj:InvokeASCallback("_root", "lua2fs_animationsMoveInWeekEvent", GameLoader:GetGameText(self.weeklyEventInfo.title_key), GameLoader:GetGameText("LC_MENU_weekly_tips"))
  end
end
local lastUpdatedTime = os.time()
function GameUIWeeklyEvent:Update(dt)
  local flash_obj = self.mFlashObj
  if flash_obj then
    if os.time() - lastUpdatedTime > 0 then
      lastUpdatedTime = os.time()
      local deltaTime = self.weeklyEventInfo.end_timespan - GameUIBarLeft:GetServerTimeUTC()
      local timeStr = ""
      if deltaTime > 0 then
        timeStr = GameUtils:formatTimeStringAsPartion2(deltaTime)
      else
        timeStr = GameLoader:GetGameText("LC_MENU_EVENT_OVER_BUTTON")
      end
      flash_obj:InvokeASCallback("_root", "setWeekEventTime", timeStr)
    end
    flash_obj:InvokeASCallback("_root", "OnUpdateList")
  end
end
function GameUIWeeklyEvent:UpdateMissionInfo(index)
  local flash_obj = self.mFlashObj
  if flash_obj == nil then
    return
  end
  local missionInfo = self.weeklyMissionInfo[index]
  if missionInfo == nil then
    return
  end
  flash_obj:InvokeASCallback("_root", "setEventInfo", index, missionInfo)
end
function GameUIWeeklyEvent:RefreshMissionList()
  local flash_obj = self.mFlashObj
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "clearWeekEventlist")
    flash_obj:InvokeASCallback("_root", "initWeekEventlist")
    local missionsCnt = #self.weeklyMissionInfo
    for i = 1, missionsCnt do
      flash_obj:InvokeASCallback("_root", "addWeekEventlist", i)
    end
    flash_obj:InvokeASCallback("_root", "setWeekEventListArrowVisible")
  end
end
function GameUIWeeklyEvent:RefreshChestProgressBar()
  local flash_obj = self.mFlashObj
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "initAwardProcessBar_Weekly", self.weeklyEventInfo.box_info.total_score, self.weeklyChestInfo, self.weeklyEventInfo.box_info.finished_score)
  end
end
function GameUIWeeklyEvent:RefreshTopThreeList()
  local flash_obj = self.mFlashObj
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "SetRankInfoPanel", self.weeklyTopThreeInfo, self.weeklyEventInfo.top3_info.self_score)
  end
end
function GameUIWeeklyEvent:RefreshWeeklyEventPage()
  self:RefreshMissionList()
  self:RefreshChestProgressBar()
  self:RefreshTopThreeList()
end
function GameUIWeeklyEvent:ShowItemDetailForGameItem(gameItem)
  if GameHelper:IsResource(gameItem.item_type) then
    return
  end
  if gameItem.item_type == "krypton" then
    if gameItem.level == 0 then
      gameItem.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(gameItem.number, gameItem.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(gameItem.number, gameItem.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, gameItem.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, gameItem.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  ItemBox:ShowGameItem(gameItem)
end
function GameUIWeeklyEvent:ShowRankList()
  if self.mFlashObj == nil then
    return
  end
  if self.weeklyEventRankInfo == nil then
    return
  end
  local rankListData = self.weeklyEventRankInfo
  local data = {}
  data.count = #rankListData.rank_list
  data.self = {
    user_name = rankListData.self_info.name,
    point = rankListData.self_info.score,
    level = rankListData.self_info.level
  }
  data.rank_level = rankListData.self_info.rank
  data.RankingCeil = 99999
  data.LevelText = GameLoader:GetGameText("LC_MENU_EXPEDITION_RANK_LEVEL_SHEET")
  local rank_desc_text = GameLoader:GetGameText("LC_MENU_SA_INTEGRAL_LABBLE")
  self.mFlashObj:InvokeASCallback("_root", "showInstanceRankList", data, rank_desc_text, true)
end
function GameUIWeeklyEvent:UpdateRankListItem(itemIndex)
  if self.mFlashObj == nil then
    return
  end
  local DataString = -1
  local rankInfo
  local rankList = self.weeklyEventRankInfo.rank_list
  if itemIndex > 0 and itemIndex <= #rankList then
    rankInfo = rankList[itemIndex]
  end
  if rankInfo then
    local DataTable = {}
    table.insert(DataTable, rankInfo.rank)
    table.insert(DataTable, rankInfo.name)
    table.insert(DataTable, rankInfo.level)
    table.insert(DataTable, rankInfo.score)
    DataString = table.concat(DataTable, "\001")
  end
  self.mFlashObj:InvokeASCallback("_root", "SetListItem", itemIndex, DataString)
end
function GameUIWeeklyEvent:ShowRankRewardList()
  if self.mFlashObj == nil then
    return
  end
  if self.weeklyEventRankRewardInfo == nil then
    return
  end
  self.mFlashObj:InvokeASCallback("_root", "showRankRewardList", #self.weeklyEventRankRewardInfo.ranking_awards)
end
function GameUIWeeklyEvent:UpdateRankRewardItem(indexItem)
  if self.mFlashObj == nil then
    return
  end
  local DataString = -1
  local ItemInfo
  local rankRewards = self.weeklyEventRankRewardInfo.ranking_awards
  if indexItem > 0 and indexItem <= #rankRewards then
    ItemInfo = rankRewards[indexItem]
  end
  if ItemInfo then
    local DataTable = {}
    table.insert(DataTable, ItemInfo.begin_rank)
    table.insert(DataTable, ItemInfo.end_rank)
    table.insert(DataTable, #ItemInfo.awards)
    for i = 1, #ItemInfo.awards do
      local extInfo = ItemInfo.awards[i]
      extInfo.indexItem = indexItem
      extInfo.mcIndex = i
      table.insert(DataTable, ItemInfo.awards[i].number)
      table.insert(DataTable, ItemInfo.awards[i].item_type)
      table.insert(DataTable, ItemInfo.awards[i].no)
      table.insert(DataTable, GameHelper:GetAwardTypeIconFrameNameSupportDynamic(ItemInfo.awards[i], extInfo, GameUIWeeklyEvent.UpdateRankRewardItemIcon))
      table.insert(DataTable, GameHelper:GetAwardCount(ItemInfo.awards[i].item_type, ItemInfo.awards[i].number, ItemInfo.awards[i].no))
    end
    DataString = table.concat(DataTable, "\001")
  end
  self.mFlashObj:InvokeASCallback("_root", "setRankRewardItemData", indexItem, DataString)
end
function GameUIWeeklyEvent.UpdateRankRewardItemIcon(extInfo)
  DebugOut("UpdateRankRewardItemIcon")
  DebugTable(extInfo)
  extInfo.icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(extInfo, nil, nil)
  GameUIWeeklyEvent:GetFlashObj():InvokeASCallback("_root", "UpdateRankRewardItemIcon", extInfo)
end
function GameUIWeeklyEvent.ExcuteJumpCmd(cmd)
  GameUIActivityEvent:TryGotoMenu(cmd)
end
function GameUIWeeklyEvent:OnFSCommand(cmd, arg)
  if cmd == "closeWeekEvent" then
    GameUIWeeklyEvent:Close()
  elseif cmd == "UpdateWeeklyEventChestItem" then
    local item = self.weeklyChestRewardItemInfo[tonumber(arg)]
    local iteminfo = GameHelper:GetItemInfo(item)
    local info = {
      m_icon = iteminfo.icon_frame,
      pic = iteminfo.icon_pic
    }
    info.num = GameHelper:GetAwardCountWithX(item.item_type, item.number, item.no)
    self.mFlashObj:InvokeASCallback("_root", "setBoxAwardListItems_Weekly", tonumber(arg), info)
  elseif cmd == "ShowWeeklyEventChestItem" then
    local m_args = LuaUtils:string_split(arg, "_")
    self.weeklyChestRewardItemInfo = self.weeklyChestRewardInfo[tonumber(m_args[2])]
    DebugOut("enenenenen")
    if self.weeklyChestRewardItemInfo then
      self.mFlashObj:InvokeASCallback("_root", "showBoxDetail_Weekly", tonumber(m_args[1]), #self.weeklyChestRewardItemInfo)
    end
  elseif cmd == "ShowWeeklyChestItemInfo" then
    if self.weeklyChestRewardItemInfo then
      local item = self.weeklyChestRewardItemInfo[tonumber(arg)]
      self:ShowItemDetailForGameItem(item)
    end
  elseif cmd == "showWeekEventInfo" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(string.gsub(GameLoader:GetGameText(self.weeklyEventInfo.desc_key), "!@#", "\n"))
  elseif cmd == "showWeekEventRank" then
    NetMessageMgr:SendMsg(NetAPIList.week_cycle_activiy_rank_req.Code, nil, GameUIWeeklyEvent.WeeklyEventRankInfoCallback, true, nil)
  elseif cmd == "showWeekEventReward" then
    NetMessageMgr:SendMsg(NetAPIList.week_cycle_activiy_rank_awards_req.Code, nil, GameUIWeeklyEvent.WeeklyEventRankRewardInfoCallback, true, nil)
  elseif cmd == "GetWeeklyEventChest" then
    local index = tonumber(arg)
    local curBox = self.weeklyChestInfo[index]
    local net_packet = {
      box_score = curBox.score
    }
    NetMessageMgr:SendMsg(NetAPIList.week_cycle_activiy_get_box_req.Code, net_packet, nil, true, nil)
  elseif cmd == "UpdateWeeklyEventMissionInfo" then
    self:UpdateMissionInfo(tonumber(arg))
  elseif cmd == "weekEventGoTo" then
    self.ExcuteJumpCmd(arg)
  elseif "UpdWeeklyEventRankItem" == cmd then
    self:UpdateRankListItem(tonumber(arg))
  elseif "UpdateWeeklyEventRewardItem" == cmd then
    self:UpdateRankRewardItem(tonumber(arg))
  elseif "ShowRankRewardItemInfo" == cmd then
    local itemType, no, cnt = unpack(LuaUtils:string_split(arg, ":"))
    GameHelper:showItemDetil({
      number = tonumber(no),
      item_type = itemType,
      no = tonumber(cnt)
    }, itemType)
  end
end
function GameUIWeeklyEvent.WeeklyEventRankInfoCallback(msgType, content)
  if msgType == NetAPIList.week_cycle_activiy_rank_ack.Code then
    GameUIWeeklyEvent.weeklyEventRankInfo = content
    GameUIWeeklyEvent:ShowRankList()
    return true
  end
  return false
end
function GameUIWeeklyEvent.WeeklyEventRankRewardInfoCallback(msgType, content)
  if msgType == NetAPIList.week_cycle_activiy_rank_awards_ack.Code then
    GameUIWeeklyEvent.weeklyEventRankRewardInfo = content
    GameUIWeeklyEvent:ShowRankRewardList()
    return true
  end
  return false
end
function GameUIWeeklyEvent.WeeklyChestInfoNtf(content)
  GameUIWeeklyEvent:SetWeeklyChestInfo(content)
  GameUIWeeklyEvent:RefreshChestProgressBar()
end
function GameUIWeeklyEvent.weeklyTopThreeInfoNtf(content)
  GameUIWeeklyEvent:SetWeeklyTopThreeInfo(content)
  GameUIWeeklyEvent:RefreshTopThreeList()
end
return GameUIWeeklyEvent
