DebugCommand = {
  dinfo = {},
  stopAll = false
}
function DebugCommand:t_tostring(maxdepth, _indent)
  local str = {}
  maxdepth = maxdepth or 4
  _indent = _indent or "--"
  local function internal(self, str, indent)
    if #indent >= maxdepth then
      table.insert(str, indent .. " ,too many depth\n")
      return
    end
    for k, v in pairs(self) do
      if type(v) == "table" then
        table.insert(str, indent .. tostring(k) .. ":\n")
        internal(v, str, indent .. indent)
      else
        table.insert(str, indent .. tostring(k) .. ": " .. tostring(v) .. "\n")
      end
    end
  end
  internal(self, str, _indent)
  return ...
end
function DebugCommand.DebugPoint(id, ...)
