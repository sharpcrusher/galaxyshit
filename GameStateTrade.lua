local GameStateTrade = GameStateManager.GameStateTrade
local GameTradeTabBar = LuaObjectManager:GetLuaObject("GameTradeTabBar")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameCommonBackground = LuaObjectManager:GetLuaObject("GameCommonBackground")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectDragger = LuaObjectManager:GetLuaObject("GameObjectDragger")
local GameObjectDraggerItem = GameObjectDragger:NewInstance("bagItem.tfs", false)
GameStateTrade.dragItemOffset = -70
function GameObjectDraggerItem:OnAddToGameState(state)
  local frame = ""
  if DynamicResDownloader:IsDynamicStuff(GameObjectDraggerItem.DraggedItemTYPE, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(GameObjectDraggerItem.DraggedItemTYPE .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      frame = "item_" .. GameObjectDraggerItem.DraggedItemTYPE
    else
      frame = "temp"
    end
  else
    frame = "item_" .. GameObjectDraggerItem.DraggedItemTYPE
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
  self:GetFlashObject():InvokeASCallback("_root", "ChangeToBigger")
end
function GameObjectDraggerItem:OnEraseFromGameState(state)
  DebugOut("erase itemDragger from GameFleetInfoBag and GameFleetInfoUI")
  self.DraggedItemID = -1
  self.unloadItem = false
  self.equipItem = false
  self.m_isAutoMove = false
end
function GameObjectDraggerItem:OnReachedDestPos()
  if self.DraggedItemID and self.DraggedItemID ~= -1 then
    if self.sellItem then
      GameItemBag:SellItem(self.DraggedItemID)
    end
  else
    GameStateTrade:onEndDragItem()
  end
end
function GameStateTrade:onEndDragItem()
  GameItemBag:onEndDragItem()
  GameStateTrade:EraseObject(GameObjectDraggerItem)
end
function GameStateTrade:InitGameState()
  self.currentTab = -1
end
function GameStateTrade:OnFocusGain(previousState)
  GameObjectDraggerItem:Init()
  self.previousState = previousState
  GameTradeTabBar:LoadFlashObject()
  GameItemBag:LoadFlashObject()
  self:AddObject(GameCommonBackground)
  self:AddObject(GameItemBag)
  self:AddObject(GameTradeTabBar)
  self:ForceCompleteCammandList()
  GameTradeTabBar:Init(GameTradeTabBar.TAB_EQUIP)
  GameObjectDraggerItem:LoadFlashObject()
end
function GameStateTrade:OnFocusLost(newState)
  self:OnChangeTab(self.currentTab, -1)
  GameItemBag:UnloadFlashObject()
  GameTradeTabBar:UnloadFlashObject()
  GameObjectDraggerItem:UnloadFlashObject()
  self:EraseObject(GameItemBag)
  self:EraseObject(GameTradeTabBar)
  self:EraseObject(GameObjectDraggerItem)
end
function GameStateTrade:SetTab(index)
  if self.currentTab ~= index then
    self:OnChangeTab(self.currentTab, index)
  end
end
function GameStateTrade:OnChangeTab(pre, cur)
  self.currentTab = cur
  GameItemBag:OnChangeToDummyBag(cur)
end
function GameStateTrade:BeginDragItem(itemId, itemType, initPosX, initPosY, posX, posY)
  DebugOut("BeginDragItem: ", itemId, itemType, posX, posY)
  GameObjectDraggerItem.DraggedItemID = itemId
  GameObjectDraggerItem.DraggedItemTYPE = itemType
  GameObjectDraggerItem:SetDestPosition(initPosX, initPosY)
  GameObjectDraggerItem:BeginDrag(tonumber(self.dragItemOffset), tonumber(self.dragItemOffset), tonumber(posX), tonumber(posY))
  GameStateManager:GetCurrentGameState():AddObject(GameObjectDraggerItem)
  GameItemBag:onBeginDragItem(GameObjectDragger)
end
function GameStateTrade:OnTouchPressed(x, y)
  DebugOut("check pressed")
  if GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  DebugOut("check pressed ok")
  GameStateBase.OnTouchPressed(self, x, y)
end
function GameStateTrade:OnTouchMoved(x, y)
  DebugOut("check moved")
  if GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  DebugOut("check moved ok")
  GameStateBase.OnTouchMoved(self, x, y)
  local dragItemId = GameObjectDraggerItem.DraggedItemID
  if dragItemId and dragItemId ~= -1 then
    GameObjectDraggerItem:UpdateDrag()
  end
end
function GameStateTrade:OnTouchReleased(x, y)
  DebugOut("check release")
  if GameObjectDraggerItem:IsAutoMove() or GameUIGlobalScreen.IsShowingLoading then
    return
  end
  DebugOut("check release ok")
  GameStateBase.OnTouchReleased(self, x, y)
  local dragItemId = GameObjectDraggerItem.DraggedItemID
  if dragItemId and dragItemId ~= -1 then
    if GameItemBag.dragItem then
      local onSell = GameItemBag:GetFlashObject():InvokeASCallback("_root", "IsOnSell")
      DebugOut("onSell: ", type(onSell), onSell)
      if onSell == "true" then
        GameObjectDraggerItem.sellItem = true
        local posInfo = GameItemBag:GetFlashObject():InvokeASCallback("_root", "GetSellPos")
        local ptPos = LuaUtils:string_split(posInfo, "\001")
        GameObjectDraggerItem:SetDestPosition(tonumber(ptPos[1]), tonumber(ptPos[2]))
      else
        GameObjectDraggerItem.sellItem = false
      end
    end
    if not GameObjectDraggerItem.sellItem then
      GameObjectDraggerItem.DraggedItemID = -1
    end
    GameObjectDraggerItem:StartAutoMove()
  end
end
function GameStateTrade:Update(dt)
  GameStateBase.Update(self, dt)
  if GameObjectDraggerItem:IsAutoMove() then
    return
  end
  if GameItemBag:GetFlashObject() then
    GameItemBag:GetFlashObject():InvokeASCallback("_root", "updateGrid", dt)
  end
end
