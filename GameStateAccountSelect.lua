local GameStateAccountSelect = GameStateManager.GameStateAccountSelect
local GameStateLoading = GameStateManager.GameStateLoading
local GameAccountSelector = LuaObjectManager:GetLuaObject("GameAccountSelector")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
function GameStateAccountSelect:InitGameState()
  self:LoadSetting()
end
function GameStateAccountSelect:OnFocusGain()
  self:AddObject(GameAccountSelector)
  if GameSettingData.LastLogin then
    GameUtils:SetLoginInfo(LuaUtils:table_rcopy(GameSettingData.LastLogin))
    DelayLoad()
  end
end
function GameStateAccountSelect:OnFocusLost()
  self:EraseObject(GameAccountSelector)
end
function GameStateAccountSelect.TryUpdateServer(registCallback)
  GameWaiting:ShowLoadingScreen()
  DebugOutPutTable(GameSettingData.PassportTable, "PassportTable")
  local AccountTable = {}
  if GameSettingData.PassportTable then
    for _, AccountInfo in ipairs(GameSettingData.PassportTable) do
      local account = {}
      account.passport = AccountInfo.passport
      account.acctype = AccountInfo.accType
      if not account.acctype then
        account.acctype = GameUtils.AccountType.mail
      end
      table.insert(AccountTable, account)
    end
  end
  DebugOut("TryUpdateServer")
  DebugOutPutTable(AccountTable, "AccountTable")
  if registCallback then
    GameUtils:UpdateServerList(registCallback, AccountTable)
  else
    GameUtils:UpdateServerList(GameStateAccountSelect.UpdateServerCallback2, AccountTable)
  end
end
function GameStateAccountSelect:LoadSetting()
  DebugOut("GameStateAccountSelect:LoadSetting")
  GameUtils:CheckUpdateSettingData()
  GameUtils:DebugOutTableInAndroid(GameSettingData.Tap4funAccount)
  GameUtils:DebugOutTableInAndroid(GameSettingData.PassportTable)
  if not GameSettingData.PassportTable and GameSettingData.Tap4funAccount then
    GameSettingData.PassportTable = {}
    table.insert(GameSettingData.PassportTable, LuaUtils:table_rcopy(GameSettingData.Tap4funAccount))
  end
  GameUtils:DebugOutTableInAndroid(GameSettingData.PassportTable)
  if GameSettingData.Gateway then
    local url = "http://" .. GameSettingData.Gateway .. "/"
    if GameUtils:URLIsHTTPS(GameSettingData.Gateway) then
      url = "https://" .. GameSettingData.Gateway .. "/"
    end
    GameUtils.HTTP_SERVER = url
  end
  DebugOutPutTable(GameSettingData, "SettingData")
  GameUtils:SaveSettingData()
  GameUtils._ActiveServerInfo = nil
  GameLoader:LoadGameText(GameSetting:GetSavedLanguage(), {
    "menu",
    "story",
    "alert",
    "item",
    "quest",
    "newquest",
    "quest170",
    "npc",
    "fleet",
    "newfleet",
    "battle",
    "newbattle",
    "event",
    "achievement",
    "tech",
    "help"
  })
  GameUIGlobalScreen:LoadFlashObject()
  GameTip:LoadFlashObject()
  if AutoUpdate.isSupportQuickStartGame and AutoUpdate.isQuickStartGame then
  else
    self.TryUpdateServer()
  end
end
function GameStateAccountSelect.UpdateServerCallback2(content)
  GameWaiting:HideLoadingScreen()
  GameUtils:DebugOutTableInAndroid(content)
  if content.error then
    DebugOut("UpdateServerCallback error")
    if content.error == "passport_not_valid" then
      local errorTitle = GameLoader:GetGameText("LC_MENU_ERROR_TITLE")
      local errorContent = GameLoader:GetGameText(AlertDataList:GetTextId("passport_not_valid"))
      GameUIGlobalScreen:ShowMessageBox(1, errorTitle, errorContent, GameStateAccountSelect.TryUpdateServer)
    end
  else
    GameStateAccountSelect.mAccountData = content
    GameAccountSelector:GenerateAccountTable()
    if IPlatformExt.getConfigValue("UseExtLogin") == "True" then
      GameUtils:printByAndroid("UpdateServerCallback  IPlatformExt")
      if #GameAccountSelector.LoginTable == 0 then
        GameUtils.IPlatformExtLogin_Standard_GateWay()
      else
        GameUtils:DebugOutTableInAndroid(GameAccountSelector.LoginTable)
        if not GameUtils:GetLoginInfo() then
          GameUtils:SetLoginInfo(LuaUtils:table_rcopy(GameAccountSelector.LoginTable[1]))
          DelayLoad()
          GameAccountSelector:UpdateLoginInfo()
        end
        local mLoginInfo = GameUtils:GetLoginInfo()
        if mLoginInfo and not mLoginInfo.ServerInfo then
          local ServerInfo = GameStateAccountSelect.mAccountData.recommand_server
          assert(ServerInfo)
          mLoginInfo.ServerInfo = ServerInfo
          GameUtils:SetLoginInfo(mLoginInfo)
          DelayLoad()
          NetMessageMgr:InitNet()
          GameUtils.IPlatformExtLogin_Standard_Server()
        end
      end
    elseif GameUtils:IsQihooApp() then
      GameUtils:printByAndroid("print  UpdateServerCallback  IsQihooApp")
      if GameUtils.qihoo_token and GameUtils.qihoo_token ~= "-1" then
        if #GameAccountSelector.LoginTable == 0 then
          GameUtils:printByAndroid("print  UpdateServerCallback  #GameAccountSelector.LoginTable == 0")
          local LoginInfo = {}
          local ServerInfo = GameStateAccountSelect.mAccountData.recommand_server
          assert(ServerInfo)
          LoginInfo.ServerInfo = ServerInfo
          GameUtils:SetLoginInfo(LoginInfo)
          DelayLoad()
          NetMessageMgr:InitNet()
          GameStateManager:SetCurrentGameState(GameStateLoading)
        else
          GameUtils:printByAndroid("print  UpdateServerCallback  #GameAccountSelector.LoginTable ~= 0   setLoginInfo")
          if ext.IsRestartGame() and GameSettingData.LastLogin and GameSettingData.LastLogin.PlayerInfo and GameSettingData.LastLogin.ServerInfo and GameSettingData.LastLogin.AccountInfo and GameSettingData.LastLogin.AccountInfo.accType == GameUtils.AccountType.qihoo then
            GameAccountSelector.LoginTable[1] = GameSettingData.LastLogin
          else
            for index, account_info in ipairs(GameAccountSelector.LoginTable) do
              if account_info.AccountInfo and account_info.AccountInfo.accType and account_info.AccountInfo.accType == GameUtils.AccountType.qihoo then
                if index == 1 then
                  break
                else
                  table.remove(GameAccountSelector.LoginTable, index)
                  GameAccountSelector.LoginTable[1] = account_info
                  break
                end
              end
            end
          end
          GameUtils:SetLoginInfo(LuaUtils:table_rcopy(GameAccountSelector.LoginTable[1]))
          GameAccountSelector:UpdateLoginInfo()
          DelayLoad()
        end
      elseif GameSettingData.qihooSwitch and GameSettingData.qihooSwitch == "true" then
        GameSettingData.qihooSwitch = nil
        GameUtils:SaveSettingData()
        IPlatformExt.logout = IPlatformExt.logout or IPlatformExt.loginOut
        IPlatformExt.logout()
        GameUtils:printByAndroid("===========GameSettingData.qihooSwitch============111")
      else
        IPlatformExt.Login()
      end
    elseif #GameAccountSelector.LoginTable == 0 then
      local LoginInfo = {}
      local ServerInfo = GameStateAccountSelect.mAccountData.recommand_server
      assert(ServerInfo)
      LoginInfo.ServerInfo = ServerInfo
      GameUtils:SetLoginInfo(LoginInfo)
      DelayLoad()
      NetMessageMgr:InitNet()
      GameStateManager:SetCurrentGameState(GameStateLoading)
      GameObjectTutorialCutscene.isSkipTutorialVisable = false
    else
      GameObjectTutorialCutscene.isSkipTutorialVisable = false
      for k, v in pairs(GameAccountSelector.LoginTable) do
        local isCreatedActor = v.isCreateActor or 1
        if isCreatedActor == 1 then
          GameObjectTutorialCutscene.isSkipTutorialVisable = true
          break
        end
      end
      if not GameUtils:GetLoginInfo() then
        GameUtils:SetLoginInfo(LuaUtils:table_rcopy(GameAccountSelector.LoginTable[1]))
        DelayLoad()
        GameAccountSelector:UpdateLoginInfo()
      else
        TPSafeExecute(function()
          GameAccountSelector:UpdateLoginInfo()
        end)
      end
    end
  end
end
