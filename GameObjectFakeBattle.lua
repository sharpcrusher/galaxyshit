local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local GameObjectFakeBattle = LuaObjectManager:GetLuaObject("GameObjectFakeBattle")
local TutorialQuestManager = TutorialQuestManager
function GameObjectFakeBattle:OnFSCommand(cmd, arg)
  if cmd == "AnimOver" and self.callback then
    DebugOutBattlePlay("GameObjectFakeBattle:OnFSCommand")
    self.callback()
  end
end
function GameObjectFakeBattle:HideAllFakeBattleEffect()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "HideAllFakeBattleEffect")
  end
end
function GameObjectFakeBattle:ExcuteUtilsCommand(commandName)
  DebugOutBattlePlay("GameObjectFakeBattle:ExcuteUtilsCommand")
  DebugOutBattlePlayTable(commandName)
  for k, v in pairs(commandName) do
    if v[1] == "ShowShipAppear" then
      DebugOutBattlePlay("ShowShipAppear", v[2], v[3])
      GameObjectBattleReplay:ShowShipAppear(v[2], v[3])
    elseif v[1] == "HideShipInFakeBattle" then
      DebugOutBattlePlay("HideShipInFakeBattle", v[2], v[3])
      GameObjectBattleReplay:HideShipWithAlpha(v[2], v[3])
    elseif v[1] == "SetShipDisappearInFakeBattle" then
      DebugOutBattlePlay("SetShipDisappearInFakeBattle", v[2], v[3])
      GameObjectBattleReplay:SetShipDisappearWithAlpha(v[2], v[3])
    elseif v[1] == "SetPlayer1Avatar" then
      GameObjectBattleReplay.m_battleResult.player1 = v[2]
      GameObjectBattleReplay.m_battleResult.player1_avatar = v[3]
    elseif v[1] == "SetPlayer2Avatar" then
      GameObjectBattleReplay.m_battleResult.player2 = v[2]
      GameObjectBattleReplay.m_battleResult.player2_avatar = v[3]
    end
  end
end
function GameObjectFakeBattle:PlayAnim(animName, animOverCallBack)
  if type(animName) == "table" then
    self:ExcuteUtilsCommand(animName)
    self.callback = animOverCallBack
    self.m_isAutoCallBack = true
    self.m_timeCounter = 0
  else
    DebugOutBattlePlay("GameObjectFakeBattle:PlayAnim ", animName, type(animOverCallBack))
    local isIn = true
    if LuaUtils:string_startswith(animName, "in ") then
      animName = string.sub(animName, 4, -1)
    end
    if LuaUtils:string_startswith(animName, "ou ") then
      animName = string.sub(animName, 4, -1)
      isIn = false
    end
    self:GetFlashObject():InvokeASCallback("_root", "PlayFakeBattleAnim", animName, isIn)
    self.callback = animOverCallBack
    self.m_isAutoCallBack = false
    self.m_timeCounter = 0
  end
end
function GameObjectFakeBattle:OnAddToGameState(TheGameStateAddTo)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self.m_timeCounter = 0
  self.m_isAutoCallBack = false
end
function GameObjectFakeBattle:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameObjectFakeBattle:Update(dt)
  if self.m_isAutoCallBack then
    self.m_timeCounter = self.m_timeCounter + dt
    if self.m_timeCounter > 3000 and self.callback then
      DebugOutBattlePlay("GameObjectFakeBattle:Update")
      self.callback()
      self.callback = nil
      self.m_isAutoCallBack = false
    end
  end
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
  end
end
