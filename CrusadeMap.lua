CrusadeMap = luaClass(nil)
function CrusadeMap:ctor(flash)
  self.flash_obj = flash
  self.mMapRoot = "_root.time_space.point_1x1.map"
end
function CrusadeMap:MapFrameUpdate(dt)
  if self.flash_obj then
    self.flash_obj:InvokeASCallback("_root", "MapFrameUpdate", dt)
  end
end
function CrusadeMap:ResetShip()
  if self.flash_obj then
    self.flash_obj:InvokeASCallback(self.mMapRoot, "resetAll")
  end
end
function CrusadeMap:RestoreShip(shipPos)
  if self.flash_obj then
    self.flash_obj:InvokeASCallback(self.mMapRoot, "RestoreShip", shipPos)
  end
end
function CrusadeMap:GetShipPos()
  if self.flash_obj then
    local shipPos = self.flash_obj:InvokeASCallback(self.mMapRoot, "GetShipPos")
    DebugOut("GetShipPos: " .. shipPos)
    return shipPos
  end
end
function CrusadeMap:SetEnemy(enemyItemName, isVisible, pos, statusFrm, shipCount, shipFrms, forceTitle, forceStr, enemyName, lvStr, statusStr)
  if self.flash_obj then
    DebugOut("root " .. self.mMapRoot)
    self.flash_obj:InvokeASCallback(self.mMapRoot, "SetEnemy", enemyItemName, isVisible, pos, statusFrm, shipCount, shipFrms, forceTitle, forceStr, enemyName, lvStr, statusStr)
  end
end
function CrusadeMap:ResetEnemy()
  if self.flash_obj then
    self.flash_obj:InvokeASCallback(self.mMapRoot, "ResetEnemy")
  end
end
function CrusadeMap:SetAttackPath(count, path)
  if self.flash_obj then
    DebugOut("GameUICrusade:SetPath " .. path)
    self.flash_obj:InvokeASCallback(self.mMapRoot, "SetAttackPath", count, path)
  else
    DebugOut("flash_obj is nil.")
  end
end
function CrusadeMap:CalculateMaxMapRootOffset()
  if self.flash_obj then
    self.flash_obj:InvokeASCallback(self.mMapRoot, "CalculateMaxMapRootOffset")
  else
    DebugOut("flash_obj is nil.")
  end
end
