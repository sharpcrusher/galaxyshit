print("----------------Lua world is Run--------------!")
local stackCount = 12
local function getStack()
  local contentTable = {}
  for i = 3, stackCount do
    local curInfo = debug.getinfo(i)
    if curInfo then
      table.insert(contentTable, "      ")
      table.insert(contentTable, curInfo.short_src)
      table.insert(contentTable, " in line:")
      table.insert(contentTable, curInfo.currentline)
      table.insert(contentTable, "   in function:")
      table.insert(contentTable, curInfo.name)
      table.insert(contentTable, "\n")
    end
  end
  local ret = table.concat(contentTable)
  return ret
end
TPPrint = nil
function TPSafeExecute(func, ...)
