local adjutant_skill = GameData.adjutant_skill.adjutant_skill
adjutant_skill[1] = {
  spell_id = 1,
  buff = "10051.0",
  buff_range_type = "32.0"
}
adjutant_skill[2] = {
  spell_id = 2,
  buff = "10051.0",
  buff_range_type = "31.0"
}
adjutant_skill[3] = {
  spell_id = 3,
  buff = "10051.0",
  buff_range_type = "35.0"
}
adjutant_skill[4] = {
  spell_id = 4,
  buff = "10053.0",
  buff_range_type = "34.0"
}
adjutant_skill[5] = {
  spell_id = 5,
  buff = "10057.0",
  buff_range_type = "33.0"
}
adjutant_skill[6] = {
  spell_id = 6,
  buff = "10059.0",
  buff_range_type = "32.0"
}
adjutant_skill[7] = {
  spell_id = 7,
  buff = "10059.0",
  buff_range_type = "31.0"
}
adjutant_skill[8] = {
  spell_id = 8,
  buff = "10059.0",
  buff_range_type = "35.0"
}
adjutant_skill[9] = {
  spell_id = 9,
  buff = "10059.0",
  buff_range_type = "34.0"
}
adjutant_skill[10] = {
  spell_id = 10,
  buff = "10180.0",
  buff_range_type = "42.0"
}
adjutant_skill[11] = {
  spell_id = 11,
  buff = "10013.0",
  buff_range_type = "33.0"
}
adjutant_skill[12] = {
  spell_id = 12,
  buff = "10180.0",
  buff_range_type = "41.0"
}
adjutant_skill[13] = {
  spell_id = 13,
  buff = "10180.0",
  buff_range_type = "45.0"
}
adjutant_skill[14] = {
  spell_id = 14,
  buff = "10180.0",
  buff_range_type = "44.0"
}
adjutant_skill[15] = {
  spell_id = 15,
  buff = "10064.0",
  buff_range_type = "2.0"
}
adjutant_skill[16] = {
  spell_id = 16,
  buff = "20018.0",
  buff_range_type = "33.0"
}
adjutant_skill[17] = {
  spell_id = 17,
  buff = "20002.0",
  buff_range_type = "33.0"
}
adjutant_skill[18] = {
  spell_id = 18,
  buff = "20001.0",
  buff_range_type = "32.0"
}
adjutant_skill[19] = {
  spell_id = 19,
  buff = "20011.0",
  buff_range_type = "32.0"
}
adjutant_skill[20] = {
  spell_id = 20,
  buff = "20003.0",
  buff_range_type = "34.0"
}
adjutant_skill[21] = {
  spell_id = 21,
  buff = "20011.0",
  buff_range_type = "34.0"
}
adjutant_skill[22] = {
  spell_id = 22,
  buff = "20006.0",
  buff_range_type = "11.0"
}
adjutant_skill[23] = {
  spell_id = 23,
  buff = "20007.0",
  buff_range_type = "11.0"
}
adjutant_skill[24] = {
  spell_id = 24,
  buff = "20005.0",
  buff_range_type = "11.0"
}
adjutant_skill[25] = {
  spell_id = 25,
  buff = "20014.0",
  buff_range_type = "11.0"
}
adjutant_skill[26] = {
  spell_id = 26,
  buff = "20012.0",
  buff_range_type = "11.0"
}
adjutant_skill[27] = {
  spell_id = 27,
  buff = "20013.0",
  buff_range_type = "11.0"
}
adjutant_skill[28] = {
  spell_id = 28,
  buff = "10051.0",
  buff_range_type = "32.0"
}
adjutant_skill[29] = {
  spell_id = 29,
  buff = "10051.0",
  buff_range_type = "31.0"
}
adjutant_skill[30] = {
  spell_id = 30,
  buff = "10051.0",
  buff_range_type = "35.0"
}
adjutant_skill[31] = {
  spell_id = 31,
  buff = "10052.0",
  buff_range_type = "32.0"
}
adjutant_skill[32] = {
  spell_id = 32,
  buff = "10052.0",
  buff_range_type = "31.0"
}
adjutant_skill[33] = {
  spell_id = 33,
  buff = "10052.0",
  buff_range_type = "35.0"
}
adjutant_skill[34] = {
  spell_id = 34,
  buff = "10053.0",
  buff_range_type = "34.0"
}
adjutant_skill[35] = {
  spell_id = 35,
  buff = "10061.0",
  buff_range_type = "35.0"
}
adjutant_skill[36] = {
  spell_id = 36,
  buff = "10057.0",
  buff_range_type = "33.0"
}
adjutant_skill[37] = {
  spell_id = 37,
  buff = "10056.0",
  buff_range_type = "33.0"
}
adjutant_skill[38] = {
  spell_id = 38,
  buff = "10013.0",
  buff_range_type = "33.0"
}
adjutant_skill[39] = {
  spell_id = 39,
  buff = "20018.0",
  buff_range_type = "33.0"
}
adjutant_skill[40] = {
  spell_id = 40,
  buff = "20002.0",
  buff_range_type = "33.0"
}
adjutant_skill[41] = {
  spell_id = 41,
  buff = "20001.0",
  buff_range_type = "32.0"
}
adjutant_skill[42] = {
  spell_id = 42,
  buff = "20011.0",
  buff_range_type = "32.0"
}
adjutant_skill[43] = {
  spell_id = 43,
  buff = "20003.0",
  buff_range_type = "34.0"
}
adjutant_skill[44] = {
  spell_id = 44,
  buff = "20011.0",
  buff_range_type = "34.0"
}
adjutant_skill[45] = {
  spell_id = 45,
  buff = "20006.0",
  buff_range_type = "11.0"
}
adjutant_skill[46] = {
  spell_id = 46,
  buff = "20007.0",
  buff_range_type = "11.0"
}
adjutant_skill[47] = {
  spell_id = 47,
  buff = "20005.0",
  buff_range_type = "11.0"
}
adjutant_skill[48] = {
  spell_id = 48,
  buff = "20014.0",
  buff_range_type = "11.0"
}
adjutant_skill[49] = {
  spell_id = 49,
  buff = "20012.0",
  buff_range_type = "11.0"
}
adjutant_skill[50] = {
  spell_id = 50,
  buff = "20013.0",
  buff_range_type = "11.0"
}
adjutant_skill[51] = {
  spell_id = 51,
  buff = "10180.0",
  buff_range_type = "43.0"
}
adjutant_skill[52] = {
  spell_id = 52,
  buff = "10180.0",
  buff_range_type = "44.0"
}
adjutant_skill[53] = {
  spell_id = 53,
  buff = "20010.0",
  buff_range_type = "11.0"
}
adjutant_skill[54] = {
  spell_id = 54,
  buff = "20005.0",
  buff_range_type = "35.0"
}
adjutant_skill[55] = {
  spell_id = 55,
  buff = "10064.0",
  buff_range_type = "2.0"
}
adjutant_skill[56] = {
  spell_id = 56,
  buff = "20013.0",
  buff_range_type = "33.0"
}
adjutant_skill[57] = {
  spell_id = 57,
  buff = "20018.0",
  buff_range_type = "33.0"
}
adjutant_skill[58] = {
  spell_id = 58,
  buff = "20011.0",
  buff_range_type = "32.0"
}
adjutant_skill[59] = {
  spell_id = 59,
  buff = "10180.0",
  buff_range_type = "42.0"
}
adjutant_skill[60] = {
  spell_id = 60,
  buff = "20012.0",
  buff_range_type = "11.0"
}
adjutant_skill[61] = {
  spell_id = 61,
  buff = "20051.0",
  buff_range_type = "11.0"
}
adjutant_skill[62] = {
  spell_id = 62,
  buff = "20012.0",
  buff_range_type = "36.0"
}
adjutant_skill[63] = {
  spell_id = 63,
  buff = "10056.0",
  buff_range_type = "36.0"
}
adjutant_skill[64] = {
  spell_id = 64,
  buff = "20010.0",
  buff_range_type = "34.0"
}
adjutant_skill[65] = {
  spell_id = 65,
  buff = "10053.0",
  buff_range_type = "34.0"
}
adjutant_skill[66] = {
  spell_id = 66,
  buff = "20018.0",
  buff_range_type = "35.0"
}
adjutant_skill[67] = {
  spell_id = 67,
  buff = "10059.0",
  buff_range_type = "35.0"
}
adjutant_skill[2002] = {
  spell_id = 2002,
  buff = "10043.0",
  buff_range_type = "100.0"
}
