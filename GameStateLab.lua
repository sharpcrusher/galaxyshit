local GameStateLab = GameStateManager.GameStateLab
local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
local GameUICrusade = require("data1/GameUICrusade.tfl")
STATE_LAB_SUB_STATE = {
  SUBSTATE_NORMAL = 1,
  SUBSTATE_CRUSADE = 2,
  SUBSTATE_COSMOS = 3
}
GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
function GameStateLab:InitGameState()
end
function GameStateLab:OnFocusGain(previousState)
  DebugOut("GameStateLab:OnFocusGain")
  self.lastState = previousState
  self:AddObject(GameUILab)
  if self.mEnterCallback then
    self.mEnterCallback()
    self.mEnterCallback = nil
  end
end
function GameStateLab:OnFocusLost(newState)
  DebugOut("GameStateLab:OnFocusLost")
  GameUILab.nextState = newState
  self:EraseObject(GameUILab)
end
function GameStateLab:SetEnterCallback(enterCallback)
  self.mEnterCallback = enterCallback
end
function GameStateLab:GoToSubState(subState)
  self:OnLeaveSubState(self.mCurSubState)
  self.mCurSubState = subState
  self:OnEnterSubState(self.mCurSubState)
end
function GameStateLab:OnEnterSubState(subState)
  if subState == STATE_LAB_SUB_STATE.SUBSTATE_NORMAL then
    GameUILab:EnterLab()
  elseif subState == STATE_LAB_SUB_STATE.SUBSTATE_CRUSADE then
  end
end
function GameStateLab:OnLeaveSubState(subState)
  if subState == STATE_LAB_SUB_STATE.SUBSTATE_NORMAL then
  elseif subState == STATE_LAB_SUB_STATE.SUBSTATE_CRUSADE then
  end
end
function GameStateLab:IsInCrusadeState()
  return self.mCurSubState == STATE_LAB_SUB_STATE.SUBSTATE_CRUSADE
end
function GameStateLab:OnTouchMoved(x, y)
  if GameUtils:GetTutorialHelp() then
    return
  end
  GameStateBase.OnTouchMoved(self, x, y)
end
